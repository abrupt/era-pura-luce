#!/bin/bash

# # Script pour des notes de bas de page spécifiques à chaque fichier
# for file in $(find $PWD/texte/ -name "*.md");
# do
#   title="${file##*/}"
#   sed -i.bak '/^\[\^/ s/\\\.$//g; /^\[\^/ s/\.$//g;' ${file}
#   pandoc -f markdown -t markdown -s --wrap=none --markdown-headings=atx --id-prefix=${title%.*}_ ${file} -o ${file}
#   # Il est aussi possible de remplacer le point final par un "\."
#   # pour éviter des erreurs avec citeproc lors d'une citation "author-in-text"
#   # de type : "[^1]: @citation.", seule sur une ligne en note de bas de page
#   sed -i.bak '/^\[\^.*[^)!?]$/ s/$/\\\./g' ${file}
#   rm ${file}.bak
# done

make pdf
