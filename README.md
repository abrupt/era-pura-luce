# ~/ABRÜPT/PIERRE-AURÉLIEN DELABRE/ERA PURA LUCE/*

La [page de ce livre](https://abrupt.cc/pierre-aurelien-delabre/era-pura-luce/) sur le réseau.

## Sur l'ouvrage

Cette cartographie vise à inscrire l’œuvre-vie de Pasolini dans les enjeux esthétiques, éthiques, sociaux et stratégiques de notre temps — et ce en la confrontant aux héritages marxistes hétérodoxes ou postmarxistes ayant infusé les nouvelles formes de radicalité dont nous sommes les contemporains. *Avec* Pasolini, donc, dans la confrontation heuristique de sa lucidité, de ses obsessions, de ses excès quelquefois, avec nos propres exigences de lutte pour la défense du vivant et de ses territoires. À rebours de l’eschatologie néolibérale et des impasses de la raison pétrifiée, en subvertissant l’esprit et la lettre du dogmatisme révolutionnaire, nous tenterons ici d’opposer aux nouvelles configurations sociales et culturelles du capitalisme mondialisé, à ses fausses libertés, à ses frontières bien réelles, matérielles et sémantiques, une nouvelle forme d’espérance.


## Sur l'auteur

Né en 1988 en région parisienne.<br>
Fabrique des textes et des images.<br>
Habite aujourd’hui à Nîmes.

Son site : [Esprit de l'utopie](https://espritdelutopie.com/).

## Sur la licence

Cet [antilivre](https://www.antilivre.org/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International ([CC-BY-NC-SA 4.0](LICENSE-TXT)).

La version HTML de cet antilivre est placée sous [licence MIT](LICENSE-MIT).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
