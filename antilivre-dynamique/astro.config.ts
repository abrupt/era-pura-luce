import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";
import remarkUnwrapImages from "remark-unwrap-images";
import smartypants from "remark-smartypants";
import remarkTextr from "remark-textr";
import remarkSuper from "remark-super";
import glsl from "vite-plugin-glsl";

// Functions for remarkTextr
function pointExclamation(input: any) {
  return input.replace(/ !/gim, " !");
}
function pointVirgule(input: any) {
  return input.replace(/ ;/gim, " ;");
}
function pointInterrogation(input: any) {
  return input.replace(/ \?/gim, " ?");
}
function deuxPoints(input: any) {
  return input.replace(/ :/gim, " :");
}
function pourcentage(input: any) {
  return input.replace(/ %/gim, " %");
}

// https://astro.build/config
export default defineConfig({
  site: "https://www.antilivre.org",
  base: "/era-pura-luce",
  trailingSlash: "always",
  server: {
    port: 3000,
  },
  devToolbar: {
    enabled: false,
  },
  markdown: {
    smartypants: true,
    // drafts: true,
    remarkPlugins: [
      [
        smartypants,
        {
          dashes: "oldschool",
          ellipses: true,
          quotes: true,
          backticks: false,
          openingQuotes: { single: "“", double: "« " },
          closingQuotes: { single: "”", double: " »" },
        },
      ],
      [
        remarkTextr,
        {
          plugins: [
            pointExclamation,
            pointVirgule,
            pointInterrogation,
            deuxPoints,
            pourcentage,
          ],
          options: { locale: "fr" },
        },
      ],
      remarkUnwrapImages,
      remarkSuper,
    ],
    remarkRehype: {
      footnoteLabel: "Notes",
      footnoteBackLabel: "Retour au contenu",
    },
  },
  integrations: [
    mdx({
      // drafts: true,
    }),
    sitemap(),
    react(),
  ],
  prefetch: true,
  compressHTML: true,
  vite: {
    plugins: [glsl()],
    assetsInclude: ["**/*.xml"],
    build: {
      chunkSizeWarningLimit: 4000,
    },
  },
  build: {
    assets: "assets",
  },
});
