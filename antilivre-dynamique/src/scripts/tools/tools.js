// Shuffle array
export const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

// Random number (min & max included)
export const randomNb = (min, max) =>
  Math.floor(Math.random() * (max - min + 1) + min);

// Random Float number (min & max included)
export const randombNbFloat = (min, max) => Math.random() * (max - min) + min;

// export const isTouchDevice = () => (('ontouchstart' in window)
//     || (navigator.maxTouchPoints > 0)
//     || (navigator.msMaxTouchPoints > 0));

export const isTouchDevice = () => {
  if (window.matchMedia("(hover: none) and (pointer: coarse)").matches) {
    return true;
  }
  return false;
};

export const plusOrMinus = () => {
  const result = Math.random() < 0.5 ? -1 : 1;
  return result;
};

export const capitalizeFirstLetter = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1);
