import "@styles/world.css";
import * as THREE from "three";
import { Suspense, StrictMode, useEffect, useState } from "react";
import { Canvas } from "@react-three/fiber";
import {
  EffectComposer,
  DepthOfField,
  Bloom,
  BrightnessContrast,
  Noise,
  Vignette,
  ToneMapping,
} from "@react-three/postprocessing";
import { ToneMappingMode } from "postprocessing";
import * as TOOLS from "@scripts/tools/tools.js";
import { Sparkles } from "@components/World/Scene/Sparkles.tsx";

import { A11yAnnouncer } from "@react-three/a11y";
// import { Perf } from "r3f-perf";
import { Leva, useControls } from "leva";
import Scene from "@world/Scene/Scene.tsx";
import { LoadingScreen, StartScreen } from "@interface/LoadingScreen.tsx";
import useWorld from "@world/stores/useWorld.tsx";
import Controls from "@world/Scene/Controls.tsx";

function World() {
  const [hash, setHash] = useState(false);
  const bodyPosition = useWorld((state: any) => state.bodyPosition);

  const {
    bloomLuminanceThreshold,
    bloomLuminanceSmoothing,
    bloomHeight,
    bloomOpacity,
    bloomIntensity,
    brightness,
    brightnessContrast,
    vignetteOffset,
    vignetteDarkness,
    noiseOpacity,
    DOFFocusDistance,
    DOFFocalLength,
    DOFBokehScale,
    DOFHeight,
  }: any = useControls("Postprocessing", {
    bloomLuminanceThreshold: {
      value: 0,
      step: 0.01,
      min: 0,
      max: 1,
    },
    bloomLuminanceSmoothing: {
      value: 0.9,
      step: 0.01,
      min: 0,
      max: 5,
    },
    bloomHeight: {
      value: 300,
      step: 1,
      min: 0,
      max: 1000,
    },
    bloomOpacity: {
      value: 3,
      step: 0.01,
      min: 0,
      max: 5,
    },
    bloomIntensity: {
      value: 1,
      step: 0.01,
      min: 0,
      max: 10,
    },
    brightness: {
      value: 0,
      step: 0.01,
      min: 0,
      max: 10,
    },
    brightnessContrast: {
      value: 0.1,
      step: 0.01,
      min: 0,
      max: 10,
    },
    vignetteOffset: {
      value: 0,
      step: 0.01,
      min: 0,
      max: 1,
    },
    vignetteDarkness: {
      value: 1.1,
      step: 0.01,
      min: 0,
      max: 10,
    },
    noiseOpacity: {
      value: 0.1,
      step: 0.01,
      min: 0,
      max: 5,
    },
    DOFFocusDistance: {
      value: 0,
      step: 0.01,
      min: 0,
      max: 5,
    },
    DOFFocalLength: {
      value: 0.02,
      step: 0.01,
      min: 0,
      max: 5,
    },
    DOFBokehScale: {
      value: 2,
      step: 0.01,
      min: 0,
      max: 10,
    },
    DOFHeight: {
      value: 480,
      step: 1,
      min: 0,
      max: 1000,
    },
  });

  useEffect(() => {
    // if (window.location.hash === "#ctrl") {
    //   setHash(true);
    // }
    // const handleHashChange = () => {
    //   const newHash = window.location.hash;
    //   if (newHash === "#ctrl") {
    //     setHash(true);
    //   } else {
    //     setHash(false);
    //   }
    // };
    // window.addEventListener("hashchange", handleHashChange);
    return () => {
      // setHash(false);
      // window.removeEventListener("hashchange", handleHashChange);
    };
  }, []);

  return (
    <StrictMode>
      <Suspense fallback={<LoadingScreen />}>
        {hash ? <Leva hidden collapsed /> : <Leva hidden collapsed />}
        <Canvas
          shadows
          gl={{
            outputColorSpace: THREE.SRGBColorSpace,
          }}
          camera={{
            fov: 45,
            rotation: [0, Math.PI * 0.25, 0],
            position: bodyPosition,
          }}
        >
          <Scene />
          <Controls />
          {/* {hash && <Perf position="top-left" />} */}
          <EffectComposer multisampling={0} enableNormalPass={false}>
            <BrightnessContrast
              brightness={brightness}
              contrast={brightnessContrast}
            />
            <ToneMapping mode={ToneMappingMode.ACES_FILMIC} />
            <Bloom
              luminanceThreshold={bloomLuminanceThreshold}
              luminanceSmoothing={bloomLuminanceSmoothing}
              height={bloomHeight}
              opacity={bloomOpacity}
              mipmapBlur
              intensity={bloomIntensity}
            />
            <DepthOfField
              focusDistance={DOFFocusDistance}
              focalLength={DOFFocalLength}
              bokehScale={DOFBokehScale}
              height={DOFHeight}
            />
            <Noise opacity={noiseOpacity} />
            <Vignette
              eskil={false}
              offset={vignetteOffset}
              darkness={vignetteDarkness}
            />
          </EffectComposer>
          <Sparkles
            count={1000}
            opacity={1}
            scale={20}
            size={TOOLS.randombNbFloat(0.5, 3)}
            speed={0.4}
          />
        </Canvas>
        <A11yAnnouncer />
        <StartScreen />
      </Suspense>
    </StrictMode>
  );
}

export default World;
