import * as THREE from "three";
import { useRef } from "react";
import { useFrame, extend } from "@react-three/fiber";
import * as TOOLS from "@scripts/tools/tools.js";
import { MeshDistortMaterial } from "@react-three/drei";
import { useControls } from "leva";
import { UnrealBloomPass } from "three-stdlib";

extend({ UnrealBloomPass });

export function Firefly({
  size = 1,
  speed = 0.1,
  direction = 1,
  color = "white",
  emissive = "white",
  textNumber = 1,
  initX = 1,
  initY = 1,
  initZ = 1,
  ...props
}) {
  const mesh: any = useRef();
  const refDistort: any = useRef();

  const {
    opacityFirefly,
    speedFirefly,
    speedDistort,
    distortMax,
    clearcoat,
  }: any = useControls("Firefly", {
    opacityFirefly: {
      value: 1.0,
      step: 0.01,
      min: 0,
      max: 1,
    },
    speedFirefly: {
      value: 0,
      step: 0.001,
      min: 0,
      max: 1,
    },
    speedDistort: {
      value: 4,
      step: 0.01,
      min: 0,
      max: 10,
    },
    distortMax: {
      value: 0.4,
      step: 0.01,
      min: 0,
      max: 2,
    },
    clearcoat: {
      value: 1,
      step: 0.01,
      min: 0,
      max: 1,
    },
  });

  const textDisplay = () => {
    const texts = [...document.querySelectorAll(".text")];
    const footnotes = document.querySelector(".footnotes");
    const allNotes = document.querySelectorAll(".footnotes li");
    texts.forEach((el) => {
      el.classList.remove("show");
    });
    document.querySelector(".texts")?.classList.add("show");

    const footnoteref = texts[textNumber].querySelectorAll(
      '[id^="user-content-fnref-"]',
    );

    texts[textNumber].classList.add("show");
    texts[textNumber].scrollIntoView();

    if (footnoteref.length > 0) {
      footnotes?.classList.add("show");
      const listNb = footnotes?.querySelector("ol");
      listNb.start = footnoteref[0].innerHTML;
      allNotes.forEach((el) => {
        el.classList.add("hide");
      });
      footnoteref.forEach((el: any) => {
        const refNb = el.hash;
        const footnoteContent = footnotes?.querySelector(
          '[id^="' + refNb.substring(1) + '"]',
        );
        footnoteContent?.classList.remove("hide");
      });
    } else {
      footnotes?.classList.remove("show");
    }
  };

  useFrame((state) => {
    refDistort.current.distort = THREE.MathUtils.lerp(
      refDistort.current.distort,
      distortMax,
      0.05,
    );
    const elapsedTime =
      state.clock.getElapsedTime() * (speedFirefly == 0 ? speed : speedFirefly);
    mesh.current.position.x =
      direction * Math.sin(elapsedTime * 4) * Math.cos(elapsedTime * 2) * 10 +
      initX;
    mesh.current.position.y =
      Math.cos(elapsedTime * 3) * Math.sin(elapsedTime * 6) * 2 + initY;
    mesh.current.position.x =
      direction * Math.sin(elapsedTime * 6) * 10 + initZ;
  });

  return (
    <group ref={mesh}>
      <mesh
        castShadow
        onClick={textDisplay}
        onPointerMove={() => {
          document.body.style.cursor = "pointer";
        }}
        onPointerEnter={() => {
          document.body.style.cursor = "pointer";
        }}
        onPointerLeave={() => {
          document.body.style.cursor = "default";
        }}
        {...props}
      >
        <sphereGeometry args={[size, 64, 64]} />
        <MeshDistortMaterial
          ref={refDistort}
          speed={speedDistort}
          // envMap={envMap}
          // bumpMap={bumpMap}
          color={"#000000"}
          roughness={0}
          metalness={1}
          bumpScale={0.05}
          clearcoat={clearcoat}
          clearcoatRoughness={clearcoat}
          radius={1}
          distort={distortMax}
          transparent={true}
          opacity={opacityFirefly}
        >
          <meshPhysicalMaterial
            roughness={0}
            metalness={1}
            // transparent={true}
            // opacity={0.5}
            color={color}
            emissive={emissive}
            envMapIntensity={0.2}
          />
        </MeshDistortMaterial>
      </mesh>
    </group>
  );
}

export function Fireflies({ count }: any) {
  return (
    <group>
      {[...Array(count)].map((el, index) => (
        <Firefly
          key={index}
          color="#000000"
          emissive="#000000"
          speed={TOOLS.randombNbFloat(0.005, 0.05)}
          size={TOOLS.randombNbFloat(1, 1)}
          direction={TOOLS.plusOrMinus()}
          initX={
            TOOLS.plusOrMinus() === 1
              ? TOOLS.randomNb(-30, -10)
              : TOOLS.randomNb(10, 30)
          }
          initY={TOOLS.randomNb(-10, 10)}
          initZ={
            TOOLS.plusOrMinus() === 1
              ? TOOLS.randomNb(-30, -10)
              : TOOLS.randomNb(10, 30)
          }
          position={[
            TOOLS.plusOrMinus() === 1
              ? TOOLS.randomNb(-30, -10)
              : TOOLS.randomNb(10, 30),
            TOOLS.randomNb(1, 4),
            TOOLS.plusOrMinus() === 1
              ? TOOLS.randomNb(-30, -3)
              : TOOLS.randomNb(3, 30),
          ]}
          textNumber={index}
        />
      ))}
    </group>
  );
}

export function Terrain() {
  return (
    <>
      <Fireflies count={20} />
    </>
  );
}
