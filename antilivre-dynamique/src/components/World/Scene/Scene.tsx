import { AudioLoader, AudioListener } from "three";
import { useEffect, useState, useRef } from "react";
import { useThree, useLoader } from "@react-three/fiber";
import Lights from "@world/Scene/Lights.tsx";
import { Terrain } from "@world/Scene/Terrain.tsx";
import useWorld from "@world/stores/useWorld.tsx";
import { isMuted } from "@components/stores/interfaceStore.ts";

type Props = {
  url: string;
};

function PlayBackgroundSound({ url }: Props) {
  const backgroundSound: any = useRef();
  const [sound, setSound] = useState(false);
  const worldEntered = useWorld((state: any) => state.worldEntered);

  const { camera } = useThree();
  const [listener]: any[] = useState(() => new AudioListener());
  const buffer = useLoader(AudioLoader, url);

  useEffect(() => {
    isMuted.listen((muted) => {
      if (muted) {
        setSound(true);
      } else {
        setSound(false);
      }
    });
    if (isMuted.get() == true) {
      backgroundSound?.current.setVolume(1);
      backgroundSound?.current.play();
    } else {
      backgroundSound?.current.pause();
    }
  }, [worldEntered, sound]);

  useEffect(() => {
    const _sound = backgroundSound.current;
    if (_sound) {
      _sound.setBuffer(buffer);
      _sound.setLoop(true);
      _sound.setVolume(0.04);
      _sound.play();
    }
  }, [buffer, camera]);

  useEffect(() => {
    const _sound = backgroundSound.current;
    camera.add(listener);
    return () => {
      camera.remove(listener);
      if (_sound) {
        if (_sound.isPlaying) _sound.stop();
        if (_sound.source && (_sound.source as any)._connected)
          _sound.disconnect();
      }
    };
  }, []);

  // @ts-ignore
  return <audio ref={backgroundSound} args={[listener]} />;
}

function Scene() {
  const worldLoad = useWorld((state: any) => state.worldLoad);

  useEffect(() => {
    worldLoad();
  }, []);
  const backgroundColor = useWorld((state: any) => state.backgroundColor);

  return (
    <>
      <color args={[backgroundColor]} attach="background" />

      <Lights />
      <Terrain />
      <PlayBackgroundSound url="/era-pura-luce/sounds/background.mp3" />
    </>
  );
}

export default Scene;
