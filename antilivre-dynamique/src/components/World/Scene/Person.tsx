import { CapsuleCollider, useRapier, RigidBody } from "@react-three/rapier";
import { useFrame } from "@react-three/fiber";
import { useKeyboardControls, PositionalAudio } from "@react-three/drei";
import { useState, useEffect, useRef } from "react";
import { useControls } from "leva";
import * as THREE from "three";
import { isMobile } from "react-device-detect";
import usePhase from "@world/stores/usePhase.tsx";
import useController from "@world/stores/useController.tsx";
import { isMenuOpen, isMuted } from "@components/stores/interfaceStore.ts";

const direction = new THREE.Vector3();
const frontVector = new THREE.Vector3();
const sideVector = new THREE.Vector3();
// const cameraPosition = new THREE.Vector3();

function Person({ position = [0, 0, 0] }: any) {
  // const worldSound = useWorld((state: any) => state.worldSound);
  // const worldSoundPlay = useWorld((state: any) => state.worldSoundPlay);
  const { personSpeed } = useControls({
    personSpeed: {
      value: 5,
      step: 0.01,
      min: 0,
      max: 100,
    },
  });
  const SPEED = personSpeed;
  const body: any = useRef();
  const footstepSound: any = useRef();
  const [subscribeKeys, getKeys] = useKeyboardControls();
  const { rapier, world } = useRapier();
  const start = usePhase((state: any) => state.start);
  const restart = usePhase((state: any) => state.restart);
  const phase = usePhase((state: any) => state.phase);
  const [menuOpen, setMenuOpen] = useState(false);

  const forwardJoystick = useController((state: any) => state.forward);
  const backwardJoystick = useController((state: any) => state.backward);
  const rightwardJoystick = useController((state: any) => state.rightward);
  const leftwardJoystick = useController((state: any) => state.leftward);

  const bodyHeight = 2;
  const bodySize = 1;
  const bodyRaySource = bodyHeight / 2;
  let grounded = true;
  let jumpActive = false;
  let bobActive = false;
  let bobTimer = 0;
  let bobMagnitude = 0.05;
  let bobFrequency = 10;
  let forwardMove: any = false;
  let backwardMove: any = false;
  let rightwardMove: any = false;
  let leftwardMove: any = false;

  const reset = () => {
    body.current.setTranslation({
      x: position[0],
      y: position[1],
      z: position[2],
    });
    body.current.setLinvel({ x: 0, y: 0, z: 0 });
    body.current.setAngvel({ x: 0, y: 0, z: 0 });
  };

  const collision = () => {
    if (jumpActive) {
      const rayDirection = { x: 0, y: -1, z: 0 };
      const rayOrigin = body.current.translation();
      rayOrigin.y -= bodyRaySource + 0.1;
      const ray = new rapier.Ray(rayOrigin, rayDirection);
      const hit = world.castRay(ray, 10, true);
      grounded = hit && hit.collider && Math.abs(hit.toi) <= 0.5;
      if (grounded) {
        grounded = true;
      }

      sound();
    }
  };

  const sound = () => {
    if (!isMuted.get()) {
      // footstepSound?.current.setVolume(10)
      footstepSound?.current.play();
    }
  };

  const jumpAction = () => {
    if (!jumpActive) {
      sound();
      jumpActive = true;
      const rayDirection = { x: 0, y: -1, z: 0 };
      const rayOrigin = body.current.translation();
      rayOrigin.y -= bodyRaySource + 0.1;
      const ray = new rapier.Ray(rayOrigin, rayDirection);
      const hit = world.castRay(ray, 10, true);
      grounded = hit && hit.collider && Math.abs(hit.toi) <= 0.5;
      if (grounded) {
        body.current.setLinvel({ x: 0, y: 10, z: 0 });
        grounded = false;
      }
    }
  };

  useEffect(() => {
    isMenuOpen.listen((open) => {
      if (open) {
        setMenuOpen(true);
      } else {
        setMenuOpen(false);
      }
    });

    const unsubscribeReset = usePhase.subscribe(
      (state: any) => state.phase,
      (value: any) => {
        if (value === "ready") reset();
      },
    );

    // const unsubscribeJump = subscribeKeys(
    //   (state) => state.jump,
    //   (value) => {
    //     if (value) jump();
    //   },
    // );

    const unsubscribeAny = subscribeKeys(() => {
      start();
    });

    return () => {
      unsubscribeReset();
      // unsubscribeJump();
      unsubscribeAny();
    };
  }, []);

  useEffect(() => {
    if (isMobile && phase === "ready") {
      start();
    }
  }, [forwardJoystick, backwardJoystick, leftwardJoystick, rightwardJoystick]);

  useFrame((state, delta) => {
    let { forward, backward, leftward, rightward, jump }: any = getKeys();
    if (menuOpen) {
      forward = backward = leftward = rightward = jump = false;
    }
    let move = false;
    let bodyPosition = null;

    if (forward || forwardJoystick) forwardMove = true;
    if (backward || backwardJoystick) backwardMove = true;
    if (rightward || rightwardJoystick) rightwardMove = true;
    if (leftward || leftwardJoystick) leftwardMove = true;
    if (jump && !jumpActive) jumpAction();

    if (
      forwardJoystick ||
      backwardJoystick ||
      rightwardJoystick ||
      leftwardJoystick ||
      forward ||
      backward ||
      rightward ||
      leftward
    )
      move = true;
    if (move && !jumpActive) {
      bobActive = true;
      // setFootSound(true);
      sound();
    }

    // movement
    if (body.current) {
      const velocity = body.current.linvel();
      bodyPosition = body.current.translation();

      if (jumpActive) {
        if (grounded) {
          forwardMove = false;
          backwardMove = false;
          rightwardMove = false;
          leftwardMove = false;
          bobActive = false;
          // setFootSound(true);
          // setTimeout(() => {
          //   setFootSound(false);
          // }, 400);
        }
      }
      grounded ? (jumpActive = false) : (jumpActive = true);
      if (bodyPosition.y < -20) restart();

      // Camera
      // cameraPosition.copy(bodyPosition);
      state.camera.position.set(bodyPosition.x, bodyPosition.y, bodyPosition.z);
      state.camera.position.y +=
        Math.sin(bobTimer * bobFrequency) * bobMagnitude;

      if (bobActive) {
        const waveLength = Math.PI;
        const nextStep =
          1 + Math.floor(((bobTimer + 0.0001) * bobFrequency) / waveLength);
        const nextStepTime = (nextStep * waveLength) / bobFrequency;
        bobTimer = Math.min(bobTimer + delta, nextStepTime);

        if (bobTimer == nextStepTime || jumpActive) {
          forwardMove = false;
          backwardMove = false;
          rightwardMove = false;
          leftwardMove = false;
          bobActive = false;
          bobTimer = 0;
          // setFootSound(false);
        }
      }

      frontVector.set(0, 0, backwardMove - forwardMove);
      sideVector.set(leftwardMove - rightwardMove, 0, 0);
      direction
        .subVectors(frontVector, sideVector)
        .normalize()
        .multiplyScalar(SPEED)
        .applyEuler(state.camera.rotation);
      body.current.setLinvel({ x: direction.x, y: velocity.y, z: direction.z });
    }
  });

  return (
    <>
      <RigidBody
        ref={body}
        canSleep={false}
        colliders={false}
        position={position}
        mass={1}
        restitution={0}
        friction={0}
        enabledRotations={[false, false, false]}
        onCollisionEnter={collision}
      >
        <mesh>
          <CapsuleCollider args={[(bodyHeight - bodySize) / 2, bodySize / 2]} />
          <PositionalAudio
            loop={false}
            url="./sounds/footstep.mp3"
            distance={1}
            ref={footstepSound}
          />
        </mesh>
      </RigidBody>
    </>
  );
}

export default Person;
