import * as THREE from "three";
import { useRef } from "react";
import { isMobile } from "react-device-detect";
import { useFrame } from "@react-three/fiber";
import {
  // ContactShadows,
  Environment,
  // AccumulativeShadows,
  // RandomizedLight,
  // Sky,
  Clouds,
  Cloud,
  // Lightformer,
} from "@react-three/drei";
import { useControls } from "leva";
// import useWorld from "@world/stores/useWorld.tsx";

export default function Lights() {
  // const backgroundColor = useWorld((state: any) => state.backgroundColor);

  const {
    directionalLightIntensity,
    ambientLightIntensity,
    environmentIntensity,
  } = useControls("Lights", {
    directionalLightIntensity: {
      value: 1.0,
      step: 0.01,
      min: 0,
      max: 10,
    },
    ambientLightIntensity: {
      value: 0.5,
      step: 0.01,
      min: 0,
      max: 50,
    },
    environmentIntensity: {
      value: 2,
      step: 0.01,
      min: 0,
      max: 50,
    },
  });
  let background;
  if (isMobile) {
    background = "background4k.jpg";
  } else {
    background = "background8k.jpg";
  }

  const ref: any = useRef();
  const { color, growth, seed, opacity, volume, bounds, speed } = useControls(
    "Cloud",
    {
      growth: { value: 100, min: 0, max: 200, step: 1 },
      color: "#222222",
      opacity: { value: 0.8, min: 0, max: 1, step: 0.01 },
      seed: { value: 0.3, min: 0, max: 100, step: 0.01 },
      volume: { value: 200, min: 0, max: 1000, step: 1 },
      bounds: { value: 200, min: 0, max: 1000, step: 1 },
      speed: { value: 0.01, min: 0, max: 1, step: 0.001 },
    },
  );
  useFrame((state) => {
    ref.current.rotation.y = Math.cos(state.clock.elapsedTime) * speed;
    ref.current.rotation.x = Math.sin(state.clock.elapsedTime) * speed;
  });
  return (
    <>
      <directionalLight
        position={[10, 10, 10]}
        intensity={directionalLightIntensity}
      />
      <ambientLight intensity={ambientLightIntensity} />
      <Environment
        background={true}
        environmentIntensity={environmentIntensity}
        files={`/era-pura-luce/environment/${background}`}
      />
      <group ref={ref}>
        <Clouds
          material={THREE.MeshLambertMaterial}
          limit={400}
          range={undefined}
        >
          <Cloud
            concentrate="outside"
            growth={growth}
            color={color}
            opacity={opacity}
            seed={seed}
            bounds={bounds}
            volume={volume}
          />
        </Clouds>
      </group>
    </>
  );
}
