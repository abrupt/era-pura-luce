import { OrbitControls } from "@react-three/drei";
import useWorld from "@world/stores/useWorld.tsx";

function Controls() {
  const bodyPosition = useWorld((state: any) => state.bodyPosition);
  return (
    <>
      <OrbitControls
        minDistance={0}
        maxDistance={0.01}
        enableZoom={false}
        target={bodyPosition}
        makeDefault
      />
    </>
  );
}

export default Controls;
