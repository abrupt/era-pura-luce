import { create } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";

export default create(
  subscribeWithSelector((set) => {
    return {
      phase: "ready",

      start: () => {
        set((state: any) => {
          if (state.phase === "ready") return { phase: "playing" };

          return {};
        });
      },

      restart: () => {
        set((state: any) => {
          if (state.phase === "playing" || state.phase === "ended")
            return { phase: "ready" };

          return {};
        });
      },

      end: () => {
        set((state: any) => {
          if (state.phase === "playing") return { phase: "ended" };

          return {};
        });
      },
    };
  }),
);
