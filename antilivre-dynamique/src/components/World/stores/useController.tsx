import { create } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";

export default create(
  subscribeWithSelector((set) => {
    return {
      forward: false,
      backward: false,
      rightward: false,
      leftward: false,

      forwardOn: () => {
        set(() => {
          return {
            forward: true,
            backward: false,
            rightward: false,
            leftward: false,
          };
        });
      },
      backwardOn: () => {
        set(() => {
          return {
            forward: false,
            backward: true,
            rightward: false,
            leftward: false,
          };
        });
      },
      rightwardOn: () => {
        set(() => {
          return {
            forward: false,
            backward: false,
            rightward: true,
            leftward: false,
          };
        });
      },
      leftwardOn: () => {
        set(() => {
          return {
            forward: false,
            backward: false,
            rightward: false,
            leftward: true,
          };
        });
      },

      joystickStop: () => {
        set(() => {
          return {
            forward: false,
            backward: false,
            rightward: false,
            leftward: false,
          };
        });
      },
    };
  }),
);
