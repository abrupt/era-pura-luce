import { create } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";

export default create(
  subscribeWithSelector((set) => {
    return {
      worldDimension: 100,
      worldRandom: Math.random(),
      backgroundColor: "#000000",

      worldLoaded: false,
      worldEntered: false,

      bodyPosition: [0, 5, 0],

      worldLoad: () => {
        set(() => {
          return { worldLoaded: true };
        });
      },

      worldEnter: () => {
        set(() => {
          return { worldEntered: true };
        });
      },
    };
  }),
);
