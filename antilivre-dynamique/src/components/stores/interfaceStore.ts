import { atom } from "nanostores";

export const isMenuOpen = atom(false);
export const isMuted = atom(true);
