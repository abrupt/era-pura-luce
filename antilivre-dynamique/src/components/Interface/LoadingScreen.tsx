import { useProgress } from "@react-three/drei";
import "@styles/loading.css";
import useWorld from "@world/stores/useWorld.tsx";
import { isMuted } from "@components/stores/interfaceStore.ts";

let progressPercent = 0;
export function LoadingScreen() {
  const { progress } = useProgress();
  progressPercent = progress > 1 ? Math.floor(progress) - 1 : 0;
  return (
    <div className="loading">
      <div className="loading__content">
        <div className="text-anim">
          <p>
            <span className="letter">U</span>
            <span className="letter letter--dynamic">Ü</span>
          </p>
        </div>
        <div className="loading__bar">
          <div
            className="loading__progress"
            style={{ width: `${progressPercent}%` }}
          ></div>
        </div>
        <button
          className="loading__button loading__button--disable"
          disabled={true}
        >
          Explorer
        </button>
      </div>
    </div>
  );
}

export function StartScreen() {
  const worldLoaded = useWorld((state: any) => state.worldLoaded);
  const worldEnter = useWorld((state: any) => state.worldEnter);
  const enter = (e: any) => {
    e.preventDefault();
    document.querySelector(".loading__button")?.blur();
    worldEnter();
    isMuted.set(false);
    document.querySelector(".loading")?.classList.add("loading--done");
  };
  if (worldLoaded) {
    progressPercent = 100;
  }
  return (
    <div className="loading">
      <div className="loading__content">
        <div className="text-anim">
          <p>
            <span className="letter">U</span>
            <span className="letter letter--dynamic">Ü</span>
          </p>
        </div>
        <div
          className={`loading__bar ${worldLoaded ? "loading__bar--done" : ""}`}
        >
          <div
            className="loading__progress"
            style={{ width: `${progressPercent}%` }}
          ></div>
        </div>
        <button
          className={`loading__button ${worldLoaded ? "loading__button--enable" : "loading__button--disable"}`}
          disabled={false}
          onClick={enter}
        >
          Explorer
        </button>
      </div>
    </div>
  );
}
