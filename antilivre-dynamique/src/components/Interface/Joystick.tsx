import { useState, useEffect } from "react";
import { Joystick, JoystickShape } from "react-joystick-component";
import useController from "@world/stores/useController.tsx";

function JoystickInterface() {
  // state
  const forwardOn = useController((state: any) => state.forwardOn);
  const backwardOn = useController((state: any) => state.backwardOn);
  const rightwardOn = useController((state: any) => state.rightwardOn);
  const leftwardOn = useController((state: any) => state.leftwardOn);
  const joystickStop = useController((state: any) => state.joystickStop);

  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
  }, []);

  const joystickMove = (e: any) => {
    switch (e.direction) {
      case "FORWARD":
        forwardOn();
        break;
      case "BACKWARD":
        backwardOn();
        break;
      case "RIGHT":
        rightwardOn();
        break;
      case "LEFT":
        leftwardOn();
        break;
      default:
        break;
    }
  };

  return (
    mounted && (
      <>
        <div
          style={{
            position: "fixed",
            bottom: "10px",
            left: "50%",
            transform: "translateX(-50%)",
          }}
          className="joystick joystick--move"
        >
          <Joystick
            size={50}
            stickSize={25}
            baseColor="#aaaaaa99"
            stickColor="#ffffff99"
            minDistance={20}
            throttle={100}
            baseShape={JoystickShape.Square}
            stickShape={JoystickShape.Square}
            move={(e) => joystickMove(e)}
            stop={() => joystickStop()}
          />
        </div>
      </>
    )
  );
}

export default JoystickInterface;
