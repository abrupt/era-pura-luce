<div class="text" data-number="0">

# I. Era pura luce (éloge d'un marxisme hérétique)

<div class="epigraphe">

C'est là qu'a commencé, concrètement, dirais-je, poétiquement, physiquement, mon marxisme.

--- Pier Paolo Pasolini

</div>




## 1. Nom de pays : Pigneto

Balbec n'est pas Pigneto. Et pourtant, ainsi que la vieille église de
Balbec pour le narrateur de la *Recherche du temps perdu*, Pigneto fut
longtemps pour moi la matière d'un rêve que la réalité ne pouvait que
brutalement écorcher :

> Cette vaste vision céleste dont il me parlait, ce gigantesque poème
> théologique que je comprenais avoir été écrit là, pourtant quand mes
> yeux pleins de désirs s'étaient ouverts devant la façade, ce n'est pas
> eux que j'avais vus[^1].

Il en est ainsi du rêve quand celui-ci s'arrache à la lucidité,
s'autonomise et déploie ses ailes de sublime abstraction.

Chez Proust, c'est précisément à la nouvelle hégémonie bourgeoise que le
sujet se cogne --- le regret d'un temps où les rapports de classe
s'affirmaient dans la pleine naturalité de leur éclat (criminel).

Et bien qu'affleurent l'acide et le soufre, dans toutes ces longues
descriptions de salons, d'opéras, de vacances normandes, rien ne laisse
présager sous la plume du Français le moindre attachement pour le
prolétariat.

Si la classe des subalternes n'est pas dénuée d'un certain charme
--- pensons à ce cocher érudit qui trimballe Albertine et la surveille
partout, à la grogne de Françoise, à la « poésie » de Céleste, etc. ---,
Proust pose un regard situé socialement, situé beaucoup trop haut,
beaucoup trop loin du sol, pour rendre compte de l'expérience concrète
d'un corps prolétaire ; son regard, malgré lui, discrimine,
essentialise, classifie, ne laisse place qu'au folklore, à la tendresse
hautaine.

Et cette part d'ombre que le sommeil abrite, ce réservoir intarissable
des désirs et des errances spéculaires, à mesure que l'objet se dévoile,
à la lumière du jour, un corps n'est plus qu'un corps, et l'idée dont il
fut enveloppé, tellement plus belle que ce corps lui-même, a déjà
condamné toute possibilité de rencontre effective.

L'altérité, dans la *Recherche*, est donc ce point limite qui
circonscrit le repli du sujet sur lui-même ; pourvu que les idées
subsistent contre les corps, contre le temps, contre la réalité : *mon
amour chante, il est si beau quand je suis seul*.

L'agonie proustienne a le parfum crépusculaire d'un siècle qui aura vu
mourir les dernières coquetteries, souliers noirs ou rouges des
duchesses, aubes fin-de-siècle d'une aristocratie en décomposition. Le
narrateur de la *Recherche* rêve ainsi de Balbec, de sa ville vieille,
de son clocher, mais sa ratiocination devance la réalité, l'élaboration
n'est plus prospective, c'est un acte de décès.

Quant à Pasolini, simple petit-bourgeois de Bologne, jeté dans un monde
rural puis périurbain, il fait la rencontre de réalités sociales qu'il
n'a pas préalablement rêvées. Quand il arrive à Rome, au début des années 50,
il fait ainsi l'expérience d'une classe dont il n'avait jamais
soupçonné l'existence ; et, avec elle, d'un monde entièrement nouveau et
de son langage.

Un espace dans l'espace, et non plus en lui-même[^2] --- cette
extranéité est le motif même de son amour et de son engagement. Car
Pasolini est bien décidé à se laisser contaminer par ce monde
parasitaire, un monde qui n'a pas eu besoin de prendre conscience de
lui-même pour chanter.

Tandis que la classe ouvrière traditionnelle, celle de Turin, de Milan,
concentrée dans les grandes périphéries industrielles du Piémont et de
Lombardie, ce prolétariat engagé dans les rapports de production légaux,
a intériorisé le caractère *naturel* des lois qui régissent l'économie
capitaliste, et ce au nom d'une petite espérance d'embourgeoisement, le
sous-prolétariat des faubourgs romains n'a rien sacrifié de sa vitalité.

Cette rencontre bouleverse les coordonnées de sa compréhension marxiste
de l'histoire et des rapports de classe : quelle est donc cette classe
négligée par le marxisme officiel ? Quelle peut bien être sa fonction
dans l'histoire ?

Il joint alors sa propre errance à celle des petits voyous et des
gamins des faubourgs, écrit des romans-documentaires, apprend le
romanesco, traîne dans les cafés de Pigneto ; sa « carrière
littéraire », il ne la marchande pas dans les salons de la capitale,
mais en apprenant et en restituant cette langue qui échappe à la
traditionnelle mise en récit du monde ; les exigences narratives
s'effaçant devant elle, afin d'en révéler l'éclat, la gratuité surtout,
son absence de pré-texte.

En 1957, Pasolini publie son premier grand ouvrage de poésie en langue
italienne, une élégie aux anges trahis de la révolution. C'est également
un premier pas de côté vis-à-vis de l'optimisme béat des adeptes du
marxisme officiel --- décentrement de sa raison critique concomitant
d'une nouvelle lucidité historique : la lutte des classes existe,
certes, et elle continuera d'exister, mais les nouvelles implications
sociales et culturelles du capitalisme ont relégué l'idéal ouvriériste
dans les limbes de l'Histoire --- ci-gisent les cymbales de l'espérance
révolutionnaire.

Puis vint le cinéma : *Accattone*, 1961. Il faut s'emparer à tout
prix de cette réalité moribonde qui ne résistera pas longtemps à la
colonisation systématique des marges par la centralité néocapitaliste.
Et il faut faire vite : Franco, Walter et les autres tiennent un monde
fragile entre leurs mains.

À l'adolescence, c'est ainsi que je suis entré dans l'œuvre de Pasolini :
avec les *Cendres de Gramsci*, avec *Accattone*. Je découvrais un
monde qui avait objectivement disparu. Mais un adolescent, dans l'espace
fou de sa solitude, déploie toujours des efforts attendrissants pour
s'obstiner à croire, encore, à la matérialité de son amour.

Pigneto fut donc notre chapelle rêvée, notre Balbec à nous.

Lors de mes premiers voyages à Rome, je ne m'y suis pas rendu. Rome est
immense et son centre historique justifie que nous nous y perdions. Mais
il faut également reconnaître que j'aimais l'idée qu'en dehors du
centre, entièrement gagné par le tourisme de masse et l'uniformisation
culturelle, se logeait quelque part, toujours, un foyer de vie marginale
et contestataire.

Il est temps de défaire notre rêve.

Pigneto, aujourd'hui, est un « nouveau quartier d'artistes » : cafés
rétro, bars à foison, restaurants avec patio et fanfare, plusieurs
épiceries de nuit.

« Artiste » est un nom générique : il désigne essentiellement un
petit-bourgeois doté d'un certain capital symbolique, et s'habillant
plutôt simplement, sans les oripeaux du dandy, sans la grossièreté
grimée du prolétaire.

On dit également de Pigneto que c'est un « vieux quartier ouvrier ».
Cette appellation étrange désigne un quartier où la hausse des loyers et
la politique de la ville ont *de facto* et depuis longtemps chassé les
derniers ouvriers. Un « vieux quartier ouvrier », c'est donc l'autre nom
donné à un « nouveau quartier d'artistes ».

Au cœur de Pigneto se trouve une institution historique : le café
*Necci*. 

Mal desservi, comme tous les quartiers périphériques de Rome, la bière y
est pourtant aussi chère que dans le centre-ville. Mais les jeunes
cadres romains n'ont pas les moyens d'habiter au centre de Rome.
Personne n'a les moyens de vivre au centre de Rome. Le centre de Rome
est parsemé de logements vides.

Sur un mur, dans le petit patio du café *Necci*, il y a une photo de
Pasolini. C'est une photo retouchée. Le poète apparaît vêtu d'un
pull-over où est écrit  : « Mo' sto bene » (littéralement : « Je vais
bien maintenant »). Je n'aime pas cette photo. Elle accable le poète
d'un style qui n'était pas le sien. Elle cherche à le réconcilier,
envers et contre tout, avec notre époque.

Dans le petit patio du café *Necci*, il y a donc des jeunes cadres et
des étudiants qui boivent des bières artisanales et savourent des
*bruschette*. Et quelques vieux gamins également.

Ces vieux gamins de Pigneto sont bien plus modestes que leurs voisins de
table, ils se contentent d'un café noir et d'un verre d'eau. À les
entendre, tous les vieux gamins de *Necci* ont connu Pasolini. Et, à qui
veut bien les écouter, ils racontent leur histoire, toujours singulière,
avec le poète. Bien vite, ils nous proposent de leur acheter un mauvais
livre relatant cette histoire. Qu'importe, finalement, la véracité de
leur témoignage. Je leur achète un livre. Et je discute avec eux,
pendant des heures.

En rentrant dans ma petite chambre, à quelques encablures du centre de
Pigneto, j'égraine des photos du café *Necci* au temps de Pasolini. À
l'époque, le café était ouvert sur la rue, il n'existait pas encore de
patio, quelques tables posées à même une route en terre parsemée de
cahots, soumise à la sécheresse et aux fluctuations du temps. Nul
bitume, en ce temps-là. Le quartier de Pigneto ressemblait alors à un
village de montagne : de petites maisons basses, quelques terrains
vagues, des sourires et d'interminables parties de football.

Je me demande ce que Pasolini représentait pour tous ces gars avec
lesquels il aimait partager un café : était-il le poète du coin ? Un
ami ? Un fou ?

Désormais il s'affiche sur plusieurs façades du quartier : des peintures
relativement soignées, des fresques, de simples graffitis où, tel San
Gennaro sur les murs de Naples, il se rappelle à nous, nous juge et nous
sourit.

Mais de toutes les représentations de Pasolini qui apparaissent sur les
murs de Pigneto, aucune ne me plaît vraiment : ses traits me paraissent
grossiers, des couleurs fourbes en guise d'auréoles, des citations
déconnectées de la signification historique et sociale, de la justesse
que leur donnait le poète.

J'ai donc passé plusieurs matinées dans le petit patio de *Necci* à
discuter avec Roberto, Guido ou Alberto. J'ai également passé des
après-midi entiers à écouter des étudiants et des cadres parler boulot,
amour, fêtes, vacances. Je me suis ennuyé beaucoup à Pigneto. Je n'ai
jamais reconnu les visages, les sourires, les éclats de cette jeunesse
sous-prolétaire aperçue dans *Accattone*.

Dans *Accattone*, précisément, Pasolini avait à cœur de saisir et de
restituer les derniers éclats d'une vitalité populaire, ces jeux d'amour
et de hasard sur des terrains vagues, rixes pleines d'insouciance et de
force, sans rogner la spécificité d'une langue parcourue d'influences
complexes et disséminées : romanesco tissé de calabrais, de napolitain,
de dialectismes improbables.

Ceci alors qu'il pressentait déjà que nous nous situions à l'aune de
bouleversements anthropologiques majeurs ; les classes populaires
s'apprêtant à sacrifier leur *ethos*, leurs valeurs, leurs langues
justement, en inscrivant leur désir dans la culture-monde du capitalisme
intégral.

Dix ans après *Accattone*, à l'aune des années 70 donc, Pasolini a
conscience qu'il ne pourrait plus tourner son film : les Franco, Walter
ou autres ont tout simplement disparu. Mais pouvait-il se douter, qu'un
jour à Pigneto, les cadres et les étudiants se substitueraient
entièrement aux ouvriers et aux petits voyous ? Pouvait-il imaginer que
le café *Necci*, naguère ouvert sur le monde, allait devenir ce café
rétro où la culture des années 60 s'étalerait complaisamment sur les
murs ? Pire : où l'on placarderait son portrait accompagné de ces
quelques mots ridicules : « Mo' sto bene » ?

En quelques dizaines d'années, et du fait de sa proximité avec le centre
historique de Rome, Pigneto a donc achevé sa mue : c'est désormais un
véritable quartier d'artistes. Et qu'importe s'ils fabriquent de l'art,
ces artistes de Pigneto, l'art a épousé la plasticité d'une marchandise,
il n'est qu'un faire-valoir, une modalité de distinction sociale, un
style de vie décorrélé de la vie même.

Bien sûr, il ne peut s'agir seulement de veiller sur notre conscience coupable : comment éviter, pourtant, de sombrer dans la crasse naïveté de ces jeunes cadres et de ces étudiants sirotant des bières artisanales à des prix faramineux dans le petit patio de *Necci* ? Le problème, évidemment,
n'étant pas la bière artisanale en soi, son prix ou son mode de
production, ou encore l'homogénéité des postures de ses adeptes, mais
bien celui d'un conformisme enlisé dans une adoration de sa propre
simplicité, aveugle quant au fait que les formes de son existence
sociale sont à penser en relation avec l'éradication de cette culture
ouvrière qui, ruse sinistre de l'Histoire, lui octroie aujourd'hui
encore son cachet.

Et quant au Pigneto d'hier, celui de Pasolini, celui des cafés radieux
et des rires fracassants, celui-ci n'est plus qu'une pièce de musée, un
nom, le nom d'un pays disparu.

</div>

<div class="text" data-number="1">

## 2. Les jours de la résistance

Selon Marx, le prolétariat est la classe dont la fonction historique est
d'émanciper toute l'humanité. Cette caractéristique messianique du sujet
révolutionnaire, nous la retrouvons constamment au sein de la tradition
marxiste ; ce qu'elle a de singulier chez Pasolini, c'est précisément
que ce postulat stratégique de la théorie marxiste constitue une
exigence à la fois éthique et esthétique.

En tant que bourgeois *et* en tant que marxiste, comme il se plaisait à
le répéter, le jugement de Pasolini est doublement déterminé. Ce qui
pourrait donner lieu à une tension indépassable : si la théorie marxiste
vise à rendre compte de la société du point de vue de ses
contradictions, l'idéologie bourgeoise vise à pacifier nos
représentations du monde social ; l'une se fonde sur l'explicitation de
la lutte des classes, l'autre sur sa neutralisation.

Bien que viscéralement communiste, le regard de Pasolini
s'affranchit cependant du point de vue strictement sociologique, même
d'inspiration marxiste, autant que de la perception immédiate,
indéterminée, abstraite, telle que la pratique la phénoménologie de
Husserl et de ses héritiers, et vise l'appréhension d'un monde
(intégralement structuré par la forme-marchande[^3]) dont la matière
spécifique (corps prolétaires, cultures vernaculaires, etc.) excède le
caractère de l'objet. 

Il ne peut s'agir de projeter sur une altérité quelconque les
déterminations d'une forme (savoir, narration, poème) qui la précède ;
cette reconnaissance de l'altérité supposant nécessairement une
suspension du jugement, un décentrement du regard --- une
rencontre. Ce qui implique que le regard se laisse
affecter en retour par cet « autre » qui lui échappe
toujours.

L'altérité n'est donc pas déterminée seulement de façon négative,
en miroir par les rapports de classe (en tant qu'objet d'un système de
domination économique et culturelle), mais fait valoir, en sa qualité de
sujet autonome, la persistance d'un monde (de ses valeurs, de ses
traditions, de ses usages, etc.) soustrait à la marche de l'Histoire, au
développement capitaliste, à l'hégémonie culturelle de la bourgeoisie.

Autre élément caractéristique du marxisme pasolinien --- s'inscrivant
ici dans l'héritage de Gramsci ---, la contestation de l'hégémonie
ouvrière au sein du camp prolétarien : les classes paysannes pauvres
n'ont pas à rallier mécaniquement la cause ouvrière, elles doivent
exister dans la lutte en demeurant ce qu'elles sont, avec leurs cultures
propres, leurs savoir-faire, leurs langues, etc., qui sont autant de
*faits positifs*, et non de pures déterminations négatives.

Pasolini se situe en contrepoint de la « pensée sociologique », et de
Bourdieu en particulier, lequel, dans un texte emblématique de sa
méthode et des apriorités idéologiques qui la fondent, soutient qu'il
n'existe pas de culture spécifique chez les dominés, que ceux-ci sont
dans l'incapacité de produire un langage qui leur soit propre ; le
langage du dominé, quand il ne se résout pas au silence, ne serait donc
que réaction plus ou moins humiliée, plus ou moins servile, devant les
manifestations de normes dominantes les renvoyant mécaniquement au fait
de leur domination :

> C'est pourquoi l'ambivalence à l'égard des dominants et de leur style de
> vie, si fréquente chez les hommes exerçant des fonctions de service, qui
> balancent entre l'inclination à la conformité anxieuse et la tentation
> de se permettre des familiarités et de dégrader les dominants en se
> haussant jusqu'à eux, représente sans doute la vérité et la limite de la
> relation que les hommes les plus démunis de capital linguistique, et
> voués à l'alternative de la grossièreté et de la servilité,
> entretiennent avec le mode d'expression dominant[^4].

Il nous revient d'abord d'expliciter la différence de méthode opposant
une certaine approche littéraire d'inspiration marxiste à une approche
strictement sociologique du monde social.

*A priori*, lorsqu'un poète sans conviction révolutionnaire fait la
rencontre de réalités sociales dont il ne sait rien, il n'est pas tenu
d'en rendre compte de façon rigoureusement scientifique, et peut donc
assumer de vivre une expérience sensible et de la manifester le plus
librement possible. Maïakovski affirmait cependant que le poète
communiste se devait d'approfondir sa compréhension des causes
matérielles de la vie sociale, en lisant par exemple des traités
d'économie, qu'il s'agissait pour lui du seul moyen d'échapper à la
pétrification romantique de sa subjectivité[^5]. Ce qui ne signifie
aucunement que le poème doive épouser la forme de la théorie, mais
plutôt qu'il en constitue un champ d'application général, entrelacé à
d'autres matériaux sensibles. Autrement dit : acuité sensible et savoirs
objectifs se médiatisent réciproquement sur un plan a-théorique.

Une fois affirmée l'hétérogénéité de principe distinguant
méthodologiquement la pratique sociologique de la démarche littéraire
d'inspiration marxiste ou communiste, il nous revient de creuser le
différend opposant spécifiquement Pasolini à Bourdieu sur la question
cruciale de la domination.

Les normes dominantes ne tendent pas seulement à exclure les dominés,
elles tendent également à les intégrer à une société qui les opprime, en
leur fournissant le sentiment de la validité majoritaire tout en les
maintenant sous l'effet d'une domination objective. C'est précisément ce
qui distingue une culture bourgeoise de la distinction d'une culture
néocapitaliste d'adaptation à « marche forcée ».

L'une des critiques qu'il serait permis de faire à Bourdieu est de
penser le « social » essentiellement du point de vue des inégalités de
patrimoine (matériel ou symbolique), alors que malgré la perpétuation,
voire l'accroissement de certaines inégalités objectives, Pasolini
considère que l'« intégrisme » de la société néocapitaliste est
un mal infiniment plus pernicieux dans la mesure où il arrache les
classes subalternes à toute forme de vie autonome.

Si nous désignons cette société comme « néocapitaliste », c'est en raison de l'usage massif des nouveaux moyens techniques et technologiques se
trouvant à disposition de la production, des échanges et des nouvelles
modalités de consommation. Ces moyens tendent à inscrire la subjectivité
du producteur dans le réseau immatériel des nouveaux désirs et des
valeurs induits par la société dite « de consommation ». La
prolétarisation du producteur est ainsi redoublée par l'acculturation du
consommateur, et le prolétariat se retrouve donc doublement appauvri :
du fait de sa position dans les rapports de production, du fait de la
destruction de sa culture spécifique.

Malgré la domination totalitaire du mode de vie petit-bourgeois,
son écrasement des cultures spécifiques, Pasolini ne cesse d'opposer à
l'axiomatique sociologique d'inspiration critique une requalification de
la culture subalterne en tant que manifestation réelle d'un *ethos* :
ainsi la culture du subalterne ne serait pas seulement, telle que la
pense Bourdieu, déterminée par la domination économique et culturelle de
la bourgeoisie, mais conserverait une part à la fois relative et
irréductible d'autonomie et donc de positivité.

Ce pour quoi, instinctivement, le jeune Pasolini se tourne vers le
frioulan, un dialecte dont il n'a pas hérité, qui n'est pas même celui
de sa mère, qu'il apprend seul, en écoutant parler les paysans alentour :

> ... j'avais dix-huit ans lorsque je l'ai utilisé pour la première fois.
> Je ne l'ai pas fait dans un souci de réalisme ; ça n'impliquait personne
> en particulier, encore moins une sociologie du pauvre, ou autre classe
> sociale. Si j'ai choisi le dialecte frioulan, c'est pour des raisons
> purement esthétiques[^6]...

Ces « raisons purement esthétiques », nous pouvons les comprendre comme
excédant à la fois les exigences de la science sociologique et celles de
la lutte marxiste révolutionnaire.

La « sociologie du pauvre » ne rend pas compte de la positivité
inhérente à la culture subalterne, mais circonscrit la vie populaire,
paysanne ou ouvrière, à une lecture comparatiste qui prend appui sur une
connaissance de la société « en général » et des inégalités qui la
traversent.

C'est également le cas de la lutte marxiste révolutionnaire
quand elle fonde sa justification sur l'universelle précarité des
conditions de travail et de vie, et non sur les manifestations
culturelles spécifiques qui survivent dans les marges de la civilisation
capitaliste.

La démarche pasolinienne est évidemment tout autre : c'est
précisément à partir de ce qui échappe au centralisme culturel de la
bourgeoisie, l'excède ou s'y soustrait, que ces « pures formes de vie »
sont saisies et retraduites dans le langage de l'art.

C'est ainsi que les *Poèmes à Casarsa* (1942) se trouvent une
langue, foisonnante, multiple --- car le dialecte n'est jamais
chimiquement pur, non la manifestation essentielle d'un peuple,
son *Volksgeist*, mais l'expression historique d'une culture spécifique ;
ce pour quoi il demeure ouvert aux inflexions, aux écarts, à la
démultiplication de ses formes propres à une ville, à un village, à une
simple bourgade, se soustrayant ainsi à toute tentative d'unification et
d'institutionnalisation.

La langue dialectale permet également de rendre visibles des
éléments d'un monde particulier dont le florentin hégémonique ne saurait
rendre compte. Ce qui ne signifie pas qu'il faille renoncer à faire
usage de ce dernier --- Pasolini n'y renoncera jamais ---,
mais simplement que toute langue nationale porte en elle les valeurs de
sa culture étatisée, qu'elle tend par conséquent à appauvrir le langage
de la réalité.

À rebours de toute conception téléologique de l'Histoire
--- qu'elle soit réactionnaire ou progressiste : qu'elle présuppose une
conservation d'un passé-déjà-mort ou qu'elle vise une émancipation sans
contenu réel ---, le marxisme de Pasolini n'est donc aucunement inféodé à un
discours sur l'Histoire. Et ce double arrachement, à la vieille langue
des essences historiques, à la novlangue des progressismes de tous
bords, enrichit sa compréhension marxiste du monde. Elle lui permet de
demeurer marxiste *en tant que poète*.

Si Pasolini s'aventure sur les terrains de l'observation
anthropologique, cela ne l'empêche pas de maintenir vive cette sensibilité du
regard porté sur les phénomènes de la vie sociale. En atteste, par exemple, la façon
dont il réussit à articuler, dans ses articles polémiques notamment, une
compréhension marxiste des rapports sociaux avec les caractères d'une
altérité excédant toujours le statut d'objet --- cette articulation
esquissant peut-être la possibilité d'une réélaboration épistémologique
du marxisme lui-même.

L'écroulement des frontières disciplinaires, la positivité toujours
singulière de l'« objet », autrement dit la reconnaissance attentive
d'une altérité, de sa spécificité, de sa différence, invitent en tout
cas à repenser une théorie des rapports sociaux qui se situerait à
mi-chemin du savoir sociologique et de l'appréhension métalogique du
monde. 

S'il fallait le formuler en convoquant l'histoire de la philosophie,
nous serions enclins à considérer que le regard porté par Pasolini sur
le monde social réussit là où la neutralité axiologique d'un Weber ou
l'*épochè* phénoménologique d'un Husserl se confrontent à leurs impasses
méthodologiques, à leur tendance notamment à constituer l'altérité comme
objet d'un savoir, qu'il soit objet de déterminations matérielles et
symboliques du point de vue d'une totalité sociale abstraite ou objet
d'appréhension hors-sol.

Mais le marxisme strictement objectiviste, incapable de fonder son
objet sur la différence et le spécifique, et qui vise conjointement la
réalisation d'une émancipation abstraitement universelle, ne nous paraît
pas moins problématique.

*A contrario*, le marxisme hérétique de Pasolini propose en germe une
épistémologie alternative qu'il ne tient qu'à nous de déplier afin d'en
mesurer la pertinence et les potentialités. C'est uniquement ainsi que
nous persisterons à considérer encore la tradition théorique
d'inspiration marxiste comme « horizon indépassable de notre
temps[^7] ». Ce qui suppose d'ouvrir une brèche au cœur
de ses fondations philosophiques, d'y opérer un certain nombre de
décentrements et de retours --- cette mutation de la théorie marxiste se
faisant sans doute plus fidèle à Marx lui-même qu'à
nos contemporains de la gauche communiste :

> \[La classe ouvrière\] n'a pas à réaliser d'idéal, mais seulement à
> libérer les éléments de la société nouvelle que porte dans ses flancs la
> vieille société bourgeoise qui s'effondre[^8].

Cette citation de Marx --- nous aurons l'occasion d'y revenir tout au
long de cet ouvrage --- nous semble contenir un élément programmatique
absolument fondamental et dont l'actualité ne cesse de se faire toujours
plus brûlante.

Que nous dit Marx ? Qu'il ne s'agit pas de viser une *libération à venir*
au nom d'une espérance purement rhétorique, mais de tendre vers le
*déjà existant*, de le déployer et de le défendre, ou, sur une modalité
plus offensive, de l'arracher à la domination totalitaire de la
bourgeoisie.

Autrement dit : il s'agit de résister au génocide, tant culturel
qu'écologique, qui ne cesse de sévir et s'accompagne toujours d'une
justification fondée sur l'idée d'un Idéal et/ou d'un Progrès à
réaliser, de libérer ainsi nos puissances de vie et de travail aliénées.

Le Progrès, dans la langue bien ficelée des libéraux, et désormais des
néolibéraux, cela signifie recouvrir l'intégralité du monde --- et
au-delà sans doute --- d'une accumulation infinie de marchandises. Mais
cette image d'un monde recouvert de plastique ne doit pas donner
l'illusion que notre tragédie contemporaine n'affecterait que des
paysages : selon Pasolini, les visages, et de façon très concrète,
n'échappent pas à l'*universelle structuration marchande de la
société*.

Strate après strate, nous tenterons de penser le conflit ouvert,
dans l'œuvre et la vie de Pasolini, de la lumière et de son envers :
*disparition du paysage* et *distorsion du sourire*.

Mais revenons d'abord à sa rencontre inaugurale avec le communisme.

Tout comme Gramsci, qui est né et a grandi aux portes de la
Barbagia, en Sardaigne, l'une des régions paysannes les plus préservées
culturellement d'Italie, la rencontre originaire de Pasolini avec le
prolétariat se fait sur le terrain de la ruralité. C'est à Casarsa et
ses alentours que tout a commencé. Casarsa, où il s'agissait de fuir la
guerre, avec sa mère et son frère cadet, en attendant que le fleuve de
l'Histoire recouvre sa stabilité, ses eaux calmes et pacifiées, son long
cours sans soubresaut ni horreur. 

Casarsa, donc, patelin excentré, entre
Pordenone et Udine, ces deux portes du monde, perdu dans la campagne
frioulane, où Pasolini fera une rencontre qui marquera sa vie, son œuvre :

> Je me suis trouvé là, physiquement : d'un côté les journaliers tous
> ensemble liés par leurs drapeaux, leur foulard rouge autour du cou ; de
> l'autre côté les patrons. Et voyez-vous, sans la moindre lecture
> marxiste, tout naturellement, je me suis rangé du côté des journaliers
> frioulans[^9]...

Alors que Pasolini découvre une nouvelle réalité et son langage, son frère cadet part rejoindre les maquis dans les
montagnes du Nord :

> Mon frère s'en alla, par un muet matin
>
> de mars, sur un train, clandestin,
>
> son revolver dans un livre : et c'était pure lumière[^10].

Pier Paolo, quant à lui, erre dans la campagne frioulane, ses plaines,
ses rivières, correspond avec quelques amis de Bologne, écrit des
poèmes, s'initie au dialecte local, la langue des journaliers en lutte :

> Avec les quelques paysans des alentours,
>
> je vivais la glorieuse vie du persécuté
>
> par des édits atroces : et c'était pure lumière[^11].

Mais le frère maquisard est tué par d'autres partisans ; une
querelle fratricide, sans doute. Et Pier Paolo demeure donc seul avec sa
mère, désenfantée une seconde fois[^12]. 

La mythologie inaugurale est ici fondamentalement contrastée, tragique,
comme toute mythologie : d'un côté, la mort, à peine héroïque, du frère
aimé, l'humble détresse de la mère ; de l'autre, une rencontre qui
induira sa « conversion[^13] » au marxisme.

Osons une comparaison hasardeuse : lorsque le leader de la
révolution bolchévique apprend que son frère a été condamné à mort,
accusé d'avoir mené avec un groupuscule de jeunes révolutionnaires un
attentat contre le Tzar, ce dernier réagit en disqualifiant
l'action kamikaze romantique du frère et de ses camarades : « Nous ne
suivrons pas cette voie-là. Ce n'est pas la bonne[^14]. » Tandis que sa
condition sociale aurait pu lui épargner la peine capitale, par
héroïsme, par orgueil, par panache, le frère refuse la grâce qui lui est
proposée en échange d'un acte de servilité et de repentance. Et se
prépare donc à mourir en martyr de la Révolution.

Essayons d'interpréter ces quelques mots de Lénine. La voie qu'il
faut emprunter, pense-t-il, ne peut pas être celle de l'action
groupusculaire, la véritable radicalité doit être une radicalité de
classe et de masse. Il s'agit de s'organiser, d'organiser les masses.
Seul le prolétariat ouvrier, dirigé par un Parti révolutionnaire et muni
d'une Théorie adéquate, sera en mesure de renverser le
capitalisme[^15], etc.

C'est une tragédie analogue qui détermine, chez Lénine comme chez
Pasolini, la nature de leur rencontre avec la théorie marxiste. Et si
leur engagement a pris des voies radicalement différentes, ayons
cependant l'impudence de remarquer qu'il existe une affinité entre le
brûlot de Lénine à l'égard des gauchistes de son temps[^16] et les
acerbes critiques[^17] que le poète italien formulera à l'égard de la
jeunesse militante.

Selon Lénine, une tendance infantile du communisme doit être
combattue : radicalité abstraite et posture révolutionnaire, volonté
d'en découdre, refus de différer l'action, de médiatiser sa pratique par
une théorie fondée sur des principes objectifs et rationnels, etc. Le
« gauchisme » est ainsi présenté comme une manifestation inefficiente et
même contre-productive de la volonté révolutionnaire ; le combattre
constitue donc l'une des tâches du révolutionnaire professionnel[^18].

À la fin des années 60, dans un contexte socio-historique radicalement autre,
Pasolini produira également une critique des gauchistes de son temps. Il existe cependant une différence notable dans leur traitement de la question : pour le poète, le gauchisme n’est pas tant la « maladie infantile du communisme » que celle de la petite-bourgeoisie ; son idéologie ayant contaminé l’ensemble de la société et donc également la jeunesse militante[^19].

Autre différence remarquable (qui ne peut être expliquée seulement
par la différence de registre de leur œuvre respective) : lors de sa
rencontre avec le prolétariat rural, Pasolini ne s'intéresse pas tant à
la lutte des journaliers qu'à la manifestation, à travers cette lutte,
d'une culture spécifique, paysanne, frioulane, susceptible d'apporter un
contrepoint à l'horizon platement universaliste du marxisme officiel. Si
le point de vue de Lénine demeure celui de l'universelle émancipation du
monde par un prolétariat unifié, capable d'imposer une direction et un
cadre à l'ensemble de la société, le statut du spécifique, chez
Pasolini, ainsi que son articulation à l'universel, diffère donc
radicalement.

Lorsque le jeune Pier Paolo s'installe dans le Frioul, il ne connaît
alors qu'un seul monde, celui de la bourgeoisie de Bologne, sa langue et
ses valeurs. En rencontrant les journaliers en lutte, il fait la
rencontre d'un monde étranger et réalise ainsi que le monde contient en
puissance une pluralité de mondes possibles, une pluralité de mondes qui
échappent à la conscience bourgeoise. Le postulat selon lequel, malgré
la domination totalitaire de la bourgeoisie, quelque chose s'échappe
toujours, et que ce quelque chose est d'abord un langage, constituera
une véritable boussole sensible et épistémologique dans la vie et
l'œuvre de Pasolini.

Autre enseignement des années de Casarsa, qu'il partage ici avec
Lénine, non seulement le monde est irréductible à sa représentation
bourgeoise, mais celui-ci est violemment divisé en classes sociales.
Ainsi la seule espérance possible, la seule espérance qui puisse se
présenter au jeune Pasolini comme dotée d'un contenu véritable, ne peut
être que celle d'une humanité réconciliée avec elle-même au terme d'un
processus révolutionnaire :

> Cette lumière était espérance de justice :
>
> je ne savais laquelle : la Justice[^20].

Bien que formulée de façon encore relativement abstraite, cette
espérance n'en est pas moins concrètement communiste. Ce qui signifie
qu'elle vise une défense de la terre par celles et ceux qui la
travaillent, une réappropriation des grands moyens de production, une
émancipation du travail aliéné et donc une abolition des rapports de
production fondés sur l'exploitation d'une classe par une autre :

> Dans l'histoire, la justice fut conscience
>
> d'une division humaine des richesses,
>
> et l'espérance prit une nouvelle lumière[^21].

Ici, le salut révolutionnaire implique une sécularisation de l'Éden
chrétien ; il s'agit d'un salut, certes, mais d'un salut pour les
vivants. Et ces vivants, très concrètement, pour le jeune Pasolini, ce
sont les corps resplendissant de lumière des journaliers en lutte.

Appliquons-nous à saisir ensemble ces deux aspects de la question :
l'universelle émancipation du prolétariat *et* le corps prolétaire
spécifique. Le postulat marxiste-léniniste se voit à la fois confirmé et
infirmé : le vécu et l'histoire doivent coïncider. Au risque de renier
l'un au détriment de l'autre, et donc de les disqualifier tous deux.

Depuis son point de vue de jeune bourgeois bolognais, dont la
connaissance du monde est encore circonscrite à une culture classique
pétrifiée, c'est précisément cette dialectique du vécu et de l'histoire
qui lui permet d'opérer un saut dans la matière vive des mondes
prolétaires et sous-prolétaires :

> Ainsi l'aube naissante fut une lumière
>
> en dehors de l'éternité du style[^22]...

Cette rencontre de Pasolini avec le marxisme fait figure de
« conversion[^23] » en cela qu'elle modifie sa conscience, défait le
langage des choses auquel son enfance bourgeoise l'avait initié et
l'ouvre sur la diversité sociale et culturelle des mondes humains. Et
s'il s'agit également d'une conversion théorique --- il consacrera de
longues heures solitaires à l'étude de Marx, Lénine ou Gramsci ---
insistons sur le fait qu'il s'agit avant tout d'une conversion sensible,
charnelle.

Alors que les actions strictement militantes du poète, ses « petites œuvres »,
seront toujours menées parallèlement à son œuvre littéraire
--- « actions parallèles qui n'entraînent pas l'abandon de la
littérature[^24] » ---, il nous faut cependant interroger le caractère
éminemment politique, bien qu'excédant tout discours sur l'Histoire, de
celle-ci. Par-delà les engagements conjoncturels qui furent les siens,
donc, nous défendrons dans le chapitre suivant l'hypothèse selon
laquelle toute l'œuvre de Pasolini s'insère dans une tradition secrète à
laquelle il revenait de faire droit, *dans* et *par* la littérature, à
une autre raison du communisme.

</div>

<div class="text" data-number="2">

## 3. Enfance du communisme

Le langage poétique se voit engagé dans une éternelle guerre de position
contre la langue du pouvoir, sa police et ses académies. Mais peut-il
sortir de sa confrontation stationnaire à l'*ordre des choses*, et se
doter d'une fonction active au sein d'un processus révolutionnaire ?

Autrement dit : la poésie est-elle nécessairement vouée à regarder
le monde pourrir ?

Les mouvements révolutionnaires ont toujours activé « des espaces dans
l'espace[^25] », espaces où des actions poétiques, plurielles,
compulsives, effrénées ont su se manifester et furent le plus souvent
réprimées par le pouvoir central.

Ce fut le cas de Vladimir Maïakovski, de Sergueï Essenine, d'Ossip
Mandelstam, des poètes de langue yiddish lors de la mal nommée « Nuit
des poètes assassinés » en 1952, et la liste est si longue que nous
choisissons ici seulement d'évoquer celles et ceux que nous connaissons
et que nous aimons, celles et ceux dont les humiliations, les
exécutions, les « suicides » nous pèsent encore, et dont nous estimons
qu'ils participèrent activement à l'esprit et à la lettre de la
contre-révolution.

Tout processus émancipateur se caractérise par une irruption et un
« partage du sensible[^26] ». Pour autant, il ne s'agit pas de
constituer le langage poétique comme prérequis à toute action
révolutionnaire, mais de faire primer le fait positif et autonome d'une
expression poétique sur le « règne des fins », même dans le cas où ces
fins se présentent à nous comme « révolutionnaires ».

Ce pour quoi nous suivons Michel Leiris quand il nous dit que l'art ne
doit aucunement être réduit à une fonction illustrative ou
représentative, qu'il ne peut avoir pour vocation d'incarner ou de
matérialiser du discours --- même communiste, même révolutionnaire ---,
que son autonomie est gage d'une société engagée dans une émancipation
totale :

> Si la Révolution appelle un art et une littérature révolutionnaires par
> leur signification immédiate, il ne faut pas que cette exigence tactique
> soit satisfaite au détriment de la stratégie et qu'on oublie que, pour
> être totalement révolutionnaires, autrement dit, pour répondre à tous
> les besoins de la Révolution, un art et une littérature ne doivent pas
> simplement viser à exalter, propager ou orienter l'esprit
> révolutionnaire, mais tendre, au moins par certains de leurs aspects, à
> transformer d'ores et déjà en préfiguration du futur « homme intégral »
> l'homme d'aujourd'hui qui commence à peine à se défaire de ses chaînes.
> D'où la valeur révolutionnaire de toutes les œuvres qui tendent à ruiner
> les stéréotypes rassurants sur lesquels l'homme aliéné croit pouvoir se
> fonder, soit qu'elles bouleversent de fond en comble la vision que l'on
> a du monde (Picasso), soit qu'elles donnent une conscience plus brûlante
> de la condition humaine (Kafka), soit qu'elles découvrent à l'homme et à
> la femme les ambiguïtés et les doubles fonds de leurs désirs
> (Bataille)[^27].

L'art poétique est expérimental par essence. Ce qui signifie qu'il
engage son créateur sur une voie qu'il ne connaît pas. Et si le poète
campe dans sa forge, ce n'est pas tant pour réaliser un objet qu'il
aurait déjà en tête (un discours joliment imagé par exemple), ou une
certaine représentation de cet objet (l'autoportrait du pouvoir), que
pour découvrir un rapport nouveau.

Nous nous situons sur un plan différent de celui du travail de l'artisan
dont Marx nous dit que c'est le fait d'avoir conçu son œuvre *dans sa
tête* avant de la réaliser *avec ses mains* qui le distingue de
l'abeille[^28]. Rabattre la création artistique sur la production
artisanale --- et bien que nous comprenions la nécessité de désidéaliser
la figure du créateur --- tend à rabattre l'acte expérimental sur l'acte
producteur d'objets *utiles* à notre vie, à circonscrire ainsi toute
forme de travail artistique à la production de valeurs.

*A contrario*, ce sont précisément les caractères expérimentaux de
l'acte créateur qui en font quelque chose de réellement révolutionnaire :

> Travailler sans directives données de l'extérieur, sans idées préconçues
> --- ou presque --- et comme s'il allait à la découverte, c'est sans
> doute le meilleur moyen pour l'artiste ou l'écrivain d'échapper aux
> stéréotypes et de faire ainsi œuvre vraiment authentique et créatrice.
> Pour le créateur, il ne s'agit pas d'accomplir un « chef-d'œuvre »,
> --- notion qui se réfère à l'époque ancienne où, pour être reçu maître dans
> une corporation, il fallait faire ses preuves d'habile artisan en
> exécutant une œuvre reconnue proprement magistrale. Cette notion garde
> encore un sens en société capitaliste, celui d'œuvre digne par
> excellence d'être acquise par un amateur ou par un musée, mais elle ne
> peut être admise en société révolutionnaire, puisqu'elle suppose que
> l'œuvre est traitée comme une denrée susceptible d'être plus ou moins
> recherchée. Pour le créateur, il s'agit toujours d'expérimenter et de
> s'aventurer : quand il commence un travail, il ne sait pas exactement où
> cela le mènera et c'est, justement, pour savoir où cela le mène qu'il
> travaille. Très précisément, c'est cette façon d'avancer comme on
> débroussaille à coups de machette qui s'appelle « créer[^29] ».

L'art implique donc son sujet en l'excentrant, en le projetant par
l'usage créateur de la langue (écrite, orale, cryptique) dans le langage
de la réalité --- un langage qui n'est jamais donné et sans cesse à
découvrir : ce n'est pas le poète qui fait le poème lucide, mais le
poème qui fait le poète voyant.

Par-delà toute morale, par-delà toute grammaire, l'acte créateur
repousse les hiérarchisations induites par le Savoir ; l'expérience
artistique se faisant ainsi l'objet d'un « connaître » qui brise les
schémas existants de la connaissance.

Tant sur le plan des significations que sur celui des justifications,
l'ouvrage excède toujours plus ou moins la fonction que l'artiste lui
assigne initialement. En d'autres termes, l'expérience artistique accuse
son objet autant qu'il dévisage son sujet.

Par sa pratique même, dans sa contestation de l'ordre du Discours, dans
son subvertissement du régime de la Représentation, l'art est relation à
soi, au monde, sur le mode d'une confrontation expressive à l'altérité
--- *je est un autre* : relatif notamment à ce « double fond » du désir,
à nos ambiguïtés les moins assignables dans la grammaire du connu.

Michel Leiris, encore :

> Dans le domaine artistique et littéraire, un créateur ne peut pas être
> un homme satisfait de la culture existante. Ce qui le pousse à la
> recherche, c'est le besoin de rompre avec ce qui existe et de faire
> autre chose. De sorte qu'une société, même communiste, ne peut prendre
> des mesures visant à l'« encourager », car cela tendrait *ipso facto* à
> le domestiquer. Elle ne peut que lui garantir l'exercice de son absolue
> liberté d'investigation. Cela, sans réticences, et en considérant que
> ces travaux effectués en toute liberté ne peuvent qu'aider la Révolution
> dans sa marche vers la totale liberté[^30].

L'histoire moderne se fonde sur un déchirement ontologique : si l'être
et son expression ne font qu'un, affirme Marx, le travail, en tant que
pôle central de la praxis humaine, a été dévoyé sous régime de
production capitaliste ; ce déchirement inaugural reposant en premier
lieu sur la rupture métabolique de l'Homme et de la Nature :

> L'opposition entre la ville et la campagne ne peut exister que dans le
> cadre de la propriété privée. Elle est l'expression la plus flagrante de
> la subordination de l'individu à la division du travail, de sa
> subordination à une activité déterminée qui lui est imposée. Cette
> subordination fait de l'un un animal des villes et de l'autre un animal
> des campagnes, tout aussi bornés l'un que l'autre, et fait renaître
> chaque jour à nouveau l'opposition des intérêts des deux parties[^31].

Il faut insister sur le fait que toutes les oppositions discursives qui
dérivent de cette rupture renvoient à une série d'aliénations très
concrètes ; celles-ci ayant la consistance ontologique d'un camion qui
nous écrase ou d'une forêt qui brûle, elles ne peuvent donc aucunement
apparaître comme simples distorsions de la conscience[^32].

Le communisme vise à libérer le vivant de cette opposition inaugurale
qui le déchire ; cette libération s'opérant comme accomplissement du
devenir de l'être en tant que langage --- c'est ici « l'énigme résolue de
l'histoire » :

> Ce communisme en tant que naturalisme achevé = humanisme, en tant
> qu'humanisme achevé = naturalisme ; il est la vraie solution de
> l'antagonisme entre l'homme et la nature, entre l'homme et l'homme, la
> vraie solution de la lutte entre existence et essence, entre
> objectivation et affirmation de soi, entre liberté et nécessité, entre
> individu et genre[^33].

Sur le terrain de la praxis révolutionnaire, cette opposition devra être
surmontée par un sujet révolutionnaire en capacité de fédérer, sans les
soumettre à la Loi des grands nombres et aux structures pétrifiées de la
gauche institutionnelle, les mondes paysans, ouvriers et
sous-prolétaires --- les difficultés pratiques et les apories
stratégiques rencontrées au sein des luttes révolutionnaires étant
souvent corrélées à l'homogénéisation d'un sujet révolutionnaire qui se
reconnaît lui-même dans l'exclusion de l'*autre*.

Sur le terrain poétique, la réconciliation des termes de l'opposition
s'incarne dans le visage de l'enfance ; ce dernier n'ayant pas encore
conféré au *paysage* les déterminations d'un *cadre* : l'enfant
reconnaît le monde, et se reconnaît lui-même à travers lui, non par la
médiation d'un concept ou d'une culture déterminée, mais à partir de ses
perceptions sensibles.

Est-ce à dire, si nous voulons de nouveau rencontrer un paysage, qu'il
nous soit nécessaire de destituer une part importante d'acquis sociaux
et culturels ? Oui et non. Car la connaissance spécifique de la géologie
ou de l'agriculture permet évidemment au poète de comprendre un paysage,
d'en apprécier l'histoire ou d'en déplorer l'altération ; mais il lui
est tout autant nécessaire de se laisser happer, surprendre par ses jeux
de couleurs, ses reliefs, par le caractère accidentel de la roche et le
miracle de la flore se débattant avec elle.

Avant Marx et ses héritages contradictoires, Hölderlin a tenté de
composer une dialectique de la Tradition et du Paysage, sans inféoder
l'un à l'autre, et encore moins à l'Histoire --- c'est une figuration du
feu, celui de la *nouvelle origine*, qui intègre les éléments de
l'antagonisme (Nature, Tradition) et les dissout[^34] dans une image
de notre salut :

> Ainsi, osez ! votre héritage, votre acquis,
>
> Histoires, leçons de la bouche de vos pères,
>
> Lois et coutumes, noms des Dieux anciens,
>
> Oubliez-les hardiment pour lever les yeux,
>
> Comme des nouveau-nés, sur la nature divine.
>
> Alors, votre esprit à la lumière du ciel
>
> Embrasé, d'un souffle tendre de vie
>
> Votre poitrine abreuvée comme au premier jour,
>
> Quand bruiront sous leurs fruits d'or les forêts,
>
> Jailliront les sources du rocher, quand la vie
>
> Du monde, son esprit de paix, vous saisiront
>
> Et l'âme vous berceront comme un chant sacré,
>
> Qu'alors perçant les délices d'une belle aube
>
> Luiront d'un éclat nouveau les verdures de la terre
>
> Et la montagne et la mer, les nuages et les astres,
>
> Que ces nobles forces, tels des frères héros,
>
> Venant sous vos yeux vous feront battre le cœur
>
> Ainsi qu'à des écuyers dans un désir de prouesses
>
> Et d'un monde vôtre et beau, alors tendez-vous les mains,
>
> Donnez-vous votre parole et partagez votre bien[^35].

Le nouveau-né vit dans le *langage* et non encore dans la *langue*. Ses
yeux s'ouvrent sur un monde vierge de significations ; c'est un regard
offert à la lumière --- poème de feu et de cendres. Ni capture ni
hiérarchisation des perceptions, mais pur abandon dans le feu de la
*nouvelle origine*.

L'image du feu hölderlinien se conclut sur un appel au partage, à la
communion, emblématique, pensons-nous, de cette tradition cachée, de ce
communisme de l'enfance, ou enfance du communisme, visant l'unité
dialectique[^36] de l'être et du langage dans la perspective d'une
écologie qui serait à la fois sémantique et matérielle.

C'est en ce sens que cette terre, en sa myriade d'éléments qui la
composent, minéraux, végétaux, animaux, en sa totalité inachevable dans
l'enclos du discours, pourra être *nôtre* --- non dans l'esprit *du
maître et du possesseur*, mais dans celui de l'enfant, qui fait corps
avec le monde sans apriorité culturelle ni classification surplombante.

Cette figuration hölderlinienne de l'enfance n'est pas un appel à la
régression, mais à la destitution de ces « leçons de la bouche de vos
pères, / lois et coutumes, noms des dieux anciens » qui assignent au
monde une signification morte.

Par-delà la question du Savoir, c'est une guerre silencieuse menée
contre un certain usage du monde. Mais il n'est pas question pour autant
d'anéantir la tradition en soi, seulement de pulvériser le rapport
aliéné qui la place devant la Nature en tant que corps étranger.

Nietzsche, dans le sillon de Hölderlin, rend admirablement compte
d'un processus de transvaluation qui rompt avec la synthèse vide des
philosophies de l'histoire : s'il s'agit bien de se débarrasser du poids
de la Morale, cette forme pétrifiée de la tradition, il ne s'agit pas
pour autant de s'appesantir dans une posture régressive, de rugir
mécaniquement contre l'Ordre, mais de parvenir au stade de la nouvelle
Aurore, ce devenir-enfant de l'être, qui n'est pas régression, donc,
mais transformation et nouvelle subjectivation des valeurs, non plus du
point de vue du Droit, mais depuis l'être et son langage, en leur unité
conflictuelle et féconde.

Et Nietzsche d'affiner la figuration poétique de l'enfance, d'en dégager
à grands traits quasi mythologiques les principaux mouvements :

> L'enfance est innocence et oubli, un renouveau et un jeu, une roue qui
> roule d'elle-même, un premier mouvement, une sainte affirmation[^37].

L'oubli, vertu cardinale de la philosophie nietzschéenne, contre une
conception totalitaire de l'Histoire, permet d'envisager le vide et donc
le possible, bifurcation et renouveau, quête d'innocence et de joie, à
l'aune de la nouvelle aurore.

Sur le terrain de la praxis poétique, du *langage*, cet oubli peut
se traduire par une méfiance à l'égard de la syntaxe, par un pessimisme
devant le caractère communicationnel de la langue.

À ce jeu, il y a les bulldozers : Ghérasim Luca, Samuel Beckett,
Valère Novarina, aujourd'hui Laura Vasquez sans doute, etc.

Puis il y a ceux, Maïakovski ou Mandelstam en premier lieu, qui
n'ont jamais renoncé à ce « quelque chose d'écrit », inscrivant leur
poétique dans l'ordre des significations, mais sans jamais cesser de le
martyriser, de le distordre. Le langage n'est pas la langue, certes,
mais ce que peut la poésie est affaire de langue et de langage mêlés.
Autrement dit : le langage poétique épuise les potentialités factices de
la langue, et l'engage dans un *espace ouvert* qui mêle le visible et
l'invisible, le son et le sens, l'écrit et le non-écrit.

Le différend stratégique est bien réel : si les bulldozers visent
purement et simplement une dissolution du sens et une désintégration de
l'image, les seconds constituent le langage comme un « champ
stratégique » où le sens et l'image sont arrachés à leur bienséance et
déploient leur efficacité matérielle dans l'investissement et le
dépérissement de l'ordre du discours et du régime de la représentation.

Plus proches de nous, Nathalie Quintane ou Jean-Marie Gleize
contribuent à habiter l'espace de la littérature en en
contestant les ors et les colonnades. Qu'il s'agisse du réalisme
ponctuel de la première (Lautréamont via Proust), ou de la nudité et/ou
littéralité du second (Rimbaud via Ponge), tout est bon pour demeurer
dans les choses mêmes, document ou lisière, ne jamais s'encombrer de
procédés qui recouvriraient le bruit de l'eau dans une rivière ou la
forme objective d'une chaussure.

Ainsi se profile une autre fonction politique de la « poésie »
(dans le cas de Quintane et Gleize, nous nous évertuons à appeler ça
ainsi, faute de mieux) : une mise en question permanente de la grandeur
de la littérature en tant que domaine de pratiques symboliques séparé du
vivant et/ou de la vie sociale.

Si la littérature demeure bien un acte symbolique, elle se fait
conjointement l'objet d'une recherche d'efficacité matérielle --- « prose
tactique[^38] », écrit Benjamin Fouché, qui se reconnaîtrait sans
doute dans cette filiation profane. Cette exigence matérialiste n'en est
pas moins en tension permanente avec l'essence symbolique de la langue,
il serait donc imprudent de s'en remettre à une dichotomie figée entre
matérialisme et symbolisme ; cette recherche d'efficacité matérielle se
déployant toujours *depuis* et *dans* le champ symbolique du langage
humain. À moins de prendre la littérature pour ce qu'elle ne sera jamais :
une rivière ou une chaussure.

Cette quête d'une langue basse et matérielle excède-t-elle la
fonction strictement communicationnelle du langage ? Distinguons deux
types de « communicabilité » : le pouvoir de nommer des objets et d'en
partager socialement l'appréhension, d'une part ; des effets de
discours, de l'autre. Le second type s'offre assez naturellement comme
la proie d'une certaine littérature : dans son *ABC de la
barbarie*[^39], par exemple, Jacques-Henri Michot passe un grand coup
de balai sur le terrain miné des linguisteries journalistiques ;
Quintane, quant à elle, si elle s'attache à dire des choses que tout le
monde voit, ou que tout le monde est en capacité de voir dans une
circonstance déterminée de la vie sociale, n'hésite pas non plus à
pulvériser les effets de discours qui contribuent à recouvrir la réalité
sous des couches de velours ou de sidération ; son approche est donc
tout à la fois réaliste, ponctuelle *et* critique.

L'approche critique ne contrevient pas à la quête de littéralité,
l'une et l'autre soutiennent ensemble une tension dialectique qui porte
à son comble l'effraction de la littéralité dans le champ du langage
--- une littéralité qui épuise donc quelquefois la « communicabilité » de la
langue, et peut puiser conjointement à la source rimbaldienne sa
nécessité d'une forme objective et inaccaparable --- une forme, osons le
dire, en un sens infiniment plus concret que dans son acception
idéologique et dévoyée : « antifasciste ».

La centralité du régime symbolique (le fait que le langage institué
se déploie depuis une grammaire donnée, des valeurs déterminées, un bon
sens populaire ou autres mythologies bourgeoises) est donc mise à mal
par la pure matérialité de la langue (nudité, littéralité, crudité) ; et
ce alors même que l'efficacité matérielle de la langue fait retour à la
réalité, souvent la plus ordinaire, la plus universelle --- ni plus ni
moins : « *Chaussure* ne résulte pas d'un pari ; il ne présente aucune
prouesse technique, ou rhétorique. Il n'est pas particulièrement pauvre,
ni précisément riche, ni modeste, ni même banal. Ce n'était pas un
projet, mais ce n'est pas un brouillon, mais il n'a pas encore trouvé sa
fin[^40]. »

En prenant le parti unique de l'efficacité matérielle ou du
symbolisme décadent, on passe sans doute à côté de la cohérence des
destins contradictoires mais complémentaires de l'héritage rimbaldien.
Le lyrisme, par exemple, que Rimbaud contribue à remettre en mouvement,
est attaqué par tous les bouts : sa supposée transparence, son
exaltation du Moi ou de l'Histoire, sa quête de transcendance, etc.
C'est ignorer qu'il précède et excède la littéralité dans l'exacte
mesure où la littéralité se rappelle au lyrisme comme une exigence à la
fois éthique et esthétique de matérialité.

Le lyrisme est attaqué de toutes parts --- à l'exception du principal
théoricien de la littéralité qui nous rappelle ce que l'histoire
littéraire lui doit :

> Et c'est peut-être une première et fondamentale caractéristique de la
> démarche hugolienne : la poésie est ce langage qui ne peut rien dire
> qu'en posant incessamment la question de sa possibilité et de sa
> légitimité, la poésie n'est pas véhicule de réponses toutes faites,
> mais creusement d'un espace où le sens (du moins pour Hugo) doit
> pouvoir surgir ; en l'occurrence, évidemment, et c'est ce qui apparaît
> si l'on observe non pas tel ou tel poème, mais la dynamique du geste
> hugolien, c'est ce creusement qui est essentiel, non le surgissement,
> puis la saisie du sens. Ce creusement, qui fait de la poésie un acte et
> de cet acte une question toujours maintenue, une tension productrice, ne
> va pas sans ouvrir, à l'intérieur du moi, une faille, une fissure, par
> où pourra s'engouffrer toute la suite (jusqu'à la « disparition
> élocutoire » du poète). L'essentiel est là : que, dès sa naissance
> romantique, la poésie personnelle est une question pour elle-même,
> qu'il n'y a jamais eu, sauf dans les Histoires de la Littérature, de
> poésie lyrique transparente, qu'il y a une réflexion lyrique où se pose
> simultanément la double question de l'apparition et de la disparition
> du « je[^41] ».

Ainsi le lyrisme ne cesse de se chercher un sujet, d'en nier la
substance dans l'éparpillement des choses ou des traditions, de se
réinventer un *moi*, de le voir disparaître, puis réapparaître et se
dédoubler, se démultiplier, tendre vers le monde, faire monde(s), etc. À
ce titre, le lyrisme a toujours à faire avec les « accidents du
sol[^42] », les anfractuosités de la vie sociale, mais ne se résigne
jamais à un quelconque naturalisme : *soyons prodigieusement matériels
et bassement exaltés !*

Ce pour quoi le culte de la littéralité doit également prendre la mesure
de sa potentielle naïveté : matérialisme appauvri ou art radical étalé
dans des galeries, l'immanentisme est une autre façon de coller à la
peau d'un monde qui n'existe pas. N'hésitons pas à aiguiser notre
exigence d'une vie « totale » --- dans le sens qui est celui de Leiris et
non plus celui de Hegel ---, c'est-à-dire d'une existence qui se rapporte
non seulement à l'existant mais également au possible, à exiger un monde
pour celle-ci, et non un slogan ou autre bel objet plastique exposé dans
une vitrine de Saint-Germain-des-Prés ou de Pigneto.

Pour un jeune garçon qui a grandi sous le fascisme, ce n'est pas tant la
littéralité, la nudité ou la crudité de la langue qui importe, ce retour
à la bassesse de la vie et des choses, que de faire voler en éclat toute
centralité symbolique par un excès de brisures, un nombre infini de
déplacements sémantiques, de nouvelles configurations syntaxiques. C'est
ainsi que, loin de Pigneto, loin de Saint-Germain-des-Prés, lorsque le
jeune Pier Paolo découvre *Une saison en enfer*, d'instinct, il choisit
son camp :

> La lecture de Rimbaud et de la poésie symboliste et décadente m'a fait
> prendre conscience de façon mécanique, automatique, que j'étais contre
> le fascisme, et elle a donc eu une fonction politique positive[^43].

La « poésie symboliste et décadente », pour peu qu'on la réduise à
cette désignation, a bien le pouvoir de maltraiter la fonction
strictement communicationnelle du langage institué, et donc également sa
« littéralité ». Dans le cas particulier de Rimbaud, il s'agit de
creuser le sol vicié de sa langue nationale, d'en maltraiter les
représentations, de la tordre jusqu'à inventer de nouvelles lignes de
force au cœur même de ses fondations ; sa poétique, toute symboliste
qu'elle soit, mobilisant une matérialité qui brise l'ordre du discours,
en éparpille les petits bouts de quartz et les enfonce cruellement dans
les représentations pacifiées de la vie des peuples et des nations.

Mais pourquoi Rimbaud et tant de poètes après lui s'en prennent-ils à la
langue en général, et à leur propre langue en particulier ? N'est-elle
pas notre héritage commun, ce sur quoi nous serions en mesure de fonder
notre communauté humaine et nationale ?

Précisément : cette langue, à l'ère de l'État-nation et de ses
dynamiques impériales, est devenue un instrument politique au service de
l'hégémonie linguistique des nations civilisatrices ; à ce titre, son
imposition est nécessairement liée à la relégation ou à l'exclusion des
langues subalternes qui seules étaient en mesure de produire un rapport
à la réalité échappant à cette clôture.

Aux « barbares », qui par définition ne parlent pas la langue du colon
ou ne la parlent pas comme il faut, la poésie moderniste se joint en
faisant valoir sa propre sauvagerie. C'est ainsi que Rimbaud, stratège
sur le terrain de la syntaxe et militant d'une forme libre, s'attelle à
la fabrication de projectiles ayant pour destin de faire trembler les
vitrines du grand Parnasse.

Pasolini a beau reconnaître le geste rimbaldien, sa puissance
destituante, son « antifascisme » viscéral, le jeune poète de Bologne
n'a encore jamais mis un pied sur une place de Rome ou de Milan, il erre
aux marges de la modernité, dans la périphérie de Bologne, dans la
campagne frioulane, et s'intéresse donc à d'autres traditions,
dialectales, idiomatiques, vulgaires et menacées.

Marx remarque que le vieux sentimentalisme des sociétés féodales a été
noyé dans les *eaux glacées du calcul égoïste*[^44], mais il ne fait pas
grand cas du génocide culturel perpétré par la forme culturelle du
capitalisme à l'égard des traditions vernaculaires ; on peut même
supposer que l'hégémonie linguistique serait au service de ce devenir
révolutionnaire d'un prolétariat transnational et unifié. Si
Pasolini affirme à plusieurs reprises que Marx est conscient du
génocide culturel perpétré par le capitalisme, insistons donc sur le
fait que le théoricien révolutionnaire ne s'en émeut que
raisonnablement, tant sa conception de l'histoire est encore imprégnée
d'étapisme et de téléologie progressiste. Il faut croire qu'ici Pasolini se réfère plutôt à l’insu du discours qu’à la clarté du *logos*.

Et pourtant, si Spinoza considère que les difficultés des humains reposent essentiellement
sur leurs difficultés à communiquer[^45], il est peut-être temps
d'infirmer le postulat selon lequel une langue universelle devrait
primer sur l'harmonie des langues, l'homogénéité linguistique sur
l'entrelacement des dialectes --- rien ne sert de communiquer si nous
n'avons plus rien à nous dire.

Près d'un siècle après Hölderlin, dans une lettre à son éditeur, Rainer
Maria Rilke, cherchant à « expliquer » le sens de ses *Élégies de Duino*,
écrit déjà qu'il perçoit une mutation anthropologique majeure dans nos
sociétés européennes de l'entre-deux-guerres ; celle-ci ayant tout à
voir avec une fracturation de la conscience en partie liée à une
aliénation du pouvoir de désignation de la langue --- nous ne sommes plus
en mesure, dit-il, de nous reconnaître dans les choses que nous nommons :

> Aujourd'hui, l'Amérique nous inonde de choses vides, indifférentes, de
> pseudo-choses, *d'attrapes de vie*... Une maison, au sens américain, une
> pomme ou une grappe de raisin américaines, n'ont *rien* de commun avec
> la maison, le fruit, la grappe qu'avaient imprégnés les pensives
> espérances de nos aïeux... Les choses douées de vie, les choses vécues,
> *conscientes de nous*, sont sur leur déclin et ne seront pas remplacées.
> *Nous sommes peut-être les derniers qui auront connu encore de telles
> choses*. Nous avons la responsabilité de sauvegarder non seulement leur
> souvenir (ce serait peu de chose, et bien peu sûr), mais leur valeur
> humaine et larique (au sens des divinités du foyer). La terre n'a pas
> d'autre issue que de devenir invisible : *en nous*, qui participons pour
> une part de nous-mêmes à l'Invisible, qui en possédons (au moins) des
> actions, et qui pouvons augmenter notre capital d'Invisible pendant que
> nous sommes ici, --- *en nous* seulement peut s'accomplir cette
> transfiguration intime et durable du Visible en Invisible, en une
> réalité qui n'ait plus besoin d'être visible et tangible, de même que
> notre propre destin, en nous, ne cesse *de se faire à la fois invisible
> et plus présent*[^46].

C'est une double aliénation qui se fait jour --- double du point de vue
de la langue allemande qui, contrairement à la langue française, est
dotée de deux termes différenciés pour désigner l'aliénation[^47] :
*Entfremdung*, qui renvoie au sentiment d'étrangeté de l'être qui ne
reconnaît plus son monde ; *Entausserung*, qui renvoie à un processus de
mauvaise objectivation, d'objectivation pillée et dénaturée.

Si l'être est privé de ses capacités à se reconnaître et à reconnaître
le monde par la médiation de la langue, c'est que la langue du Capital
est devenue une langue vide, strictement instrumentale. Et ce qui est
valable pour le monde des choses l'est également pour celui des idées :
que sont devenus la « Liberté », l'« Homme », le « Savoir »,
l'« Universel » dans la bouche des sentinelles de l'ordre existant ?

Hegel, déjà, avait à cœur de redonner à l'universalité un contenu
concret. Mais tous les contenus de la philosophie hégélienne ne sont que
des contenus opportunistes, intégrés et donc neutralisés dans le cadre
d'un processus historique surplombant et donc également abstrait.

Hölderlin, qui fut un compagnon de Hegel, avec lequel il partagea,
dit-on, une chambre à Tübingen, avec lequel, dit-on encore, il planta un
arbre en hommage à la Révolution française, fait émerger dans sa
poétique une conception radicalement autre de l'histoire, une conception
qui l'engagera sur une voie non académique.

La folie de Hölderlin est avant tout un défaut de paiement, une
soustraction à la marche de l'Histoire, une désertion ; quant à Hegel,
aux premières inspirations géniales, de Berne, de Tübingen, de
Francfort, sur la belle totalité grecque éclatée, succédera le poids
d'un système qui n'en finira plus d'épuiser l'Histoire.

Pasolini, quant à lui, est l'enfant d'un siècle qui n'a plus rien
d'innocent : il ne peut s'abriter dans l'ignorance des contre-finalités
que l'histoire bourgeoise impose au monde, sa destruction. À la folie de
Hölderlin, il substitue un mode d'être tragique, engagé dans une joute
avec la réalité bourgeoise --- l'esprit tragique impliquant une telle
confrontation, fort éloignée des petites dramaturgies et des grands
systèmes de pensée totalitaires : il s'agit de regarder la mort dans les
yeux.

On sait que le jeune Marx était un lecteur de Hölderlin, qu'il s'initia
même à la poésie --- peu de poèmes subsistent ---, avant de plonger dans
l'étude du matérialisme antique, puis dans la lecture de Hegel, de
Feuerbach et des économistes anglais ; sans rejouer le Marx humaniste
contre le Marx critique de l'économie politique, il est cependant
pertinent d'insister sur ce « moment-Hölderlin » dans la formation
intellectuelle du jeune Marx.

C'est à cette tradition méconnue, de communistes profanes, de poètes à
l'acuité extraordinaire, à la sensibilité convulsivement portée
vers une *sauvegarde* du sensible, que nous associons Pasolini ; il
s'agit d'une tradition à la fois alternative et complémentaire au
marxisme philosophique, sa condition de possibilité peut-être ; la
reconnaissance, en tout cas, d'une forme potentielle du communisme,
indissociablement éthique et esthétique.

Nous n'avons esquissé ici que quelques traits de cette tradition à
partir du refus primordial de l'opposition Homme/Nature, mais, comme
toute tradition non institutionnalisée, sa survivance dépend de ce qu'il
nous reste à y découvrir pour un *communisme déjà existant* : une
écologie du sens couvant une écologie matérielle, une poétique
communiste donc, ou, pour le dire autrement, une raison poétique du
communisme.

Celle-ci ne peut se justifier par un discours sur l'Histoire, seulement
par la primauté du langage sur toute forme de Savoir --- Empédocle
jetant au feu « Histoires, leçons de la bouche de vos pères, / lois et
coutumes, noms des Dieux anciens » en cette forge de Vulcain où il
poussa lui-même son dernier souffle --- qui fut également le premier,
celui de la *nouvelle origine*, extase ultime, dissolution et joie,
figurant alors l'enfance comme éternel recommencement du souffle et de
la vision.

La politisation de l'art à laquelle nous enjoint le communisme
benjaminien[^48] --- qui acte la mort de l'aura (*hic et nunc*) à l'ère
de l'avènement des technologies reproductives --- repose sur le constat
que la nouvelle production culturelle (photographies, films, impressions
à grande échelle, et désormais tout ce qui circule en boucle dans
l'immatérialité de nos réseaux) doit être politisée du fait même de
cette reproductibilité potentiellement infinie. Mais le constat de
Walter Benjamin repose sur un principe stratégique et non ontologique :
la production artistique, d'une part, peut demeurer artisanale ; de
l'autre, la politisation de l'art demeure un concept à préciser
--- s'agit-il de greffer un contenu politique à une forme standardisée ? Ou
de réinventer une forme tout en se soumettant aux nouvelles conditions
techniques de la modernité capitaliste ? Si nous comprenons plutôt bien
ce que signifie un « art politique » s'agissant de Brecht, il est plus
périlleux de statuer sur le potentiel politique d'un Samuel Beckett ou
d'un Paul Celan ; ce dernier s'exprimant, nous prévient-il, seulement du
point de vue des corneilles.

C'est ici que nous semble particulièrement intéressante l'idée d'Adorno
selon laquelle Celan ou Beckett, en se soustrayant à l'ordre du
Discours, à la structure traditionnelle du récit ainsi qu'au régime de
la représentation, donnent à voir une action sur la langue ouvrant des
brèches au cœur de nos consciences rationalistes, consuméristes et donc
nécessairement pauvres en langage[^49].

Ce pour quoi, après avoir distingué les bulldozers et les stratèges, il
nous semble important d'envisager leur alliance : le travail du montage,
en cela qu'il se confronte aux nouvelles conditions techniques de notre
temps, est en capacité de faire jouer Beckett avec Marx --- art de la
destitution *et* recomposition d'un horizon stratégique sur le terrain
de l'art.

Longtemps nous avons cru qu'un art, afin d'être efficace politiquement,
conséquent, substantiel, devait être porteur d'un contenu (moral,
politique, idéologique) ; aujourd'hui, en cette époque de clôture
sémantique qu'induit la langue néolibérale, nous pensons au contraire
que c'est en convoquant la puissance destituante du langage au sein de
nos horizons stratégiques que nous rendrons possible la formation d'un
art émancipé de la rationalité économique, de la forme-marchande et de
l'idéologie consumériste.

Inversement, il ne peut être question de demeurer rivés à une tactique
ponctuelle --- il nous faut recomposer un horizon stratégique dans le
« domaine de l'art ». Ce que fit Pasolini, obstinément, en diversifiant
sa pratique (poèmes, articles, films), en se confrontant à la technique,
en prenant le risque de se contredire et d'inventer une langue neuve.

Se distinguant peut-être de la poétique de la pure errance
baudelairienne --- actant la cessation du possible tout en se
réconfortant dangereusement au sein de la Beauté ---, Hölderlin, Rilke
ou Pasolini, pour ne citer qu'eux, les plus emblématiques selon nous de
cette tradition inavouée, visent bien une rédemption : il s'agit de
maintenir vivante cette dialectique de la Terre et des Ciels, de la
Ville et de la Campagne, de la Raison et du Sensible, autant
d'oppositions abstraites que leur communisme hétérodoxe ou cryptique
s'est juré de réconcilier.

Il ne pourrait être question de limiter cette tradition aux arts
littéraires : l'odieux Picasso disait qu'il avait passé sa vie à
apprendre à dessiner comme un enfant ; l'acte de figuration, dans la
peinture, s'il tend à reproduire une certaine représentation du monde,
contribue à épuiser les potentialités du regard, à figer l'enfance dans
une maîtrise de la réalité ; et c'est donc à une dialectique de la
maîtrise et de l'effacement que nous convie l'acte créateur ; celle-ci
impliquant d'assumer une part de destruction, d'oubli et de cruauté.

Paul Klee, quant à lui, conçoit son œuvre comme une ontologie du
devenir, jamais comme une totalité achevée[^50] ; ce caractère
d'inachèvement de ses dessins et de ses peintures, leur dynamique
ouverte aux potentialités non encore advenues de l'ouvrage, esquissant
une certaine image de l'enfance --- qui n'est aucunement une
représentation béate de pureté : *tous les anges sont terribles* ---
songeons, par exemple, à ces angelots qui jouent espiègles au pied
d'allégories, sur les murs de l'*Oratorio di San Lorenzo* à Palerme, en
subvertissent la solennité, en contrarient le pouvoir d'évocation.

Cette part de destruction, d'oubli et donc de cruauté que nous associons
à la peinture de Picasso, cette importance du mouvement et de la
contingence qui caractérise l'art de Klee ont pour vocation de
matérialiser une certaine idée de l'innocence, d'une innocence qui n'est
pas diluable dans le laïcisme et la tolérance de la société de
consommation, d'une innocence indocile, impure, exigeante, qui vise un
au-delà du monde au cœur du monde lui-même --- interstice dont Rilke nous avertit qu'il serait le lieu de l'ange par excellence :

> ... *il n'y a ni En-deçà, ni Au-delà, rien que la grande Unité* où ces
> êtres qui nous surpassent, les « anges », sont chez eux[^51].

Chez l'auteur des *Élégies de Duino*, cet *espace ouvert* recouvre la
matière d'une prose versifiée où l'esprit des morts se coltine la
matérialité du poème --- espace du langage qui se confond le plus souvent
avec l'enfance en tant que puissance destituante.

Quant à Pasolini, son horizon stratégique ne cesse de se recomposer :
si la première image frioulane de l'enfance épouse encore les traits
de la pure innocence (« Fontaine de rustique amour[^52] »), celle-ci
mute en projectiles dans *Transhumaniser et organiser* ( »... sachez que
je suis ici prêt / à fournir des poésies sur commande : des engins\*. /
\*Même explosifs[^53]. ») avant de faire l'épreuve d'une réécriture
désenchantée dans *La Nouvelle jeunesse* (« Fontaine d'amour pour
personne[^54] »). Ainsi, tout au long de son œuvre poétique, Pasolini
ne cesse d'inscrire l'enfance et ses métamorphoses dans l'espace du
langage --- ce qui a peu de chances de laisser la littérature indemne.

Soutenir une telle visée suppose un courage éthique et créateur
remarquable : continuer à créer, continuer à agir, en assumant cette
part de destruction, d'oubli, de ce que nous avons aimé, chéri, au
sacrifice même des motifs qui nous ont conduits à persévérer dans l'art
et dans la vie ; il y aura quelque chose après tout ça, et si ce quelque
chose n'est rien, nous avons le devoir de continuer quand même, car ce
quelque chose qui n'est peut-être rien est pourtant tout ce que nous
avons.

C'est sans doute ici que l'on perçoit l'une des plus merveilleuses
manifestations de cette *vitalité désespérée* qui innerve toute la
poésie de Pasolini :

> Je crie, en ce ciel où habita ma mère :
>
> « Avec une incorrigible naïveté
>
> --- à l'âge où l'on devrait pourtant être un homme ---
>
> j'oppose l'arbitraire à la dignité
>
> (qui, d'ailleurs, a cessé d'intéresser nos fils).
>
> Et, contre un peu de science de l'histoire, qui me fait connaître
>
> l'étendue de la tragédie d'une histoire qui s'achève,
>
> je m'adjuge toute l'innocence de la vie future[^55] !

Cette « innocence de la vie future » s'inscrit dans une conception de
l'histoire qui n'est déjà plus celle de Hegel ou du matérialisme
orthodoxe --- qui se fait retour et recommencement, dans le sillage de
Hölderlin peut-être, dans celui de Nietzsche également.

Dans la mesure où la radicalité de Pasolini puise sa source dans une
certaine image de l'enfance, et non dans un verbalisme sans ancrage, ou
encore dans le souvenir d'une résistance mythifiée --- ravivée par les
sursauts bolivien, cubain ou vietnamien[^56] ---, son espérance, toute
révolutionnaire qu'elle puisse être, n'est jamais rhétorique : elle est
*seulement* amour.

Non un idéal d'égalité et de liberté que la classe
ouvrière serait chargée de réaliser, donc, mais la croissance dans les
« soleils de l'âme » d'une exigence toujours renouvelée de Justice :

> Vous avez voulu avoir un poète sur ce banc
>
> lustré par les culottes de tant de pauvres diables ?
>
> D'accord, savourez. La Justice
>
> devient la voix d'hirondelles aveugle, aux désœuvrements 
>
> de la Poésie. Et non parce que la Poésie aurait le droit
>
> de délirer sur un peu d'azur, sur un misérable jour
>
> sublime qui naît de la mélancolie et de la mort.
>
> Mais parce que la poésie est Justice. Justice qui croît
>
> en liberté, dans les soleils de l'âme, où s'accomplissent
>
> en paix les naissances des jours, les origines et les fins
>
> des religions, et les actes de culture
>
> sont aussi des actes de barbarie,
>
> et quiconque juge est toujours innocent[^57].

Quelle correspondance (!), ici, entre le feu hölderlinien qui, sous le
regard du nouveau-né, embrase l'Histoire, la Culture, le Savoir, et
l'âme pasolinienne au sein de laquelle se déploie le jugement poétique
tel qu'esquissé dans ce poème : critique insolente, radicale,
désespérée, du point de vue de l'éternelle enfance, des accomplissements
et des supposées prouesses de la modernité capitaliste.

Si nous devions associer l'enfance --- cet implacable « jugement
innocent » sur le monde --- à une fleur, comment ne pas l'associer au
genêt léopardien qui ne cesse de croître sur les flancs dévastés du
Vésuve, à rebours de toutes ces représentations épiques de la grandeur
de l'Homme, leur exaltation dans une langue morte :

> Sur les flancs calcinés de ce mont formidable,
>
> Sombre exterminateur de l'homme et des cités,
>
> Nulle plante ne croît au souffle des étés ;
>
> Toi seul, sur le versant du gouffre inabordable,
>
> Tu fleuris et souris et parfumes les airs,
>
> Solitaire genêt, qui te plais aux déserts[^58].

La comparaison du langage de l'enfance au genêt pourrait être étendue au
désert de significations qui inscrit l'enfant au cœur d'un monde vierge
de Culture et d'Histoire, et dont l'oubli --- vertu nietzschéenne par
excellence --- constitue la condition de déploiement et de
redéploiement.

Entre oubli et mémoire, donc, dans le sillage involontaire de Benjamin,
Pasolini invente une dialectique nouvelle sur le terrain poétique ;
celle-ci visant une rédemption qui pourrait bien se traduire par le
renversement du capitalisme, mais également et peut-être surtout par la
manifestation concrète d'un *communisme déjà existant* --- c'est-à-dire :
par une abolition de la Politique en tant que domaine de pensées et
d'actions séparé du langage.

Cette (trans)dialectique[^59] est vertigineuse et se déploie au
cœur d'une tradition cachée que nous reconnaissons comme alternative et
complémentaire au marxisme philosophique --- et dont nous tentons seulement
ici d'esquisser quelques traits, de façon partiale mais non arbitraire,
conscients des limites inévitables de notre tentative.

Ajoutons qu'il n'est pas question de *survivance* : nos résistances ne
sont pas assimilables à un patrimoine de vieilles représentations et de
vieux discours. Si nous avons à cœur d'honorer la mémoire des vaincus,
ce n'est pas pour les pendre une seconde fois au pilori d'une conscience
fatiguée, déclinante, mais seulement afin de rendre compte de cette
exigence de Justice qui n'a jamais cessé de croître, en nous, à la
lumière de l'enfance. Cette exigence, qui porte la marque d'une décision
inaugurale, nous engage à choisir entre le monde du langage et celui du
pouvoir.

Chez Pasolini, quelle est donc cette *image* de la liberté en son
point de surgissement inaugural ? Nous pensons bien sûr à une lettre du
très jeune Pier Paolo à l'adresse de son ami Franco Farolfi :

> ... il y a trois jours, Paria et moi sommes descendus dans les recoins
> d'une joyeuse prostitution, où de grasses mammas et l'haleine de
> quadragénaires dénudées nous ont fait penser avec nostalgie aux rivages
> de l'enfance innocente. Nous avons ensuite pissé avec désespoir. 
> 
> L'amitié est une très belle chose. La nuit dont je te parle, nous avons
> dîné à Paderno, et ensuite dans le noir sans lune, nous sommes montés
> vers Pieve del Pino, nous avons vu une quantité énorme de lucioles, qui
> formaient des bosquets de feu dans les bosquets de buissons, et nous les
> enviions parce qu'elles s'aimaient, parce qu'elles se cherchaient dans
> leurs envols amoureux et leurs lumières, alors que nous étions secs et
> rien que des mâles dans un vagabondage artificiel.
> 
> J'ai alors pensé combien l'amitié est belle, et les réunions de garçons
> de vingt ans qui rient de leurs mâles voix innocentes, et ne se soucient
> pas du monde autour d'eux, poursuivant leur vie, remplissant la nuit de
> leurs cris. Leur virilité est potentielle. Tout en eux se transforme en
> rires, en éclats de rire. Jamais leur fougue virile n'apparaît aussi
> claire et bouleversante que quand ils paraissent redevenus des enfants
> innocents, parce que dans leur corps demeure toujours présente leur
> jeunesse totale, joyeuse. Même quand ils parlent d'Art ou de Poésie.
> J'ai vu (et je me vois moi-même ainsi) des jeunes parler de Cézanne, et
> on avait l'impression qu'ils parlaient d'une de leurs aventures
> amoureuses, l'œil brillant et trouble. Ainsi étions-nous, cette
> nuit-là ; nous avons ensuite grimpé sur les flancs des collines, entre
> les ronces qui étaient mortes et leur mort semblait vivante, nous avons
> traversé des vergers et des bois de cerisiers chargés de griottes, et
> nous sommes arrivés sur une haute cime. De là, on voyait clairement deux
> projecteurs très loin, très féroces, des yeux mécaniques auxquels il
> était impossible d'échapper, et alors nous avons été saisis par la
> terreur d'être découverts, pendant que des chiens aboyaient, et nous
> nous sentions coupables, nous avons fui sur le dos, la crête de la
> colline. Nous avons alors trouvé une autre clairière herbeuse, en cercle
> si réduit que six pins à peu de distance les uns des autres suffisaient
> à l'entourer ; nous nous sommes étendus là, enveloppés dans nos
> couvertures, et en parlant agréablement entre nous, nous entendions le
> vent souffler et faire rage dans les bois, et nous ne savions pas où
> nous nous trouvions ni de quels lieux nous étions entourés. Aux
> premières lueurs du jour (qui sont une chose indiciblement belle), nous
> avons bu les dernières gouttes de nos bouteilles de vin. Le soleil
> ressemblait à une perle verte. Je me suis déshabillé et j'ai dansé en
> l'honneur de la lumière ; j'étais tout blanc, alors que les autres
> enveloppés dans leurs couvertures comme des Péons, tremblaient au vent.
> Nous avons ensuite lutté, dans la lumière de l'aube jusqu'à l'épuisement ;
> puis, nous nous sommes allongés, nous avons allumé le feu en l'honneur
> du soleil, mais le vent l'a éteint[^60]...

Et si toute l'œuvre poétique de Pasolini puisait sa source dans l'image
qui s'esquisse ici ? Avec le temps, celle-ci fut bien sûr affinée, sans
cesse recomposée, mais l'image est bien là, déjà là, dans cette lettre
du jeune Pasolini rédigée au tout début de l'année 1941.

Cette image n'est déjà plus tout à fait celle de l'enfance, mais celle de
l'adolescence, âge de la confrontation par excellence, de la
transition douloureuse ; cette abondance de lucioles faisant écho aux
miradors féroces et aux chiens, à l'ordre militarisé et aux espaces
enclos ; tout comme cette flamme, offerte au soleil, et que le vent
éteint.

Toute l'œuvre de Pasolini se déploie dans la confrontation désespérée de
cette image de la liberté avec l'irréalité du monde ; cette image, qui
est donc *fonction* à la fois de sa poétique et de son marxisme
hérétique, apparaissant comme la *raison* d'un type d'espérance qui n'a
plus rien à voir avec un discours sur l'Histoire[^61].

Quant aux lucioles, qui constitueront le motif à la fois réel et
emblématique des analyses de Pasolini sur les mutations
anthropologiques, politiques et culturelles des années qui viendront,
ces lucioles qui auront tout à voir avec cette image primordiale, qui en
constitueront même l'un des éléments quasi prophétiques, ces lucioles qui
incarnent déjà l'insurrection lumineuse de la parole poétique dans la
nuit de l'Histoire, nous les retrouverons donc plus de trente ans plus
tard, dans les *Écrits corsaires* (1975), et ce pour en déplorer la
disparition.

</div>

<div class="text" data-number="3">

## 4. Le flâneur et le corsaire

Quand il arrive à Rome au début des années 50, Pasolini néglige les
salons de la bourgeoisie et se jette à l'assaut des *borgate*, faubourgs
et terrains vagues. C'est à ce titre qu'il actualise la figure du
flâneur baudelairien, telle qu'esquissée dans les analyses de Benjamin,
mais en lui faisant subir une inflexion profonde.

Ce n'est pas seulement une différence de « caractère » qui détermine le
contraste entre ces deux types de flâneurs, mais un certain rapport au
monde matériel et social.

Pour le comprendre, il nous faut envisager d'abord leur espace de
déploiement respectif : là où le Paris des années 1850 est structuré
matériellement et socialement par les nouveaux rythmes de la production
et de la consommation marchande, la périphérie romaine des années 1950
demeure en retrait de la nouvelle civilisation consumériste de
l'après-guerre.

Si le contraste entre leurs deux « décors » est saisissant, celui qui
engage leurs dispositions subjectives, et donc leurs modalités d'ancrage
au sein de ces espaces, ne l'est pas moins.

Selon Benjamin, le flâneur baudelairien est condamné à vivre au seuil de
la ville et du monde, dans l'anonymat et l'ivresse des foules :

> Le flâneur se tient encore au seuil, aussi bien de la grande ville que
> de la classe bourgeoise. Ni l'une ni l'autre ne l'a encore terrassé. Il
> n'est chez lui ni dans l'une, ni dans l'autre. Il va chercher asile dans
> la foule[^62].

Les foules parisiennes, aimantées par le spectacle de la marchandise,
apparaissent alors comme un phénomène corollaire de l'extension et de la
concentration des espaces dédiés à la consommation marchande. Et ce sont
précisément au sein de ces espaces que s'affirme le caractère
névrosé et asocial de la figure du flâneur, qui se fond
dans la foule afin de s'oublier dans un spectacle de corps et de
marchandises :

> La foule n'est pas seulement le plus récent asile du réprouvé ; c'est
> aussi la plus récente drogue de ceux qui sont délaissés. Le flâneur est
> un homme délaissé dans la foule. Il partage ainsi la situation de la
> marchandise. Il n'a pas conscience de cette situation particulière, mais
> elle n'en exerce pas moins son influence sur lui. Elle le plonge dans la
> félicité comme un stupéfiant qui peut le dédommager de bien des
> humiliations. L'ivresse à laquelle le flâneur s'abandonne, c'est celle
> de la marchandise que vient battre le flot des clients[^63].

Nulle « foule » dans la périphérie romaine des années 1950, mais de
petits groupes d'ouvriers, de gamins, de voyous, de prostitués. Étalages
épars de babioles ou de légumes, attroupements fugaces, errances.
Rien de comparable, donc, avec le Paris du Second Empire.

Pour autant, les corps, à Pigneto ou Rebibbia, s'épient et se
marchandent aussi. En s'adonnant à un tel commerce, Pasolini alimentait
sans doute ce sentiment d'offense et d'humiliation qu'il associait à son
homosexualité. Tout comme Genet dans les ruelles de Tanger, il éprouvait
une véritable jouissance à « se faire avoir » --- payant ainsi le tribut
d'un monde qui n'était pas le sien. 

C'est pourtant cette confrontation physique, concrète, charnelle qui lui
permettait d'éviter de sombrer dans l'adoration et le folklore, cette
idéalisation du sous-prolétariat qu'on ne cessa de lui reprocher.

Assurément, dès les années 60, le succès de ses films, sa réputation
grandissante, l'audience et le retentissement de ses articles polémiques
lui ont permis d'accéder à une catégorie sociale supérieure : il était
doté du capital économique et symbolique lui permettant de fréquenter
les meilleures *trattorie* du centre de Rome, la caste des intellectuels
romains, les milieux universitaires. Il eut simplement le mérite, sans
renoncer à une production littéraire et artistique exigeante, d'occuper
une position subversive à l'intérieur de l'espace de production et de
diffusion de l'industrie bourgeoise de l'art.

Ce qui l'a conduit à occuper une double position intenable : au sein du
sous-prolétariat, d'abord, il fut l'artiste, le bourgeois, le camarade
peut-être, mais jamais le frère ; et, au sein de l'industrie culturelle, ensuite, il demeura le renégat, l'idéaliste, le provocateur à
l'engagement purement rhétorique, un terroriste des arts et des lettres.

Mais ce qui semble être un redoublement de ses contradictions nous
apparaît pourtant comme une seule et même stratégie d'ordre à la fois
esthétique et éthique. Son sentiment de persécution, qu'il ait été de
nature névrotique ou qu'il se soit fondé sur de véritables offenses, fut
le revers inévitable de cette position contradictoire qu'il occupa en
tant qu'artiste, intellectuel, bourgeois *et* en tant que marxiste.

Au risque de paraître paradoxal, inconséquent, nihiliste, il assuma
cette position toute sa vie, jusqu'au bout. Ainsi son œuvre, même les
pièces détachées, les bribes, les fragments, toute son œuvre porte la
même et indéfectible obstination, celle de se loger dans les interstices
d'un monde en décomposition.

Mais quelle autre tâche peut-elle être celle de l'artiste aujourd'hui ?
L'artiste bourgeois, ou petit-bourgeois, c'est-à-dire la grande majorité
des artistes, n'a pas à feindre une existence de classe qui n'est pas la
sienne. Il ne suffit pas de décréter la « mort de l'art », ni de se
donner à voir comme chantre d'un art prolétarien --- dont nous ne nions
pas qu'il puisse exister mais dont nous affirmons qu'il est le plus
souvent une posture de bourgeois endetté à l'égard d'une classe qui
n'est pas ou plus la sienne.

Par ailleurs, à une époque où les masses prolétaires et sous-prolétaires
ont succombé à l'attrait du spectacle de la marchandise, et bien que ce
spectacle leur renvoie une image de leur exclusion, il
n'est plus possible d'envisager, à l'exception peut-être de quelques
quartiers de Naples ou de Palerme --- exceptions romantiques pour le
voyageur cultivé, issu des franges aristocratiques de la
petite-bourgeoisie ---, une réactualisation de la figure du flâneur.

À moins d'inventer d'autres *correspondances* --- ici, par exemple,
entre la figure baudelairienne du flâneur et celle du Black bloc :

> La masse apparaît ici comme l'asile qui protège l'asocial de ses
> poursuivants. C'est cet aspect qui, de tous les aspects menaçants de la
> grande ville, est devenu le plus tôt manifeste[^64].

& :

> ... tout mouvement de transformation sociale un peu significatif semble
> exiger l'anonymat, la confrontation violente avec les autorités et une
> mise en scène de soi en pleine position de pouvoir sur la
> ville[^65].

Mais la figure du corsaire, telle que nous entendons en esquisser
quelques traits caractéristiques, et ce à partir d'une image de
l'errance pasolinienne, semble échapper à cette correspondance : si le
flâneur est doté d'un véritable désir de foule, qu'il puise en elle sa
force, sa sécurité, l'assurance de sa révolte, le corsaire, quant à lui,
demeure hostile aux masses, terrains de multiples réifications et de
soulèvements spontanés.

S'il se caractérise par un degré d'insolence et de provocation bien plus
grand, c'est qu'il est encore plus seul : il ne peut plus se fondre dans
une foule qui le rejette de façon systématique et aveugle. Ce qui
accroît naturellement le sentiment de son impuissance, mais également le
caractère spectaculaire de sa révolte.

Le corsaire ne peut consentir à l'appauvrissement du langage, à sa dilution dans
un slogan. Il assume de ne se situer nulle part : ni dans les antres
académiques du pouvoir, ni dans la révolte estudiantine, ni au sein des
grosses machineries bureaucratiques. Il ne maquille sa solitude d'aucune
brillantine ni identification à un groupe, à un mouvement. Et ce, bien
qu'il sache reconnaître dans une révolte ce qui fait naître une
véritable subjectivation politique et ce qui n'est que répétition ou
farce.

Son langage, expression pulsionnelle de son sens historique, est
une offense à toute forme d'efficacité communicationnelle : il sait être
concis devant l'emphase républicaine et bavard devant un tract de la
jeunesse gauchiste. C'est qu'il a l'impudence de croire que toute
résistance au néocapitalisme, instinctive ou intellectuelle, mérite une
parole, et non un discours, et non un slogan, afin de se manifester.

Prêt à se jeter à l'assaut de survivances si celles-ci venaient à
se présenter à lui, le corsaire ne se fait cependant que très peu d'illusions.
L'idée à laquelle il s'accroche, il en connaît la détermination
historique, la faillite : c'est le produit d'une époque sans espérance
révolutionnaire. Et il s'accroche à elle, désespérément.

Il navigue ainsi entre les grandes eaux tumultueuses du
spectacle de la marchandise, le théâtre des progressismes de bas étage,
l'avènement de l'ère télévisuelle, tout en refusant de se fondre dans la
masse, de poser un pied à terre et d'y camper. 

Par ailleurs, et bien qu'il en ait purgé les ornements discursifs ainsi
que les ressorts téléologiques, son rapport au monde est toujours
médiatisé par une conscience théorique (marxiste) des rapports sociaux ;
ce qui accroît naturellement la conscience de sa propre réification et
donc également son sens de la réalité.

Le flâneur baudelairien et le corsaire pasolinien, du fait de leur
ancrage en des espaces urbains différenciés, entretiennent également un
rapport antagonique à leur environnement « naturel[^66] ».

Ainsi, pour le gamin de la périphérie de Bologne, l'adolescent de
Casarsa puis le jeune adulte errant dans la banlieue romaine, les
étoiles demeurent une réalité empirique bien que menacée :

> Il n'y avait aucun éclairage public, alentour, sur des kilomètres, et la
> lune ne s'était pas encore levée. Restaient les étoiles. Les superbes
> étoiles de la jeunesse, que d'ailleurs on ne regarde presque plus, alors
> qu'elles continuent à resplendir, de leur lumière granuleuse et agitée,
> jusque dans leur calme suprême. Leur tremblement hésitant était un
> langage[^67].

Tandis que la jeunesse de Pasolini est très concrètement marquée par ce
langage, pour Baudelaire, enfant d'un Paris en plein essor industriel et
marchand, les étoiles baignent déjà dans un flot d'abstraction[^68] ;
la perte de cosmos se manifestant ici-bas --- en miroir --- par la
désagrégation de ce langage des fleurs et des choses muettes auquel le
poète s'accroche à cor et à cri :

> Pourtant, sous la tutelle invisible d'un Ange,
>
> L'Enfant déshérité s'enivre de soleil,
>
> Et dans tout ce qu'il boit et dans tout ce qu'il mange
>
> Retrouve l'ambroisie et le nectar vermeil.
>
>  
>
> Il joue avec le vent, cause avec le nuage,
>
> Et s'enivre en chantant du chemin de la croix ;
>
> Et l'Esprit qui le suit dans son pèlerinage
>
> Pleure de le voir gai comme un oiseau des bois.
>
>  
>
> Tous ceux qu'il veut aimer l'observent avec crainte,
>
> Ou bien, s'enhardissant de sa tranquillité,
>
> Cherchent à qui saura lui tirer une plainte,
>
> Et font sur lui l'essai de leur férocité[^69].

Si cette perte accable l'ensemble de la société, un seul être résiste.
Mais l'enfant, dépositaire des forces de la Nature et contrepoint au
règne de l'Esprit, voit sa puissance reniée. Au sein de cette société
ayant relégué la Nature à un ensemble inerte, et fondu le pouvoir de
l'Esprit dans l'intérêt capitaliste, tout le condamne --- à commencer par
la mère elle-même qui le constitue comme « poids trop
onéreux[^70] ».

Sous les traits de l'enfant s'esquissent naturellement ceux du poète. En
liant son destin pulsionnel à celui des choses de la nature
--- naissance, croissance, floraison ---, il en affirme le caractère
intempestif, obstiné. Mais la mesquinerie du monde social et les
ravages de la révolution industrielle conduisent la foule à rejeter cet
amoureux transi dont l'amour (« antique Palmyre[^71] ») n'est plus que
refrain malaisant et plaintif. 

Tel albatros, sa puissance mue en infirmité. Le poète endure alors le
déchirement du monde comme son déchirement propre --- le poème naît de
ce déchirement : vaine tentative de réparation, vengeance par dérision,
reconquête sans volonté de possession, etc. 

Chantre ridicule de l'éternelle jeunesse, le poète cherche ainsi à
venger son amour. Mais il ne peut le faire que masqué. Évitant les
métaphores rouillées, ces représentations d'une antériorité lointaine,
rurale ou océanique, les étoiles occuperont désormais les marges
occultes de sa poésie.

Que reste-t-il alors de ce cosmos originaire ? Une image lointaine, si
lointaine que le poète demeure impuissant et ne peut que chanter un
amour irrémédiablement perdu :

> Par delà les confins des sphères étoilées,
>
> Mon esprit, tu te meus avec agilité[^72]

Cette pure extériorité du cosmos implique d'inventer un nouveau type de
rapport à l'histoire, non plus par totalité ou analogie, mais par
*correspondance*, soit la mise en rapport d'éléments hétérogènes :
époques, astres, fleuves, usines, insurrections paysannes, etc.

La mobilisation d'une époque révolue permet d'accuser notre modernité ;
elle permet conjointement de se lier *par* le poème à notre « nature »
perdue :

> J'aime le souvenir de ces époques nues,
>
> Dont Phœbus se plaisait à dorer les statues.
>
> Alors l'homme et la femme en leur agilité
>
> Jouissaient sans mensonge et sans anxiété,
>
> Et, le ciel amoureux leur caressant l'échine,
>
> Exerçaient la santé de leur noble machine[^73].

Le poète-libertin, devenu membre d'une race maladive, continue donc de
chanter sa jeunesse. S'il est condamné à le faire en mobilisant de vieux
ressorts mythologiques, c'est que le rapport au lointain s'est désagrégé
dans la morne féerie de la marchandise ; la forme se déployant tel
« ardent sanglot » avant de s'écraser au pied de l'Histoire --- formation
et déformation d'une image de notre salut, éternel recommencement d'une
beauté dont le destin ne peut être que précaire.

Toute la poésie de Baudelaire danse ainsi au bord d'un précipice ; la
beauté ne s'y laisse saisir que par à-coups, éclats, brisures. Ce pour
quoi Benjamin, dans son grand livre sur Paris, fera du poète français
une figure tutélaire de la modernité poétique ; le stratagème
baudelairien reposant sur une conscience lucide et désespérée de son
époque, sa clôture immanentiste au sein de laquelle le poème se débat,
forclos dans le monde de la marchandise et des nouvelles politiques
urbaines.

*A contrario*, la poésie de Pasolini eut le mérite de constituer la
réalité elle-même comme un absolu : le cosmos y est réintégré à la
conscience comme totalité à la fois lointaine et intérieure. C'est ainsi
que les *Poèmes à Casarsa*, déjà, malgré leur langueur et leur goût de
l'abstraction, ont su reconquérir le lointain dans la langue elle-même,
en faisant notamment usage du dialecte frioulan, langue de la superbe
immobilité paysanne ; l'esprit tragique en est ainsi atténué, et les
étoiles sont sauves.

Le statut du mythe est fondamentalement différent : si le souvenir
de Phœbus convoque un passé aux lointains échos, les journaliers en
lutte donnent corps à une image concrète de l'unité du monde --- unité
reposant entièrement sur l'autosuffisance du monde paysan où mythe et
réalité se confondent.

Ce qui vaut pour la campagne frioulane vaut également pour la périphérie
romaine. Ainsi, à l'aune des années 50, au début des années 60, les
faubourgs de Rome conservent non seulement une certaine intimité avec la
réalité cosmique, mais également une relative autonomie culturelle :

> ... une vie à soi, et, au fond, libre, même dans les banlieues les plus
> pauvres ou carrément misérables[^74].

Le rapport empirique de Pasolini à cette réalité sous-prolétaire est
une « bénédiction » :

> Comme je bénis mon amour traditionnel et non orthodoxe pour le peuple
> (qui n'a jamais été un mythe ouvriériste), à travers lequel j'ai vécu et
> je vis hors de l'enfer auquel j'étais destiné de par ma naissance, mon
> patrimoine et ma culture[^75] !

Loin de la belle figure de l'ouvrier, dont les années 50 alimentent le
mythe jusqu'à l'indigestion, cet amour instinctif du peuple, plutôt que
de muer en idéal populiste, lui intime de s'affranchir des limites de sa
subjectivité bourgeoise et de faire un pas vers le monde :

> comme le veut le grossier journal
>
> de la cellule, le dernier
>
> imprimé qu'on agite : os
>
>  
>
> de la vie de tous les jours,
>
> pure, de n'être que trop
>
> proche, absolue, de n'être que
>
>  
>
> trop misérablement humaine[^76].

Si le flâneur colle à son objet (esthétique de la défaite), en détourne
potentiellement la fonction (les surréalistes ne laissant rien indemne
sur leur passage), le corsaire trimballe sa subjectivité (à la fois
singulière et radicale) sur tous les fronts de la réalité humaine. Alors
que Baudelaire succombe à cette modernité au sein de laquelle il se
débat, Pasolini guette méthodiquement la brèche de lumière qui illumine
encore ses marges. Renonçant à sa « belle âme », il s'efforce ainsi de
confronter la pureté de son cœur au chaos de la vie sociale :

> me défendre, attaquer, avoir
>
> le monde sous les yeux, et non
>
> seulement dans mon cœur[^77]...

Autrement dit : si la poétique baudelairienne renonce à habiter le corps
de la réalité (celui-ci s'est désagrégé) pour se réfugier dans les
éclats féeriques de la marchandise, la poétique pasolinienne s'obstine à
marchander sa part de vérité aux confins de la centralité capitaliste,
en quête d'un démenti farouche à l'uniformisation du monde :

> Il y a des visages, avec un sourire d'adolescent
>
> qui démontrent qu'aucune société ne contient tout à fait le
> monde[^78].

Ce sourire, longtemps, il fut celui des sous-prolétaires romains, celui
d'Accattone, de Ninetto et de tant d'autres. Mais ce monde
des *borgate* est appelé à disparaître ; la seconde révolution bourgeoise
réussissant là où la Révolution industrielle, la Monarchie et le
Fascisme ont échoué. La télévision, notamment, prend place au cœur de
tous les foyers ; la culture petite-bourgeoise se trouvant ainsi le
moyen d'une pénétration massive qui contribuera à porter atteinte une
seconde fois à ce « langage hésitant », celui des astres et des humains,
en leur union fragile et nécessaire.

Pour reconquérir une cosmologie vivante --- c'est-à-dire : un rapport
intériorisé au cosmos ---, Pasolini délaisse la banlieue romaine et
guette dans le lointain terrestre une manifestation de sainte insoumission
à la marche de l'Histoire : Éthiopie, Palestine, Inde, etc. Laissant
derrière lui ses anciennes amours, le poète fait donc une nouvelle fois
un pas vers le monde, vers des mondes qui échappent à la totalité
pétrifiée du marxisme officiel et renouvellent sans cesse le caractère
irréfragable de ses marges.

Si les premiers ouvrages portent bien la marque de l'effacement
historique de leur objet (la réalité paysanne menacée, l'univers des
*borgate* romaines), Pasolini refuse de se complaire dans une vague idée
de notre nature perdue. Et c'est donc encore le motif originaire de la
modernité poétique qui en constitue la source intarissable --- *l'amour
est* toujours *à réinventer* :

> Ce n'est qu'aimer, et que connaître,
>
> qui compte, non d'avoir aimé,
>
> ni d'avoir connu. C'est angoisse
>
>  
>
> que vivre d'un amour
>
> révolu. L'âme ne grandit plus[^79].

À rebours de l'Histoire, donc, en quête d'une forme neuve, *enfer ou
ciel qu'importe*, usant du mythe pour défaire la narration des
vainqueurs, en révéler les angles morts, les beautés rescapées,
vivantes. Sans subsumer pour autant un contenu social inédit sous une
forme fixe : c'est la forme même, la forme seulement qui intéresse le
poète. Autrement dit : tout ce qui donne forme au monde indépendamment
de la forme-universelle-marchande des sociétés capitalistes avancées.
Une forme qui peut puiser dans le mythe ses contours et ses motifs, mais
non ses déterminations : l'*Orestie* d'Eschyle mute et devient *Carnet
de notes pour une Orestie africaine* (1970), au point d'opposer un
contrepoint littéral à la forme classique de son énonciation.

</div>

<div class="text" data-number="4">

## 5. Sauvegarde

*Cimitero acattolico di Roma*, un soir d'automne. Pasolini, en hégélien
neurasthénique[^80], s'agenouille devant la grande histoire du
mouvement ouvrier, et balbutie :

> pourrai-je désormais œuvrer de passion pure,
>
> puisque je sais que notre histoire est finie[^81] ?

Sous la forme qu'elle a pris aux XIX^e^ et XX^e^ siècles, avec quantité de
défaites et trahisons subies, avec ses nombreux coups d'éclat également,
cette histoire-ci, prétendument homogène, dont l'inéluctable
accomplissement des fins fut brandi par les tenants d'un étapisme naïf
ou machiavélique, nul doute qu'elle a achevé sa course dans les méandres
d'un monde globalisé, entièrement structuré par la marchandise et soumis
à l'application d'une rationalité exclusivement économique à toutes les
sphères du vivant.

Nous sommes entrés, dit Pasolini, dans l'ère du néofascisme de la
société de consommation. Et ce nouveau fascisme, celui-ci, suppose une
adhésion plus viscérale, plus intime, qu'un simple bras tendu. Chemin
faisant, il n'y a plus d'Histoire. 

Cette lucidité mélancolique marque-t-elle l'achèvement de toute
espérance révolutionnaire ? D'un certain type d'espérance
révolutionnaire. Quant à l'amour qui la soutient, il demeure
inébranlable. Et nous convie à penser autrement la mise en récit de nos
luttes.

À partir d'un détour par la pensée d'Ernst Bloch (sous l'ombre angélique de
Benjamin), et tout en nous appuyant sur cette lecture pessimiste de la
modernité par Pasolini, essayons donc de faire brièvement droit à un
autre usage de l'histoire.

Hérité de Hegel, mais passé à l'essoreuse du matérialisme historique,
puis de la théorie critique, il s'agit d'un usage comme *sauvegarde* des
« excédents culturels » non assimilables par l'Histoire des vainqueurs :

> ... ce qui éclaire et anime l'utopie concrète, c'est aussi une
> sauvegarde, le sauvetage de tout un *excédent* culturel qui continue de
> nous concerner --- en particulier à travers les allégories de l'art et
> les symboles religieux --- et que la fin des idéologies ne liquide
> pas[^82].

Cette *sauvegarde* ne peut être considérée comme une simple préservation
de la mémoire des vaincus, elle doit ouvrir une brèche, un nouvel espace
de subjectivation collective. En tant que réécriture conflictuelle des
continuités à l'œuvre au sein de nos histoires de luttes, de fêtes et de
résistances mutilées, elle annonce ainsi l'avènement d'un nouveau type
de totalité, dynamique, hétérogène, fragmentée, ouverte sur des jeux
d'alliances insoupçonnés.

Cette conception alternative de la totalité s'appuie surtout sur la
conscience d'un point limite où germe l'incendie : l'*autre* est
irréductible à la fin que *je* lui assigne.

Par ailleurs, cette *sauvegarde* est intrinsèquement liée à la possibilité
d'une recréation et donc d'une recomposition d'un devenir
révolutionnaire --- analogue à l'espace analytique qui n'est pas le
recensement des faits passés (conscients ou refoulés), ou leur
traduction dans la langue du concept, ce qui supposerait dans les deux
cas une soumission au *déjà écrit* de l'Histoire, au signifiant-maître
inaugural, au Nom-du-Père, et donc un enlisement dans la vieille
narration spectrale et son théâtre des représentations éthérées, mais
une véritable production subjective, un acte de recréation du sujet qui
invente son histoire en articulant autrement faits objectifs et
dynamiques subjectives.

Cette *sauvegarde* n'est donc ni statique ni confinée à la subjectivité du
philosophe, mais s'appuie sur un travail d'archiviste et s'actualise
conjointement dans le fait concret de nos luttes.

C'est tout le sens du concept d'*utopie concrète* dans l'œuvre de Bloch,
qui oppose à la subjectivité du vieux romantisme, enlisée dans la
contemplation de l'écart qui la sépare de l'existant, une inscription
dans la matérialité des rapports de classe et de domination.

Car l'utopie blochienne n'est pas une production spontanée *ex nihilo* :
non seulement elle s'ancre dans l'existant, mais elle puise ses motifs
dans le langage des formes de vie passées --- rapport fécond de l'utopie
à la *sauvegarde* qui la soutient, de la *sauvegarde* à l'utopie qui la
convoque.

Cette articulation blochienne de la *sauvegarde* et de l'utopie s'oppose
donc en tout point :

1) à l'idée d'un « Homme nouveau » ;

2) au passéisme, au culte de l'ancien et au désir de revivre un passé
qui n'est plus ;

3) à l'utopie abstraite, qui se projette dans l'avenir en faisant fi du
déjà existant et des traces d'un passé meurtri ou d'un possible non
advenu ;

4) à l'hypothèse libérale d'une « fin de l'Histoire ».

C'est aussi et peut-être surtout l'image d'une joie postcapitaliste qui
la soutient et l'éclaire, une façon de *faire commun* qui tranche avec
la stéréotypie des modes de vie hérités de l'histoire bourgeoise.

Pasolini le martèle à partir des années 70 : la principale erreur de sa
génération fut de confondre l'histoire en soi avec l'histoire
bourgeoise[^83]. Prenant acte de cette confusion désastreuse,
notre tâche, celle de notre génération, plutôt que de s'enliser dans la
mélancolie prérévolutionnaire, ne serait-elle pas de donner corps à
cette histoire possible et non advenue ?

Il ne s'agit pas d'une idée solitaire et saugrenue, mais d'une
conviction puisant sa force dans l'aiguisement du regard qu'induit
précisément cette œuvre de *sauvegarde* : au nom de la seule espérance en
laquelle nous reconnaissons nos sœurs, nos frères, vivants et morts,
faire valoir l'histoire d'un *commun* qui unirait les générations et les
formes de vie spécifiques sans les abolir dans les méandres d'un
universalisme homogène et donc vide.

Si la culture bourgeoise est dotée d'institutions centralisées lui
permettant d'opérer une conservation de ses objectivations politiques,
religieuses, artistiques, le prolétariat et le sous-prolétariat sont
exclus de ce processus de conservation et semblent inéluctablement
avancer vers une nouvelle forme de préhistoire.
Pourtant, et malgré l'urgence qui motive notre geste, cette *sauvegarde*
ne pourra pas se faire sous la forme d'une réécriture bourgeoise de
l'Histoire : il ne peut s'agir de remplacer un contenu déterminé
(l'Histoire des vainqueurs) par un autre contenu déterminé (le roman
universel de nos luttes et de nos défaites), et ce tout en refusant de
questionner la forme même de cette conservation institutionnelle de
l'Histoire.

Il nous faut inventer une forme neuve qui ne soit pas
conservation statique mais *sauvegarde* active. C'est uniquement ainsi que
l'histoire en tant que « passé toujours présent[^84] », ainsi que le
formule Jean Hyppolite, continuera de tenir dans nos mains.

Si la tâche est ardue, nous ne partons pas de rien :

1) il existe toujours des continuités énigmatiques qui lient les formes
de vie d'hier et celles d'aujourd'hui (et bien que ces dernières
puissent échapper à l'œil fatigué de l'ethnologue, du documentariste ou
du poète) ;

2) un travail de *sauvegarde* a déjà été opéré et le sera encore
(*Accattone*, *Mamma Roma* en attestent, à nous de reprendre un fil
rompu) ;

3) malgré toute la gravité des constats que nous faisons, et malgré la
nature génocidaire du néocapitalisme, le monde échappera toujours plus
ou moins à sa clôture (la totalité néocapitaliste demeure ainsi
traversée de contradictions, de brèches).

L'histoire bourgeoise vise à annihiler la finitude de l'espèce humaine,
à refouler le négatif : mort, souffrance, limites, les siennes et celles
de ses milieux de vie. Mais toute positivité qui refoule le négatif tend
à édifier une représentation irréelle du monde et de l'histoire ; cette
représentation contribuant à nier le caractère miraculeux de l'existant
et donc conjointement le mystère de la mort :

> Un vieux sage se lamentait, disant qu'il était plus facile de sauver
> l'homme que de le nourrir. Le socialisme à venir, lorsque tous seront
> assis à la table, lorsque tous pourront s'y asseoir, devra affronter
> plus que jamais, en un combat particulièrement difficile et paradoxal,
> le renversement bien connu de ce paradoxe : il est plus facile de
> nourrir l'homme que de le sauver. Et cela veut dire qu'il lui faudra
> parvenir à la lumière, tant en ce qui le concerne qu'en ce qui *nous*
> concerne, en ce qui concerne la Mort et le secret rouge par excellence :
> qu'il y ait un monde[^85].

Ce « socialisme à venir » ne pourra plus faire l'économie d'une
métaphysique. Non un dépassement naïf du matérialisme dialectique (*le
rêve contre la matière*), mais une réintégration du rêve au sein du
matérialisme dialectique :

> La philosophie n'est plus philosophie *si elle n'est pas
> dialectico-matérialiste*, mais il faut aussi affirmer, maintenant et à
> jamais, que *le matérialisme dialectique n'en est pas un s'il n'est pas
> philosophique*, c'est-à-dire s'il n'avance vers de grands horizons
> ouverts[^86].

Cette réintégration du rêve en tant que « virtualité en train de
devenir[^87] » au sein de notre lecture matérialiste des rapports
socio-historiques, en impliquant la conscience de notre finitude, permet
d'offrir une dignité nouvelle à la mort : « Vie est mort, et mort est
aussi une vie[^88]. »

Autrement dit : la reconnaissance du négatif permet à celui-ci de se
transmuer en positif ; la conscience de notre finitude apparaissant
comme condition d'une ouverture infinie sur l'invisible.

Ce refus hölderlinien de considérer la mort comme le pur négatif de la
vie puise notamment sa source chez le poète-philosophe d'Agrigente,
selon lequel il n'existe nul commencement ni fin : « Je te dirai encore :
il n'est de naissance pour aucun, / Tous mortels, point de fin à la
mort funeste, / Mélange seulement, échange de mélanges. / Naissance est
son nom pour les hommes[^89]. »

Ainsi l'acceptation de notre finitude ouvre notre conscience sur une
totalité cosmique qui nous comprend et nous excède. Une telle approche
cosmologique nous inspire conjointement un excentrement de notre
subjectivation politique (nous ne sommes pas « le » centre du cosmos) et
une revitalisation de la mémoire des vaincus (nous partageons cette
existence cosmique avec des êtres qui ne sont plus ; d'un point de vue
cosmique, nous faisons donc corps avec l'invisible). En d'autres termes,
nous nous déployons au sein d'une totalité qui nous échappe
nécessairement, et cette double conscience, à la fois de nos limites (se
sentir une petite partie d'un tout) et de la totalité au sein de
laquelle nous nous insérons, constitue le fondement d'une
(cosmo)politique radicalement autre.

Toute utopie concrète ne peut se déployer qu'à partir d'une telle
cosmologie, qui s'oppose donc en tout point à l'anthropocentrisme que
charrie l'utopie bourgeoise, celle-ci reposant sur une foi aveugle dans
le développement infini des forces productives et des moyens
technologiques au service de la production, de la circulation et de la
consommation des marchandises ; l'exploitation sans limites des
ressources humaines et non humaines se justifiant ainsi au nom d'une fin
abstraite.

La subjectivité bourgeoise semble épouser la structure centraliste et
surplombante de l'État-nation moderne, lequel, s'il fonde le plus
souvent sa pérennité sur l'adhésion des masses à leur propre domination,
révèle toujours sa vraie nature, autoritaire et conquérante, puisant
notamment ses racines dans les grands modèles de domination impériale,
dès lors que ces masses refusent leur adaptation à « marche forcée » aux
exigences sans cesse réajustées de l'économie capitaliste.

C'est ainsi que D. H. Lawrence établit, via une ode aux cyprès
étrusques, une funeste correspondance entre le pouvoir de Rome et celui
de l'Amérique :

> Le mal, qu'est-ce que le mal ? 
>
> Il n'y a qu'un mal, c'est nier la vie 
>
> Comme Rome a nié l'Étrurie 
>
> Comme la mécanique Amérique continue de nier Montezuma[^90].

Lawrence n'est pas le seul à convoquer ainsi des civilisations disparues
en guise de contrepoint à la domination impérialiste des États-nations
occidentaux ; Benjamin également, ainsi que le notent Michael Löwy et
Robert Sayre[^91], fait appel à la sagesse de traditions
précapitalistes :

> Il semble que les plus anciens usages des peuples nous adressent une
> manière d'avertissement, pour nous garder du geste rapace quand vient le
> moment d'accepter ce que nous recevons si richement de la nature. Car
> nous ne sommes pas en mesure d'offrir à la terre mère quelque chose qui
> nous soit propre. C'est la raison pour laquelle il convient de montrer
> du respect en prenant, tout en lui restituant une partie du tout que
> nous recevons pour toujours, et à jamais, et ce avant même de prendre
> possession de ce qui nous revient. Ce respect s'exprime à travers la
> vieille coutume de la libation. C'est peut-être même cette très ancienne
> expérience morale qui se maintient, transformée, dans l'interdiction de
> ramasser des épis oubliés, et de récupérer les grappes tombées au sol,
> ceux-ci s'avérant profitables à la terre et aux ancêtres qui donnent la
> bénédiction. Dans la coutume athénienne, ramasser les miettes pendant le
> repas était interdit, car elles appartiennent aux héros. --- Que la
> société, un jour, sous l'effet de la détresse et de l'avidité, dégénère
> au point de ne pouvoir recevoir que par le vol les dons de la nature, au
> point d'arracher les fruits encore verts afin d'en tirer profit sur le
> marché, et d'avoir à vider chaque plat pour seulement se rassasier, et
> son sol s'appauvrira, et la terre donnera de piètres récoltes[^92].

Que ces traditions soient non seulement précapitalistes mais le plus
souvent préromaines n'est pas insignifiant : il s'agit de mettre en
question le développement capitaliste en tant qu'il est également un
processus civilisationnel.

Ce pour quoi l'intervention technique --- qu'invoque une interprétation
volontariste de la pensée de Benjamin --- s'inscrit dans la
perspective stratégique plus globale d'un frein d'urgence imposé aux
locomotives de l'Histoire.

L'anthropologie libertaire, qu'elle prenne ou non appui sur cette
réélaboration benjaminienne de l'acte révolutionnaire, tend le plus
souvent à articuler la reconnaissance de réalités sociales menacées,
voire disparues, à une critique explicite de la civilisation
capitaliste. 

Ce qui est remarquable : cet intérêt pour des civilisations pré ou
para-historiques témoigne d'un scepticisme à l'égard de l'histoire en
soi et de l'institution dont elle se soutient, l'Écriture. 

Ce qui est encore plus remarquable : ce scepticisme n'est pas la
manifestation puérile d'un naturalisme bourgeois (« la Nature contre
la Civilisation »), mais l'expression d'un véritable rapport
dialectique à ces histoires menacées ; ce qui ouvre notamment à de
nouvelles possibilités d'alliance stratégique sous l'égide de la
critique de l'impérialité du Capital.

Pour autant, s'il s'agit bien de soutenir un nouvel imaginaire politique,
radical et transnational, comment des (contre-)civilisations disparues,
antérieures à l'hégémonie romaine ou latino-occidentale, telles les
(contre-)civilisations étrusque, nuragique ou occitane, seraient-elles
en mesure de pénétrer notre époque et de donner corps à notre désir d'un
monde postcapitaliste ?

> Nous n'avons pas à nous demander comment appliquer à nos conditions
> actuelles d'existence l'inspiration d'un temps si lointain. Dans la
> mesure où nous contemplerons la beauté de cette époque avec attention et
> amour, dans cette mesure son inspiration descendra en nous et rendra peu
> à peu impossible une partie au moins des bassesses qui constituent l'air
> que nous respirons[^93].

Cette pure contemplation peut-elle nous satisfaire ? Est-elle
suffisamment armée pour déployer notre utopie ? C'est-à-dire : pour
activer un processus révolutionnaire qui soit en mesure de trouver sa
signification dans notre présent ? Au platonisme révolutionnaire de
Simone Weil, nous préférons ici l'acte benjaminien d'une rédemption,
l'acte révolutionnaire dialectiquement imbriqué à un refus de se
soumettre à la tempête de l'Histoire occidentale et à ses miroitements
progressistes.

Ce refus, afin de se défaire des risques de récupération réactionnaire,
implique d'abattre deux clôtures :

1) une conception de l'Homme en tant qu'être achevé :

> Les hommes ne sont pas achevés ; donc leur passé ne l'est pas non
> plus. Il continue, sous d'autres signes, à travailler avec nous, avec
> l'élan qui vient de ses interrogations, avec l'expérimentation que
> représentent ses réponses ; nous sommes tous dans la même barque[^94].

2) une conception de l'Histoire en tant que structure narrative figée :

> La découverte de l'avenir dans le passé, voilà ce qu'est la philosophie
> de l'histoire et, par conséquent, aussi l'histoire de la philosophie.
> C'est pourquoi l'adieu à Hegel n'est pas plus un adieu que la première
> rencontre avec lui[^95]...

Cette nouvelle rencontre avec Hegel doit être considérée comme un
défrichage de l'Histoire des vainqueurs : retour critique et
recomposition d'une autre forme d'histoire, en lieu et place de
l'histoire bourgeoise.

La finalité d'une telle entreprise de défrichage n'est pas
seulement d'honorer les vaincus --- ce qui reviendrait à les empiler dans
le grand roman de nos défaites --- mais de viser, ici et maintenant, un
*salut pour les vivants* (définition blochienne du communisme) :

> Et cela en ce temps-ci, sur cette terre-ci, dans le royaume de notre
> contenu de liberté enfin susceptible d'être mis au jour[^96].

Cette actualisation ne peut s'effectuer en dehors d'un processus
révolutionnaire. Autrement dit : ne plus s'étourdir dans la
commémoration et l'hommage institutionnels --- formes de la conscience
pétrifiée qui ensevelissent la volonté sous des couches d'impuissance ---,
mais viser un acte de libération qui soit inscrit dans la
matérialité des rapports de classe et de domination tout en se faisant
simultanément acte de *sauvegarde* des résidus hérités de l'histoire des
vaincus.

Le cycle de l'histoire bourgeoise voit ainsi sa clôture brisée. Sans
ménagement ni artifice. Le présent quête dans le passé les motifs de son
engagement et vise conjointement la libération de ses potentialités non
advenues :

> Telle est l'immémoriale visée vers le bonheur \[...\] --- visée qui, au
> lieu d'enjoliver et de clore, comme celle de Hegel, le monde existant,
> noue alliance avec le monde non encore existant, avec les propriétés du
> réel qui sont porteuses d'avenir[^97].

Cet avenir nous regarde. Tout comme le passé nous regarde. Dans le sens
où ces « propriétés du réel qui sont porteuses d'avenir » ne se logent
pas seulement dans le pur présent, mais nécessitent afin d'éclore que
notre capacité à transformer la réalité soit à la fois une manière pour
les générations à venir de se sentir visées par nous et une façon pour
les vaincus d'habiter notre monde.

« Poétiquement », pourrait-on ajouter, tant la formule de Hölderlin se
fait jour au terme d'une réélaboration cosmicisante de la conscience
poétique, au sein de laquelle, donc, « vie est mort, et mort est
aussi une vie ».

Cette abolition des frontières entre vie et mort, visible et invisible,
suppose également une abolition des frontières entre humains et non
humains, et donc inévitablement entre Nature et Culture. Ce qui en appelle,
nous le verrons, à un bouleversement des coordonnées du temps (spatialisé)
et de l'espace (plat) et ouvre ainsi l'histoire à une multitude de formes
possibles.

Lorsque Pasolini, dans *Poésie en forme de rose* (1964), se projette
dans un futur qui lierait Nature et Culture[^98], en abolirait
l'antagonisme, deux lectures sont permises : celle d'un attrait
pour l'extase apocalyptique, ou bien celle d'une espérance non
circonscrite au cadre du développement capitaliste. L'usage de la
*sauvegarde*, en lieu et place d'un « grand roman de nos défaites », se
soutient de cette seconde lecture : la main tendue vers une nouvelle
forme d'espérance.

</div>

<div class="text" data-number="5">

## 6. De l'amour

Dans *Drame de la Jalousie* d'Ettore Scola (1970), le personnage de
Marcello Mastroianni, désemparé en amour et subissant les affres de la
jalousie, interroge son chef de section du Parti communiste :
--- « Que dit le marxisme sur l'amour ? » Après avoir soupiré et regardé
ses pieds, son chef lui répond : --- « Il ne dit rien. »

Contrairement au chef de section de Mastroianni, nous pensons que le
marxisme a quelque chose à nous dire de l'amour. Mais quoi ?

> Le marxisme ne me semble pas pouvoir passer pour conception
> du monde. Y est contraire, par toutes sortes de coordonnées frappantes,
> l'énoncé de ce que dit Marx. C'est autre chose, que j'appellerai un
> évangile. C'est l'annonce que l'histoire instaure une autre dimension du
> discours, et ouvre la possibilité de subvertir complètement la fonction
> du discours comme tel, et, à proprement parler, du discours
> philosophique, en tant que sur lui repose une conception du
> monde[^99].

Nous rejoignons Lacan sur l'idée que le marxisme ne doit pas être réduit
à une conception du monde, qu'il subvertit les coordonnées du « discours
comme tel » ; quant à sa portée potentiellement évangélique, nous
essaierons d'y voir plus clair à partir de la critique pasolinienne de
l'amour bourgeois.

Mais essayons d'abord d'envisager ce que pourrait être une théorie
marxiste de l'amour, qui soit à la fois critique et prospective, sans
être pour autant un nouveau discours du pouvoir, ou une nouvelle
conception totalitaire du monde. 

Peu de théoriciens marxistes se sont appliqués à penser l'amour. Adorno
fait donc ici figure d'exception. Ce dernier s'en prend notamment à la
doxa bourgeoise qui stipule que l'amour serait le seul espace social où
les sujets n'auraient pas à s'en remettre à la volonté, mais seulement à
la loi de leur cœur :

> Si l'amour doit représenter dans la société la perspective d'une
> société meilleure, il ne peut le faire comme une simple enclave
> pacifique, mais par une opposition consciente. Celle-ci exige
> précisément ce moment de volonté que lui interdisent les bourgeois pour
> qui l'amour ne sera jamais suffisamment naturel. Aimer, c'est être
> capable de ne pas laisser dépérir l'immédiateté sous la pression
> omniprésente de la médiation, de l'économie et, dans cette fidélité,
> l'amour se médiatise lui-même, il devient contre-pression opiniâtre.
> Celui qui a la force de s'attacher fermement à l'amour aime
> vraiment[^100].

L'amour, forme créative de désintéressement, ne peut être considéré
comme désengagement pacifique au milieu d'un champ de bataille.
Autrement dit : il ne peut y avoir de pureté du sentiment dans un monde
gangréné par le calcul égoïste et la réification tout terrain --- s'en
remettre aux « intermittences du cœur », c'est donc le plus souvent se
soumettre à la loi du marché :

> L'amour qui, sous l'apparence de la spontanéité irréfléchie et fière
> de sa prétendue sincérité, s'abandonne entièrement à ce qu'il prend
> pour la voix du cœur, s'enfuyant dès qu'il ne croit plus entendre
> cette voix, est précisément --- dans sa souveraine indépendance ---
> l'instrument de la société. Passif sans le savoir, il enregistre les
> chiffres qui sortent à la roulette des intérêts. En trahissant l'être
> aimé, il se trahit lui-même[^101].

Seule une volonté farouche, en capacité de « s'attacher fermement »,
serait donc en mesure d'imposer un contre-ordre à un monde dominé par la
bourgeoisie, mais dont l'amour ébauche des coordonnées nouvelles :

> La fidélité ordonnée par la société est le moyen même de n'être pas
> libre, mais seule la fidélité permet à la liberté de se rebeller contre
> les ordres de la société[^102].

Adorno s'inscrit ici en contrepoint de la conception stendhalienne
de l'amour en tant que cristallisation, désir de conquête et de
possession. Nous tenterons ici de démontrer que cette dernière est
emblématique de l'amour bourgeois --- que nous le caractérisions de
« romantique » ou de « romanesque[^103] », cela ne
change pas grand-chose à l'affaire.

Chez Julien Sorel[^104], par exemple, la volonté n'est nullement
soumise à l'amour (au sens d'Adorno) mais exclusivement au désir de
conquête et de possession. Et sur ce jeu des séductions qui alimente la
société bourgeoise du début du XIX^e^ siècle, à force d'abnégation et
d'ardeur romanesque, celui-ci occupera une place forte.

Mathilde est convoitée, c'est ce qui fonde sa valeur. Mais Julien, lui,
ne se compare à personne : s'il épouse la conception bourgeoise de la
valeur, ce qui le conduit à désirer Mathilde (au terme d'un processus de
cristallisation amoureuse), il conserve cependant une part de vérité
romanesque faisant structurellement défaut à ses concurrents bourgeois.

L'aristocratisme ici ne se définit plus par le sang, mais par la
soustraction d'un homme aux valeurs hégémoniques de la société
bourgeoise ; par la capacité d'un individu, en somme, à se distinguer.
Ce pour quoi Julien ne fera pas figure de simple parvenu, mais bien de
héros romanesque. Tout ce qu'il détient, il n'en a pas hérité : il l'a
conquis.

Échappe-t-il pour autant à la société bourgeoise ? Absolument pas : il en
révèle la nature obscure, arrogante et féminicide ; sa
conception romanesque de l'amour étant même au fondement de l'impunité
de la nouvelle bourgeoisie.

Si Julien ne se compare pas à ses concurrents sur le marché de l'amour,
c'est uniquement parce qu'il puise à d'autres sources le motif de son
désir et de son action : la figure de Napoléon.

Dans tous les cas, l'*aimé* est nié dans le mouvement même de sa
désignation. Quant à sa « valeur », elle se voit déterminée par un
ensemble d'attributs (charisme, beauté, capital symbolique, etc.)
prenant l'apparence de faits naturels qui façonnent l'attrait, la
sélection, ainsi que la forme de la rencontre et de la relation.

Ce qui distingue ce nouvel aristocratisme de la conquête et la forme
purement contractuelle de l'amour bourgeois, ce n'est donc plus la
condition sociale mais l'autonomie (relative) des valeurs motivant
l'action de séduire et de posséder ; en rupture, donc, avec l'homme
moyen, le petit-bourgeois, l'ignorant ou le lâche.

Appliquée à d'autres champs sociaux, cette figure du conquérant sera au
fondement de la méritocratie --- qui justifie notre société de classes
tout en invisibilisant ses fondements au nom d'une certaine idée de la
République. Elle rend ainsi compte d'une caste qui se serait faite
seule, ayant conquis le pouvoir sans en avoir hérité au préalable
--- c'est le modèle du patron-travailleur, du *self-made man*, de tous ceux
qui prétendent jouer leur vie en exploitant autrui.

L'intérêt anthropologique de la lecture de Stendhal repose sur une
explicitation littéraire du cadre socio-historique au sein duquel va
prospérer la nouvelle formule de l'amour bourgeois. 
Plus d'un siècle
plus tard, Pasolini s'en fera l'observateur acerbe : la bourgeoisie est
une maladie, martèle-t-il[^105], d'autant plus contagieuse et terrible
qu'elle n'est plus réservée à la grande bourgeoisie, qu'elle trouve son
ferment dans la petite-bourgeoisie avant de se répandre dans toutes les
classes de la société.

Dans *Théorème*[^106] (1968), la famille représentée est issue de la
grande bourgeoisie lombarde, mais elle est « idéologiquement
petite-bourgeoise[^107] », indique-t-il. Cette idéologie
petite-bourgeoise contamine d'abord l'être issu des classes
dominantes de la société, sous tous les aspects de son devenir,
extérieurs comme intérieurs, ses postures discursives et son
comportement, ses goûts et ses états d'âme.

Dès leur plus jeune âge, insiste Pasolini, les bourgeois ont le
visage « marqué par l'absence précoce de tout désintéressement et de
toute pureté[^108] » : leurs rêves, ainsi que leurs amours, sont
dotés d'un caractère fondamentalement fantasmatique, c'est-à-dire
soutenus d'une narration comme scénario de contournement du réel,
charriant une somme de représentations dont la fonction est de nier
l'altérité en lui attribuant les caractères d'un objet, d'un objet qui
sera toujours en acte ou en puissance objet de conquête et de
possession.

Dans *Théorème*, c'est d'ailleurs tout le drame de cette famille
bourgeoise de Lombardie : nul n'est en mesure d'assumer l'abandon auquel
il s'est préalablement livré auprès de ce jeune Christ à la peau de
lait. Il en est ainsi de l'abandon charnel comme de toute beauté : le
bourgeois guette toujours un retour sur investissement, une plus-value
ou un gain compensatoire.

Ce mal bourgeois ne se soigne pas, prévient Pasolini : seule la
domestique, étrangère à l'idée même de possession, sera donc capable
d'un abandon véritable. Doublement : dans l'acte charnel lui-même, mais
également de par sa loyauté à la nouvelle subjectivation qui s'y
active.

Ce pur abandon serait-il au fondement d'un évangile ? Perte de soi dans
la rencontre et bouleversement des coordonnées du monde : il induit bien
--- revenons à Lacan --- une autre dimension de la réalité, sainte et
révolutionnaire --- ce qui, pour Pasolini, revient sans doute au même.

Dans la mesure où les membres de cette famille sont
tous contaminés par le mal bourgeois de la possession, ils s'engageront
chacun dans des pratiques et dans des états de conscience qui porteront
jusqu'au bout la marque de leur névrose de classe : Art, Sexe, Folie, ou
encore le geste tolstoïen d'une Dépossession (le plus efficace !)
conduisant le père à abandonner son usine à ses ouvriers puis à se jeter
nu dans quelque désert métaphorique.

Il ne s'agit là, selon Pasolini, que de multiples dispositions
génériques dont use la bourgeoisie afin de traiter l'incurable cancer
qui la ronge : le bourgeois, « ni sur le plan individuel, ni sur le plan
collectif[^109] », n'est en mesure de trouver le salut, son âme
est incapable de s'abandonner et donc de se perdre dans la rencontre.

*A contrario*, l'âme de la domestique est disposée à accueillir le vide.
Ce qui engage son être, dans toutes les dimensions de son existence,
vers Dieu. Or, s'il fallait chercher quelque part une figuration du Dieu
pasolinien, c'est sans doute à Giovanni Pascoli que nous ferions appel,
ce frère humain, athée comme lui, qui convoque le divin à l'instant même
où le vide menace d'engloutir toute signification humaine :

> Mais lorsque votre tête et vos yeux se plient
>
> vers les profonds abysses où loin, très loin,
>
> on entrevoit dans le fond Véga qui luit... ?
>
>  
>
> Alors moi, toujours, moi l'une ou l'autre main
>
> je jette à un rocher, un arbre, une stèle,
>
> à un fil d'herbe, par horreur du néant !
>
> à un rien, pour ne pas tomber dans le ciel[^110] !

Et plus loin, dans le même poème :

> outre ce que je vois et ce que je pense,
>
> ne trouver nul fond, ne jamais trouver pause,
>
> d'un espace immense à l'autre espace immense ;
>
>  
>
> peut-être, au bas, peu à peu espérer... quoi ?
>
> La halte ! La fin ! Le terme ultime ! Je,
>
> je toi, de nébuleuses en nébuleuses,
>
>  
>
> d'un ciel à un ciel, en vain et toujours, Dieu[^111] !

Ce vertige est le privilège de l'opprimé et du minoritaire, jamais du
bourgeois ; ce dernier ne pouvant que veiller douloureusement sur sa
conscience, chercher en vain une adéquation entre les valeurs énoncées
par le Christ sur la croix et le pragmatisme économique de sa classe.

La subjectivité bourgeoise reconnaît dans l'individu le foyer
autonome d'actions et de pensées qui attesteraient de son conformisme ou
de sa singularité. Cette hypostase est à la fois la condition et l'effet
d'une pratique sociale fondée sur la propriété privée et l'échange ; ce
qui conduit notamment Michel Foucault à souhaiter la
désindividualisation du champ social :

> L'individu est le produit du pouvoir. Ce qu'il faut, c'est
> « désindividualiser » par la multiplication et le déplacement des divers
> agencements. Le groupe ne doit pas être le lien organique qui unit des
> individus hiérarchisés, mais un constant générateur de
> « désindividualisation[^112] ».

Est-il pour autant question d'abattre l'individualité en soi ? Pour
Lukács, celle-ci doit être considérée comme l'un des pôles de la praxis,
et donc également de son envers, l'aliénation[^113]. En la supprimant,
nous prendrions le risque d'invisibiliser la souffrance et de désamorcer
l'un des foyers de l'émancipation.

Selon le marxiste hongrois, il s'agirait plutôt de construire les
conditions d'une désatomisation de la société --- ce qui signifie :
penser à nouveau en termes de lutte des classes et de corps
collectifs ; mais également : désubstantialiser l'individu lui-même en
faisant valoir l'enjeu dialectique d'une praxis conçue à la fois comme
produit de déterminations et action sur ces mêmes déterminations.

Ces deux opérations conjointes présentent une autre articulation du pôle
individuel et du pôle social, celle-ci induisant nécessairement une autre
conception de l'amour, aux antipodes de la formule de l'amour bourgeois.

Pour Pasolini, l'amour authentique conduit à la ruine de toutes les
valeurs sur lesquelles le bourgeois fonde une certaine idée de son
destin --- c'est un vertige qui ne laisse guère indemne le « mal ---
véritable, la Possession[^114]... » --- une expérience de dépossession
dont l'individu apparaît à la fois comme l'agent et la limite.

Dans le cadre de la vie bourgeoise, cette expérience est inhibée :
si la relation à l'*autre* apparaît de prime abord comme un rapport
consenti entre deux individus libres et émancipés, le bourgeois est
incapable de suspendre son jugement, de s'abandonner dans l'espace de la
rencontre et donc de faire l'expérience d'une telle dépossession. À
moins de s'en remettre à une expérience mystique assignée à un lieu clos
de son existence. Et quand bien même il se livrerait à un quelconque
abandon, même involontaire, il s'agirait toujours de le rentabiliser.

À rebours d'une conception du désir comme désir d'objet, Deleuze affirme
que nous ne désirons jamais quelqu'un en particulier, mais toujours un
certain agencement : fenêtre, odeur, lumière, paysage, etc.[^115]. Le
désir ne serait donc aucunement réductible à un jeu de miroirs
narcissique, mais engagerait nos affects en des espaces[^116] où les
griffes de la possession et les boursouflures de l'ego ne sauraient
avoir de prise. Et ce, bien que nous soyons plongés dans un monde où
l'ombre de la marchandise menace d'engloutir à chaque instant la moindre
parcelle de liberté amoureuse.

Rapportée notamment à la proposition d'Adorno, selon laquelle c'est
au prix d'un effort de la volonté, d'un engagement et d'une lutte active
que l'amour se médiatise lui-même, cette conception deleuzienne du désir
nous invite à reconsidérer radicalement la notion d'ascèse : non plus
comme restriction du désir, mais bien comme sa condition.

Autrement dit : l'amour est cet espace des différences qu'il s'agit de
défendre à tout prix contre les assauts de la société.

Ce pour quoi les vrais amoureux excellent dans l'art de la
désertion. Mais leur fuite ne les conduit jamais hors du monde --- ou
alors : hors d'un certain monde seulement --- et fonde la possibilité du
monde lui-même, soit l'affirmation d'un ensemble de pratiques et de
valeurs arraché au critérium de la rentabilité et à l'étalon de la
valeur.

Lorsque Breton affirme que la beauté sera convulsive ou ne sera
pas[^117], il ne dit pas autre chose. Le monde des amoureux n'est pas
une chambre close ; celui-ci invoque les éléments d'un *dehors* qui
excède l'immanentisme de la société marchande ; l'espace et le temps,
sous des formes neuves, faisant effraction au cœur d'un commun
désœuvrement. Ainsi les vieilles significations n'ont plus cours, tandis
que l'abandon des corps --- dans le *Théorème* de Pasolini, puisqu'il est
éprouvé par les membres d'une famille bourgeoise, c'est une expérience
de la chute : la jouissance est traumatique, rien ne sera jamais plus
comme avant[^118], etc. --- couve l'imminence d'un renversement, la
possibilité d'une vérité révolutionnaire... Moins emphatiquement : il
s'agit bien d'une bonne nouvelle (εὐαγγέλιον) --- autrement dit : d'un
évangile avant la lettre.

</div>

<div class="text" data-number="6">

## 7. Hurler dans un désert

À la toute fin de *La Dolce vita* (1960), sur une plage (non loin d'Ostie, déjà,
à Passoscuro), une équipée de bourgeois décadents, à coups de fantaisie
et de champagne, repousse les limites de la nuit ; pensons alors au visage de Mastroianni.

Au cœur de cette représentation exquise d'un certain crépuscule,
ce dernier guette un corps oublié : la figuration d'une jeunesse pure
et sans apparat. Mais le gouffre qui le sépare de ses partenaires de
jeu, dès lors qu'il regarde au loin, est aussi profond que celui qui le
sépare de la jeune femme aperçue.

Le visage de Mastroianni est non seulement marqué par cette double
distance --- vis-à-vis de la jeune femme et vis-à-vis de ses partenaires ---,
mais également par l'impuissance : il est incapable de se lever, de
renoncer à sa fête, de faire un pas vers elle.

Ce visage féminin est l'appel d'un monde déchiré par la division
capitaliste du travail ; l'impossibilité d'un rapport concret,
c'est-à-dire absolu[^119], à la réalité traduisant une absence de
médiatisation de cette dernière par une juste conscience des rapports de
classe (et de genre). Autrement dit : Mastroianni ignore ce qui
détermine l'impossibilité de leur amour.

Si, fidèle à la tradition matérialiste, nous estimons que la conscience
est avant tout déterminée par la position de l'être dans les rapports de
production et d'échange, le travail théorique (d'inspiration marxiste)
doit cependant avoir pour fonction de médiatiser cet état d'impuissance
par une compréhension de ses causes et d'éclairer alors les voies de son
dépassement.

Appliquer ainsi de telles catégories marxistes à l'analyse de la
mélancolie de Mastroianni peut paraître fantaisiste. Si les idées sont
bien des *forces matérielles*[^120] dès lors qu'elles s'emparent des
masses, il ne suffit pas de mettre à l'étude une caste de bourgeois
romains pour inverser le cours de l'Histoire. D'autant que l'enjeu est
ici plus existentiel (individuel et interindividuel) que véritablement
révolutionnaire (c'est-à-dire : nécessairement collectif). Quand bien
même le personnage de Mastroianni se plongerait dans l'étude de la
tradition marxiste, il n'est pas dit que la jeune femme ait réellement
existé indépendamment de sa subjectivité inquiète. Et, si son existence
était avérée, il n'est pas non plus dit que l'accroissement de sa
conscience théorique lui permette de faire réellement un pas vers elle.

L'intellectuel marxiste, sincèrement engagé dans les dynamiques sociales
de son temps, qui tente non seulement d'interpréter le monde mais vise
également sa transformation, contrairement à l'intellectuel acritique,
simple commentateur de l'ordre capitaliste et amoureux des nuances
artificielles de son propre jugement, est toujours aux prises avec une
contradiction terrible : qu'importe finalement son origine de classe,
facteur déterminant parmi d'autres de son être de classe, du simple fait
de sa fonction, en tant qu'intellectuel, dans les rapports de production
et de diffusion du Savoir, il se trouve occuper une position privilégiée
au sein des processus émancipateurs de son temps.

Ajoutons que plus l'intellectuel travaille et se spécialise, plus il
creuse la distance qui le sépare des masses. Technicité et jargon, en
tant que sous-produits de la spécialisation du travail intellectuel sous
régime de production capitaliste, participant de l'illusion théoriciste.
Mais on ne comble pas une faille dans le rapport pratique à la vie par
l'intensification et la systématisation de nos élaborations
conceptuelles --- contre-finalité de l'engagement strictement
intellectuel : à l'impuissance se joint même la culpabilité.

Ainsi que le fit Althusser, on peut railler cette culpabilité de classe,
la disqualifier en brandissant la « lutte des classes dans la
théorie[^121] ». Mais ce n'est pas ainsi que Pasolini concevait son
engagement : l'angoisse, la solitude étaient assumées par le poète comme
conditions essentielles de son action dans le monde ; une action qui,
sans cet état méditatif, sans cette quête d'une lumière faisant
irruption au cœur de l'absence, du vide peut-être, ne serait rien
d'autre qu'une nouvelle morale.

Penser que la lutte des classes pourrait s'autonomiser dans le champ
théorique, et donc que la théorie se suffirait à elle-même, qu'elle
serait en soi un engagement suffisant, cela reviendrait à inscrire la praxis
dans un horizon théoriciste fort éloigné de l'héritage gramscien de
Pasolini. Il y a chez Gramsci, tout comme chez Pasolini à sa suite,
l'idée que l'intellectuel petit-bourgeois doit s'efforcer de ne jamais
considérer ses recherches comme autonomes à l'égard des dynamiques
socio-historiques de son temps, qu'il doit en permanence produire la
critique de sa propre position, de ses inévitables tendances
théoricistes, du Savoir comme pôle de concentration du capital
immatériel, etc.

Cette conscience tragique de l'intellectuel épris de révolution est
également liée à un retard, une disjonction temporelle entre la théorie
et la pratique. Il ne sera jamais possible d'unifier entièrement
production théorique et action révolutionnaire, seulement de les
articuler. Cette diachronicité est notamment cause d'une solitude subie
chez l'intellectuel marxiste, celui-ci ayant le sentiment d'arriver toujours trop
tard là où se modifie le monde :

> Nous sommes peut-être les derniers, et en effet notre rêve est très
> proche de la réalité : \[de la banale cartographie\] de tout lieu
> --- \[Nous sommes « en retard », nous sommes des Alexandrins pourris, nous
> sommes des hommes cultivés\][^122]

Pasolini ne croit plus en la mission de « l'intellectuel de gauche » ;
ce pour quoi il assume une solitude non seulement subie mais volontaire.
Ainsi écrit-il à l'adresse d'un frère poète :

> Ton exclusion de toi-même d'un monde qui du reste t'excluait a été une
> longue ascèse, faite de nuits et de jours, où l'on rit et l'on pleure,
> comme des personnages naïfs d'opéras romantiques sans commencement ni
> fin, avec leur croix et leur délice : une longue ascèse où, au lieu de
> prier, tu as chanté les formes du monde lointain[^123].

La prière n'ayant guère ici pour vocation de se coltiner la matérialité
du monde, mais de la fuir dans une adresse transcendantale. Quant au
chant, bien qu'il se forge dans la demeure clandestine du poète, dans
l'interstice de ses errances, il a pour vocation d'actualiser un certain
devenir du monde. Mais cette actualisation est nécessairement idéale,
projective, hors-sol, et donc le plus souvent inoffensive.

Quand le chant mute et devient cri, sans renoncer pour autant à la
conscience tragique qu'il s'efforce de conjurer, qu'il prend ainsi place
dans la modernité et guette une forme neuve, il prend alors le risque de
se faire entendre ; ce fut notamment le cas de Maïakovski : les
lendemains ne chantent plus, ils crissent ; seule façon pour le poète de
s'arracher aux cristallisations de l'utopie abstraite et d'affirmer son
appartenance à un irréductible présent.

Si Pasolini s'était contenté de chantonner son amour de la
tradition, Fortini l'aurait sans doute admis comme un nouvel avatar du
néoclassicisme. Inoffensif, donc. Ce qu'il ne put admettre, ce fut
précisément l'ampleur et l'efficacité (relative) de son hurlement :

> Une voix qui hurle dans le désert ne saurait parler dans un
> microphone[^124].

Ce microphone apparaît comme l'allégorie d'une industrie culturelle que
Fortini, dans le sillon du Benjamin de la période brechtienne,
investissait exclusivement comme un champ stratégique ; ce dernier
reprochant à Pasolini d'user sans scrupule des moyens importants qui se
trouvaient à sa disposition tout en négligeant son devoir d'« artiste
engagé », de s'accrocher ainsi au prestige que lui conférait sa position
en jouant le jeu d'une « société du spectacle » qu'il prétendait
pourtant dénoncer.

Précisons que Pasolini, dès lors qu'il prend conscience de la formidable
capacité de récupération de cette industrie culturelle, notamment à
travers le traitement médiatique de son œuvre cinématographique, abjure
sa *Trilogie de la vie*[^125]. Dans une société unilatéralement
structurée par la marchandise, justifie-t-il, l'image d'une sexualité
émancipée se voit immédiatement neutralisée par un certain marketing de
l'émancipation. Il répond donc par *Salò*, œuvre proprement intolérable,
réécriture de l'œuvre sadienne et métaphore d'une société néocapitaliste
où l'individu devient son propre bourreau.

Cependant, et bien qu'il ait parfaitement conscience de la capacité de
récupération (et donc de neutralisation) de l'industrie culturelle,
Pasolini ne renoncera jamais à user des moyens dont il dispose, et
notamment ceux de l'industrie cinématographique, s'évertuant toujours à
produire une forme proprement irrécupérable, indissociablement liée à un
contenu politique, mais non inféodée à un discours militant.

Fortini défend l'idée qu'il faut rompre à tout prix avec un certain
passé, cette grandeur dont l'Italie moderne est désormais amputée :

> Pour Leopardi comme pour Fortini, la distance avec le passé glorieux est
> irrécupérable, mais tandis que le premier exprime des regrets et un
> sentiment de perte, le second affirme la nécessité de s'éloigner du
> passé lorsque, pour les intellectuels italiens, églises et arcs
> deviennent les alibis de leur immobilité[^126].

C'est précisément cette immobilité --- passéisme et désengagement --- que
Fortini reprochera systématiquement à Pasolini. Mais sa critique
est-elle bien pertinente ?

Sur la forme : la poétique des *Cendres* assumerait une prosodie
mélancolique, défaitiste, dans le vieux style léopardien, un chant
désespéré qui refuse de soulever les masses ; ne croyant plus au pouvoir
des masses faisant l'histoire.

Sur le fond : il y serait question d'une idéalisation de la paysannerie
et du sous-prolétariat, quand les moteurs de la transformation sociale
seraient désormais les masses ouvrières et le mouvement étudiant ; elle
passerait donc à côté des enjeux réels de l'histoire *en train de se
faire*.

Nous pouvons suivre Fortini jusqu'à un certain point : Pasolini ne
s'intéressait guère aux masses ouvrières ni aux spécificités et aux
contradictions du mouvement étudiant. 

Mais à travers les mutations de sa poétique (écrite, filmique,
polémique), si l'immobilité a conservé son importance, en tant que motif
de fond (à travers son usage du mythe notamment), Pasolini renouvela
constamment ses formes, réadaptant son expression poétique aux nouveaux
outils techniques dont il disposait et l'ajustant obstinément à sa
compréhension des nouveaux rapports de classe à l'échelle mondiale :
déplacement de la figure prolétarienne classique vers le
sous-prolétariat, puis vers le prolétariat et le sous-prolétariat dits
du « tiers-monde ».

En hurlant ainsi son amour du spécifique et de la tradition, Pasolini
commettait bien un acte révolutionnaire, mais selon des coordonnées qui
échappaient au progressisme de Fortini : d'une part, Pasolini n'a cessé
de se rapporter, de façon certes instinctive, à la division
internationale du travail, ce pour quoi il négligea la condition
ouvrière européenne et se tourna plutôt vers les cultures subalternes,
européennes et extra-européennes ; d'autre part, il partageait avec
Benjamin le constat que le capitalisme européen nous fait percevoir
« les monuments de la bourgeoisie comme des ruines avant qu'ils ne
s'écroulent[^127] », que l'intellectuel européen est donc condamné à
penser depuis la dissolution de sa raison et de sa fonction dans la
posthistoire.

Pour mieux saisir le différend qui oppose Fortini et Pasolini, rien
n'est plus révélateur qu'une comparaison de leur traitement des ruines.

Ici, Fortini, dans *Feuille de route* (1946) :

> Maintenant que je m'aperçois que je t'aime
>
> Italie, que je te salue
>
> Nécessaire prison.
>
>  
>
> Non pas pour les rues dolentes, pour les villes
>
> Marquées comme des visages humains
>
> Non pas pour la cendre de passion
>
> des églises, non pas pour la voix
>
> De tes livres lointains
>
>  
>
> Mais pour ces mots
>
> Tissés de plèbe, qui battent
>
> Martèlent dans la tête,
>
> Pour cette peine présente
>
> Qui en toi m'enveloppe étranger.
>
>  
>
> Pour cette langue mienne que je dis
>
> À des hommes graves et ardents de futur
>
> Libres dans la douleur forte camarade.
>
> Maintenant il ne suffit même pas de mourir
>
> Pour ton nom vain et antique[^128].

Près de vingt ans plus tard, dans *Poésie en forme de rose* (1964),
Pasolini semble lui répondre :

> Une seule ruine, rêve d'une arche,
>
> d'une voûte romaine ou romane,
>
> dans un champ où écume un soleil
>
> dont la chaleur est tranquille comme une mer ;
>
> réduite en cet état, la ruine est sans amour. Usage
>
> et liturgie, maintenant profondément révolus,
>
> vivent dans son style --- et dans le soleil ---
>
> pour qui en comprendrait la présence, la poésie.
>
> Tu fais quelques pas et tu es sur la Via Appia
>
> ou sur la Tuscolana : là tout est vie,
>
> pour tous. Ou plutôt, celui qui tout ignore
>
> du style et de l'histoire est un meilleur
>
> complice de cette vie. Les significations qu'elle revêt
>
> échangent dans la paix sordide
>
> indifférence et violence. Des milliers,
>
> des milliers de personnes, polichinelle
>
> d'une modernité de feu, dans le soleil
>
> dont le sens est lui aussi à l'œuvre,
>
> se croisent fourmillant obscures
>
> sur les trottoirs aveuglants, sur fond
>
> de HLM enfoncées dans le ciel.
>
> Je suis une force du Passé.
>
> Mon amour ne réside que dans la tradition.
>
> Je viens des ruines, des églises,
>
> des retables, des villages
>
> abandonnés dans les Apennins ou les Préalpes,
>
> où ont vécu mes frères.
>
> Je roule sur la Tuscolana comme un fou,
>
> sur la Via Appia comme un chien sans maître.
>
> Ou je regarde les crépuscules, les matins
>
> sur Rome, sur la Ciociaria, sur le monde,
>
> comme les premiers actes du Posthistorique
>
> auquel j'assiste, par privilège d'ancienneté,
>
> du bord extrême de quelque âge
>
> enseveli. Monstrueux, celui qui est né
>
> des entrailles d'une morte.
>
> Et moi, fœtus adulte, je rôde
>
> plus moderne que tout moderne
>
> pour trouver des frères qui ne sont plus[^129].

Fortini conserve une foi intacte en l'*Aufhebung* et ses promesses
d'*avvenire* : il s'agit toujours de s'arracher aux ruines et de guetter
un au-delà du monde existant, le communisme ; Pasolini, quant à lui,
acte notre entrée dans une nouvelle préhistoire : l'utopie ne peut plus
tenir à la prospective vaine et sans cesse remâchée des lendemains qui
chantent. Il s'accroche ainsi désespérément aux ruines, et cette loyauté
au passé donne à son utopie un ancrage et une forme qui tranchent
nécessairement avec l'espérance progressiste de Fortini.

À travers leurs échanges, Walter Siti et Franco Fortini tombent d'accord
sur le caractère irrémédiablement antidialectique de l'utopie
pasolinienne : s'affranchissant de toute perspective de dépassement sur
un plan supérieur, celle-ci ne peut, pensent-ils, que s'enraciner dans
une esthétique de la défaite. Selon Siti, cette impossibilité de la
synthèse dialectique serait même la raison pour laquelle Pasolini était
incapable d'écrire des romans, ce dernier ne pouvant concevoir une mise
en forme assimilable de sa matière poétique.

Mais retournons la critique : ne serait-ce pas précisément parce qu'on
ne consomme pas la poésie, contrairement au roman qui se laisse
« engloutir[^130] », que celle-ci demeure soustraite à la
marchandisation du monde ? Et, dans le cas de Pasolini, cette
« antithèse sans dialectique » ne serait-elle pas le propre d'une poésie
qui s'affranchit simplement d'un certain didactisme ?

À rebours sans doute de la poétique brechtienne (nous n'y reviendrons
pas, clame Pasolini[^131]), cette dernière guette une parole neuve en
mobilisant des forces vitales nécessaires à son accomplissement. Et s'il
est remarquable, dans le sillon sans doute involontaire de Benjamin, que
cet accomplissement apparaisse conjointement comme acte de rédemption
pour les générations passées, il ne s'agit pas pour autant d'un repli
passéiste, plutôt d'une remobilisation offensive de ces cultures
« variées et particulières[^132] » mises en péril par la nouvelle
hégémonie culturelle et politique de la bourgeoisie occidentale.

Ainsi la poétique pasolinienne ne peut être réduite à un « pur refus » :
elle maintient toujours un rapport dialectique, non seulement avec les
résidus de cultures vernaculaires qu'elle entend reconnaître et
défendre, mais également avec la culture du pouvoir, qu'il s'agisse de
celle des pères (ancienne bourgeoisie), ou de la nouvelle culture
hédoniste (nouvelle bourgeoisie).

Si Pasolini refuse de s'intégrer, de se trahir, il ne rechigne pas
pour autant à l'adaptation, critique et productive, en vue de réinventer
toujours sa propre radicalité ; c'est en cela que son œuvre demeure
vivante, authentiquement révolutionnaire, qu'elle se distingue ainsi de
la rhétorique habituelle de la contestation :

> La condamnation totale et intransigeante qu'ils avaient prononcée sans
> discrimination contre tous les pères les avaient empêchés d'avoir avec
> ces pères un rapport dialectique, grâce auquel ils auraient pu les
> dépasser, continuer de l'avant. Le refus pur est aride et malfaisant. À
> travers le refus, les jeunes se retrouvèrent ainsi bloqués dans
> l'histoire[^133].

S'il est d'usage de dénoncer le repli passéiste de Pasolini, comment ne
pas déceler dans la cristallisation utopique, sans ancrage ni
prospective concrète, un égal repli sur l'avenir ? Ce n'est pas tant
qu'il faille négliger les générations à venir, il s'agit seulement de
briser le miroir aveuglant de l'espérance progressiste qui, en
justifiant au nom de l'avenir un développement infini des forces
productives (même en un sens socialiste), contribue paradoxalement à
sacrifier ses propres enfants.

C'est précisément ce qui distingue leurs deux conceptions du
communisme : une conception comme *idéal de société à réaliser* ; une
autre comme *défense et libération du déjà existant*.

On peut lire Marx par les deux bouts. Mais c'est sans doute à l'insu
du marxisme officiel que Pasolini demeure fidèle, et non au dogme
productiviste[^134]. Près de cinquante ans après sa mort, le consensus
populaire à propos de la « crise écologique » ne lui donne-t-il pas
raison ? Le communisme peut-il encore viser une réappropriation des
forces productives sans bouleverser drastiquement, quantitativement et
qualitativement, la productivité ? Et quant aux moyens de production,
archidéterminés par la surproduction capitaliste, ne revêtiraient-ils
pas, dans une « société communiste », le caractère d'outils sans usage ?
Nous y reviendrons.

Cette confrontation d'un « communisme de la tradition » et d'un
« communisme de l'avenir » leur permet en tout cas de développer une
conception et des usages particuliers de la langue.

Pour Fortini, il s'agit de renoncer au charme des pierres, de refuser de
se perdre dans les antiques ruelles du patrimoine glorieux, matériel ou
littéraire de la grande Italie, de fonder notre commun sur cette
« langue tissée de plèbe », unique héritage vivant d'une
Italie populaire en quête d'elle-même :

> Parce que plus que dans les cathédrales détruites, notre foyer est dans
> notre langue. La langue des dialectes paysans et citadins, la langue des
> livres, l'Italie est dans sa langue si ancienne et nouvelle, cette
> langue qui conserve ce qui reste, vivant, de la tradition, et qui
> prépare tout le futur[^135].

*A contrario*, bien qu'ayant écrit le gros de son œuvre poétique en
florentin, Pasolini accuse sa langue nationale d'être un facteur
d'homogénéisation culturelle --- et, jusqu'au bout, dans sa réécriture de
*La Meilleure jeunesse*, il usera du dialecte afin de rendre justice à
un monde paysan devenu minoritaire :

> ... Je pleure un monde mort
>
> Pourtant, moi qui le pleure, je ne suis pas mort.
>
> Si nous voulons aller de l'avant, il nous faut pleurer
>
> le temps qui ne reviendra plus et dire non
>
>  
>
> à cette réalité qui nous a enfermés
>
> dans sa prison[^136]...

Si Fortini condamne cet encloîtrement dans la posthistoire --- « nous ne
croyons plus aux dieux lointains... » ; ce qui signifie : il faut
avancer, construire un autre monde sur les ruines de l'ancien ---,
Pasolini abjure la tradition rationaliste qui continue de croire
aveuglément en sa propre nécessité. Autrement dit : Fortini inscrit bel
et bien son engagement dans un système de valeurs universelles fondé sur
l'idée d'un « progrès dans l'histoire » dont le communisme serait
l'accomplissement, et à ce jeu, les larmes de Pasolini apparaissent
comme un non-sens historique.

Pasolini n'y va pas non plus avec le dos de la cuiller : à ses yeux,
Fortini incarne cet aliment intellectuel du prolétariat que Totò et
Ninetto, à la fin de *Uccellacci e Uccelini* (1966), n'hésiteront guère
à dévorer.

Mais cette ultime saillie peut sembler réductrice au regard des
élaborations contradictoires de Fortini, qu'on ne peut ranger si
facilement dans le camp de la Raison triomphante. 
En nous intéressant,
par exemple, à ce qu'il dit de Straub, nous comprenons non seulement ce
que fut la richesse et la complexité de sa conception du marxisme, mais
également l'intérêt que revêt sa conception des liens entre art et
communisme :

> \[Straub est\] l'exemple presque unique d'un artiste chez qui se
> rejoignent la dialectique du négatif selon Adorno, la dialectique de la
> prophétie selon Benjamin et la dialectique du présent concret que porte
> Brecht[^137].

L'évocation de cette trinité profane (Adorno, Benjamin, Brecht) atteste
que Fortini s'intéressait aux héritages contradictoires du marxisme ; on
ne peut donc le réduire si facilement au courant froid ni au courant
chaud de la tradition révolutionnaire, il puise bien à toutes ses
sources. Ce qui le distingue du poète de la tradition, ce sont
précisément des efforts théoriques nourris d'une curiosité que Pasolini
réservait à d'autres lectures, moins directement associées à l'actualité
de la tradition marxiste --- raison pour laquelle, sans doute, il ne fut
jamais en mesure de reconnaître ses propres frères.

Dans un texte tardif, Fortini recompose un rêve de révolution à partir
des « fragments de la vie fausse, de ces moments où la forme mystifiée
de ce monde vacille et laisse reluire les figures de la délivrance
future[^138] » ; son obstination dialectique le conduit ainsi à
repenser son « utopie » sous une forme qui peut sembler assez étrangère
à la *sineciosi* pasolinienne[^139]. Formulons cependant l'hypothèse
que le poète des ruines n'a jamais renoncé à une perspective de
délivrance, sous une forme sans doute moins prospective, plus cloisonnée
aux ruines elles-mêmes, mais bien réelle : « excès d'amour de ma
désespérance[^140] », disait-il. En tout cas, cette reconsidération de
la forme fragmentaire, qui revêt un caractère indissociablement éthique
(reconnaissance du négatif) et esthétique (primat du montage sur le
*déjà écrit* du discours), trouve indéniablement un écho chez le
Pasolini des dernières œuvres.

Revenons au poème des ruines. Que signifie cette formule : « plus
moderne que tout moderne » ? L'abjuration par Pasolini de la tradition
rationaliste serait-elle un devenir possible de la modernité critique ?
Sa mutation sous l'effet d'une nouvelle lucidité historique ? La seule
façon de demeurer moderne consisterait-elle ainsi dans l'attitude
critique radicale ? Soit l'acte de déconstruction de cette modernité
elle-même ?

Voici comment Foucault définit l'attitude philosophique du moderne :

> Cette attitude philosophique doit se traduire dans un travail d'enquêtes
> diverses ; celles‑ci ont leur cohérence méthodologique dans l'étude à la
> fois archéologique et généalogique de pratiques envisagées simultanément
> comme type technologique de rationalité et jeux stratégiques des
> libertés ; elles ont leur cohérence théorique dans la définition des
> formes historiquement singulières dans lesquelles ont été problématisées
> les généralités de notre rapport aux choses, aux autres et à nous-mêmes.
> Elles ont leur cohérence pratique dans le soin apporté à mettre la
> réflexion historico-critique à l'épreuve des pratiques concrètes. Je ne
> sais s'il faut dire aujourd'hui que le travail critique implique encore
> la foi dans les Lumières ; il nécessite, je pense, toujours le travail
> sur nos limites, c'est-à-dire un labeur patient qui donne forme à
> l'impatience de la liberté[^141].

L'attitude singulière de Pasolini à l'égard du monde, des hommes, de
l'actualité politique et des nouveaux phénomènes culturels ne fut jamais
une attitude de repli. S'il est vrai qu'il négligea des pans importants
de la nouvelle tradition critique, ses poèmes comme ses textes
polémiques, ses films comme ses ouvrages plus expérimentaux prirent le
plus souvent la forme d'enquêtes (*appunti*) dédiées à l'analyse
concrète de la société italienne ou des sociétés entrevues lors de ses
voyages (Inde, Éthiopie, Palestine, Maroc, etc.).

Ainsi sa quête de fraternité perdue ne conspire aucunement contre la
modernité en soi, elle en est plutôt l'affirmation critique, en
rupture avec les formes positives de la conscience de soi, une mise en
question de toutes ces généralités qui nous étourdissent au point de
prendre l'apparence de la réalité elle-même.

Non seulement nous reconnaissons Pasolini dans cette figure d'enquêteur
philosophique, que Foucault définit comme étant celle du moderne par
excellence, mais nous le retrouvons également dans le geste rimbaldien
d'une contestation poétique de l'ordre symbolique. Malgré cette double
inscription (philosophique et poétique) dans la modernité, Fortini
maintient pourtant que l'engagement polémique et littéraire de Pasolini
souffre de la rigidification de sa posture de persécuté, que celle-ci se
traduit notamment par un refus de considérer les manifestations inédites
du corps social en lutte.

Il insiste : Pasolini serait un homme du passé, son marxisme un musée de
lumières vaincues par la modernité ; sans contester au poète la beauté
intrinsèque de son œuvre, la qualité de sa technique et de sa langue, il
lui demande donc explicitement de se retenir d'intervenir dans les
enjeux concrets de la politique nationale et des mouvements sociaux en
cours. Mais demander à un poète, qui plus est à un poète d'inspiration
marxiste, de se maintenir en marge des dynamiques sociales de son temps,
cela ne revient-il pas à lui demander de se taire ?

Si Pasolini hurle dans un désert, ce n'est pas seulement que ses
dispositions subjectives le condamnent à la solitude et donc à
l'incompréhension, c'est également que la révolution anthropologique
néocapitaliste a objectivement accentué, creusé les processus
d'atomisation et de réification :

> Aucun désert ne sera jamais plus désert qu'une maison, une place, une
> rue où vivent les hommes mille neuf cent soixante-dix ans après
> Jésus-Christ. Ici, c'est la solitude. Coude à coude avec ton voisin qui
> s'habille dans les mêmes grands magasins que toi, fréquente les mêmes
> boutiques que toi, lit les mêmes journaux que toi, regarde la même
> télévision que toi, c'est le silence[^142].

Si nous comprenons la position critique de Fortini à l'égard de
Pasolini, que nous refusons de la réduire à un aveuglement progressiste,
nous comprenons tout autant les résistances de Pasolini. Il n'est plus
temps de choisir son antagonisme, d'y camper : ni l'optimisme
progressiste ni le pessimisme conservateur ne peuvent apparaître comme
des solutions unilatérales viables ; ils doivent être articulés dans le
cadre d'une pensée révolutionnaire faisant tenir l'ancien et le neuf, la
tradition et la contestation, au sein d'une dialectique qui reste encore
à forger, et qui devra se risquer à descendre dans la rue, dans le
fourmillement des foules hostiles.

Pasolini n'était dupe de rien, il savait que les mondes qui lui tenaient
tant à cœur avaient déjà disparu, qu'il ne s'agissait aucunement de les
ressusciter ni de croire en une quelconque survivance. Si les lucioles
se font rares, qu'elles font le plus souvent place à la nuit sans éclat
d'une nouvelle préhistoire, la lutte n'en est pas moins nécessaire.
Seulement le poète doit s'adapter : en armant son exigence créatrice
d'une lucidité nouvelle (conscience et critique de la formidable
capacité d'absorption de la positivité révolutionnaire du capitalisme),
en s'efforçant de produire une image du pouvoir et/ou une contre-image
utopique de sa désagrégation (*Salò*, *Pétrole*, etc.), en tenant
obstinément à cette éternelle jeunesse du monde vers laquelle Pasolini
n'a jamais cessé de courir (la Palestine notamment).

Par ailleurs, s'il est vrai que le jugement porté par Pasolini sur la
jeunesse militante des années 70 fut particulièrement sévère, il n'est
guère possible de saisir l'objet véritable de sa critique et sa
signification toute conjoncturelle si nous fermons méthodiquement les
yeux sur sa pratique concrète d'artiste ; sa capacité, notamment, à
entrer en relation sans apriorisme avec des groupes d'individus aux
idéologies et aux pratiques qu'il ne partage pas.

C'est ainsi qu'il n'hésite pas à réaliser un documentaire avec des
membres de *Lotta continua* sur l'assassinat du cheminot anarchiste
Pinelli par la police italienne en 1969 --- il s'en justifie de la façon
suivante :

> Pourquoi ai-je fait ce film avec un groupe de jeunes militants de *Lotta
> Continua* ? Il existe sûrement une raison, mais pour être sincère, je ne
> saurais la dire. J'ai critiqué en son temps, avec violence et peut-être
> de façon inopportune, l'action politique des jeunes : beaucoup de mes
> critiques se sont malheureusement révélées justes, et je ne les abjure
> pas. Toutefois il me semble que la tension révolutionnaire réelle est
> éprouvée aujourd'hui par ces minorités d'extrême gauche. La critique
> globale et quasi intolérante que celles-ci expriment contre l'État
> italien et la société capitaliste me trouve être en accord sur le fond,
> bien que rarement sur la forme. Ce pour quoi, tant que j'en suis capable
> et que j'en ai la force, c'est à elles que je m'unis[^143]...

Fortini peut accuser Pasolini de hurler dans un désert, nous pensons
pourtant que sa critique du « gauchisme » des années 70, articulée à sa
capacité à faire avec celles et ceux qu'il critique, cette capacité donc
à assumer certaines contradictions, les siennes propres et celles de son
temps, ainsi que sa propension à penser et agir, en parfaite liberté,
tant intellectuelle qu'artistique, témoignent d'un rapport décomplexé à
la praxis : toute son œuvre (poétique, filmique, polémique) demeure,
envers et contre tout, profondément politique.

Et nous ajoutons, en répondant directement à Fortini, qui accusait
Pasolini d'avoir négligé les nouvelles formes de théorie
critique (celle de Francfort notamment), que le marxisme hérétique de
Pasolini est jalonné de potentielles affinités avec nombre de praticiens
et de théoriciens révolutionnaires, de son époque ou de la nôtre, et
qu'il ne tient qu'à nous, en opérant un travail rigoureux et honnête,
d'en actualiser les divergences, les points de rencontre et les aspects
complémentaires. Sans aucune ambition d'exhaustivité, avec toutes les
limites qui sont les nôtres, en assumant une part d'arbitraire et de
partialité, c'est ce que nous tentons de faire dans le cadre de cet ouvrage.

</div>

<div class="text" data-number="7">

## 8. Les impasses de la Raison

Bien que fondamentalement dialectique --- c'est ce que nous défendons
ici à rebours de Fortini ou Siti ---, jamais la poétique pasolinienne ne
cherche à atténuer le *différend* entre Histoire et Réalité. Chose si
difficile à accepter pour l'intellectuel progressiste se reconnaissant
encore dans le vieil humanisme libéral, celui qui dissout toute la
cacophonie du monde dans un discours sur l'Histoire.

C'est que la Raison, bourgeoise et acritique, à laquelle il n'a pas su
renoncer, lui a fait perdre le sens de la réalité, du vivant et de sa
dignité :

> Vois-tu, Gennariello, la plupart des intellectuels laïcs et
> démocratiques italiens se donnent de grands airs, parce qu'ils se
> sentent virilement « dans » l'histoire. Ils acceptent, dans un esprit
> réaliste, les transformations qu'elle opère sur les réalités et les
> hommes, car ils croient fermement que cette « acceptation réaliste »
> découle de l'usage de la raison[^144].

La Raison marxiste n'échappe pas non plus à la critique. Bien qu'elle
ait imposé son Sujet --- le Prolétariat --- à l'histoire bourgeoise,
ce dernier ne fut pas à la hauteur des exigences de la Théorie
révolutionnaire qui avait pourtant soigneusement déterminé son destin et
défini sa tâche. Pire : son intégration à la marche de l'Histoire ne se
fit pas sur le mode d'un renversement du capitalisme, mais sur celui de
son éternelle perpétuation.

Non seulement la révolution prolétarienne mondiale ne fut pas au bout du
chemin, mais les espérances qu'elle a soulevées ont servi la cause
qu'elle combattait : en ralliant subjectivement les travailleurs au
Progrès, elle préparait ainsi le terrain à l'acceptation
généralisée de cette « réalité qui n'existe pas[^145] ».

À ce jeu, l'intellectuel de gauche a gaspillé ses forces. Son combat,
aussi sincère qu'il ait pu être, n'en était pas moins gangréné par une
foi aveugle dans le « développement en soi », superbe abstraction à
laquelle seul le régime capitaliste de production et d'échange était en
mesure de promettre un corps : au nom de la disparition de la misère,
d'un accès universel au soin et à l'éducation, d'une augmentation
qualitative et quantitative du « bien-être », l'idéologie progressiste
justifiait ainsi d'accroître indéfiniment les cadences de la production
et la frénésie des échanges.

Dans quelle mesure le « miracle italien[^146] » a-t-il accompli cette
promesse ?

> La seule réalité qui palpitait avec le rythme et l'effort de la vérité,
> était celle --- impitoyable --- de la production, de la défense de la
> monnaie, de l'entretien des vieilles institutions encore essentielles au
> nouveau pouvoir et ce n'était assurément pas les écoles, ni les
> hôpitaux, ni les églises[^147].

Cet extrait est tiré de la « Note 59 » de *Pétrole* dans laquelle
Pasolini s'attarde sur une séquence emblématique de la nouvelle
politique italienne ; de mai à octobre 1972, le gouvernement démocrate-chrétien, qui se voit dans l’obligation de négocier avec la résurgence électorale de l’extrême droite, tend alors à l’intégration conflictuelle du néofascisme à l’appareil (idéologique et institutionnel) d’État :

> La réalité, poursuivant son chemin, comme le voulaient ses lois réelles,
> transformait les Italiens à travers de nouveaux phénomènes de
> permissivité : certains acquis, qui, avec les socialistes au
> Gouvernement, s'appelaient Réformes, étaient devenus irréversibles.
> L'Italie s'orientait vers l'Hédonisme de la Consommation --- \[si le
> lecteur me permet cette définition hâtive\] --- dont le temple n'était
> certes pas l'Église. Un fasciste hédoniste est une contradiction dans
> les termes. Le pouvoir était pris \[dans l'impasse\] de cette
> contradiction[^148].

Mais s'agit-il véritablement d'une contradiction ? Le néocapitalisme ne
serait-il pas doté d'une capacité d'absorption de toute idéologie *a
priori* contradictoire ?

Afin de circonscrire la nature de ce « nouveau pouvoir », faisons un
bref détour par l'un de ses plus illustres représentants : Eugenio
Cefis[^149]. Un discours prononcé par celui-ci, le 23 février 1972 à
l'Académie militaire de Modène, nous renseigne sur ses intentions. Non
seulement cette intervention condense tous les éléments caractéristiques
de la nouvelle religion capitaliste, mais elle en esquisse certaines
tendances stratégiques lourdes :

- émergence de nouveaux organes politiques transnationaux et de nouveaux agents technocratiques de la politique nationale, inféodés les uns comme les autres aux intérêts d'un Capital de plus en plus concentré et pourtant de plus en plus difficile à identifier et à circonscrire ;
- implantation infinie de ces multinationales aux quatre coins du monde ;
- diversification de leur production et conquête infinie de nouveaux marchés ;
- asservissement des nations « en voie de développement » par le monopole des nouvelles technologies et des savoirs qui leur sont liés.

À ce titre, le « néocapitalisme » ne semble rien d'autre que l'extension
et le perfectionnement du vieux capitalisme, en sa tendance
accumulatrice, monopolistique et impérialiste, prenant simplement appui
sur un développement exponentiel des nouvelles technologies au service
de la production et des échanges.

Est-ce à dire que le néocapitalisme[^150] ne se distinguerait de la
forme antérieure du capitalisme que par un développement exponentiel de
ses moyens ? Oui et non. Car ce développement quantitatif est également
un saut qualitatif : l'implantation totalitaire du Capital impliquant un
bouleversement des valeurs, une domestication accrue des corps et des
consciences, leur adaptation aux nouvelles normes de la production et
des échanges, mais également aux nouvelles possibilités offertes à l'individu de
devenir un consommateur total de son existence.

À ces perfectionnements appliqués au champ économique se joignent ainsi
de nouvelles technologies de contrôle ayant pour fonction d'assigner
les corps et les consciences aux jeux d'un marché en extension
perpétuelle, de les adapter, à « marche forcée » si cela est nécessaire,
à des normes qui excèdent donc assez largement le cadre strictement
économique.

Ce pour quoi le développement capitaliste porte atteinte non seulement à
l'environnement dit « naturel » (dans le cas de l'industrie pétrolière,
la destruction des écosystèmes est reconnue comme particulièrement
massive), mais également à toute forme de vie humaine (langues,
traditions, rapports symboliques et matériels à la « nature », etc.).

Afin de se prémunir contre toute sorte de contestation populaire dans
les espaces où la multinationale a pour vocation de s'implanter, Cefis
appelle à faire valoir la dépendance des nations subalternes à l'égard
des nouvelles technologies au service de la production et des échanges
--- dans le cas particulier de l'industrie pétrolière : extraction des
ressources, transformation chimique de ces dernières, diversification de
ses usages, commercialisation, etc.

Mais au sein des sociétés capitalistes avancées, le sentiment d'un
déracinement généralisé contribue également à faire naître un certain
nombre d'oppositions que la gauche, empêtrée dans son progressisme,
n'est pas en capacité de traduire sur le terrain de la politique
institutionnelle. Ce qui laisse bien évidemment le champ libre aux
nouvelles « oppositions » fascistes qui exploitent ce sentiment en lui
greffant une forme identitaire et raciste.

Cependant, afin de parvenir formellement au pouvoir, le néofascisme est
contraint de s'adapter à des normes (hédonistes et transnationales) qui
entrent en contradiction avec son socle de valeurs traditionnelles.
Autrement dit : la Raison néocapitaliste est à la fois instrumentale (au
service de la rationalisation de la production et des échanges) et
intégrative (capable d'absorber des idéologies *a priori*
contradictoires afin de demeurer hégémonique).

Cette intégration du néofascisme à la Raison capitaliste n'en
demeure pas moins instrumentale : la fonction d'éléments programmatiques
identifiés comme « fascisants » étant de servir l'exclusion, voire
l'éradication, des producteurs surnuméraires et des consommateurs
déviants. Le néofascisme faisant ici le jeu, non seulement des exigences
productives, mais des nouvelles configurations territoriales, sociales
et culturelles induites par ce « nouveau capitalisme ».

Ainsi le phénomène politique « Giorgia Meloni », par exemple, n'est
rien de plus qu'une certaine mise en récit des exigences de la
production et des normes du marché. Si cette narration raciste contribue
à un accroissement des politiques répressives, pour ne pas dire
génocidaires, à l'égard de toute forme de vie non intégrée, il ne s'agit
pas pour autant d'un changement de nature du capitalisme, seulement
d'une explicitation, en des termes plus identitaires mais non moins
technocratiques, d'exigences et de normes générées par celui-ci.

Le nouveau fascisme, disait Pasolini, portera le nom d'antifascisme.
C'est déjà le cas. Et si Giorgia Meloni a contribué à produire une
articulation inédite entre un vieux signifiant politique et
l'antifascisme idéologique de Bruxelles, on pourrait ajouter que le
fascisme traditionnel, cette force inouïe de répression et
d'acculturation, dans la plupart des démocraties occidentales, a bien
été enterré par le crétinisme technocratique et le culte aveugle rendu
au développement économique.

Devant l'opposition de façade entre postfascisme et néolibéralisme
--- qui se trouvent bien souvent des alliances cyniques et conjoncturelles :
Trump, Meloni, Orban, etc. ---, la gauche demeure le plus souvent
illisible. Elle est notamment coupable d'avoir contribué à l'effort
d'adaptation à « marche forcée » de la classe ouvrière occidentale au
capitalisme mondialisé, ainsi qu'à l'éradication méthodique des mondes
marginaux, ruraux et périurbains, et ce au terme d'une atomisation
sociale et d'une précarisation matérielle et symbolique inédite.

« L'intégration ne compensait pas la trahison[^151] » : non seulement la
gauche n'a pas su imposer une raison dissidente --- une raison que je
m'efforcerai de nommer communiste ---, mais elle a contribué à nourrir un
consentement généralisé au culte du Progrès, et donc à disqualifier
toute manifestation vivante échappant aux nouvelles normes économiques
et sociales, techniques et culturelles imposées par le néocapitalisme
--- bref : elle s'est coupée de l'histoire en croyant la réaliser.

La contre-culture de gauche n'échappe pas à l'accusation. Selon
Pasolini, la contestation des figures d'autorité traditionnelles a pris
la forme d'une contestation de la tradition en soi, conduisant à
l'impossibilité de nourrir un rapport dialectique à l'histoire, ce qui
lui aurait permis de s'insérer en elle et donc de la transformer :

> L'Opposition s'était réorganisée, et avait retrouvé une certaine unité
> traditionnelle, fondée sur une idée rhétorique du Pouvoir compris
> traditionnellement comme « vieux, stupide, obtus, sans dilemmes ». Le
> gauchisme avait perdu ses masses, parce que sous-culture de contestation
> contre sous-culture du pouvoir est une antithèse qui ne peut finir que
> dans l'échec de la première[^152].

L'inféodation, aux deux bords de l'échiquier politique, de toute
expression populaire à la Raison néocapitaliste se manifeste
comme affirmation incontestable de la nouvelle civilisation capitaliste,
socialement déchirée et homogène politiquement, techniquement avancée et
culturellement inerte.

La civilisation capitaliste se caractérise par un processus massif
d'uniformisation et d'anéantissement des possibles ; ce que l'idéologie
néolibérale justifie par une lutte à mort de tous contre tous. À la fin,
il ne pourra rester qu'un seul modèle de développement, qu'une seule
manière de vivre et de penser, qu'un seul « type d'homme », celui qui
aura su s'adapter à l'extension du domaine de la lutte à toutes les
sphères du vivant.

Malgré ce que peuvent en dire les tenants d'un certain « darwinisme
social », ce projet génocidaire va jusqu'à trahir Darwin lui-même, qui
pensait non seulement que l'évolution des espèces était à la fois
multiple, locale et provisoire, mais que l'application de sa théorie de
l'évolution aux sociétés humaines était une grossière erreur
épistémologique[^153]. Cette homogénéisation des buts (accroissement et
tendance monopolistique du capital), des moyens (exploitation du vivant
sous toutes ses formes) et des modèles de vie sociale (la société de
consommation, ce qu'elle charrie de valeurs conformistes et
d'injonctions à se comporter « comme tout le monde ») est donc une
aberration au regard de ses propres références intellectuelles.

Penser le stade tardif du capitalisme comme aboutissement d'un projet
civilisationnel dégénéré doit non seulement nous aider à mieux
comprendre ses dynamiques réelles, mais également à rendre compte de la
possibilité d'une histoire postcapitaliste. Et bien qu'il soit
relativement difficile de se faire une idée concrète de ce que
signifierait une lutte globale qui ne soit plus seulement une simple
option dans le champ des possibles institutionnels, mais un acte de
destitution des valeurs consuméristes, un mouvement de réappropriation
consciente et raisonnée des moyens de production et d'échange, une
prolifération de modalités spécifiques d'existence collective et donc
l'avènement d'une contre-civilisation communiste.

Qui dit destitution ne dit pas table rase : le dialogue ininterrompu
avec les traditions vernaculaires, d'une part, avec la tradition
critique occidentale, d'autre part, doit nous permettre d'opérer une
action sur le présent qui se ferait conjointement *sauvegarde*,
c'est-à-dire reconnaissance et actualisation de la mémoire des vaincus,
et en même temps visée prospective, utopique.

Par ailleurs, cette contre-civilisation ne pourra pas être un retour à
l'époque précapitaliste, mais une organisation postcapitaliste de
l'existant ; ce qui implique donc de repenser les notions de *croissance*
et de *progrès*, en les soustrayant non seulement au dogme du
développement à tout prix, mais également à toute critique métaphysique
et technophobe du « système technicien ».

Ce qui distingue Marx des utopistes qui le précédèrent est une autre
articulation (matérialiste et dialectique) de l'Homme et de la
Technique. En effet, sa critique se fonde sur la catégorie d'usage et
non sur la prétendue nature maléfique de l'outil. En d'autres termes, si
les outils dissimulent tant de contre-finalités sociales, c'est tout
simplement parce que leur forme et leur fonction sont *a priori*
déterminées par les exigences de la production et/ou les nouvelles
modalités d'échange et de consommation.

Dans la mesure où le « bluff technologique[^154] » se déploie dans un
cadre économique, social et culturel qui est lui-même déterminé par une
forme historique du capitalisme, la critique doit donc s'efforcer de ne
pas investir l'outil d'une aura qu'il ne peut posséder en lui-même. 
Ni
neutralité de principe ni aura maléfique, il s'agit de redialectiser le
rapport entre le sujet humain et la technique, d'inventer un usage
émancipé et créateur des outils dont nous disposons, plutôt que de
statuer sur la nature intrinsèquement mortifère de ces derniers, ou
encore de nous abandonner aveuglément au charme et aux prouesses des
« nouvelles technologies ». 

L'exigence de redialectiser notre rapport à la technique repose
également sur une hypothèse utopique concrète : que nous est-il possible
de faire d'autre que d'habiter les ruines de l'âge capitaliste ? C'est à
l'intérieur de cette condition que nous devons agir : même la fuite, la
bifurcation ou la désertion apparaissent comme des tactiques déterminées
par la forme historique de ce monde. En atteste très concrètement la
façon dont des collectifs urbains ou périurbains se réapproprient des
lieux abandonnés à l'ère postindustrielle afin d'y établir des espaces
de vie et de partage. En atteste également la façon dont de jeunes
militants écologistes s'efforcent de refertiliser des terres ravagées
par l'agro-industrie et la pollution des sols.

Cette exigence de redialectisation appliquée à l'usage de la technique
revêt un caractère indissociablement éthique et politique : il s'agit
toujours de partir de l'existant et de le transformer ;
ce qui distingue une utopie concrète d'un rêve innoffensif de révolution.

Malgré tout l'intérêt que nous portons à la pensée d'Ivan Illich, on ne
peut manquer de souligner chez lui une certaine tendance à considérer
l'outil en soi en lieu et place de sa fonction déterminée par les
exigences de la production et la forme des échanges, à considérer ainsi
« l'homme face à la machine » en lieu et place des formes techniques de
la domination de classe :

> Ce qui m'intéresse n'est pas l'opposition entre une classe d'hommes
> exploités et une autre classe propriétaire des outils, mais l'opposition
> qui se place d'abord entre l'homme et la structure technique de l'outil,
> ensuite --- et par voie de conséquence --- entre l'homme et des
> professions dont l'intérêt consiste à maintenir cette structure
> technique[^155].

Nous refusons pour autant de renoncer à la puissance critique et
prospective que nous inspire cette tradition utopique, nous en
actualisons seulement les potentialités en la confrontant à une étude
rigoureuse de la tradition marxiste et poststalinienne, en militant pour
son entrelacement avec l'existant et les possibilités techniques qui
nous sont offertes de nous réapproprier très concrètement nos vies.

Si nous parlons depuis la disparition (celle de notre tradition
ouvrière, celle des cultures spécifiques, celles des espèces animales et
végétales), notre utopie se fonde sur une dialectique de l'existant et
du possible qui vise bien la perpétuation de la vie sur terre
--- autrement dit : « il s'agit d'être utopiste ou de disparaître[^156] ».
C'est donc de façon tout à fait pragmatique qu'il doit s'agir de
recomposer de nouvelles intelligibilités techniques, et ce à partir des
savoirs et des technologies dont nous disposons --- ce qui nous
permettra, en fonction des nécessités tactiques et/ou vitales du moment,
soit de réinventer des usages, soit de débrancher la « machine ».

Outre cette position de principe, celle d'une obstination
révolutionnaire qui se loge dans les conjonctures de notre temps, et
quand bien même cette époque nous fait horreur, cela nous invite
également à repenser cosmologiquement le communisme, non seulement quant
à la relation de l'humain au vivant, à la terre et à l'outre-espace, mais
également quant à la relation que nous entretenons vis-à-vis des objets
qui nous environnent, qu'il s'agisse d'outils conviviaux ou des débris
sans usage de l'âge capitaliste.

La technophobie couve toujours une réinstitution de l'Homme en tant que
sujet souverain, maître et possesseur de ses objets, non seulement
naturels mais techniques. Il ne suffit pas de pulvériser la conception
mécaniste du vivant, il s'agit également de réinsuffler une « âme » aux
objets techniques qui participent concrètement de nos modes d'existence,
et ce afin de ne pas reproduire en pensée et en acte un monde déchiré
par l'anthropocentrisme et ses « schèmes d'asservissement » :

> On pourrait nommer philosophie autocratique des techniques celle qui
> prend l'ensemble technique comme un lieu où l'on utilise les machines
> pour obtenir de la puissance. La machine est seulement un moyen ; la fin
> est la conquête de la nature, la domestication des forces naturelles au
> moyen d'un premier asservissement : la machine est un esclave qui sert à
> faire d'autres esclaves. Une pareille inspiration dominatrice et
> esclavagiste peut se rencontrer avec une requête de liberté pour
> l'homme. Mais il est difficile de se libérer en transférant l'esclavage
> sur d'autres êtres, hommes, animaux ou machines ; régner sur un peuple
> de machines asservissant le monde entier, c'est encore régner, et tout
> règne suppose l'acceptation des schèmes d'asservissement[^157].

Ajoutons que la domination, de quelque nature qu'elle soit, défigure
l'oppresseur en même temps qu'elle amoindrit la puissance
d'agir de l'opprimé. Dans le cas des objets techniques, il ne s'agit pas
d'investir la machine d'un visage humain, de s'émouvoir devant un
ordinateur ou un cyborg, mais de penser les systèmes techniques comme
des systèmes relationnels qui médiatisent le rapport des humains à
eux-mêmes et à leurs milieux de vie.
Quand la machine est assignée au
rang de pur moyen, elle est conjointement un moyen d'asservir :
« un esclave qui sert à faire d'autres esclaves ». Que cet
asservissement s'exerce à l'encontre d'autres humains ou de
cet « ensemble inerte » que l'on appelle « nature », il s'agit toujours
d'un symptôme et d'une aggravation de ce déchirement inaugural de l'être
et du langage, tel qu'il se manifeste théoriquement au sein de la
tradition rationaliste bourgeoise, tel qu'il se perpétue pratiquement à
travers la division capitaliste du travail et l'extractivisme effréné.

Un certain néonaturalisme tend pourtant à effacer les traces du travail humain,
industriel comme artisanal ; au mieux, il invisibilise les rapports de
production et d'échange, et donc les rapports de classe ; au pire, il
annonce la formation d'une caste de survivants dans le chaos d'un
effondrement civilisationnel ayant déjà débuté. Ce à quoi nous opposons
un *communisme déjà existant*, son inscription dans la matérialité des
rapports de classe, son ouverture sur de nouvelles façons de fabriquer
du commun. 

Toute communauté révolutionnaire doit prendre le risque de son
dépassement. Ce qui suppose non seulement une critique radicale du
naturalisme en tant que produit et condition de la civilisation
capitaliste, mais également un refus de sa version *new age* ; cette
dernière réintroduisant une âme à la Nature en l'ôtant à l'Homme
--- inversion strictement mécanique de la perspective cartésienne et donc
perpétuation d'une conception pétrifiée du rapport entre Nature et
Culture.

*Par-delà nature et culture*, quel rapport ce communisme pourrait-il
entretenir avec le vivant, humain et non humain, avec le terrestre et
l'extra-terrestre, avec l'animé et l'inanimé ? Face au néonaturalisme
qui imprègne si souvent la critique abstraite des systèmes techniques,
Frédéric Neyrat privilégie par exemple l'hypothèse d'une cosmicisation
du communisme en mesure de révéler l'espèce humaine à elle-même, non
plus par le miroir déformant de sa propre puissance prométhéenne, ni
même par le biais d'un nouvel anthropomorphisme aveugle, mais en lui
offrant de se reconnaître comme partie prenante du cosmos.

Ouverture ici sur une autre totalité qui placerait le lointain,
l'inaccessible, le multiple, divers et foisonnant, au cœur de notre
présent. Ce « communisme du lointain », dont Neyrat s'obstine à tracer,
non les contours, mais la forme sans cesse mouvante et projective, ne
fait pas abstraction de nos enjeux proprement terrestres, les ouvre
plutôt sur une reconfiguration de notre rapport à nous-mêmes, à l'espace
et au temps :

> Si un communisme est encore possible, un communisme des terreux,
> c'est-à-dire un communisme pour les damnés de la terre, il se formera à
> partir d'une étrange correspondance avec ce qui est hors
> d'atteinte[^158].

Ce communisme du lointain est donc indissociable d'un communisme pour
les vivants. Et, en effet, il ne s'agit pas tant de fomenter une
nouvelle politique pour le cosmos que de conspirer à la destitution de
notre propre identité souveraine ; les communautés humaines étant ainsi
appelées à devenir autres qu'elles-mêmes, à se nier dans ce devenir, un
devenir qui excède nécessairement toute centralité humaine[^159].

Nous parlons ainsi depuis une raison qui n'est plus la Raison
universelle de l'Occident capitaliste, mais une raison à la fois
cosmique (Benjamin via Neyrat et l'afrofuturisme) et stratégique
(Benjamin via Löwy et Bensaïd), une raison qui vise, même
douloureusement, même tragiquement, la réalisation, au terme d'une
« lente impatience[^160] », du communisme --- une « réalisation » qui ne
peut être la réalisation d'un idéal ni la stricte conservation de
l'existant, mais plutôt sa défense et donc simultanément la libération
de ses potentialités, passées, présentes et futures.

Faire droit à une utopie qui soit à la fois concrète et radicale --- non
plus une belle option politique ou un chant incantatoire, mais une
praxis revêtant une forme réellement révolutionnaire --- implique une
conscience critique à l'égard du « procès civilisationnel » lui-même.
Cette critique doit nous engager conjointement dans une action commune
de défense et d'organisation de l'existant. Mais ce que nous nommons
« défense de l'existant » ne peut se limiter au pur présentisme, à la
stricte actualité des êtres et des groupes sociaux --- il s'agit
également de penser et d'agir « depuis la rédemption[^161] ».

Pour ce faire, l'utopie doit se soumettre à une compréhension
matérialiste des tendances objectives du capitalisme, mais également à
une quête du *dehors*, de l'inconnu, du radicalement autre, et fonder sur
le différend la possibilité d'une communauté (post)humaine
--- « communauté négative » dirait Neyrat, dans le sillon éclairé/éclairant
de Jean-Luc Nancy.

Contre toute attente, Pasolini, dont nous soutenons qu'il fut le poète
d'un *communisme déjà existant*, le « guetteur mélancolique » de mondes
menacés ou tout simplement disparus, esquissa également ce que nous
proposons de nommer sa propre voie du cosmos.

Dès 1963, dans *La Rage* :

> Peut-être le sourire des astronautes : c'est lui, peut-être, le sourire
> de l'espoir véritable, de la paix véritable. Interrompues, ou fermées,
> ou sanglantes les voies de la terre, voici que s'ouvre, timidement, la
> voie du cosmos[^163].

Et il ne cessera d'y revenir, dans *Pétrole* (posthume) notamment :

> Dans mon rêve révélateur, j'ai compris, une fois pour toutes, que ce
> n'est pas la mer qui est notre véritable origine, à savoir le ventre
> maternel \[originaire\] (auquel, de toutes nos forces nous tendons à
> revenir) : notre véritable origine, c'est l'espace. C'est là que nous
> sommes vraiment nés : dans la sphère du cosmos. Dans la mer, nous sommes
> peut-être nés une deuxième fois. Et donc l'attraction de la mer est
> profonde, mais celle de l'espace céleste l'est infiniment
> davantage[^164].

Cette réélaboration de l'origine humaine, de la mer vers le cosmos,
d'une origine terrestre vers une origine extra-terrestre, est
concomitante d'un renoncement non seulement à l'histoire bourgeoise,
mais également à la contre-histoire communiste occidentale. C'est même
depuis ce double renoncement que nous tenterons d'explorer ce que fut la
proposition cosmicisante du poète, en essayant notamment d'expliciter
le différend ou la potentielle affinité qu'elle est susceptible d'entretenir
avec le co(s)mmunisme de Neyrat et l'afrofuturisme dont il se fait le passeur.

Lorsque Pasolini, ce gosse ingrat d'une histoire linguistique et
littéraire qui fut peut-être le seul ferment conséquent de ce qu'il est
permis aujourd'hui de nommer *Italie*, rompt avec la poétique des cendres,
qu'il s'arrache à la nouvelle religiosité de son temps, qu'il « abjure »
également ce temps de la Raison prolétarienne et de l'Espérance creuse,
qu'il autorise sa langue à ne plus s'autoriser que d'elle-même, et non
plus d'un discours sur l'Histoire, qu'il reconnaît enfin dans un sourire
le seul amour non feint, véritable, qui continue de le lier au monde,
par la chair et par le cœur, c'est une double et irrémédiable rupture
qui s'opère : rupture avec l'histoire littéraire bourgeoise (Dante, puis
Leopardi), rupture également avec la contre-histoire littéraire marxiste
(Brecht et ses héritiers).

Même Fortini, le frère, même Brecht, le père, n'ont plus rien à dire.
Dix années ont passé depuis la guerre, dix années d'espérances et
d'illusions, dix années de corruption des États prétendument
communistes, dix années pendant lesquelles le prolétariat a perdu son
monde, son *ethos*, sa culture, dix années qui s'achèvent sur une série de
répressions sanglantes, dix années qui auront suffi à souiller un beau
rêve de communauté universelle en adaptant le projet communiste à la
grande marche de l'Histoire --- c'est-à-dire au règne de la Raison
capitaliste :

> AUCUN DES PROBLÈMES DES ANNÉES CINQUANTE NE M'INTÉRESSE PLUS !
>
> JE TRAHIS LES BLÊMES MORALISTES QUI ONT FAIT DU SOCIALISME UN CATHOLICISME
>
> TOUT AUSSI ENNUYEUX ! AH, AH, L'ENGAGEMENT PROVINCIAL
>
> AH, AH, LES POÈTES QUI RIVALISENT DE RATIONALISME !
>
> LA DROGUE, POUR PROFESSEUR PAUVRE DE L'IDÉOLOGIE !
>
> J'ABJURE CES DIX ANNÉES RIDICULES[^165] !

Il faut admettre que la réalité des années 60 a désavoué la Raison sur
laquelle nous fondions la nécessité de notre communisme --- et c'est donc
ainsi que nous abandonnons « la route que pendant tant d'années nous
avons crue juste, par passion, par ingénuité, par conformisme[^166] ».

Allégorie dantesque de notre désastre contemporain, mais également motif
de réjouissance : l'effondrement du sens, l'abandon de toutes ces cartes
sans usage, le recouvrement des vieux chemins de l'émancipation sous la
broussaille d'un rêve neuf, soustrait aux mythologies fondatrices de
notre bêtise et de notre belle espérance, éclairent enfin les sentiers
biscornus de la posthistoire --- nous sommes libres de nous déployer dans
cette forêt obscure, ici et maintenant, sans flétrir le vivant, sans
humilier personne, sans guetter l'assentiment de nos pères.

Renoncer à un idéal de clarté pour nous attacher au corps trouble et
tentaculaire de la réalité --- broussailles foisonnantes et sentiers
escarpés. Ce qui suppose un renversement épistémologique tel que celui
opéré par Nietzsche à l'égard de la méthode cartésienne :

> Ici, et contre la préconisation de Descartes dans les *Règles pour la
> direction de l'esprit*, la méthode ne part plus du plus simple et du
> plus facile. Renversant la règle cartésienne, elle part au contraire du
> plus saisissant et du plus riche, du phénomène qui donne le plus à
> penser précisément parce qu'il dépasse et excède ce que déjà nous sommes
> en mesure de penser et de saisir[^167].

En finir avec les canons épistémologiques d'un marxisme platement
cartésien, qui délègue à l'Histoire sa vérité, et libérer nos
puissances utopiques, en partant de l'existant. Notre communisme ne peut
plus dépendre de la représentation d'un monde futur libéré de
l'exploitation : il doit être *le mouvement réel qui abolit* déjà
*l'ordre des choses* --- articulation vivante de l'éthique et du
politique, leur dépassement en tant que domaines de pensées et de
pratiques séparés du vivant.

Mais comment faire droit à un concept de *corps collectif* qui ne
porterait pas atteinte au visage de l'*autre* ? Autrement dit : comment
ne plus reproduire une conception de l'engagement comme hypostase d'un
contenu, d'une forme, d'un rêve peut-être, mais arrachés à la réalité,
c'est-à-dire au *divers* et au *différend* ?

Deux types de barbarie, bien distincts, mais dont la rencontre pourrait
être féconde, se manifestent aujourd'hui aux marges d'un monde suffocant :
l'« indigène » et le militant d'un *communisme déjà existant*. Leur
langage ne se laisse pas saisir aisément, ni par la sociologie critique
ni par le pouvoir. Cherchant à imposer sa clarté aux barbares,
c'est-à-dire à les dévoiler et à leur arracher la langue, l'Empire
ajuste le monde à sa guise, lui opposant des formes inédites
d'obéissance qu'il nomme « Défense des Libertés Publiques ».

Aux faux universalismes du pouvoir, nous opposons une dialectique de
l'ombre et de la lumière, une reformulation de la chose politique en
*ethos*, une inscription de cet *ethos* dans la matérialité du monde.

Si nous nous attachons à ces deux figures d'altérité radicale
--- « impossibles à récupérer[^168] » ---, c'est qu'elles sont les
dépositaires d'un langage qui ne se laisse guère absorber dans une
représentation victimaire, qui s'arrache à la commisération et au
discours, aux narrations fétides et aux mises en récit d'une dégradation
volontaire --- un langage qui manifeste ce que nous considérons, *avec*
Pasolini, comme la dernière beauté du monde.

> Or l'un des lieux communs les plus typiques des intellectuels de gauche,
> c'est la volonté de désacraliser la vie et (il faut inventer le mot) de
> désentimentaliser la vie. Chez les vieux intellectuels antifascistes,
> cela s'explique par le fait qu'ils ont été élevés dans une société
> cléricale-fasciste, qui prêchait de fausses sacralités et de faux
> sentiments. Leur réaction était donc juste. Mais le nouveau pouvoir
> d'aujourd'hui n'impose plus cette fausse sacralité et ces faux
> sentiments. C'est même lui le premier qui, je le répète, veut se libérer
> d'eux, avec toutes leurs institutions (l'armée et l'Église, disons.)
> Donc, venant des intellectuels progressistes, qui continuent à rabâcher
> les vieilles conceptions des lumières, comme si elles étaient passées
> dans les sciences humaines, la polémique contre la sacralité et les
> sentiments est inutile. Ou alors, elle est utile au pouvoir[^169].

Pasolini ne prône pas un « retour au sacré », comme on revendiquerait
aujourd'hui un « retour à la terre », il s'efforce de penser depuis la
tradition rationaliste occidentale sans cesser d'en dénoncer les chimères
et les abstractions, les sédiments idéologiques et une tendance à l'éradication
du *différend*. Ainsi il ne saurait être question
d'une abjuration du marxisme en soi, ou encore des puissances de
l'entendement : il s'agit de nous adapter,
de muter, de remettre en question nos certitudes, et jusqu'à cette
espérance sur laquelle nous avons édifié une certaine justification de
nos existences et de nos luttes.

En d'autres termes, l'irrationalisme n'est pas la réponse qu'il
faut apporter au rationalisme bourgeois ; les deux se déployant sur un
terrain binaire et donc appauvrissant. Il faut choisir, insiste Pasolini,
l'intensification de la joute plutôt que la synthèse facile :

> J'ai été rationnel et j'ai été
> 
> irrationnel : jusqu'au bout[^170].

Hegel a permis d'offrir à la philosophie un nouvel objet : le monde
historique et social. En cela, il est bien la condition de possibilité
de Marx --- au prix d'un retour sur terre pour l'Esprit qui s'était
fourvoyé dans les nuées spirituelles de l'Être. Le prolétariat apparaît
alors comme le nouveau gardien de l'histoire : agir sur le monde, ce
n'est plus agir en tant qu'agent aveugle de l'histoire bourgeoise, mais
directement sur les rapports de production et d'échange, les transformer
« communistement », etc.

Avec Marx, la totalité a beau tomber de son piédestal, elle n'en demeure
pas moins déterminée par le point de vue du Capital : c'est une totalité
inféodée au mouvement ascendant de la productivité mondiale. Ce
mouvement suppose en lui-même la destruction des modes de production
spécifiques et l'avènement d'un monopole industriel à l'échelle
mondiale. Ainsi le contrôle de la production, même par un gouvernement
marxiste révolutionnaire, s'il n'implique pas une réorientation radicale
de la production industrielle et une décélération massive de ses rythmes
productifs, s'il ne suppose pas de sauver d'autres modèles productifs
(paysans, artisanaux, etc.), ne peut conduire qu'à la désintégration du
monde classique --- en d'autres termes, à la destruction des liens entre
*production* et *tradition* :

> Quand le monde classique sera épuisé ― quand tous les paysans et les
> artisans seront morts ― quand l'industrie aura rendu inarrêtable le
> cycle de la production et de la consommation ― alors notre histoire
> prendra fin[^171].

En abjurant les années 50, c'est une certaine conception téléologique et
totalitaire de l'Histoire en tant que censée se réaliser sous la forme d'une société
plus humaine --- le socialisme --- que le poète rejette. Il en rejette
non seulement la raison (progressiste et aveugle), mais également
l'espérance (*nous avons tant chanté ce monde qui n'adviendra pas*),
toutes deux au service d'un développement infini des forces productives
et de ses contre-finalités dévastatrices.

À rebours de la gauche progressiste, l'*utopie concrète* que nous
proposons de reconsidérer, tout en partant de l'existant, quête la
bifurcation, et non un retour sur l'ancienne voie perdue. Elle n'est ni
normative ni totalitaire[^172], et suppose, ainsi que le propose
Adorno, une réintégration de la limite kantienne, ce quelque chose qui
borne nécessairement la connaissance humaine, à la fois lié à la
structure de l'entendement et à la dualité de la conscience et du monde,
du désir de connaître et de l'objet de la connaissance[^173].

Ce « retour à Kant » (critique, limite et conditions de la connaissance)
appliqué à une nouvelle épistémologie d'inspiration marxiste se justifie
afin de garder les pieds sur terre --- littéralement : pour ne pas
disparaître.

Non un retour en amont de Hegel-Marx, mais plutôt un enrichissement
de la dialectique matérialiste par la critique de la totalité et de la
positivité annihilatrice (la synthèse dialectique) si chère à Hegel et à
ses héritiers marxistes orthodoxes. Enrichie par les magnifiques efforts
de Lukács, « petite colombe parmi les sphinx[^174] », pour repenser
une ontologie dynamique et inachevable, notre critique de la
totalité[^175] ne doit pas déboucher sur l'impossibilité pour un sujet
de se rapporter au monde, de le penser et de le transformer. Elle doit
au contraire nous permettre de repenser le rapport de l'humain au vivant,
à la terre et au cosmos, en assumant seulement une part nécessaire
d'insu et d'irréductible altérité --- faisant précisément défaut à la
philosophie de Lukács, qu'il s'agisse de son exégèse des philosophies
dites irrationnelles[^176], ou encore de son ontologie tardive[^177].

Cela suppose de relire Marx différemment, de guetter la « parole
muette, pas la clarté[^178] », de redéfinir la totalité historique à partir de ses angles morts, de ses failles, de ses impasses, de repenser intégralement le
statut de la Raison et de son Sujet.

Dès *La Religion de notre temps* (1961), soutenant une telle intention
philosophique sur le terrain poétique, Pasolini acte ainsi le
décentrement du sujet révolutionnaire, depuis l'Europe vers l'Afrique :

> Afrique ! Ma seule
>
> alternative.......................
>
> .................................................[^179]

Trois ans plus tard, il insiste :

> La négritude, dis-je, qui sera raison[^180].

Avant de parvenir à saisir ce déplacement de la raison révolutionnaire,
de la Raison universelle occidentale vers la « négritude », revenons au
mouvement dialectique inhérent à la poétique pasolinienne. 

Ici, trois extraits de *La Religion de notre temps* faisant état de ce
mouvement --- il s'agit de trois moments d'une beauté remarquable qui
amorcent un dialogue créatif débouchant sur la nécessité de faire
advenir un langage neuf :

&nbsp;

1)

> Si le soleil revient, si tombe le soir,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>si la nuit a un goût de nuits à venir,
>
> si un après-midi de pluie semble revenir
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>d'époques trop aimées et jamais entièrement eues,
>
> je ne suis plus heureux, ni d'en jouir ni d'en souffrir ;
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>je ne sens plus, devant moi, toute la vie...
>
> Pour être poètes, il faut avoir beaucoup de temps :
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>des heures et des heures de solitude sont le seul moyen
>
> pour que quelque chose se forme, qui est force, abandon,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>vice, liberté, pour donner du style au chaos.
>
> Moi maintenant j'en ai peu : à cause de la mort
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>qui s'avance, au crépuscule de la jeunesse.
>
> Mais aussi à cause de ce monde inhumain qui est le nôtre,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>qui enlève le pain aux pauvres et la paix aux poètes[^181].

2)

> Dans ce monde coupable qui ne fait qu'acheter et mépriser,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>le plus coupable, c'est moi, desséché par l'amertume[^182].

3)

> Maintenant je sens en moi un goût de pluie qui vient de tomber,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>toute vivacité de la vie a un fond de larmes :
>
> seule une force confuse me dit qu'un nouveau temps
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>commence pour tous et nous oblige à être nouveaux.
>
> Peut-être --- pour qui a senti et s'est donné --- c'est l'engagement
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>non plus à sentir et à se donner, mais à penser et à se chercher
>
> si le monde commence à cesser d'être le monde
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>dans lequel, lui appartenant déjà, nous sommes nés, objet d'histoire
>
> d'abord cru éternel, puis fertile : toujours reconnu.
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Mais même le temps de la vie est de penser, et non de vivre,
>
> et puis la pensée est maintenant privée de méthode et de mots,
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>lumière et confusion, préfiguration et fin,
>
> la pure vie elle-même est en train de se dissoudre dans le monde.
>
> <span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Donquichottesques et durs, nous agressons la nouvelle langue
>
> que nous ne connaissons pas encore, que nous devons tenter[^183].

Exemple, ici, d'un mouvement dialectique dont le terme ne serait guère
conclusif, mais ouvert sur un possible irréductible à la synthèse ou à
la réconciliation :

> ... il faut sacrifier la cohérence
>
> à l'incohérence de la vie, tenter un dialogue
>
> créatif, même contre notre conscience[^184].

La défaite de la Raison (morale, politique, esthétique) implique de
tenter quelque chose de nouveau ; cette tentative préparant le
« transhumanisme » --- ou nouvel humanisme cosmicisé --- de l'œuvre
poétique ultérieure :

> ... Et moi, dans mon petit recoin,
>
> sous le beau soleil du monde,
>
> arabe ou chrétien,
>
> de la Méditerranée ou de l'Océan Indien,
>
> inadapté à l'histoire, inadapté à moi,
>
> je m'adapterai à la terre future,
>
> quand la Culture redeviendra Nature[^185].

Ce transhumanisme se soutient bien d'une raison dé-coloniale : implosion
de l'opposition Nature/Culture, figuration de l'Afrique comme terre de
la réconciliation future, etc. Précisons cependant que cette figuration
ne peut être (ici nous excédons assez largement la figuration utopique
de l'Afrique telle que Pasolini la présente dans ce poème) pensée comme
naturalisation unilatérale de la culture : si la Culture est appelée à
« redevenir » Nature, la Nature doit également « redevenir » Culture. Et
cela en un sens très concret : l'agriculture vivrière peut se
réapproprier des terres, des terres qui pourront ainsi échapper aux
industries lourdes et à l'altération capitaliste des conditions
d'habitabilité sur terre.

Mais tout cela ne suffit pas. Cet horizon utopique ne peut être
qu'abolitionniste (suppression du centralisme blanc) et projectif
(interroger notre présent depuis le futur). Le « co(s)mmunisme » de
Neyrat a notamment trouvé dans l'afrofuturisme, non des réponses, mais
une façon radicalement autre de penser notre présent --- c'est-à-dire
conjointement « notre » salut et celui des générations passées :

> L'Afrofuturisme exprime en effet avec mille voix le message suivant :
> les révolutions antérieures n'ont pas suffi. Alors celle qui doit venir
> doit être plus ample, plus intense encore, elle doit être planétaire,
> c'est-à-dire toucher à la planétarité de la Terre, son écologie générale
> et son inscription cosmique. C'est trop demander ? Il fallait y penser
> avant ; désormais, demander l'impossible est la moindre des choses, la
> seule issue possible --- voilà ce que disent les Anges Noirs de
> l'histoire, ces intercesseurs entre futur et passé, espace
> extra-terrestre et terrestre, désespoir et révolte[^186].

Exiger plus, exiger l'impossible : il ne s'agit pas d'une option
dans le champ des possibles, ni même d'un imaginaire inédit ou
postmoderne, mais d'une nouvelle hybridation du langage depuis laquelle
cette exigence est formulée ; langage-mutant exigeant de l'humanité tout
entière qu'elle envisage son propre dépassement, hors des coordonnées de
la Raison occidentale, blanche et racialiste.

Que l'on s'entende bien sur ce que le terme de « transhumanité »
recouvre : il ne s'agit pas de nier l'humain en l'humain, mais de
guetter en l'humain une part d'inhumanité, sa part maudite, barbare ou
divine, cette part qui échappe à l'emprise de l'Homme sur les humains et
sur leurs milieux de vie, qui excède donc la Raison instrumentale et
toute forme d'emprise intellectuelle et physique d'une certaine idée de
l'Homme sur une certaine idée de ce qu'il n'est pas.

D’une façon analogue, la perspective posthumaniste (et indissociablement postnaturaliste donc), qui s’esquisse notamment dans ces quelques vers de Pasolini, invite à concevoir une déprise de l'Homme majeur et éclairé, une
implosion du mythe sur lequel celui-ci fonde son pouvoir et sa mission
historique, celle de guider l'humanité générique sur les chemins de la
Liberté et du Progrès. Une désolidarisation s'opère ainsi entre
l'histoire bourgeoise et ses propres enfants. C'est une raison sensible
du communisme qui arme les consciences et guide nos luttes : non plus
une représentation d'un monde-déjà-mort, mais une image de notre salut
qui se recompose sur les ruines de l'opposition bourgeoise entre Nature
et Culture, au nom de leur inséparabilité originelle. 

Rien, au fond, qui ne renvoie encore au projet de Marx lui-même, celui
d'une critique de la conscience théorique et morale de la bourgeoisie,
critique jointe à la volonté révolutionnaire de faire advenir une
humanité libérée des entraves induites par la division capitaliste du
travail --- ce qui ne peut que signifier un dépassement de l'opposition
Nature/Culture et de toutes ses oppositions dérivées (Travail
manuel/Travail intellectuel, Ville/Campagne, etc.).

Si naturalisme et néonaturalisme sont solidaires de ce « sentiment
bourgeois de la nature » qui caractérise l'ère industrielle et
capitaliste, qu'il s'agit donc de les combattre, il s'agit conjointement
de destituer la Culture en tant que patrimoine de vieilles valeurs
mensongères. Non de l'abolir, mais de la transformer radicalement. Ce
pour quoi, si nous avons pu parler de double rupture (avec la culture
bourgeoise, avec la contre-culture communiste), celle-ci doit déboucher
sur un usage radicalement autre de l'histoire littéraire et artistique
dont nous avons hérité : fragmentation des Histoires de l'Art et de la
Littérature, braconnage, hybridation, montage, etc.

Cette destitution de la Culture engageant notamment la destitution
de la « belle figure de l'auteur » :

> Destitué de mon autorité, auteur
>
> qui n'est plus indispensable, chargé
>
> de poésie sans être plus poète,
>
> (la condition de poète cesse
>
> quand le mythe des hommes
>
> déchoit... et les instruments sont autres
>
> pour communiquer avec ses pareils... d'ailleurs
>
> mieux vaut se taire, en préfigurant,
>
> dans un désœuvrement narcissique, la paix dernière)
>
> --- je suis de nouveau au chômage, moi,
>
> un garçon aux mauvaises et naïves lectures
>
> qui écrit par vengeance (contre lui-même)
>
> et offre un corps de martyr aux indifférents[^187].

De cette nouvelle condition anonyme, Pasolini saura tirer une conclusion
programmatique, pour son art propre, et pour l'art en général. 

C'est par excès de réalité que la forme se décompose et se recompose
--- un excès qui se fait soustraction à l'ordre du discours et au régime de
la représentation. Qu'il s'agisse du cinéma, d'abord, puis, à l'aune des
années 70, d'une phase absolument neuve de son travail d'écriture
poétique, l'horizon destituant de son art ne saurait se fonder sur autre
chose que la réalité elle-même, nue, éclatante, profane. Et cette
réalité par excès emporte dans son mouvement convulsif et quasi
mystique l'auteur lui-même, le créateur, l'intellectuel, l'invite à se
diluer, à se réinventer en abandonnant le prophétisme des *Cendres*, à
se faire « martyr » d'une cause qui excède assez largement le cadre
strictement temporel de la lutte. Si le prophète assignait au monde une
fin, même négative (« notre histoire est finie »), le martyr agit en
méconnaissant la signification historique de son sacrifice (un « corps »
offert « aux indifférents »), acte de résistance qui ne sera payé
d'aucun revenu terrestre.

Ainsi ce n'est pas en se cherchant une origine sociale, factice, que
l'intellectuel communiste sera en mesure de faire monde avec les
« barbares », mais en destituant par tous les moyens possibles
l'imaginaire du pouvoir auquel il est organiquement lié en tant que
produit de la division capitaliste du travail. Ce qui constitue
peut-être un geste impossible, mais cela implique alors que cette
impossibilité appelle une nécessaire destitution de l'intellectuel
bourgeois lui-même. 

Rien cependant qui ne soit abolition de l'intellectuel en soi, ou de
ce qu'il y a d'intellectuel en chacun --- en ce sens, Pasolini demeure
fidèle à Gramsci :

> En réalité, lorsqu'on fait une distinction entre intellectuels et
> non-intellectuels, on fait uniquement référence à la fonction sociale
> immédiate de la catégorie professionnelle des intellectuels, autrement
> dit on prend en considération la direction dans laquelle une activité
> professionnelle spécifique pèse du poids le plus lourd : celle de
> l'élaboration intellectuelle ou celle de l'effort musculaire et nerveux.
> Ce qui signifie que s'il est possible de parler d'intellectuels, il
> n'est pas possible de parler de non-intellectuels, parce qu'il n'existe
> pas de non-intellectuels[^188].

Selon Gramsci, le monopole de l'intelligence théorique n'est qu'un effet
des structures hiérarchiques de la division capitaliste du travail. Non
seulement chaque humain *est* un intellectuel, mais la relégitimation des
intelligences prolétaires, celles des travailleurs manuels, des
travailleurs en tout genre, techniciens ou mains-d'œuvre non qualifiées,
doit nous permettre de recomposer des intelligences collectives :

> Il n'existe pas d'activité humaine dont on puisse exclure tout à fait
> l'intervention intellectuelle, il n'est pas possible de séparer l'*Homo
> faber* de l'*Homo sapiens*. Tout homme, enfin, exerce indépendamment de
> sa profession, une certaine activité intellectuelle, il est, autrement
> dit, un « philosophe », un artiste, un homme de goût, il participe d'une
> conception du monde, il possède consciemment une ligne de conduite
> morale, et donc il contribue au maintien d'une conception du monde ou à
> sa transformation, c'est-à-dire à la production de nouvelles façons de
> penser[^189].

Ces « nouvelles façons de penser », avec des outils, avec des
techniques, avec des matériaux non théoriques, sont une richesse dont
nous sommes souvent amputés. À travers elles, l'intellectuel
révolutionnaire ne peut que s'hybrider, se réinventer, nouer de
nouvelles alliances entre ses formes et la matière dont se compose le
monde, assumer le vertige de l'impensé, un écart et donc un rapport
nouveau à lui-même et au cosmos.

En abandonnant son
autorité morale, l'intellectuel révolutionnaire renonce à tous les
magistères, représentant ou censeur, porte-parole du peuple ou arbitre
des folies du monde ;
l'intellectuel bourgeois, quant à lui, a fait le tour du cadran :
ses valeurs, sa langue, cet universel vide dont il se gargarise afin de
nous empêcher de nuire, cette fausse réalité dont il est le plus ardent
défenseur --- sans nécessairement en prendre conscience, ce qui est un
comble pour un technicien de la pensée --- ne doivent plus nous
illusionner.

Mais il ne s'agira pas de reproduire les erreurs du passé : notre
rapport dialectique à l'histoire et à la culture implique le refus des
tables rases. Il s'agira donc, sur le terrain de la production
matérielle tout comme sur celui de la production intellectuelle,
d'exproprier les expropriateurs. De réinventer des usages.

C'est précisément parce que nous adhérons assez largement à la
nécessité de produire des usages offensifs de la philosophie ou de la
littérature --- « il est grand temps que la pensée redevienne ce qu'elle
est en réalité, dangereuse pour le penseur et transformatrice du
réel[^190] » --- que nous n'hésitons pas à trahir Pasolini en le
confrontant à une perspective révolutionnaire qui, tout en l'excédant,
l'éclaire simultanément sous un jour neuf.

Par ailleurs, l'intellectuel aura beau se faire le plus ardent
commentateur des effets de l'exploitation capitaliste, de la réification
tout terrain, des différentes formes d'oppression, du « désastre
écologique », etc., la critique seule ne peut rien si elle ne se lie pas
conjointement à des perspectives concrètes d'émancipation. Si nous
investissons le champ de la pensée et de l'art, ce n'est donc absolument
pas pour les laisser indemnes, mais bien pour *en faire* quelque chose
de radicalement autre.

Rien qui soit entièrement subordonné aux exigences de la lutte, mais
rien non plus qui leur soit complètement étranger : l'art et la pensée
sont des champs stratégiques où s'esquisse la possibilité de leur
dépassement en tant que domaines de pratiques séparés du vivant. Si
Pasolini figure ce vivant par un sourire ou quelque luciole, libre à
chacun d'inventer ses figures, de les organiser comme des images ou
comme des textes, des idées ou des projectiles.

Dans un long et beau poème faisant office de conclusion à *Poésie
en forme de rose* (1964), Pasolini acte de nouveau la mort d'un projet
d'émancipation sous la figure tutélaire de la Raison :

> ... LA RÉVOLUTION EST FRAPPÉE DE STÉRILITÉ
>
> SI ELLE N'A JAMAIS LA VICTOIRE[^191]...

Contre toute résignation, il s'agit d'assumer l'effondrement de la
Raison bourgeoise et de réélaborer conjointement un concept d'utopie
dont les visées soient à la fois posthumaines et cosmicisantes. Si
l'afrofuturisme exige l'impossible, le *communisme déjà existant*
investit la réalité comme un champ d'immanence où fleurit concrètement
la possibilité d'un autre rapport au cosmos.
La voie des afrofuturistes invente une esthétique depuis le futur, une
esthétique de demain pour aujourd'hui ; celle du *communisme déjà
existant* conspire depuis les ruines du capitalisme et invente, ici et
maintenant, de nouvelles façons de fabriquer du commun. Ces deux voies
nous semblent complémentaires. Mais comment, sur le terrain politique,
ou par-delà toute politique, pourraient-elles nouer une judicieuse
alliance ? Rien n'est écrit. Nous savons seulement qu'une telle alliance
ne peut advenir que dans le dépassement de cette Raison strictement
humaine, essentiellement instrumentale, qui assigne à l'Homme,
occidental, blanc, le magistère de l'Histoire.

À ce titre, l'ordre humain n'a pas à être libéré du Capital, ni du
racisme : l'hypothèse des afrofuturistes et des afropessimistes étant
que cet ordre est lui-même fondé sur l'esclavage. C'est donc le cosmos
qu'il s'agit de libérer de l'« ordre humain ». Ce qui ne revient pas à
abolir l'humain, mais à guetter ce qu'il peut y avoir d'inhumain en
lui, « ce qui empêche l'être humain de se prendre pour lui-même », « ce
qui défait en son principe l'idée d'une identité-de-l'Homme[^193]. »

Comment ne pas hésiter sur les moyens, dès lors que les fins (humaines,
posthumaines, terrestres, extra-terrestres) demeurent incertaines ? Si
cette éthique du doute n'est pas pour nous déplaire, c'est qu'elle
justifie un mouvement de déconstruction de nos certitudes
anthropocentriques et de nos inconscients (post-)coloniaux. Elle soutient
conjointement le désir d'un soulèvement humain qui s'initierait par-delà
le strict intérêt de l'espèce. Par-delà toute nécessité de conserver
l'ordre qui opprime l'existant et donc également le possible, humain et
non humain. À ce titre, le leitmotiv des *Soulèvements de la terre* est
singulièrement prometteur : « Nous ne souhaitons pas prendre soin de la
nature, nous sommes la nature qui reprend ses droits. »

L'opposition au capitalisme ne peut se permettre d'être seulement
réactive : il ne faut pas renoncer à la possibilité de gagner des
espaces de liberté non inféodés à la bourgeoisie. Et puisqu'il ne nous
paraît guère raisonnable de miser sur une accélération suicidaire du
développement capitaliste, qui mènerait à une inévitable apocalypse, il
s'agira donc d'imposer une décélération des procès de production, un
déplacement de la Raison progressiste vers une nouvelle raison
stratégique, un réoutillage conscient et raisonné de nos luttes, cela
afin que continuent de croître et de se développer les forces conjuguées
de l'esprit et de la matière --- dans les limites mêmes que nous saurons
imposer aux tendances unilatéralement destructrices de notre temps.

Une telle perspective de luttes, qui se déploie depuis des conditions
inégalitaires d'habitabilité, ne peut plus faire l'économie de sa propre
voie du cosmos. Non un délire de « margoulin », mais une réélaboration
de ses fins : de l'un au multiple, du centre vers la périphérie, du
global au local, il s'agit de « relocaliser nos démesures[^194] », de
nous trouver un chemin humain et bassement matériel vers l'infini. Pour
ce faire, on peut guetter la survenue d'un ange noir de l'histoire qui
libérerait le cosmos de l'impérialisme blanc. Dans la mesure où nos
conditions sociales nous le permettent, depuis les ruines du capitalisme
occidental donc, on peut également réapprendre à s'oublier dans le
langage des choses et fonder ici et maintenant de nouvelles façons de
*faire commun* : « Tout le cosmos était là[^194bis]... »

</div>

<div class="text" data-number="8">

## 9. Le lumpen, maladie infantile de la littérature ?

Longtemps Marx fut le nom d'un sursaut dialectique : dépassement *et*
réalisation de la Philosophie d'abord, puis conquête *et* dépérissement
de l'État bourgeois, et enfin implosion de la Politique elle-même *et*
requalification de sa visée en émancipation humaine libérée des mirages
du faux humanisme. À ce titre, les utopistes qui le précédèrent, les
anarchistes dont il était le contemporain furent renvoyés à leur
impuissance, à leur manque de lucidité stratégique ou à leurs chimères.

Mais Marx n'était ni un Dieu ni un prophète : si l'exigence, la rigueur,
la justesse avec lesquelles il tenta de décrire les mécanismes du
capitalisme et de la société bourgeoise ont armé nos esprits en plaçant
dans nos mains un inestimable outil d'analyse et de prospective
stratégique, il est temps de réactualiser sa radicalité, c'est-à-dire
également de le lire autrement.

L'un des points d'achoppement de sa théorie touche sans doute à sa
catégorie de sujet révolutionnaire ; schématiquement, celle-ci fut
circonscrite par lui et la tradition marxiste de la façon suivante :

1) toute société est divisée en classes sociales ;

2) ce sont les rapports de propriété qui déterminent ces classes ;

3) ce sont conjointement les rapports de production et d'échange ;

4) ce sont également les rapports que les individus et les groupes
sociaux entretiennent, objectivement et subjectivement, avec l'État
(apport de Lénine notamment) ;

5) la classe exploiteuse ne vise qu'un intérêt particulier qui entre
nécessairement en contradiction avec l'intérêt universel des sociétés
humaines ;

6) quant à l'intérêt de la classe exploitée, celui-ci se confond avec
cet intérêt universel ;

7) c'est donc au prolétariat, à celles et ceux qui n'ont que leur force
de travail pour vivre ou pour survivre, organisé en classe consciente
d'elle-même et de son intérêt, que revient la mission historique de
renverser la classe exploiteuse et de faire advenir le communisme (sous
la forme d'une *communauté d'individus librement associés*).

L'orthodoxie consiste à supposer l'invariance de la théorie et donc à
admettre l'invariance du monde qu'elle a pour vocation d'objectiver sous
la forme d'un savoir irrévocable. Mais celle-ci conduit à une distorsion
du monde qu'elle croit connaître. Ainsi la théorie marxiste orthodoxe
suppose un prolétariat unifié par des conditions de production communes ;
elle suppose conjointement un sous-prolétariat qui serait beaucoup
trop fragmenté, impuissant, acculturé pour s'émanciper par lui-même.
C'est donc au prolétariat seul, à toutes celles et à tous ceux qui sont
en mesure de « peser » dans les rapports de production et d'échange,
qu'il revient de libérer le monde de l'exploitation capitaliste.

Trois facteurs objectifs contribuent pourtant à déjuger ces affirmations :

1) l'inégal développement des forces productives à différents endroits
du monde (faisant aujourd'hui qu'un ouvrier dans une usine de textile en
Inde a moins le pouvoir de « peser » dans les rapports de production et
d'échange qu'un ouvrier syndiqué dans la pétrochimie en France) ;

2) les rapports de domination (post-)coloniaux qui déterminent
l'amélioration des conditions de vie et de travail d'un prolétariat
national par rapport à un autre ;

3) la masse des travailleurs improductifs, des chômeurs, des
travailleurs sans travail, dans les sociétés capitalistes avancées
notamment, qui sont exclus *de facto* des rapports de force
travail/capital.

Le propre du néocapitalisme, en sa structuration transnationale, avec
ses modes d'implantation diversifiés, est de jouer sur ces inégalités de
développement afin d'asseoir sa domination impériale
tout en confortant son hégémonie politique au sein des principales
puissances industrielles avancées.

Quand Pasolini évoque ce « nouveau pouvoir », il s'agit toujours d'un
système de domination totalitaire, à la fois économique, politique et
culturel --- essayons donc d'approcher la dynamique néocapitaliste à
partir de ces trois aspects, en les isolant artificiellement pour un
temps :

--- *son aspect économique* : les modes de production et d'échange
déterminent la recherche scientifique et soumettent ses applications au
développement infini des forces productives ;

--- *son aspect politique* : modes d'intervention étatique
(contre-réformes libérales) et guerres impérialistes ;

--- *son aspect culturel* : hégémonie de l'idéologie consumériste et
intégration des individus et des groupes sociaux à l'unidimensionnalité
de la société bourgeoise occidentale.

Cette mutation de la nature du pouvoir implique une intégration plus
profonde, plus efficace, des prolétariats à la société bourgeoise et au
capitalisme mondialisé ; cette intégration contribuant à homogénéiser la
société tout entière en diluant les cultures ouvrières, paysannes,
sous-prolétaires dans le modèle et le style de vie petit-bourgeois.

Autrement dit : la civilisation néocapitaliste suppose l'intégration des
individus et des groupes sociaux à des normes économiques,
institutionnelles, juridiques et culturelles sans cesse ajustées aux
exigences du marché ; une telle intégration mobilisant, afin de se
justifier, une promesse de bonheur quantifiable et l'assurance d'une
juste répartition du désastre à venir.

Ce modèle et ce style de vie constituent un motif d'espérance pour une
majorité d'individus sur terre ; l'idéologie dominante ayant pour
fonction d'entretenir cette espérance, de masquer les préjudices que ce
modèle fait subir aux écosystèmes humains et naturels, préjudices
refoulés par le pouvoir, et ce pour des raisons évidentes, mais
également par les « masses », c'est-à-dire par des individus et des
groupes sociaux atomisés et intégrés *en tant que tels* à la marche de
l'Histoire.

Ce phénomène massif d'intégration conduit un philosophe comme
Herbert Marcuse à repenser entièrement la question du sujet
révolutionnaire ; abandonnant le postulat marxien d'un prolétariat qui
serait *par essence* révolutionnaire, ce dernier fait valoir la
radicalité de toutes celles et ceux qui échappent objectivement et/ou
subjectivement à cette intégration.

Marcuse justifie un tel décentrement du sujet révolutionnaire par le constat d'une inféodation de l’économie libidinale des individus à un type de rationalité strictement économique qui contribuerait à ajuster leur désir et leur mode de vie aux nouvelles normes de la production et des échanges ; sa mobilisation de la théorie freudienne, son articulation à la théorie marxienne visant précisément à rendre compte de la nature intégriste et totalitaire d’une société néocapitaliste dont les corps et les esprits seraient soumis à un *principe de rendement* en tant que « forme spécifique du principe de réalité dans la société moderne[^195] ».

Paul Mattick estime que Marcuse s'est fourvoyé en actant la
disparition du sujet révolutionnaire. En désignant l'individu lui-même
comme sujet et objet unilatéraux du développement capitaliste, ce
dernier aurait forclos la possibilité de son renversement. Mattick
cherche à démontrer que le sujet révolutionnaire n'est jamais réductible
à une somme d'individus, qu'en tant que classe, consciente d'elle-même
et organisée en conséquence, celui-ci sera toujours en mesure de refuser
les nouvelles formes de son oppression. D'autant plus que cette intégration
est nécessairement limitée du fait de la baisse tendancielle du taux de
profit qui conduit les sociétés capitalistes au déclin, à la
paupérisation, au chômage de masse ou même au déclassement de la
petite-bourgeoisie :

> Sans doute les raisons ne manquent pas de penser que rien n'ébranlera
> les masses laborieuses, qu'elles préféreront la misère à la lutte contre
> le système qui en est la cause. Mais l'absence de conscience
> révolutionnaire ne signifie pas l'absence de lucidité. Il est par
> conséquent beaucoup plus vraisemblable que la classe ouvrière
> n'acceptera pas à l'infini le destin que le système capitaliste lui
> réserve ; un point de rupture peut être atteint, à partir duquel la
> conscience de classe viendra s'unir à la lucidité. L'apparition d'une
> volonté révolutionnaire, le passage à l'action autonome, ne seront pas
> forcément précédés d'une longue période d'opposition résolue, de tous
> les instants. Apathique dans certaines conditions, la classe ouvrière
> peut se révolter dans d'autres. Et parce qu'elle est vouée à souffrir
> plus que les autres classes sociales des graves à-coups de la production
> de capital et des entreprises guerrières de la classe dirigeante, elle
> sera selon toute probabilité la première à briser l'idéologie
> unidimensionnelle inhérente au règne du capital[^196].

On ne sait si Pasolini a lu Marcuse. Au regard de sa critique acerbe de
la nouvelle jeunesse militante, on pourrait rapprocher le poète italien
d'Adorno, qui se défend, dans une lettre à l'adresse de Beckett, contre
la « Marcusejungend[^197] », son arrogance petite-bourgeoise et ses
illusions libérales-libertaires. Et pourtant, tout, dans l'œuvre
polémique de Pasolini, de la fin des années 60 et jusqu'à sa mort,
semble faire écho à la théorie freudo-marxienne de Marcuse ainsi qu'à
son concept d'unidimensionnalité des sociétés néocapitalistes[^198].

Contrairement à Mattick, Pasolini ne croit plus que la classe ouvrière,
du fait de son intégration (même douloureuse) à la société consumériste,
soit en mesure d'émanciper toute la société en faisant advenir une
société sans classes. Cette hypothèse serait celle d'un marxisme empêtré
dans son invariance théorique.

À vrai dire --- et c'est ici ce qui le distingue également de Marcuse ---
le point de départ de Pasolini n'est jamais la classe ouvrière *en soi*
ni *pour soi* : son amour va toujours aux rebuts du marxisme orthodoxe,
paysans frioulans et sous-prolétaires des faubourgs de Rome. 

Autre élément qui distingue Pasolini de Marcuse : sa conception des
marges est plus ancrée socialement ; il ne s'agit pas de ceux et celles
qui auraient choisi de se soustraire volontairement aux rapports légaux
de production et d'échange, mais bien de ceux et celles qui en sont
exclus *de facto* par les configurations territoriales, culturelles,
économiques et plus généralement sociales du nouveau capitalisme.

Afin de nuancer la portée stratégique des interventions de Pasolini,
ajoutons que l'« être de classe » de ces sous-prolétaires de Pigneto
ou de Rebibbia n'est pas mécaniquement corrélé à leur potentialité
politique. À moins de considérer cette exclusion de la culture
bourgeoise ou la perpétuation de formes de vie spécifiques comme des
phénomènes dotés d'une « signification » éthique et ayant de façon
seulement dérivée une dimension proprement politique.

Par ailleurs, dans ses nombreux articles polémiques, le poète revient
sur le déclin de ces « pures formes de vie » ; dix ans après *Accattone*,
remarque-t-il, les sourires, l'insouciance, l'insolence même font
désormais place à des postures mimétiques :

> Ils sont tristes, névrosés, incertains, pleins d'une appréhension
> petite-bourgeoise ; ils ont honte d'être ouvriers ; ils cherchent à
> imiter les « fils à papa », les « imbéciles heureux ». Oui, aujourd'hui
> on assiste à la revanche et au triomphe des « fils à papa », ce sont eux
> qui représentent aujourd'hui le modèle-guide[^199].

Ce que Pasolini ne peut que déplorer :

> Alors que ceux-ci étaient fiers de ce qu'ils étaient eux-mêmes : de leur
> « culture », qui leur donnait des gestes, une mimique, des mots, un
> comportement, un savoir, des critères de jugement[^200].

Avant de conclure :

> Accattone et ses amis sont allés au-devant de la déportation et de la
> solution finale en silence, peut-être en riant de leurs
> bourreaux[^201].

Un certain type de violence nihiliste --- typiquement petite-bourgeoise ---
aurait même remplacé la petite violence ordinaire, cette brutalité
justifiée à laquelle cette classe pouvait préalablement se livrer :

> ... tous leurs défauts me semblaient être non seulement humains,
> pardonnables, mais aussi parfaitement justifiables d'un point de vue
> social. Les défauts des hommes qui se réfèrent à une échelle de valeurs
> « autre » par rapport à celle de la bourgeoisie : des hommes qui sont
> « eux-mêmes » d'une manière absolue[^202]...

Ainsi, non seulement ses conditions économiques ne se sont pas
améliorées, mais le sous-prolétariat des faubourgs a sacrifié sa langue
et sa culture :

> Leur connotation de classe est donc maintenant purement économique et
> non plus *également* culturelle[^203].

Mattick répondrait sans doute à Pasolini que ce sont toujours les
conditions économiques qui déterminent la propension des classes
subalternes à se révolter, et non leur *ethos*. En d'autres termes, la
transformation des conditions économiques d'une société produirait
toujours simultanément de nouvelles possibilités matérielles pour une
classe d'opprimés de se reconnaître et de s'organiser.

Ce à quoi il nous est permis de répondre que les nouvelles formes
dominantes du salariat (primauté du secteur tertiaire notamment),
l'application de nouvelles techniques de management (la fameuse
polyvalence prônée par des firmes comme *Lidl*), le sentiment croissant
de se sentir un boulon remplaçable au sein d'une organisation productive
qui nous échappe (lié notamment au chômage de masse au sein de
« sociétés de travailleurs sans travail[^204] »), l'hyperspécialisation
technique induite par le développement de la division capitaliste du
travail, l'ajustement des institutions éducatives aux valeurs et aux
normes néolibérales, etc., peuvent contribuer à « unidimensionnaliser »
l'existence sociale et donc à neutraliser toute possibilité concrète
d'un renversement du capitalisme.

Si Marcuse et Pasolini s'accordent sur l'idée que l'espace d'une
radicalité politique et/ou d'une autonomie culturelle ne peut désormais
se déployer que dans les marges de la société, ils ne partagent guère
cependant la même lecture « sociologique » de celles-ci.

Pour Marcuse, la radicalité politique, indissociable d'une
contre-culture (de « gauche »), se recompose à la lisière de la
petite-bourgeoisie et du sous-prolétariat : ce sont les chômeurs, les
travailleurs sans travail ou travailleurs improductifs, les artistes,
les franges déclassées ou dissidentes de la petite-bourgeoisie, etc.

Pour Pasolini, ce sont les sous-prolétaires romains des
années 60, notamment, qui donnent corps à une forme réelle d'autonomie
culturelle, non seulement parce qu'ils seraient en lutte (consciente)
contre l'hégémonie petite-bourgeoise, mais du simple fait de leur
rapport au monde, de leur existence libre et sans entrave, de leurs
joies inaccaparables.

L'un des personnages des *Détectives sauvages* de Bolaño suggère que si
la littérature fricote avec le lumpen[^205], ce n'est pas seulement par
volonté mais parce que la recomposition sociale des sociétés
néocapitalistes tend à unir sur le plan culturel et économique deux
classes anciennement séparées. Mais cette unification n'est pas une
nouvelle bohème : les conditions de vie ou de survie du sous-prolétariat
se sont encore dégradées ; quant aux artistes et aux intellectuels
petits-bourgeois, leur précarité est croissante.

Dans tous les cas, l'espace de leur rencontre n'est pas vierge de
conflictualités : au sein des centres urbains en voie de gentrification,
par exemple, la petite-bourgeoisie, même déclassée, même précarisée,
aura toujours la supériorité économique et l'avantage symbolique lui
permettant de se « distinguer ».

*A contrario*, dans ce genre de configurations socio-territoriales,
l'hégémonie culturelle semble tendanciellement du côté du
sous-prolétariat, et ce pour des raisons évidentes : la
petite-bourgeoisie occidentale, déclassée ou non, est la classe la plus
acculturée de l'histoire ; tandis que les cultures populaires ne cessent
de manifester leur attachement à des traditions, à une religion ou à une
langue menacées, et même disparues, mais qu'elles s'évertuent alors à
ressusciter tel un vieux trésor.

Dans un texte[^206] que nous qualifions aisément de raciste,
Bourdieu écrit que la langue du dominé est incapable de produire la
moindre positivité. Nous entendons par « positivité » le fait d'une
manifestation autonome/spécifique de culture et/ou de langage qui ne
serait donc pas uniquement (négativement donc) une réaction anxieuse
face à la culture et à la langue de la bourgeoisie.

Contrairement à Bourdieu, Pasolini défend l'idée que le dominé, même le
sous-prolétaire, surtout le sous-prolétaire, est en capacité de faire
monde seulement en affirmant une culture qui lui soit particulière,
distincte de la culture bourgeoise et petite-bourgeoise. Cette culture
n'est pas une identité souveraine : elle se renouvelle sans cesse et
invente ainsi des façons toujours neuves de se lier, secrètement, à des
formes de vie passée.

Considérer --- contre Bourdieu --- la positivité inhérente des formes de
vie sous-prolétaires et/ou dissidentes n'empêche aucunement d'envisager
leur rapport contradictoire, ou mieux, dialectique, à la domination. Il
s'agit même de constituer ce rapport comme un ancrage pour toute
perspective d'émancipation concrète. Pensons alors aux peuples créoles
ayant façonné depuis la langue du colon un langage qui leur soit propre ;
ou encore à l'ironie dont usaient certains esclaves afin de résister
« cryptiquement » à leur oppression[^207].

Du point de vue de Pasolini --- qui considérait que la culture des
journaliers frioulans devait primer sur le fait de leur lutte ---, il
s'agit donc de constituer cette « positivité » culturelle comme première ;
ou, pour le dire autrement, comme un point de départ à toute forme de
résistance politique.

Avant d'ébaucher une perspective révolutionnaire qui prendrait appui sur
l'autonomie culturelle des groupes sociaux et des individus subalternes,
faisons un détour par le dernier chapitre de *La Convivialité* d'Illich :
« En fait, écrit-il, l'issue à la crise imminente dépend de
l'apparition d'élites impossibles à récupérer[^208]. » Cette « classe
d'irrécupérables » se caractériserait précisément par l'autonomie de ses
savoirs, de ses techniques, de ses valeurs et de sa langue. C'est même
cette autonomie qui rendrait possible et souhaitable une volonté de rupture avec un
ordre social, économique et culturel donné.

Si Pasolini partage dans les grandes lignes l'analyse marcusienne de
l'intégration, et bien que cet intégrationnisme obscurcisse l'horizon
émancipateur des peuples subalternes, la possibilité d'un acte
d'autonomie et de résistance ne sera jamais démentie. Plus poétiquement :
il existera toujours un sourire échappant à l'unidimensionnalité de la
société néocapitaliste, et ce malgré le fait que cette société se
caractérise précisément par une extension sans précédent de sa capacité
à neutraliser le *différend*.

Le fameux pessimisme pasolinien doit également être nuancé par une idée
émergeant sobrement dès les années 60, et qui va prendre une tournure
remarquable au sein de la dernière phase de sa production poétique
(notamment dans *Trasumanar e organizzar*) : s'il est vrai que les
cultures sous-prolétaires, celles des *borgate* romaine, celles du
centre de Naples, celles de Marseille, tendent bien à disparaître, en
tout cas sous les formes qu'elles ont pu avoir, il ne peut s'agir de
nous abandonner au regret passéiste, ce qui ne manquerait guère de
ronger notre vitalité : il faut nous adapter.

Nous adapter ? À qui ? À quoi ? Et surtout comment ?

Toute la bonne volonté de la petite-bourgeoisie militante ne rendra
jamais possible la recomposition d'un bloc politique prolétarien et
sous-prolétarien combatif, mais la capacité des uns et des autres à
produire un langage commun, à partir de ce qui les rassemble sans pour
autant les nier, le peut.

Nul doute que ce langage devra être tissé de plusieurs langues.

Nul doute également que la lutte devra mobiliser toutes les puissances
de notre raison --- non en son acception naïvement bourgeoise,
c'est-à-dire essentiellement instrumentale et soutenue d'une illusion de
scientificité, mais bien stratégique : unifiant pragmatiquement et
programmatiquement les exigences de l'éthique (dont la visée est la
reconnaissance de l'*autre*) et celles du politique (dont la visée est
l'organisation matérielle des formes d'existence collective).

Le sous-prolétaire, qui fut négligé par Marx et la tradition
marxiste orthodoxe, du fait de sa non-intégration aux rapports légaux de
production et d'échange, s'offre sans doute comme une nouvelle
composante du sujet révolutionnaire. Ce qui ne signifie pas qu'il faille
jouer contre cette figure celle du prolétariat intégré, mais plutôt
esquisser une réélaboration de nos analyses de classes à partir
de celle-ci, et ce dans la mesure où elle apparaît comme force de
résistance spontanée ainsi que point de jonction avec les franges les
plus radicales de la petite-bourgeoisie militante.

Par ailleurs, s'il est encore possible de mobiliser un concept unifiant
de « sujet révolutionnaire », gageons que celui-ci se caractérisera
nécessairement par son hétérogénéité --- ce qui constituera une force
considérable d'érosion de l'unidimensionnalité néocapitaliste.

Ainsi, toujours selon Illich :

> Plus nombreux et divers en seront les hérauts, plus profonde sera la
> saisie de ce que le sacrifice est nécessaire, de ce qu'il protège des
> intérêts divers et qu'il est la base d'un nouveau pluralisme
> culturel[^209].

Le point de vue adopté par cette « classe d'irrécupérables » sera
celui d'un grand refus en capacité d'agréger sans les dissoudre toutes
les positivités spécifiques de ses composantes multiples.

Ce n'est plus *seulement* depuis la sphère du travail productif que des
individus et des groupes sociaux se lèvent désormais contre ce qui les
opprime, mais depuis les interstices de la société elle-même ; qu'il
s'agisse de la défense d'un territoire (ZAD, quartiers populaires,
etc.), d'un assaut spontané contre les nouveaux symboles de la
bourgeoisie (gilets jaunes), d'un rejet de la Biocratie (mobilisation
contre le pass sanitaire et/ou autodéfense sanitaire), de formes
inédites de mobilisation écologique (*Les Soulèvements de la terre* par
exemple), etc., tout laisse penser que nous assistons depuis vingt ans à
un déplacement du lieu même des luttes anticapitalistes.

Il ne peut s'agir pour autant de déconsidérer la sphère productive --- de
faire jouer « les chômeurs contre les salariés » ---, mais bien de
soutenir une raison stratégique permettant de lier à la masse des
improductifs les travailleurs précarisés, de greffer aux revendications
sectorielles de nouvelles formes de radicalité sur lesquelles tisser
d'improbables alliances révolutionnaires.

L'atomisation croissante du corps social semble bien valider l'hypothèse
de Negri & Hardt selon laquelle seules des multitudes agissantes, et non
plus des classes sociales, seraient en mesure de s'organiser
politiquement contre le néocapitalisme et les modes de gestion
néolibéraux de nos gouvernements. Mais --- et à moins d'inscrire très
concrètement ces multitudes dans la matérialité des nouveaux rapports de
classe --- comment ne pas considérer cette « multitude abstraite » comme
un sous-produit de l'universalité bourgeoise ? Autrement dit : comment
ne pas sombrer dans l'illusion d'un nouvel universalisme
petit-bourgeois, soustrait non seulement à la lutte des classes, mais
également à cette positivité spécifique qui caractérise les cultures
subalternes ?

Esquissons alors l'hypothèse suivante : la capacité de
résistance des nouvelles formes de lutte sera déterminée par la
propension des multitudes qui en sont les sujets à ancrer leur pensée et
leur action, et dans la matérialité des rapports de classe et de
domination, et dans les formes d'existence spécifique des classes
subalternes, à faire naître ainsi des collectifs populaires sur les
ruines matérielles et symboliques de la postmodernité capitaliste, à
constituer ces collectifs comme *outils* mais également comme *fins*
sans cesse réinventées, c'est-à-dire à la fois comme mode de défense de
l'existant et organisation corollaire d'un commun.

Si nombre de traditions populaires au sein desquelles nous étions
susceptibles de nous reconnaître semblent avoir disparu[^210], nous
observons cependant une tendance croissante au réinvestissement
politique et culturel des espaces périphériques, des campagnes et des
quartiers populaires, ainsi que l'émergence de nouveaux modèles de vie
en rupture avec l'uniformisation culturelle et le centralisme politique
des sociétés néocapitalistes : squat, retour à la vie paysanne,
néoruralité, collectifs d'autodéfense des quartiers populaires, luttes
contre la gentrification, écologie pirate, etc. Il ne saurait être question de négliger ces formes de vie et de résistance, mais plutôt d'en déjouer les tendances isolationnistes et sectaires, d'envisager leur inscription dans les rapports de classe et de domination, de les pousser à reconsidérer l'existant par-delà toute pétrification du regard ou toute pureté militante.

Il pourrait sembler ironique, quand on connaît les critiques acerbes de
Pasolini à l'égard « des chevelus et des gauchistes » de son temps, que
ce dernier puisse apparaître comme un inspirateur de ces nouvelles
formes de radicalité (notamment écologiques). Pourtant, cette
*positivité spécifique* et cette *autonomie culturelle*, en tant que
formes premières et entrelacées de toute radicalité politique, c'est
bien dans son œuvre que nous en puisons à la fois les concepts et
l'amour viscéral du *différend* qui les sous-tend. Et si ces dernières
semblent mises en péril par l'intégrisme de ladite « société de
consommation », nous demeurons fidèles à la conviction qu'« aucune
société ne contient tout à fait le monde[^211] », que l'oppression, si
puissante soit-elle, n'épuise jamais complètement les possibilités de
son abolition.

</div>

<div class="text" data-number="9">

## 10. Sur le colonialisme de l'intérieur

Si la colonisation extérieure, dans le cadre d'une analyse
marxiste-léniniste contestable[^212], apparaît comme condition première
d'un déploiement du capitalisme sous le mode de la conquête impérialiste
du monde, le « colonialisme de l'intérieur » occupe une place
structurelle dans l'organisation matérielle et la hiérarchisation
symbolique de la nation impériale. 

Essayons de comprendre cette modalité coloniale spécifique sous au moins
deux de ses aspects :

1) une double tentative de colonisation par des valeurs à
la fois permissives-hédonistes et répressives-républicaines (les
premières étant massivement acceptées, les secondes beaucoup
moins) ;

2) un processus de ghettoïsation matérielle et symbolique
(racisme d'État, division capitaliste de l'espace, suprématie des
espaces marchands et privatisés, gentrification des quartiers
populaires, militarisation des centres urbains, relégation des
périphéries et des campagnes, etc.).

Le premier aspect induit un redoublement du sens de « colonisation
intérieure », de l'âme prolétarienne et sous-prolétarienne comme effet
du processus d'intégration par le néocapitalisme tel que nous l'avons
analysé dans le chapitre précédent.

Il existe un paradoxe consistant à mobiliser Pasolini contre Bourdieu et
sa conception essentiellement négative du langage de l'opprimé, pour
finalement déboucher sur le constat d'une intégration totalitaire du
corps et de l'âme prolétarienne et sous-prolétarienne au nouveau
capitalisme. Il serait d'ailleurs tout à fait possible, et même assez
aisé, sous prétexte de déplorer le naufrage des anciennes valeurs
bourgeoises broyées par le nouveau capitalisme, de convoquer Pasolini
dans le cadre d'une critique à la fois abstraite et résignée de ladite
« société de consommation » :

> Autres modes, autres idoles,
>
> la masse, non le peuple, la masse
>
> décidée à se laisser corrompre,
>
> au monde désormais fait face,
>
> et le transforme, à tout écran, à toute vidéo
>
> s'abreuve, horde pure qui déferle
>
> avec une pure avidité, informe
>
> désir de participer à la fête.
>
> Et elle se greffe là où le Nouveau Capital la veut[^213].

Et quelques années plus tard :

> Il faut ajouter que le consumérisme peut créer des « rapports sociaux »
> *non modifiables*, soit en faisant surgir, dans le pire des cas, à la
> place du vieux clérico-fascisme, un nouveau techno-fascisme (ne pouvant
> se réaliser de toute façon qu'à condition de s'appeler antifascisme) ;
> soit, comme il est désormais plus probable, en créant, comme contexte de
> sa propre idéologie hédoniste, un contexte de fausse tolérance et de
> faux laïcisme --- c'est-à-dire de fausse réalisation des droits
> civiques[^214].

La perspective d'un « techno-fascisme », qui réaliserait les exigences
totalitaires du nouveau capitalisme en faisant advenir des « “rapports
sociaux” non modifiables » dans un « contexte de fausse tolérance et de
faux laïcisme », n'est pas des plus reluisantes. D'autant plus si nous
songeons au renouvellement et au perfectionnement des modalités de
domestication du prolétariat et du sous-prolétariat (notamment racisé) à
disposition d'un tel projet de « société », qu'il s'agisse de
l'ubérisation des rapports de production et d'échange, ou encore des
nouvelles formes d'acculturation bénéficiant d'outils et de technologies
leur permettant désormais une pénétration massive de toutes les couches
de la société. Mais en rester là reviendrait à renier ce qui
constitue à la fois le crédo marxiste par excellence, celui d'un
retournement de l'objet de la domination en sujet d'émancipation, et
cette part d'autonomie irréductible, positive, qui caractérise toujours,
selon Pasolini, les formes de vie spécifiques.

Outrepasser Pasolini au nom de Pasolini revient à chercher, malgré les
ravages anthropologiques induits par la phase terminale du capitalisme,
la perte d'autonomie culturelle des classes populaires, l'atomisation
généralisée et la dilution des êtres et des choses dans
l'unidimensionnalité de la forme-marchande, ce quelque chose d'une force
et d'un langage chevillés à ce qui demeure, même de façon bâtarde ou
orpheline, la « classe ouvrière ».

À ce titre, le film de Pierre Tonachella, *Jusqu'à ce que le jour se
lève* (2017), tend à maintenir cette idée à la fois tenace et romantique
(au sens radical de ce que cette tradition a su insuffler d'élan
combatif au marxisme plus strictement politique) qu'il faut demeurer au
plus près du prolétariat, petit ou grand, que c'est son génie propre
qu'il s'agit de révéler et de défendre, plutôt que d'encombrer nos
luttes de ces modèles d'émancipation abstraite, individualiste et
hors-sol, en dernier ressort bourgeoise, qui continuent à caractériser
le gauchisme sous des formes renouvelées.

Le circuit de la marchandise, s'il plonge la conscience atomisée dans
l'effroi de son immanence, trace conjointement, qu'il s'agisse des
résidus d'industries européennes ou des nouvelles formes de salariat ou
d'exploitation déguisée, de façon sans doute moins explicite mais non
moins concrète, le contour de nouveaux corps et de nouveaux esprits
prolétaires. Ainsi, nous sommes prêts, sans renier notre critique
de l'intégrisme néolibéral, à faire jouer Mattick *avec* Pasolini, à
penser ensemble l'intégration et ses limites en un sens obstinément
révolutionnaire.

Ce n'est pas là trahir la lucidité propre aux analyses de Pasolini sur
les mutations anthropologiques qui ont accompagné l'avènement du
néocapitalisme. Son pessimisme, dont nous avons vu qu'il portait
essentiellement sur l'Histoire, la défaite du mouvement ouvrier et la
mort de toute espérance révolutionnaire fondée sur une certaine raison
marxiste-léniniste, tout comme celui de Benjamin reprenant
Naville[^215], ne cesse d'esquisser de nouvelles modalités de
résistance ; il s'agit donc de l'organiser.

Dans *La Langue vulgaire*, par exemple, alors même qu'il décrète
ailleurs qu'il ne pourrait plus tourner Accattone, Pasolini revient sur
ce « colonialisme de l'intérieur », dont il perçoit la réalité à partir
de la question albanaise, et prend alors radicalement le parti de
l'auto-organisation des minorités opprimées, les enjoignant même, armes
à la main s'il le faut, à fonder des institutions scolaires leur
garantissant la perpétuation d'une langue albanaise en Italie.

La Culture, telle que la promeut la bourgeoisie, est bien un ensemble de
valeurs ayant pour fonction d'assigner, de distinguer ou d'exclure. Mais
elle implique conjointement l'anéantissement des cultures spécifiques à
travers notamment l'arrachement des êtres à la langue de leurs parents.
Ce processus d'acculturation du sous-prolétariat est induit par
l'absence de reconnaissance institutionnelle des langues minoritaires
qui, dans le cadre de la hiérarchisation républicaine des valeurs, sont
renvoyées à quelque chose de sale et d'inassimilable.

Toujours dans *La Langue vulgaire*, Pasolini laisse entendre qu'un
mouvement révolutionnaire, même triomphant, où il ne serait guère permis
de s'exprimer dans sa langue, ne serait qu'une nouvelle défaite
historique de notre camp ; le fétiche d'une liberté abstraite plutôt que
cette égalité des différences que prône un marxisme se faisant tout
autant l'héritier des Lumières que des luttes d'indépendance nationale.

Le poète prend à rebours tous les défenseurs de l'unification
linguistique, considérant, tout comme Ngũgĩ wa Thiong'o à propos de la
littérature kenyane[^216], que la subjectivation des minorités est un
*fait positif* qui ne peut s'accomplir que *dans* et *par* une langue
qui leur soit propre. À ce titre, nous prenons le pari que Pasolini
serait intervenu aujourd'hui en faveur d'écoles exclusivement
arabophones en France ou en Italie :

> Ou bien l'enseignement ou la protection du dialecte relève déjà d'une
> forme de traditionalisme, de conservatisme (que je considère comme
> parfaitement saine, pour les mêmes raisons qui font qu'il existe une
> « droite sublime »), ou bien il devrait devenir profondément
> révolutionnaire (quelque chose d'analogue à la défense de leur propre
> langue par les Basques ou les Irlandais) et mener aux limites du
> séparatisme, ce qui serait une lutte extrêmement saine, parce que la
> lutte pour le séparatisme n'est rien d'autre que la défense du
> pluralisme culturel qui est la réalité d'une culture. Donc : ou être
> conservateur, mais éclairé, d'une façon absolument neuve, qui n'a rien à
> voir avec la conservation au sens de la droite classique ; ou carrément
> révolutionnaire[^217].

Si la question de l'auto-organisation des groupes sociaux opprimés se
pose aujourd'hui (très vivement) dans les champs militants occidentaux,
c'est que nous assistons depuis vingt ans, en France notamment, à un
renversement de la conscience politique des vieux reliquaires d'un
soixante-huitardisme décrépi, laïcard, platement universaliste,
ex-libertaires prônant désormais et sans scrupule l'adhésion aux
valeurs, à la langue, à la morale d'État, et ce au nom de la communauté
d'intérêt national « face à la barbarie ».

Après tout, pourquoi les Albanais d'hier en Italie, les réfugiés en
France aujourd'hui, devraient-ils engager leur force dans la
construction d'espaces de partage et de transmission qui leur soient
propres, alors qu'ils pourraient très bien se rallier à la langue de
Dante ou de Molière, renier leur résidu de culture subalterne et épouser
au plus vite le modèle de vie et de pensée petit-bourgeois ?

> Pour les bourgeois français, le peuple est formé de Marocains ou de
> Grecs, de Portugais ou de Tunisiens, et ceux-ci, les pauvres diables,
> n'ont qu'à apprendre au plus vite à se comporter comme les bourgeois
> français[^218].

Ces dernières années ont vu se développer une rhétorique d'un ordre
apparemment nouveau, mais qui est en vérité aussi structurante
idéologiquement que le sont sur le plan de la division internationale du
travail les origines raciales du capitalisme : le reproche, la
stigmatisation, l'injure à l'adresse de toute forme de vie dominée dès
lors qu'elle prend conscience d'elle-même et cherche à s'affirmer ou
tout simplement à se défendre :

> ... cela constitue le sens des « castes » que nous prenons plaisir à
> condamner, dans un esprit raciste, avec un si méprisant rationalisme
> « eurocentrique[^219] »...

De façon plus générale, il est grand temps d'abolir le préjugé selon
lequel le marxisme serait nécessairement un égalitarisme des valeurs et
donc un nivellement des formes de vie spécifiques :

> Je suis marxiste au sens le plus exact du terme quand je hurle, quand je
> m'indigne contre la destruction des cultures particulières[^220]...

Et quelques lignes plus loin :

> ... je voudrais que les cultures particulières soient une contribution,
> un enrichissement et entrent dans un rapport dialectique avec la culture
> dominante[^221].

S'il faut bien reconnaître que Pasolini était plus passionnément attaché
aux journaliers en lutte rencontrés durant son adolescence frioulane
qu'à la culture ouvrière des grandes périphéries industrielles du
Piémont, c'est que sa haine instinctive de la bourgeoisie s'est toujours
fondée sur la conscience du génocide culturel qu'elle faisait subir au
monde, et aux mondes ruraux en particulier, à toutes ces façons
spécifiques de produire, de vivre et d'habiter la terre dont les mondes
paysans --- à la fois profondément ancrés sur un territoire et
transnationaux par nature --- sont encore si riches.

Nous qui lisons Pasolini en ce début de XXI^e^ siècle, comment ne pas
prendre conscience de la nécessité de nous battre pour la préservation
des pures formes de vie marginales et/ou contestataires, c'est-à-dire
également pour la survivance des langues subalternes, des dialectes, des
traditions orales et des fêtes populaires ?

À travers son rejet de l'institution scolaire, facteur d'homogénéisation
sociale et d'acculturation généralisée, Pasolini poursuit sa critique
radicale de l'État gestionnaire et centralisateur :

> L'école obligatoire est une école d'initiation à la qualité de la vie
> petite-bourgeoise. On y enseigne des choses inutiles, stupides, fausses,
> moralisantes, même dans les meilleurs des cas[^222]...

En raison notamment des systèmes d'oppression raciale inhérents à la
phase actuelle du capitalisme, qu'il s'agisse des formes de fascisme
historique réactualisées ou des formes fascisantes de néolibéralisme se
drapant sous étendard républicain, bref : que nous considérions la
situation plutôt depuis l'Italie ou plutôt depuis la France, l'école
demeure le terrain privilégié d'une double domination, à la fois
éradicatrice et « domesticatrice ». En d'autres termes, le Citoyen se
fabrique toujours sur les décombres du génocide culturel inhérent à la
nature même de ce nouveau capitalisme.

En tant qu'institution étatique, l'école se présente comme un champ
stratégique où s'affrontent à armes inégales deux conceptions de la
culture et de sa transmission : l'une faisant appel à la subjectivité de
l'apprenant, la seconde constituant précisément l'élève comme le
réceptacle de toutes ces « choses inutiles, stupides, fausses,
moralisantes ». C'est évidemment à la seconde conception que Pasolini
s'en prend. Dans *La Langue vulgaire*, tout comme dans les *Lettres
luthériennes*, une telle distinction structure sa critique et oriente
ses propositions. Car, en investissant le champ stratégique de l'école,
le poète accepte de se salir les mains, s'efforce de proposer des
solutions, tout du moins des propositions institutionnelles et
pédagogiques concrètes. C'est notamment ce qui le conduit à réclamer des
écoles albanaises en Italie, ou encore à s'intéresser de près à des
formes d'institution scolaire alternatives telles que l'École de
Barbiana.

Comme si souvent chez Pasolini, son action est mue par un élan paradoxal
et désespéré d'amour : c'est à la fois un appel à la désertion, à la
fuite, à la déterritorialisation des espaces de formation, de partage et
de vie, et une volonté de réinventer radicalement l'école ou de défendre
des formes de pédagogie autonomes.

De façon tout à fait pragmatique, moins complaisante sans doute à
l'égard du poète et de ses paradoxes, sur le terrain de l'école de la
République, cela nous invite à tenir ensemble ce que nous considérons
comme les deux pôles de la critique, celui des rapports de production et
celui des rapports de sémiotisation :

> Nous refusons de séparer, comme le font la plupart des théoriciens qui
> se réclament du marxisme, les rapports de production des rapports de
> sémiotisation. Le contrôle, par des classes ou des castes exploiteuses,
> des rapports de production, est indissociable du contrôle des moyens
> collectifs de sémiotisation qui, pour être peut-être moins visibles,
> n'en est pas moins fondamental[^223].

Ces deux pôles doivent se concilier dans le cadre d'une commune
aspiration radicale --- dans les deux sens de radicalité : une action sur
les racines, une aspiration utopique. L'autonomisation de ces pôles
est précisément cause d'une critique décorrélée de la praxis, d'une
critique qui s'est elle-même autonomisée au point de perdre tout rapport
avec la réalité, qu'il s'agisse de la critique strictement objective du
marxisme orthodoxe ou de la critique purement subjective des
« gauchistes ».

Sur un plan programmatique général, c'est-à-dire non circonscrit à des
exigences politiques immédiates, une telle conciliation devra être
consciemment et lucidement dirigée contre toutes les formes d'aliénation
se déployant sous le régime social, économique et culturel
néocapitaliste :

1) celle du *temps*, à laquelle Marx a consacré l'essentiel de
son œuvre, et dont la critique fut notamment prolongée par
Marcuse ; le salariat ou toute forme de travail aliéné induisant
une expropriation du temps, mais également une modification
qualitative de notre rapport à la réalité (à travers notamment
la division entre le temps de travail et le temps de loisir,
l'un et l'autre gouvernés par un même *principe de rendement*) ;

2) celle de l'*espace*, rendue manifeste par la tradition
initiée par Henri Lefebvre[^224], notamment, ainsi que par la
pensée écosocialiste sur le versant de ce que nous convoquons
ici sous le concept de *paysage* ; mentionnons également Kristin
Ross et son invention de l'*histoire spatiale*[^225], à partir
d'une relecture de Rimbaud et d'une revisitation de l'histoire
de la Commune de Paris ; mais également la continuation du
travail de Lefebvre par Harvey ; ou encore le concept
d'*hétérographie* qui ouvre un champ d'investigation
poético-politique tout à fait nouveau et singulier[^226] ;

3) celle des *formes de vie spécifiques*, dont Pasolini nous
parle avec passion et désespoir, mais également Rilke[^227],
et tant d'autres qui ont œuvré à la compréhension du phénomène
d'acculturation sous la domination des valeurs hégémoniques
bourgeoises ; elle comprend celle de la langue, en tant que
système de signes, écrits ou oraux permettant aux êtres de
communiquer dans une langue qui leur soit propre, c'est-à-dire
d'échanger des informations et de nommer des choses tout en
s'inscrivant dans une continuité mémorielle collective et située ;

4) celle du *langage*, enfin, qui se distingue de la langue
tout en la subsumant sous sa forme poétique et/ou non assignable
à un contenu prédéterminé dans l'ordre des significations, qui
n'est donc pas réductible à des systèmes de signes
communicables, mais noue et dénoue des rapports toujours
dynamiques entre les êtres, entre les choses, sans réduire ces
rapports à de simples échanges d'informations.

L'aliénation du langage induit toutes les autres ; à travers elle, nous
sommes arrachés au corps même de la réalité, rendus impuissants « à
déchiffrer le monde comme une page, fourmillante d'écritures insensées,
de ratures, d'arbres et d'oiseaux[^228] ». Malgré toutes les recherches
nécessaires portant sur les formes les plus objectives d'aliénation, il
est donc urgent de renverser la mesure même par laquelle nous
construisons nos connaissances concrètes du système de domination
capitaliste ; ni par extrémisme ni par dandysme, mais parce que le
langage est premier et indomptable, qu'il apparaît comme le gage d'un
maintien de l'être dans le giron de l'expression.

C'est ici que nous concevons le mystère rouge par excellence --- celui
dont Bloch nous dit qu'il devra nécessairement être reconnu par le
*socialisme à venir* ---, « le fait qu'il y ait un monde », et bien que
ce monde ne puisse se subsumer sous un Savoir, et bien qu'une part de ce
monde demeure soustraite à la Signification, une part d'innomé, une part
d'innommable, dont l'appel pourtant est irrévocable.

Ce quelque chose qui échappe à la Langue, au Savoir, à la Morale, ce
délire de poète ou de peintre, ce quelque chose qui palpe la matière, la
creuse, qui redéploie sans cesse les frontières du visible et de
l'invisible, qui échappe nécessairement à toute grammaire, ce quelque
chose de l'enfance, l'école républicaine le broie.

Toute philosophie conséquente porte en elle la mémoire de cet espace
d'exploration et de jeu, cette appréhension sensible et aberrante de la
réalité, cet en deçà de la langue et ce surplus de corps. D'ailleurs,
toutes les tâches de la Révolution et tous les tourments de l'Histoire
n'empêcheront pas Lénine de mourir un livre d'amour à la main[^229].
Lénine qui disait que proposer de la « littérature pour ouvriers » aux
ouvriers revenait à leur manquer de respect[^230].

Ce qui, pour autant, ne peut nous amener à relégitimer une forme de
léninisme sur le terrain culturel, tant Lénine lui-même fut un facteur
d'appauvrissement, pour ne pas dire d'annihilation de toute forme
expérimentale dans le domaine esthétique. Ayant fabriqué de toute pièce
le Réalisme socialiste, il s'évertua à condamner tout ce qui déviait de
son idéal d'un Art prolétarien, et ce pour des raisons sans doute plus
névrotiques que pragmatiquement politiques. Même Gorki, représentant
officiel de l'Art soviétique, fut sans cesse rappelé à l'ordre dès qu'il
déviait un tant soit peu des canons établis dans le champ artistique par
cet homme qui ne comprit décidément pas grand-chose à la littérature,
érigeant même le mauvais *Que faire ?* de Tchernychevski comme un modèle
du genre.

Tant en raison des limites qu'il impose aux artistes et aux écrivains
sur le plan de l'expérience créative, que de sa tendance à naturaliser
l'opprimé, à le maintenir dans les filets de ses propres
déterminations et dans une représentation appauvrissante de la réalité,
le Réalisme socialiste apparaît comme un symptôme du durcissement
centralisateur et autoritaire du léninisme. Ainsi, Olivier Neveux
s'interroge, par exemple, à propos d'Armand Gatti : « Qu'aurait dû être
l'existence de \[ce dernier\], le fils d'éboueur immigré, si elle avait
été mesurée, et représentative, si elle n'avait refusé de se satisfaire
de ce qui lui était octroyé, désirant toujours plus, avide, irréductible
aux succès, aux consécrations et aux reconnaissances[^231] ? »

Mais critiquer le Réalisme socialiste ne doit pas nécessairement
conduire à promouvoir les formes les plus apolitiques d'art. Autrement
dit, il est tout à fait permis de faire l'éloge de la démesure dans le
champ artistique sans pour autant prôner l'extrémisme subjectif ou
l'obsession bourgeoise de l'autonomie de l'art : « La démesure a pour
raison sinon pour ambition d'arracher les vies et leurs devenirs à tout
destin “représentatifs[^232]” », contrairement à « l'extrême » qui
demeure confiné à l'intérieur du réalisme capitaliste, aussi
spectaculaire que soit sa révolte, qui n'en est donc pas moins conforme
aux possibilités limitées et limitantes de la société bourgeoise.
D'autant que ce sont souvent les plus extrémistes, sur le terrain militant
notamment, qui sont les plus conformistes sur le plan de la pensée et du
langage.

Parmi les conformismes les plus répandus, non plus sur le terrain
militant ici mais sur celui de l'art, la haine de la théorie apparaît
comme emblématique. La théorie habite pourtant la contradiction, non
dans le sens de sa résolution, mais dans celui de son explicitation. À
rebours des anti-intellectualismes, anti-théoricismes en tout genre,
elle est seulement un facteur de démocratisation des expériences de
pensée et de langage les plus radicales sur des terrains qui leur sont
propres, et donc le plus souvent étrangers aux masses. Surtout : elle
permet de révéler ce en quoi et comment une forme d'art expérimental est
en mesure de se rapporter à la totalité, cosmique ou sociale, et tente à
sa façon de la comprendre ou même de la transformer.

Ce n'est donc pas le caractère expérimental d'un art qui le confine dans
les marges de la culture bourgeoise, mais son refus de s'expliquer.
Expliquer n'est pas justifier : il ne s'agit aucunement de convoquer
l'artiste à un quelconque tribunal, plutôt de le contraindre à affronter
l'état du monde, à sortir de sa tanière, à ne jamais abandonner le
prolétariat, à refuser une critique strictement idéologique de la réification marchande (qui s’affranchirait, par exemple, de la lutte des classes pour épouser une radicalité sans ancrage social).

Inversement, refuser de proposer des espaces d'expérimentation théorique
et sensible à des prolétaires et à des sous-prolétaires, décider à leur
place que la priorité ultime doit être de leur enseigner comment se
fondre dans une société qui les exclut, croire que l'usage de la langue
hégémonique et l'accumulation des savoirs suffisent à les émanciper,
négliger la (dés)éducation aux images, la manipulation poétique des
mots, des signes, des sons, les priver de tout espace qui ne serait pas
dédié à la fortification d'un capital symbolique, revient bien à les
mépriser.

L'enfance, en tant que *pur langage*, émancipe le monde de la
structuration marchande de nos sociétés néocapitalistes et de la
grammaire qui la soutient. Ce pour quoi il est urgent de l'empêcher de
nuire. Ce pour quoi un marxisme qui persisterait à négliger la question
du langage, et le caractère nécessairement expérimental de ses formes,
continuerait à hypostasier la lutte des classes au nom d'une raison
d'État qui ne se justifie pas ou plus. Non seulement car nous sommes
loin de la réalisation d'un État réellement socialiste, mais que
celui-ci ne peut plus constituer un but, en tout cas dans la forme sous
laquelle il a pu être pensé et réalisé par les Bolchéviques il y a
désormais plus d'un siècle.

Aujourd'hui, l'école, telle qu'elle existe, est non seulement un espace
privilégié de reproduction de toutes les injustices sociales
--- classes, genres, races ---, mais également un dispositif de colonisation
de l'enfance et de son langage qui, en tant que tel, contribue à
désubjectiver les êtres, à les amputer de leur capacité à se mouvoir
dans l'espace, le temps, la culture et la langue.

Si la procédure d'exclusion de toute altérité, de tout ce qui échappe à
la norme républicaine et/ou marchande est induite par la colonisation du
langage de l'enfance, elle contribue *a posteriori* à maintenir
l'arrachement de l'être à ses modalités expressives, en hiérarchisant de
façon raciste les langues subalternes, en disqualifiant les formes de
vie dissidentes --- ou tout simplement différentes ---, en balisant
l'errance et en assignant au temps un caractère spatialisé dédié à la
productivité marchande et à la consommation de marchandises.

Dans le cas où l'école de la République deviendrait réellement une
« école du peuple », mais qu'elle continuerait à négliger, au nom de
« l'éducation des masses », cette part d'enfance, de langage,
d'expérimentation sensible et improductive, elle n'aurait donc jamais
notre approbation. Il s'agirait toujours d'une institution autoritaire
qui dresse et qui *ensigne*[^233] sans jamais libérer les puissances
insoupçonnées qui se logent en chaque prolétaire et sous-prolétaire.

Renvoyer notre critique de l'école à un caractère de radicalité
petite-bourgeoise reviendrait à considérer que notre monde n'a besoin
que d'un petit rééquilibrage --- dans le cas de l'école : un peu moins
d'élèves par classe, une meilleure formation des enseignants, des
salaires plus élevés, etc. --- ; ce serait justifier l'universelle
structuration marchande des manifestations de la vie sociale et le
bien-fondé de la morale républicaine en tant que garde-fou de la
barbarie du marché ; ce serait acter l'inféodation des connaissances,
des arts, des compétences spécifiques aux modes de production
capitaliste, et donc consentir à la division capitaliste du travail, à
son opposition fondatrice entre travail manuel et travail
intellectuel[^234] ; ce serait considérer, enfin, que le monde tel
qu'il se (re)présente à nous est acceptable, que son ordre mérite d'être
maintenu.

Ce qui n'implique aucunement de rejeter le travail intellectuel,
théorique et critique, ou de négliger la rigueur et la méthode de toute
recherche scientifique sérieuse, bien au contraire ; il est urgent de
nous former, de nous contraindre à inventer pour cela d'autres espaces,
d'autres outils, etc.

> Donc étudie, pense, travaille, observe : la lumière est seulement dans
> la culture (ce qui ne veut pas toujours dire la culture enseignée à
> l'école), autrement dit, elle est seulement dans la renonciation
> rationnelle à toutes les fausses consolations[^235].

La « défense de l'école publique », bien que nécessaire, est
insuffisante. Une critique radicale de l'école républicaine l'est tout
autant : en tant que production institutionnelle malléable soumise aux
multiples révolutions sociales et culturelles, anthropologiques et
politiques que les classes capitalistes successives provoquent de façon
systémique afin d'inscrire et de perpétuer leur domination sur le monde.

Laurence de Cock, dans sa préface à la *Lettre à une enseignante* des
élèves de Barbiana, fustige une critique radicale de l'école qui
s'affranchirait de la lutte des classes. Il est vrai que l'abolition de
l'école n'est pas une perspective crédible si elle devait se traduire
par une exclusion des masses populaires des espaces d'apprentissage et
de formation. Mais la « société sans école[^236] » qu'Illich appelle de
ses vœux n'est pas une société qui jetterait à la rue des millions
d'enfants prolétaires et sous-prolétaires : il y est essentiellement
question d'une désinstitutionnalisation de l'école comme corollaire
d'une désinstitutionnalisation de la société.

Rappelons que Pasolini, qui a commis un avant-propos à l'ouvrage en
question, était un lecteur d'Illich, qu'il cite notamment dans la « Note 22i »
de *Pétrole*, faisant alors référence au concept d'austérité comme
condition de la convivialité illichienne. Cette « austérité » n'est pas
une restriction de la joie collective et du désir créateur, elle en est
la condition. Il s'agit, dans une démarche assez classiquement
matérialiste, de dissocier les désirs malheureux (désir de marchandise,
de réussite sociale, d'accumulation, etc.) des désirs vecteurs
d'épanouissement existentiel ; et, conjointement, de reconnaître
l'importance de nos limites comme condition éthique de l'accroissement
de nos puissances d'agir.

Opposer la critique radicale de l'école à la défense pragmatique de
l'école publique est stérile : il s'agit de les articuler. Si
l'institution scolaire est mise en question comme « lieu de prédilection
de la disciplinarisation des corps, de l'autoritarisme, l'expression
d'une République de l'ordre[^237] », c'est pour des raisons qui nous
paraissent fondées. La relégation de ces griefs (« la
disciplinarisation, l'autoritarisme, l'expression d'une République de
l'ordre »), comme s'il s'agissait de simples lubies de petits-bourgeois,
se pose non seulement comme un refus de considérer la problématique
cruciale de la subjectivation des élèves dans les processus
d'apprentissage, mais également comme un aveuglement quant aux nouvelles
formes de militarisation des espaces éducatifs se déployant sous la
bannière de la République.

L'institution scolaire cristallise cette imbrication douloureuse et
contradictoire entre le vieux monde classique, scolastique, érudit, et
le nouveau monde consumériste, hédoniste, permissif. L'un et l'autre
(l'ordre et le désordre) doivent être combattus[^238]. La radicalité
illichienne, celle de Pasolini également, invite à aiguiser notre
critique de l'institution scolaire, « à faire maintenant un nouveau pas
vers la libération complète[^239] », c'est-à-dire : à produire une
critique par les deux bouts du problème, la *lutte des classes* et le
*langage*. Ce qui implique de démythifier notre idéal d'une « école du
peuple » inféodée à l'État centralisateur et gestionnaire, à en
envisager une forme neuve, en adéquation avec notre conception de la vie
en tant qu'unité de l'être et du langage.

Concluons, ici, sur une évocation par Pasolini de ses jeunes années
à Bologne :

> Je me souviens toujours avec un plaisir intime, presque poignant, des
> matinées d'école où mes professeurs, au lieu de faire cours, se
> laissaient prendre par je ne sais quelle paresse et quelle liberté, et
> nous parlaient d'autre chose[^240].

Quelle peut être la nature de cette *autre chose* ? 

Loin de l'école, dit-on, les cartographies se dérobent --- et le monde
alors redevient langage(s).

</div>

<div class="text" data-number="10">

## 11. Foi, espérance, charité : épître aux militants

Un moment déterminant de l'histoire moderne fut le « passage » d'une
conception apolitique de la charité à une volonté politique de
transformation des structures sociales de l'économie : utopisme,
socialisme, communisme, anarchisme ont ainsi opéré une reprise de cette
vertu morale en la soumettant à la nécessité d'une action concrète, de
solidarité par exemple --- mais surtout : ils l'ont constituée comme le
motif d'une volonté, violente ou pacifique, de transformation sociale
des causes de l'injustice.

L'émergence de ces formes politiques radicales a également contribué à
« dénaturaliser » le monde social, à rendre compte de ses tendances en
tant que déterminées par l'organisation capitaliste (coloniale et patriarcale) des
rapports de production et d'échange, ainsi que par l'État-nation moderne
en tant que corps institué chargé d'administrer des populations et
d'occuper la fonction d'agent pacificateur de la conflictualité sociale.

Le sédiment d'une telle opération (de la charité vers l'amour
révolutionnaire) va conjointement mûrir au sein de la tradition
philosophique. Si cette opération était justifiée, elle n'a pas attendu
sa formalisation théorique sous l'égide d'un Hegel pour se matérialiser
dans l'action révolutionnaire d'un Thomas Münzer[^241] : la charité ne
peut plus trouver de signification réelle en dehors d'un acte de
résistance ou d'une action transformatrice, même violente.

La charité d'inspiration chrétienne suggère l'altérité, mais la
« suggère » seulement, ne la reconnaît pas comme condition nécessaire
d'un *faire commun*. En tant qu'aspiration strictement morale, elle
entretient même la dépendance de ses objets à l'égard des structures
sociales de l'économie ; le caractère essentiellement moral de la
charité contribuant à déréaliser l'être qui en est l'objet, reproduisant
ainsi, en la maquillant de bons sentiments, une relégation du sujet
opprimé dans les limbes de l'abstraction.

Mais quel est ce prochain que la charité désigne comme l'objet de notre
commisération, feinte ou sincère ? Est-il un frère ou une sœur que les
circonstances aléatoires de l'existence ont plongé dans une situation de
besoin, ou bien un membre de la classe opprimée dont la position de
dépendance est la conséquence d'une certaine organisation sociale des
rapports de production, et auquel nous venons en aide en tant que membre
de la classe dominante ?

En s'en remettant le plus souvent à la Nature ou au Destin, la
charité n'introduit pas une pensée réellement révolutionnaire, une
pensée du monde en tant qu'objet de praxis et donc sujet aux
révolutions. La misère y est seulement hypostasiée, tandis que la
conscience morale se dote de nouvelles justifications lui permettant
d'accepter le monde *tel qu'il est*.

Une critique de la charité chrétienne, ainsi entendue, nous paraît
parfaitement fondée. Elle se justifie dès lors que la bourgeoisie se met
en scène dans toute la splendeur hypocrite de son action. Il ne sera
donc pas question ici d'atténuer ou de relativiser cette critique. Bien
au contraire : il s'agira de faire valoir la politisation de cette
attitude morale, de l'ouvrir sur les exigences d'une mutation
révolutionnaire.

Nous essaierons cependant de démontrer, en nous appuyant sur le retour
critique de Pasolini aux sources contradictoires de la pensée
paulinienne, qu'un engagement radical ne peut se fonder que sur l'amour
comme principe premier et fondateur de toute action révolutionnaire. À
moins de voir l'action politique s'hypostasier elle aussi en devenant
une substance autonome séparée de la vie sociale, arrachée aux motifs
originaires de ses auteurs, contrevenant ainsi à tout ce qui la justifie
et la convoque.

L'usage que Pasolini fait de saint Paul[^242] est éminemment critique et
lui permet d'exercer sa compréhension du monde social dans le cadre d'une
réflexion d'ampleur autour des destinées contradictoires de
l'universalisme. Ainsi commence-t-il par dissocier ce qui, chez saint
Paul lui-même, tient du petit chef terroriste, doctrinaire, pharisien,
et ce qui nous convie à l'exercice d'une révolution par l'amour. L'enjeu
est de taille : il s'agit de rendre à l'amour sa portée réellement
révolutionnaire, sa capacité à unir des êtres sans pour autant
transformer cette union en actualisation du même.

Cette réflexion théorico-poétique sur la foi, l'espérance, la charité
doit être comprise à la lumière des interventions polémiques du poète
sur les nouvelles composantes du mouvement social, mais également de ses
critiques lucides à l'égard des crimes staliniens et des organisations
de la gauche ouvrière européenne.

Les quelques vers évoquant la jeunesse soviétique[^243], ainsi
que les pages des *Écrits corsaires* faisant référence à l'uniformité
des foules moscovites[^244], ne laissent aucun doute sur l'absence
d'illusions que Pasolini nourrissait à l'égard du pouvoir et de la
société soviétiques : le pouvoir soviétique s'est embourbé dans un
universalisme bourgeois, conquérant, ayant neutralisé toute
manifestation ethnique minoritaire ou politique contestataire ; tandis
que la société soviétique s'est faite dépositaire d'un esprit du temps
qui marque les visages de cette jeunesse urbaine, les lestant d'une
tristesse infinie. Et bien que Pasolini conserve une tendresse pour ces
masses soviétiques ayant réalisé l'idéal égalitaire[^245] en le
conquérant par le bas, quand l'uniformité des masses occidentales est le
pur reflet d'un égalitarisme accordé par le haut[^246].

*Vent d'est*, *Vent d'ouest*, Pasolini refuse de choisir. De part et
d'autre, l'adhésion politique aux idéaux communistes aurait engendré la
formation violente et péremptoire d'une nouvelle morale, et donc d'un
nouveau conformisme. En cela, elle conserverait une caractéristique
inaliénable du mal bourgeois, la croyance que le pur verbalisme et les
postures de radicalité seraient en mesure de pallier l'impuissance de
l'action.

Mais qu'en est-il de cette jeunesse européenne qui se révolte contre
l'uniformisation de la société de consommation ? Elle n'échappe pas non
plus à la critique de Pasolini : son « terrorisme moral » et ses
exactions seraient, pense-t-il, un symptôme de cette société déracinée
se livrant à une violence sans fondement révolutionnaire, c'est-à-dire
sans amour et donc sans légitimité.

Quand Pasolini engage un travail sur saint Paul (dans le cadre d'un
projet de film qui ne verra jamais le jour), il médite parallèlement
dans les colonnes du *Tempo* sur cette notion de charité. Ce qu'il vise
explicitement, c'est la jeunesse extraparlementaire, la nouvelle gauche,
la petite-bourgeoisie étudiante, cette composante hétérogène et inédite
du mouvement social italien et désormais mondial. Que leur reproche-t-il
exactement ? Dans l'esprit de l'adresse paulinienne, il leur reproche
d'avoir abandonné la charité, de se vautrer aveuglément dans
l'espérance révolutionnaire au nom d'une foi dogmatique.

Du point de vue de Pasolini, il ne s'agit aucunement de critiquer la
violence révolutionnaire au nom de la morale, mais simplement de la
penser sur des bases à la fois stratégiques (en considérant son
efficacité symbolique et matérielle à un moment donné d'une conjoncture
historique) et éthiques (en articulant sa visée aux moyens qu'elle
déploie).

La principale critique de Pasolini, lorsqu'il mobilise notamment le
concept de « terrorisme moral », ne touche donc pas tant à la violence
physique qu'à l'exclusion de tout point de vue contradictoire. Il accuse
notamment une course à la légitimité révolutionnaire qui ne serait le
plus souvent qu'une modalité de distinction petite-bourgeoise ; une
distorsion morale, en somme, de la radicalité politique.

Saint Paul affirme la nécessité de fonder son action (son « œuvre » de
charité) sur un élan absolu d'amour. C'est donc que la charité, en tant
que telle, ne suffit pas. Mais comment appréhender cette notion d'amour
en tant que fondement d'une action ayant pour visée de « venir en aide »
à autrui ? Et en quoi l'amour transforme-t-il la nature de l'œuvre
elle-même ?

Chez saint Paul, l'amour universel est un chemin vers Dieu. Dans une
perspective communiste, l'amour mute et se fait amour universel du genre
humain, constituant ce dernier non plus comme moyen mais comme fin. Ce
que Pasolini semble greffer à la conception paulinienne de l'amour, tout
comme à sa version communiste sécularisée, c'est la reconnaissance d'une
différence, l'exigence de constituer non plus l'Homme, générique et
abstrait, mais l'*autre*, concret, singulier, comme un infini qui
*m*'échappe toujours.

C'est un mouvement analogue qui conduit Lévinas à penser le visage
d'autrui comme irréductible à la fin que *je* lui assigne[^247].
Mais le philosophe de l'altérité circonscrit entièrement ses recherches au domaine éthique, en exclut toute traduction politique.
Chez Pasolini, dans la mesure où sa réflexion prend appui sur une
observation du mouvement social italien, ses composantes nouvelles, ses
formes inédites d'action et de revendication, l'usage de l'amour
paulinien se voit déterminé par les exigences d'une pensée *en
situation*. Plus généralement, cette notion d'amour constitue un motif
critique de régulation et de renouvellement de la pensée marxiste, et
notamment de son rapport entre théorie et pratique.

Reprenons donc le passage sur la foi, l'espérance et la charité dans l'épître
aux Corinthiens de saint Paul, et essayons de les interpréter « à la
manière de Pasolini » :

> J'aurais beau parler toutes les langues des hommes et des anges, si
> je n'ai pas la charité, s'il me manque l'amour, je ne suis qu'un cuivre
> qui résonne, une cymbale retentissante.

La langue, si elle n'est pas la manifestation d'un élan sincère d'amour
vers autrui, perd son efficacité pratique. Si la valeur d'une langue est
déterminée par le motif qui en justifie l'usage, l'universel que charrie
le communisme n'est donc pas d'abord affaire de propagande, mais de
rencontre.

> J'aurais beau être prophète, avoir toute la science des mystères
> et toute la connaissance de Dieu, j'aurais beau avoir toute la foi
> jusqu'à transporter les montagnes, s'il me manque l'amour, je ne suis
rien.

Si mon engagement n'est que le véhicule d'une doctrine, d'un bloc
théorique invariant, d'un pur discours, il aura beau être théoriquement
fondé, il n'aura aucune valeur de vérité. Il ne peut y avoir de théorie
juste sans fondement éthique. Rien ne sert de rabâcher des vérités si
celles-ci ont aboli ce qui les fonde. Une vérité ayant perdu son
fondement éthique est nécessairement une théorie vide.

> J'aurais beau distribuer toute ma fortune aux affamés, j'aurais beau me
> faire brûler vif, s'il me manque l'amour, cela ne me sert à rien.

La charité qui ne repose pas sur un élan véritable d'amour, qui
œuvre mécaniquement, formellement, sans pour autant ressaisir
perpétuellement ce qui la motive, n'a aucune valeur. Autrement dit : ce
n'est pas l'efficacité d'une action qui détermine sa valeur, mais
l'intention qui la soutient.

> L'amour prend patience ; l'amour rend service ; l'amour ne jalouse pas ;
> il ne se vante pas, ne se gonfle pas d'orgueil ;

La promotion et la mise en scène de ses œuvres, petites ou grandes, en
défigurent nécessairement la nature (et donc la portée).

> il ne fait rien d'inconvenant ; il ne cherche pas son intérêt ; il
> ne s'emporte pas ; il n'entretient pas de rancune ;

Si mon amour est rejeté par celui ou celle à qui je l'adresse, si mon
action n'est pas reconnue, si mon œuvre n'est pas comprise, je ne dois
pas pour autant nourrir de ressentiment. Au risque de me rendre
impuissant, à l'avenir, à faire ce que je crois juste. Le ressentiment,
en tant que lunette déformante de mon rapport à autrui, est l'effet
d'une action conçue de façon instrumentale, le produit d'une attente
trahie, l'espoir déçu d'un « retour sur investissement ».

> il ne se réjouit pas de ce qui est injuste, mais il trouve sa joie
> dans ce qui est vrai ;

Le ressentiment conduit à se satisfaire de ce qui est injuste. La quête
de Justice (sociale) ne peut reposer que sur une joie solidaire de ses
fondements (amour).

> il supporte tout, il fait confiance en tout, il espère tout, il endure
> tout.

Obstination à traverser la saison des espérances, petites ou grandes, à
tout supporter, à se relever, à faire ce que l'on croit juste, malgré
toutes les épreuves, toutes les déceptions, toutes les faillites dont
l'action peut être l'objet.

> L'amour ne passera jamais. Les prophéties seront dépassées, le don des
> langues cessera, la connaissance actuelle sera dépassée.

Le Savoir, la Doctrine, la Théorie révolutionnaire... tout ceci est
nécessairement imparfait, contingent, conjoncturel, périssable. Seul
l'amour fonde la possibilité d'une action dont la signification demeure
pérenne. Il ne peut donc s'agir d'obéir à un système de valeurs morales
qui nous précède, mais de ressaisir *en soi* et *par soi* le motif d'une
action qui constitue autrui comme son horizon sans cesse redéployé.

> En effet, notre connaissance est partielle, nos prophéties sont
> partielles.

La totalisation que vise le savoir théorique, si elle est légitime dans
son intention, doit conjointement prendre conscience de ses inévitables
limites. Celle-ci sera toujours faillible, inachevable, sujette aux
variations et aux révolutions. Le théoricien révolutionnaire doit donc
prendre la mesure du caractère toujours relatif, fragile, perfectible de
ses élaborations, de ses trouvailles. Autrement dit : il doit se méfier
de tous les systèmes théoriques surplombants, achevés et clos.

> Quand viendra l'achèvement, ce qui est partiel sera dépassé.

Le communisme vise le salut pour les vivants. Quand celui-ci adviendra,
toutes nos certitudes théoriques seront bouleversées. En prenant appui
sur un horizon plus modeste, ajoutons que chaque mouvement social,
chaque lutte, chaque résistance, même moléculaire, bouleverse les
coordonnées de notre cœur et de notre pensée, reconfigure ainsi nos
horizons stratégiques.

> Quand j'étais petit enfant, je parlais comme un enfant, je pensais comme
> un enfant, je raisonnais comme un enfant. Maintenant que je suis un
> homme, j'ai dépassé ce qui était propre à l'enfant.

Rien ne permet de savoir de quoi notre pensée sera faite demain. Rien ne
permet de savoir ce que nous « perdrons » et ce que nous « gagnerons »
dans l'échelle des perceptions sensibles. L'avenir n'est jamais certain.

> Nous voyons actuellement de manière confuse, comme dans un miroir ; ce
> jour-là, nous verrons face à face. Actuellement, ma connaissance est
> partielle ; ce jour-là, je connaîtrai parfaitement, comme j'ai été
> connu.

Le miroir nous renvoie toujours une image déformée de nous-mêmes. Seul
le regard de l'*autre* est vrai. Du *grand Autre*, dans le cas de saint
Paul, de Dieu. Le face-à-face (la rencontre) permettra en tout cas de
faire naître une juste connaissance de soi-même et du monde. Tout ce qui
s'établit en amont de cette rencontre est parasité par une dimension
fantasmatique et donc irréelle. Pour Pasolini, c'est le sourire de
l'*autre* qui marque mon entrée dans une dimension du monde plus totale,
irréductible à toute sorte de savoir théorique, à toute sorte de
discours sur la réalité.

> Ce qui demeure aujourd'hui, c'est la foi, l'espérance et la charité ;
> mais la plus grande des trois, c'est la charité[^248].

Concluons avec ce 13^e^ point qui ne fait que réaffirmer l'importance
conjointe de la foi, de l'espérance et de la charité, mais qui établit
cette dernière comme un fondement sans lequel les deux premières ne
seraient que des coquilles vides.

*En situation* (Marseille, printemps 2016). Je me souviens,
durant le mouvement social contre la loi travail, de ce tract accusant
vivement les employés d'un centre commercial de se détourner de la
grève. Lorsque certains d'entre nous firent simplement remarquer que ce
n'était peut-être pas la meilleure façon de persuader des travailleurs
de rejoindre le mouvement, les auteurs du tract leur rétorquèrent
sèchement : « Nous, nous faisons de la politique, pas de la charité. »
L'altérité prolétaire était ainsi niée par une radicalité purement
discursive. Plus grave : des membres précarisés de la bourgeoisie
n'ayant pas peur de reproduire une violence et un mépris à l'égard d'une
classe dont ils s'instituaient eux-mêmes comme les représentants.

Non loin de cette première assemblée, composée exclusivement de
membres de l'ultragauche, se déroulait la Nuit Debout locale. Devant
cette incarnation d'un universalisme abstrait et de son absence de sol,
les quelques radicaux présents s'empressèrent d'accuser bruyamment ces
formes de l'engagement citoyenniste d'être vectrices d'une captation
électorale petite-bourgeoise. Mais dans cette course à la légitimité
révolutionnaire, ils refoulaient aveuglément, une fois encore, leur
propre appartenance de classe, et s'érigeaient ainsi comme les
représentants d'un prolétariat idéalisé dont ils ne faisaient pas
partie.

Cette concurrence moraliste n'est qu'un jeu de pouvoir interne à la
petite-bourgeoisie, qui se déploie fort loin du prolétariat et du
sous-prolétariat réels, de leurs composantes multiples et
contradictoires, de leurs possibilités surtout de s'emparer d'une
liberté sans prérequis eschatologique ou savant.

Par ailleurs, cette course à la légitimité révolutionnaire est le
théâtre d'un affrontement spectaculaire qui peut brimer la pensée et
endommager le cœur : il suffit d'assister à certaines AG pour constater
qu'il y a quelquefois si peu de bonté au sein de ces collectifs érigeant
pourtant la solidarité comme un mot d'ordre.

Il est vrai que la fragmentation du mouvement ouvrier, sa
dissolution dans l'hédonisme marchand nous ont rendus orphelins d'un
sujet révolutionnaire. Mais cette représentation homogène du prolétariat
a toujours été une illusion, populiste ou républicaine, qui se perpétue
aujourd'hui dans la nostalgie d'un peuple-ouvrier-laïc-travailleur.

Tant de communistes sincères se sont mécaniquement repliés sur la
République, faute d'un sujet concret sur lequel faire reposer leur
imaginaire politique. Cette dernière est même devenue le sujet de
substitution de toute affirmation politique institutionnelle de gauche ;
chacun revendiquant son attachement indéfectible à l'institution ou
contraint de justifier son amour en martelant son adhésion à un
sentiment majoritaire d'appartenance à une idée du Peuple ou de la
Nation.

Quand des meurtres nihilistes ont été commis ces dernières années
par des individus ou groupes d'individus se revendiquant de l'Islam,
tous les enfants suspects de France (ceux que l'origine et la confession
des parents rendaient suspects *de facto*) ont également été sommés de
se reconnaître dans un slogan publicitaire. Mais si la mémoire,
l'hommage, le recueillement s'instituent en ordre moral, socle d'un
universalisme imposé et donc vide, les actes de mémoire, d'hommage, de
recueillement ne peuvent plus être considérés comme des actes de
liberté. Il faut faire en sorte que la voix des témoins soit en capacité
de parler à tous, et pour cela elle doit être soustraite à tout appareil
idéologique d'État.

Par ailleurs, la gloire d'une idée, celle du Peuple par exemple,
même héritée de combats nécessaires, ne peut se traduire par
l'édification de monuments somptueux, par l'institutionnalisation d'une
morale imposant à autrui de se soumettre ou bien de déserter le champ du
langage.

Lorsque François revint à Assise pour y mourir, il vit la cathédrale
qu'il avait modestement retapée, à la force du poignet, aidé de quelques
camarades : il s'agissait désormais d'un édifice énorme, écrasant, censé
manifester la grandeur de sa vie et de ses œuvres. Il en fut écœuré. Le
succès et l'audience de saint François ont contribué à en trahir la
parole, à en défigurer la visée émancipatrice.

Toute idée, tout mouvement, dès qu'ils tendent à devenir majoritaires,
tendent également à la pétrification. Celle-ci accuse en premier lieu la
bonté, la moins efficace des armes dans la conquête du pouvoir. Pour ce
qui est de la foi, l'aveuglement ne l'a jamais contrariée. Ni
l'espérance, susceptible de sacrifier l'ici-bas.

La déception de saint François est emblématique du syndrome
majoritaire (en tant que tendance, non en tant qu'état de fait : les
franciscains n'ont jamais été majoritaires). Dès lors que ses idées se
sont confrontées à la possibilité de s'étendre à toute la société, elles
ont justifié l'édification d'un patrimoine prétendument commun et qui
gravait pourtant dans la pierre le privilège de ses auteurs.

Il en est ainsi de la novlangue militante, opérateur
d'identification et donc également d'exclusion, comme des façades au
style gothique renaissant dont furent recouverts les édifices religieux
de Toscane et d'Ombrie à partir du XIII^e^ siècle : elles manifestent la
nature arrogante et imbécile des « milieux militants » comme de l'Église
catholique. Et, d'ailleurs, comment ne pas appliquer aux premiers la
prescription que Pasolini adresse à la seconde :

> Si elle veut survivre en tant qu'Église, l'Église ne peut donc
> qu'abandonner le pouvoir et faire sienne cette culture qu'elle a
> toujours haïe --- qui est par nature libre, antiautoritaire, en
> perpétuel devenir, contradictoire, collective, scandaleuse[^249].

Est-ce à dire que nous en appelons à l'amour platement universaliste ou
au culte de la tolérance ?

> La tolérance, sache-le bien, est toujours purement nominale. Je ne
> connais pas un seul exemple ni un seul cas de tolérance réelle. Parce
> qu'une « tolérance réelle » est une contradiction dans les termes. Le
> fait même de « tolérer » quelqu'un revient à le « condamner ». La
> tolérance est même une forme plus raffinée de condamnation. On dit en
> effet à celui qu'on « tolère » --- mettons, au Noir que nous avons pris
> comme exemple --- qu'il peut faire ce qu'il veut, qu'il a pleinement le
> droit de suivre sa nature, que son appartenance à une minorité n'est pas
> un signe d'infériorité, etc. Mais sa « différence » --- ou plutôt sa
> « faute d'être différent » --- reste la même aux yeux de celui qui a
> décidé de la tolérer et de celui qui a décidé de la condamner. Aucune
> majorité ne pourra jamais effacer de sa conscience le sentiment de la
> « différence » des minorités. Elle en sera toujours consciente,
> éternellement, fatalement. Par conséquent, le Noir pourra bien sûr être
> un Noir, c'est-à-dire vivre librement sa différence, même au dehors du
> « ghetto » physique, matériel qu'on lui avait assigné à des époques de
> répression. Bien sûr.
> 
> Néanmoins, la figure mentale du ghetto survit et est indestructible. Le
> Noir sera libre, il pourra vivre nominalement sans obstacle sa
> différence, etc., mais il restera toujours à l'intérieur d'un « ghetto
> mental », et malheur à lui s'il en sort.
> 
> Il ne peut en sortir qu'à la condition d'adopter le point de vue et la
> mentalité de ceux qui vivent hors du ghetto, c'est-à-dire de la
> majorité.
> 
> Aucun de ses sentiments, de ses gestes, aucune de ses paroles ne pourra
> avoir la « couleur » de l'expérience spécifique vécue par quelqu'un dont
> la subjectivité est enfermée dans les limites assignées à une minorité
> (dans le ghetto mental). Il doit se renier tout entier, et faire
> semblant que l'expérience qu'il a derrière lui soit une expérience
> normale, c'est-à-dire majoritaire[^250].

Il n'est plus temps d'édifier des monuments de marbre et de morale. Si
le communisme est bien une pratique concrète du commun, il ne peut être
question de le souiller par l'idéologie bourgeoise, cet aplanissement
des différences, cette fausse égalité qui perpétue la loi des grands
nombres et nous transforme en masse de consommateurs neutres,
réceptacles d'une morale unitaire et raciste.

Il s'agit de redonner à la pratique militante, non les aspects d'un
cours du soir de la République, mais ceux d'un espace de convivialité
dans le sens où Illich le définit :

> L'homme qui trouve sa joie et son équilibre dans l'emploi de l'outil
> convivial, je l'appelle austère. Il connaît ce que l'espagnol nomme la
> *convivencialidad*, il vit dans ce que l'allemand décrit comme la
> *Mitmenschlichkeit*. Car l'austérité n'a pas vertu d'isolation ou de
> clôture sur soi. Pour Aristote comme Thomas d'Aquin, elle est ce qui
> fonde l'amitié. En traitant du jeu ordonné et créateur, Thomas définit
> l'austérité comme une vertu qui n'exclut pas tous les plaisirs, mais
> seulement ceux qui dégradent la relation personnelle. L'austérité fait
> partie d'une vertu plus fragile qui la dépasse et qui l'englobe : c'est
> *la joie, l'eutrapelia, l'amitié*[^251].

Le rêve de révolution est intimement lié à sa réalisation, lente et
douloureuse, joyeuse et impromptue. Dans le sillage d'Illich, Silvia
Federici déplore ainsi la professionnalisation du militantisme, son
stakhanovisme, son aridité, et en appelle à la constitution de nouveaux
espaces reproductifs :

> Je crois que la gauche radicale a souvent échoué à attirer les gens par
> son manque d'attention à la dimension reproductive de notre travail
> politique --- les repas pris en commun, les chansons qui renforcent
> notre sentiment d'être un sujet collectif et les relations affectives
> que nous développons entre nous[^252].

Ajoutons ici que la constitution de nouveaux espaces de formation et de
partage pourrait bien être le remède le plus efficace à toute forme de
terrorisme idéologique. Par sa propension à expérimenter et à engendrer
de nouveaux rapports, l'activité collective de la pensée pourrait avoir
le pouvoir de nous révéler à nous-mêmes. Et, par la médiation et la
confrontation des intelligences, nous permettre de repenser les modes de
production du savoir : non plus comme procédure d'acquisition verticale,
mais comme production collective soustraite à la clarté stérile et
triomphante de l'ego.

D'un point de vue plus théorique, il s'agit peut-être de reprendre à
notre compte le renversement épistémologique que Nietzsche fait subir à
la méthode cartésienne --- ne plus guetter l'assimilation qui ramène tout
au même, mais soutenir des interprétations multiples et conflictuelles
de nos réalités partagées :

> C'est par là, précisément, que la philosophie se distingue de la simple
> assimilation (cellulaire ou logique), qu'elle n'est plus seulement une
> nutrition et une digestion qui ramène au même (constitution d'objets ou
> production de choses, propre à l'attitude naturelle comme à l'activité
> vitale), mais qu'elle est la recherche délibérée de ce qui résiste à
> l'assimilation, le plaisir pris au problématique, au merveilleux et au
> monstrueux --- bref : à ce qui, dans les phénomènes, est encore en excès
> sur ce que nous en comprenons[^253].

Cette nouvelle épistémologie d'inspiration nietzschéenne, à rebours d'un
marxisme empêtré dans son théoricisme et dans son rationalisme, nous la
considérons comme une autre forme de dépassement *et* de réalisation de
la philosophie.

Ce qui se justifie, selon Barbara Stiegler, dans la mesure où les formes
*a priori* de l'entendement, l'espace et le temps, ne sont pas des legs
intouchables de la Raison à l'être-connaissant, qu'elles sont *situées*,
historiquement, socialement, et peuvent très bien nous être arrachées
par l'emprise du néocapitalisme sur nos vies[^254].

L'enjeu n'étant donc pas seulement (ou pas tant) celui du nombre, mais
celui d'un réinvestissement collectif (même minuscule) de l'espace et du
temps. Ce qui suppose un refus de tout orgueil majoritaire susceptible
de conduire certaines luttes groupusculaires à insulter les masses au
nom d'une vérité idéologique ou d'un bloc théorique invariant[^255].

De façon plus générale, la théorie qui ne s'appuie ni ne se
confronte au réel de l'expérience collective ne peut être que vaine et
impuissante. Pensons, par exemple, au jeune Marx qui, avant d'arriver à
Paris, de se lancer notamment dans la rédaction de ses fameux
*Manuscrits de 1844*, a pu produire une critique abstraite du
capitalisme (que d'ailleurs il ne nommait pas encore comme tel) au sein
de laquelle l'ouvrier était envisagé de façon strictement théorique. Il
a fallu que ce dernier découvre les fraternités ouvrières, qu'il fasse
la rencontre concrète du prolétariat pour prendre conscience de la
positivité et de l'autonomie de ses formes de vie :

> Lorsque les ouvriers communistes se réunissent, c'est d'abord la
> doctrine, la propagande, etc., qui est leur but. Mais en même temps ils
> s'approprient par là un besoin nouveau, le besoin de la société, et ce
> qui semble être le moyen est devenu le but. On peut observer les plus
> brillants résultats de ce mouvement pratique, lorsque l'on voit réunis
> des ouvriers socialistes français. Fumer, boire, manger, etc., ne sont
> plus là comme des prétextes à réunion ou des moyens d'union.
> L'assemblée, l'association, la conversation qui à son tour a la société
> pour but leur suffisent, la fraternité humaine n'est pas chez eux une
> phrase vide, mais une vérité, et la noblesse de l'humanité brille sur
> ces figures endurcies par le travail[^256].

À mille lieues de telles considérations, indissociablement éthiques et
politiques, les vieilles organisations issues du mouvement ouvrier n'ont
pu continuer à se maintenir que dans la mesure où leur efficacité,
réelle ou supposée, justifiait encore, au nom de l'unité de combat et de
l'impératif de massification, leur nature centraliste et
autoritaire[^257]. Mais à l'ère du Spectacle des radicalités, ces
« machine\[s\] à fabriquer de la passion collective[^258] » ont été
supplantées par les nouvelles structures hégémoniques de l'Information.
Ce qui nous offre peut-être une opportunité de repenser un principe
d'organisation collective qui articulerait les moyens à des fins sans
cesse redéfinies. Refusant de considérer sa propre croissance comme son
unique but[^259], l'organisation militante sera alors en capacité de
se mettre au service de la constitution d'outils conviviaux et exigeants
lui permettant d'opposer, par la forme même de son déploiement, un
contre-modèle à l'emprise du capitalisme sur nos vies.

</div>

<div class="text" data-number="11">

## 12. Vers un romantisme révolutionnaire

Dans *Romantisme anticapitaliste et nature*, Löwy et Sayre poursuivent
leur tentative d'explicitation et d'historicisation du romantisme
révolutionnaire --- courant profane se réclamant conjointement de la
tradition révolutionnaire *et* de la tradition romantique. Au sein de
cette histoire reconstituée, hybride, prend assez naturellement place
Benjamin qui tenta de rendre une dignité philosophique au matérialisme
historique.

Dans un fragment du *Passagen-Werk*, ce dernier rend notamment compte
d'un point de rupture fondamentale avec l'orthodoxie marxiste :

> On peut voir comme un des buts méthodologiques de ce travail de
> démontrer la possibilité d'un matérialisme historique qui a annihilé en
> lui-même l'idée de progrès. C'est ici précisément que le matérialisme
> historique doit se dissocier des habitudes de pensée bourgeoise[^260].

Cette « annihilation de l'idée de progrès », nonobstant la tendance bien
réelle d'une téléologie progressiste dans la pensée marxienne, vise
pourtant à réhabiliter Marx lui-même au nom d'un postulat
anthropologique qui sous-tend toute son œuvre : le caractère métabolique
complexe qui articule lutte et unité dans le rapport entre l'espèce
humaine et ses milieux de vie.

Tel que le formule Marx dans le *Capital*, l'Homme est un être naturel
qui se transforme lui-même en transformant la Nature[^261]. Si
l'explicitation d'un tel rapport métabolique apparaît toujours au sein
de sa critique de l'économie politique, rappelons que, dès ses
premiers textes philosophiques, ce dernier cherche à déconstruire
l'opposition abstraite de l'Homme et de la Nature, opposition dont il
considère même qu'elle serait au fondement du capitalisme ; le
communisme ayant pour visée de les réconcilier, ou, pour le dire de
façon plus dialectique, d'humaniser la Nature en naturalisant
l'Homme[^262].

Si des auteurs comme John Bellamy Foster[^263], ou ici Sayre et
Löwy, se sont efforcés d'actualiser le potentiel écologiste de
l'œuvre marxienne et de la tradition marxiste qui en est issue, on ne
peut voiler cependant le fait que la pensée de Marx est elle-même
traversée d'une foi productiviste ; celle-ci fonde notamment l'hypothèse
selon laquelle le développement des forces productives accompli par la
bourgeoisie pourrait servir, au terme d'une révolution qui réaliserait
l'idéal d'une société sans classes, l'intérêt universel de
l'espèce[^264].

Selon Sayre et Löwy, l'œuvre de Benjamin serait également marquée
par un « moment » productiviste, en atteste notamment son texte de 1935,
*L'Œuvre d'art à l'époque de la reproductibilité technique*, où ce
dernier acte non seulement la disparition de l'aura dans la
production culturelle de son époque, mais invite à s'approprier la forme
et la structure sérialisées de cette production en la « politisant ».

Cette proposition (la politisation de l'art sérialisé) est déduite de ce
moment abstrait plus général d'appropriation du développement des forces
productives par les forces productives elles-mêmes, c'est-à-dire par le
prolétariat qui, en s'appropriant les moyens de production, se
réapproprierait conjointement son propre destin. Il n'est donc
aucunement question ici d'interroger le productivisme en tant que tel,
comme produit d'un rapport à la fois anthropocentrique et extractiviste
à la Nature.

Il n'est pas certain pourtant que nous puissions résoudre si aisément la
contradiction que pose le développement exponentiel des forces
productives, et notamment après Marx, tant celui-ci a depuis entraîné le
monde, et le vivant en son sein, vers une terrible menace d'extinction :
s'agit-il seulement de s'approprier collectivement la machine
productive, ou bien de la désamorcer par le retrait des forces
productives qui la font fonctionner ?

Le positionnement de Benjamin oscille souvent entre ces deux
perspectives stratégiques. Ainsi le postulat hérité de Marx (repris en
conclusion de son essai de 1935) peut être mis en balance avec un
renversement de la conception marxienne de la révolution :

> Marx avait dit que les révolutions sont la locomotive de l'histoire
> mondiale. Mais il se peut que les choses se présentent tout autrement.
> Il se peut que les révolutions soient l'acte, par l'humanité qui voyage
> dans ce train, de tirer les freins d'urgence[^265].

Ce renversement de la perspective révolutionnaire mérite sans doute
d'être considéré comme l'un des tournants majeurs de la pensée critique
dont nous sommes les contemporains. Mais s'agit-il d'un renversement
véritablement « romantique » ? Et, si tel est le cas, dans quelle mesure
celui-ci demeure-t-il « révolutionnaire » ?

Comment faire valoir, surtout, en rupture avec l'ancien romantisme,
un positionnement obstinément dialectique, à la fois critique et ouvert
sur la transformation révolutionnaire de l'ordre existant ?

> Généralement l'ancien romantisme prend distance par rapport au présent
> en se servant du passé. L'ironie romantique juge l'actuel au nom du
> passé --- historique ou psychologique --- idéalisé ; elle vit dans
> l'obsession et la fascination de la grandeur, de la pureté du
> passé[^266].

Précisons que l'énoncé de Lefebvre n'accuse aucunement un recours à la
tradition poétique de Novalis et de Hölderlin, mais un positionnement
idéologique qui se réclame (même inconsciemment) de l'Histoire des
vainqueurs.

Dans le *Talon de fer*[^267] de Jack London, Ernest, le héros
révolutionnaire, s'adresse à une foule de petits-bourgeois médusés qu'il
accuse de se faire les agents de leur propre extinction. La mise en
concurrence de tous contre tous au sein d'une économie mondialisée
génère nécessairement une perte de repères pour des classes intégrées au
capitalisme et qui vivent dans l'angoisse d'un inévitable déclassement ;
le fascisme, le postfascisme et la pensée droitière radicalisée n'ayant
pas d'autre fonction que de mystifier les tendances réelles du
développement capitaliste en identifiant un objet-cause de ce
déclassement, ce qui correspond presque toujours aux populations les
moins intégrées, surnuméraires, et le plus souvent racisées.

*A contrario*, un romantisme qui se prétend révolutionnaire doit
échapper à toute forme de passéisme et prendre appui sur des
conjonctures concrètes --- se loger au cœur des contradictions de son
temps. Son unique présupposé est celui du « vide spirituel du
moment[^268] » --- un vide que nous considérons, *avec* Pasolini, comme
le résultat de la destruction de tout enracinement humain.

Ce qui nous offre l'opportunité de revenir brièvement sur ce dont
Pasolini fut tant accusé, à savoir de se replier sur le passé et
d'abdiquer ainsi devant toute possibilité de renverser le capitalisme ou
de porter simplement atteinte à son développement effréné. 

Il le clame, à la fin des *Lettres luthériennes*, à qui saura aiguiser
son oreille, qu'il ne s'agit aucunement de se morfondre dans le regret
du passé, et encore moins de sa supposée grandeur. De façon plus
générale, il ne s'intéresse guère au passé collecté dans les manuels
scolaires, mais aux traces d'une continuité dans l'histoire, lorsque
*vitalité* et *sauvegarde* se manifestent conjointement, dans l'usage
d'une langue menacée par exemple, ou dans l'insouciance d'un sourire qui
échapperait à la hiérarchisation bourgeoise des valeurs.

S'il ne reconnaît plus les côtes frioulanes de son adolescence, s'il
déplore la balnéarisation de la péninsule italienne, s'il accuse
l'uniformisation du monde et la disparition des modes de production et
des formes de vie marginalisées par l'essor du néocapitalisme, c'est
donc toujours un amour viscéral du *différend* qui le conduit à
redéfinir sans cesse ses usages du monde.

Il s'en prendra notamment à la droite pétrifiée, à toutes ses tentatives
de récupération, d'instrumentalisation d'un passé-déjà-mort ayant pour
fonction de produire un ancrage mythifié et de justifier ainsi la
« recentralisation » du développement économique et culturel génocidaire
du capitalisme tardif. Ce repli nationaliste, passéiste, qui puise dans
les ressentiments populaires une justification de l'ordre capitaliste,
une adhésion implicite à la marche de l'Histoire, est bien une
mystification. Et Pasolini n'en démord pas.

En 1957, à l'autre bord de l'échiquier idéologique, dans la gauche
intellectuelle de son temps (plus proche de Pasolini, donc), Lefebvre
identifiait une autre forme de soumission à l'ordre existant :

> Il y a dans l'existentialisme littéraire une sorte d'espoir
> inconditionnel dans le contact avec le présent, sans distance, sans
> attitude (« sans idéologie ») permettant de dégager ses significations
> immanentes. Toute distinction, différence ou contradiction s'efface
> entre le réel et le vécu, entre le possible et le présent, entre la
> conscience et elle-même[^269].

Cette attitude, qui nous semble très largement outrepasser le cadre d'un
courant littéraire, s'offre à voir de nos jours dans deux types de
pratiques et de pensées qui se distinguent seulement par leur degré de
raffinement ou de prétendue pureté.

Il en est ainsi du recours au « capitalisme thérapeutique » qui, en
tendant à faire disparaître les contradictions au cœur desquelles nous
nous débattons, ne tardera pas, selon Illich, « à nous faire crever de
bonheur[^270] ». Nous pensons bien sûr à toutes ces techniques
thérapeutiques axées sur le traitement des manifestations névrotiques de
nos existences mutilées dont la visée est l'adaptation des sujets aux
normes hédonistes-capitalistes ; le moindre intérêt pour les causes qui
nous déterminent apparaissant comme un savoir contraignant, improductif
et mortifère.

À ce propos, dans *La Dialectique de la raison*, Adorno et Horkheimer
font dialoguer deux étudiants imaginaires dans un fragment : B. est
sommé par A. de justifier son renoncement à une carrière de médecin dans
une société au sein de laquelle l'hyperspécialisation de la profession
et l'industrie qui la soutient lui semblent problématiques ; A.
mobilisera alors toutes les armes de la logique pour le pousser à se
résigner et à épouser un destin qu'il refuse.

> Cette conversation se répète chaque fois que des individus confrontés à
> une pratique ne veulent pas renoncer à penser. La logique et la rigueur
> semblent toujours s'opposer à eux. Celui qui s'oppose à la vivisection
> ne devrait plus respirer du moment que chacun de ses mouvements
> respiratoires coûte la vie à un bacille. La logique est au service du
> progrès et de la réaction, en tout cas elle est au service de la
> réalité. Mais à l'époque d'une éducation radicalement réaliste, les
> conversations sont devenues plus rares et B., l'interlocuteur névrosé, a
> besoin d'une force surhumaine pour ne pas guérir[^271].

Adorno et Horkheimer renversent ici la perspective : « l'interlocuteur
névrosé \[...\] a besoin d'une force surhumaine *pour ne pas guérir* »
--- autrement dit : *pour ne pas renoncer à agir dans le monde tout en
ne renonçant pas à penser*. Mais comment penser (et agir) dès lors que
les forces de la logique et de la raison sont au service de l'ordre qui
nous opprime ?

Précisons qu'il ne s'agit là aucunement d'un constat qui vaudrait
pour « toutes les sociétés humaines » : la société déterminée au sein de
laquelle Adorno et Horkheimer ancrent leur propos est évidemment la
société capitaliste avancée. Celle-ci vise la neutralisation des
puissances individuelles et collectives à travers une promesse de
bien-être. Et, sur tous ceux qui s'y refuseraient, elle fait peser la
menace d'une représentation abîmée de soi, d'un gâchis, d'une faillite
de l'ego.

Autre forme de réaction au vide de notre époque, *a priori* plus proche
de nos convictions, mais qui participe également, aveuglément sans
doute, en tant que face abstraitement antagoniste de l'idéologie
bourgeoise, à la perpétuation de nos impuissances : le néoromantisme
apolitique.

Celui-ci peut prendre la forme d'une adoration acritique de la Nature ou
celle d'une haine de la Technique, ce qui, dans les deux cas, ne fait
que perpétuer, d'une façon certes inoffensive mais également inféconde,
l'opposition que nous tenons, avec Marx, comme au fondement du
capitalisme et donc corrélativement de la division capitaliste du
travail.

Ce néoromantisme sans visée révolutionnaire nous semble trouver sa cause
dans la disparition des liens qui nous lient aux traditions --- raison
pour laquelle nous vivons tels atomes jetés dans un monde en
décomposition --- raison pour laquelle, également, nos radicalités se
trouvent si souvent réduites à ce que la société marchande en fait : un
privilège pour la gauche bien intentionnée.

*A contrario*, le romantisme révolutionnaire résiste à toutes les
synthèses faciles en « \[maintenant\] simultanément la lucidité
critique, l'emploi des concepts --- et l'imagination, le rêve, en tant
qu'investigation du possible[^272] ». Cette attitude (ou cette
conscience), afin de ne pas se transformer en certitude morale ou en
posture arrogante, doit affronter le doute comme condition liminaire de
ses œuvres. 

Reprenant à notre compte la critique de Norbert Elias[^273], qui a
délaissé les écrans épistémologiques de la philosophie classique pour la
recherche sociologique, il s'agit de faire valoir la nécessité d'autrui
comme condition de ma pensée et de mon action dans le monde. Autrement
dit : reprendre Descartes par un aspect de la méthode mais non par la
logique abstraite.

Malgré la béance de nos interrogations, il s'agit surtout de se loger au
cœur des contradictions de notre temps :

> Pour le moment, l'homme nouveau, ne serait-ce pas l'homme du désaccord
> lucide et des contradictions approfondies, plutôt que l'homme de
> l'accord forcé, dépouillé de ses contradictions par un miracle
> idéologique, et par là étrangement mis en contradiction avec sa propre
> idéologie, la dialectique[^274] ?

Mais cette inscription dans les contradictions du monde, et dans les
nôtres propres, ne porterait-elle pas atteinte à notre capacité à nous
ancrer dans le présent, à vivre et à lutter ? 

Selon Nietzsche, c'est précisément notre capacité à soutenir
jusqu'au bout nos conflits, instinctuels ou logiques, qui conditionne
notre capacité à vivre et à interpréter le monde. À ce titre, il est
intéressant de noter qu'un penseur comme Lefebvre, après avoir renoncé
au marxisme orthodoxe de sa jeunesse, renouvellera sa lecture de
Nietzsche sans pour autant désavouer Marx, au prix peut-être d'une
juxtaposition conflictuelle qui n'en est pas moins singulièrement
féconde.

Il ne s'agit aucunement de demeurer à un niveau de complaisante
abstraction : si le pur langage de l'intériorité se débat toujours dans
la brume insignifiante de l'ego (subjectivisme), quel serait l'intérêt
de simplement représenter le monde des choses (objectivisme), puisqu'il
se présente déjà à nous sans que nous ayons à le convoquer ? C'est à une
confrontation, à la fois réaliste et ardente, entre le monde des choses
et nos perspectives foisonnantes, que nous convie le romantisme
révolutionnaire :

> « Nous » vivons intégralement notre temps, précisément parce que nous
> sommes déjà de cœur au-delà. Si l'homme du présent, « en nous », se sait
> en proie à l'avenir, l'homme-possible « en nous » se sait également en
> proie au présent : à un présent déjà dépassé, et d'autant plus dur.
> Cependant « nous » affirmons la beauté et la grandeur intrinsèque de la
> vie moderne, en tant qu'instables, problématiques et déchirées entre le
> passé et l'avenir[^275].

D'un point de vue eschatologique, il ne s'agit pas tant d'en finir avec
le « règne des fins » que d'inscrire cette dimension prospective au cœur
de notre engagement dans le monde.

D'une façon sans doute plus concrète, le romantisme révolutionnaire
produit une critique de la vie quotidienne en s'appuyant sur la façon
dont l'organisation du temps et de l'espace se matérialise dans des
agencements ordinaires. D'où l'importance des travaux de Lefebvre puis
de Harvey sur la politique de la ville et l'organisation capitaliste de
l'espace urbain. D'où également la pertinence des travaux de Benjamin
sur Paris, ou les descriptions pasoliniennes de la périurbanité
romaine.

Il s'agit toujours de constituer comme un point de départ, pour
notre critique et pour notre action, la façon dont la vie s'organise
concrètement afin de résister (quelquefois involontairement) --- quand
elle n'est tout simplement pas vaincue par un siècle et demi de
reconfiguration capitaliste de l'espace urbain.

En partant de la surface vulgaire du monde, le nouveau romantisme
propose indéniablement une redéfinition du « moment révolutionnaire »,
non plus comme événement qui reposerait sur une conquête du pouvoir, la
reprise en main de l'État bourgeois et du destin des forces productives,
mais comme interruption vectrice de nouveaux possibles :

> Et si la suppression de la bourgeoisie n'est pas accomplie avant un
> moment pratiquement prévisible (l'inflation et la guerre chimique en
> sont les indices), alors tout est perdu. Avant que l'étincelle ne touche
> la dynamite, la mèche qui brûle doit être sectionnée. L'intervention, le
> péril et le rythme sont, pour le politique, techniques --- et non pas
> chevaleresques[^276].

Cette intervention peut être pensée de deux façons tout à fait
complémentaires : l'une comme action technique de sabotage de la
machinerie productive, l'autre comme mise en retrait d'individus et de
groupes sociaux vis-à-vis des rapports légaux de production et
d'échange. 

Si la première implique des compétences techniques, et non des vertus
héroïques, la seconde suppose une résistance par la vie collective et
l'affirmation d'une différence (et donc d'un *différend*). Il s'agit
d'organiser autrement le temps, l'espace, de défendre un quartier contre
les assauts de la police, un squat, un jardin partagé, un espace
socialisé quelconque, etc.

À ce titre, il est tout à fait pertinent de construire des alliances
entre des formes culturelles spécifiques, autonomes, dissidentes ou tout
simplement non intégrées à l'hégémonie des modes de production et
d'échange capitalistes, et ce afin de déjouer, même à une échelle
locale, les effets sur l'espace, le temps, les corps du procès
civilisationnel capitaliste lui-même.

Norbert Elias distingue ainsi la « forme culturelle », par laquelle les
réseaux d'interdépendance humaine se forment en se médiatisant, et la
« dynamique civilisationnelle », qui vise toujours son propre
accroissement au détriment des cultures qui lui échappent[^277].
Défaire la civilisation capitaliste, c'est recomposer partout, et de
façon souvent marginale, de nouvelles modalités d'enracinement, tant
matérielles que spirituelles[^278], comme autant d'affirmations de la
vitalité des formes de culture spécifiques.

Ce qui distingue une telle approche, romantique *et* révolutionnaire,
d'un « gauchisme » qui négligerait, non seulement la lutte des classes,
mais la positivité et l'autonomie des modalités populaires
d'enracinement, c'est le refus de se replacer au centre de l'Histoire :

> « Nous » ne définirons donc ni une confrérie d'initiés, ni un dandysme
> d'intellectuels, ni une doctrine ou un système, ni quoi que ce soit
> pouvant entrer sous une dénomination analogue, mais une conscience ou
> une attitude[^279].

Ce « nous » en suspension, s'il est en mesure de se reconnaître et de
faire commun à travers l'organisation concrète du quotidien, est donc
appelé à se dépasser, loin des identités collectives, en s'efforçant de
reconnaître et d'actualiser, de défendre et de fortifier ce qui demeure
soustrait à l'hégémonie bourgeoise des valeurs et à la domestication
capitaliste des corps.

Un tel dépassement est également appelé à se réaliser dans le cadre d'un
mouvement social --- d'une façon nullement « chevaleresque » : dans cette
phase mouvementiste de la lutte, la détermination du rythme et des
modalités d'action, le calcul des risques que cette action est
susceptible de causer à ses auteurs, étant des aspects strictement
techniques de la chose politique.

En résumant ainsi l'action politique à ses aspects techniques, Benjamin
abat ses cartes : la véritable politique n'est jamais réductible à la
misère électoraliste, elle ne peut demeurer confinée à la temporalité
homogène et vide du pouvoir ; l'action politique, technique donc,
qu'elle prenne la forme d'une grève, d'un sabotage, d'une occupation,
doit viser l'irruption d'un autre rapport au temps --- et à l'espace.

Ainsi la grève, par exemple, dans la mesure où elle échappe à deux types
d'écueil --- celui de la grève comme simple instrument de négociation
(stratégie sociale-démocrate) ou celui de la grève comme moment
strictement négatif (stratégie du marxisme orthodoxe) --- est dotée à la
fois d'une négativité (suspension du temps) et d'une positivité
(nouvelle temporalité) qui sont irréductibles aux fins visées par la
politique en tant que régime de pratiques et de pensées abstraites.

À ce titre, et comme le note très justement Stiegler :

> Toute remobilisation implique d'abord, au stade où nous en sommes, que
> nous prenions politiquement au sérieux la question de l'espace et de ses
> lieux et la question du temps et de ses rythmes[^280].

En 1936, Simone Weil se réjouissait déjà des potentialités non
directement politiques de l'acte de grève et d'occupation. Dans un
contexte de répression policière réduite[^281], écrit-elle, en raison
de la conquête électorale récente du Front populaire, les ouvriers font
alors la découverte d'une joie collective :

> Joie de vivre, parmi ces machines muettes, au rythme de la vie humaine
> --- le rythme qui correspond à la respiration, aux battements du cœur,
> aux mouvements naturels de l'organisme humain --- et non à la cadence
> imposée par le chronométreur[^282].

« Indépendamment des revendications », poursuit-elle, il s'agit de
réinventer des liens de camaraderie, de partager les temps dédiés à la
reproduction de la force de travail, etc. Ce critérium vitaliste de la
lutte se justifie par l'idée que seule une relative autonomie dans
l'organisation du commun peut préparer à la réappropriation de la
machine productive.

À travers, qui sait, une prise de contrôle ouvrier de la production. Et
si nul ne peut garantir qu'un tel objectif sera atteint, Weil observe
que la joie des ouvriers durant ces journées de grève et d'occupation
repose sur leur capacité à devenir maîtres du temps et de l'espace, même
de façon éphémère. Cette joie, qui est donc le seul indicateur d'une
émancipation sans entrave, apparaît alors comme un renversement des fins
visées : qu'importe l'espérance d'une fin lointaine si la lumière
illumine le visage de celles et ceux qui luttent, ici et maintenant,
cette joie collective excédant déjà le monde légal de la production et
des échanges.

S'il est vrai que Pasolini ne s'occupa jamais de stratégie
révolutionnaire, à proprement parler, sa défense des cultures
spécifiques, de ces formes de vie sociale échappant à l'hégémonie
néocapitaliste, à son type de production monolithique et à ses normes
culturelles, sa profonde mise en question, en somme, non seulement du
Progrès mais de la Liberté fétichisée, nous permet de le considérer,
contre tout passéisme, comme l'un des inspirateurs, aux cotés de Weil ou
Benjamin notamment, d'un tel renversement de la conception marxienne de
l'action révolutionnaire.

Souvenons-nous encore de ces « visages, avec un sourire
d'adolescent / qui démontrent qu'aucune société ne contient tout à fait
le monde ». Malgré le constat accablant d'une intégration quasiment
sans limites de l'âme prolétaire au destin glacé de la marchandise, et
malgré toutes ces larmes versées sur un monde en décomposition, Pasolini
n'a cessé de guetter la possibilité d'un sourire qui échapperait à la
pétrification néocapitaliste du vivant.

Proposons ainsi de renverser la
représentation éthérée du vieux romantisme, sa mélancolie grimaçante, et
de lui opposer ce sourire sans horizon d'attente de *Trasumanar e
organizzar* (1971) ou cette « vitalité désespérée » des ouvriers
parisiens en 1936.

Que valent un tel sourire et cette joie devant les austères
exigences de la lutte révolutionnaire ? Tout. Et bien que le reproche de
s'enfermer dans une esthétique de la défaite puisse être entendu. C'est
d'ailleurs le cas de toute action défaillante sur le plan technique et
mue par un volontarisme idéologique. Mais dès lors que cette
interruption permet une resubjectivation des groupes en lutte, elle
permet également une redéfinition, toujours ouverte, de ses fins.

*A contrario*, la grève strictement instrumentale, tout comme les
mobilisations intégralement structurées par des bureaucraties
syndicales, est le meilleur moyen de hâter la ruine de toute espérance
révolutionnaire. Le radicalisme abstrait, incapable de reconnaître les
formes vives de l'histoire et de s'y loger, satisfait de lui-même et de
son verbalisme, constitue l'autre face d'une telle faillite. Ce sont
deux manifestations, pragmatique ou naïve, de l'embourgeoisement de
l'esprit révolutionnaire et de l'appauvrissement de nos pratiques de
résistance.

Dans *Le Principe espérance*, Bloch nous met en garde contre la
propension de la classe bourgeoise à entraîner le monde dans son
effondrement :

> Le phénomène de crise revêt alors le masque subjectiviste de la crainte
> et le masque objectiviste du nihilisme : il est enduré mais non élucidé,
> déploré mais non changé. Le changement est d'ailleurs impossible sur le
> terrain de la bourgeoisie, ou, pourrait-on dire, dans l'abîme qui s'est
> ouvert à elle et où elle s'est installée, et tout changement y serait
> impossible même s'il était voulu, mais il ne l'est pas. Car la
> bourgeoisie a plutôt intérêt à attirer tout autre intérêt opposé au sien
> dans sa propre défaite ; et pour amollir la vie nouvelle, elle fait de
> sa propre agonie un état apparemment fondamental, apparemment
> ontologique. L'impasse typique de l'Être bourgeois est étendue à toute
> la condition humaine en général, à l'Être *en soi*[^283].

Ce pour quoi le romantisme révolutionnaire, tel que nous entendons le
penser ici, au terme d'une hybridation conflictuelle du communisme
hérétique de Pasolini avec un certain héritage marxiste et postmarxiste,
est appelé à se loger au cœur des luttes de notre temps ; en cherchant à
expliciter (et à habiter) les contradictions qui les traversent, mais
sans nulle ambition conquérante ou hégémonique ; il s'agit d'une
attitude ouverte sur la variété des tactiques ayant pour visée
l'interruption de ces locomotives jetées sur les rails du Progrès. Dans
la mesure où nous en sommes les passagers, certes involontaires, il ne
revient qu'à « nous » d'en définir le rythme et les modalités.

Afin de donner corps à notre utopie, de l'ancrer dans l'existant, il est
donc temps de donner une forme nouvelle à notre mélancolie. Pour ne pas
laisser le vieux romantisme ni les résidus réactionnaires de l'idéologie
bourgeoise ronger notre vitalité et nos capacités concrètes de vie et de
résistance. Reprenant à notre compte la proposition de Yeats --- *We were
the last romantics*[^284] ---, nous affirmons ainsi l'actualité et le
fondement d'un romantisme révolutionnaire. Nous lui adjoignons un motif
d'espérance secret --- *excès d'amour de ma désespérance* dirait Pasolini ---
et dont l'issue, que nous ne connaîtrons pas, pourrait déterminer *a
posteriori* si nous serons sauvés ou définitivement engloutis dans le
vide spirituel de l'époque.

</div>

<div class="text" data-number="12">

# Appendice : Lettre d'un jeune homme sans histoire à Pier Paolo Pasolini

<div class="epigraphe">

On ne saurait se situer au-dessus de son époque, mais il est possible de contribuer à la constituer comme objet de conscience commune, donc de la rendre effective, et ainsi d'en envisager l'abolition[^285].

--- G. W. F. Hegel (tirant sur une gitane, au milieu des ruines, sous le ciel plombé de Berlin)

</div>

&nbsp;

Caro Pier Paolo,

&nbsp;

Je t'écris au nom de tous ceux qui, pour ne pas disparaître, doivent
assumer d'aimer ce monde à leur manière, ingrate et merveilleuse :
travailleurs improductifs, chômeurs, parasites, voyous au grand cœur,
intérimaires, travailleurs flexibles, syndiqués et militants sincères,
petits-bourgeois déclassés ayant appris à vivre en renonçant à leurs
privilèges, etc.

Le capitalisme néolibéral, depuis ta mort, il y a quarante-sept ans, a
perfectionné sa langue. Mobilisant des technologies que tu n'aurais pu
imaginer afin de mieux organiser le travail, de baliser l'errance.
Clôture qui enrégimente l'entièreté de la société. Toute la société ?
Non. Tu sais bien que tout ce qui vit a cette fâcheuse tendance à se
débattre jusqu'au bout. Mais les doctrines de l'ordre, le retour
opportuniste des vieilles valeurs clérico-fascistes et l'hédonisme
marchand ont désormais fusionné au point, qu'en France aujourd'hui,
c'est bien le parti néolibéral le plus assumé qui gouverne avec la
droite raciste la plus nauséabonde. Et la gauche ? Tu la connais, la
gauche, elle n'a pas abandonné ses beaux idéaux progressistes, en
refusant, toujours, de comprendre que le Progrès est le nom, en dernière
instance, de ce désastre.

Au sein de la grande classe d'improductifs dont je fais partie, tu n'es
pas sans savoir que nous ne sommes pas tous logés à la même enseigne :
certains luttent pour survivre, d'autres pour donner une signification à
leur existence ; certains vivent ainsi dans une précarité maîtrisée et
choisie, tandis que d'autres n'ont rien choisi et tentent seulement de
subvenir à leurs besoins les plus impérieux.

Les grands centres urbains populaires, berceaux de cette classe bâtarde
où poètes, chômeurs, émigrés ont dû apprendre à vivre ensemble,
n'existent plus --- ou sont en voie d'extinction, comme à Marseille.
L'exil vers la néoruralité des plus privilégiés abandonne la place à une
petite-bourgeoisie gentrificatrice des plus décomplexées, accroissant
les contrastes et la violence entre sous-prolétaires et
petits-bourgeois. Mais l'État travaille activement au perfectionnement
de sa doctrine de l'ordre, nous sommes donc tranquilles.

La destruction des services publics a concrètement coûté beaucoup à ma
génération : qu'il s'agisse de la ruine de l'Hôpital, qui n'est plus à
démontrer, qu'il s'agisse de l'École où, à mesure que l'État renonçait
aux moyens permettant d'assurer des conditions de travail décentes aux
salariés de l'Éducation nationale, ainsi qu'un véritable espace
pédagogique aux élèves, on misait sur l'exacerbation de notre morale
civique et républicaine, ce qui ne pouvait qu'accroître le désastre.

À l'époque[^286], tu écrivais que la violence de l'acculturation, que
les chimères du Développement et du Progrès se supportaient plus aisément
en France qu'en Italie, car les services publics, chez nous, tenaient
encore la route. Ce n'est plus le cas, cher Pier Paolo, ce n'est plus le
cas.

Parmi les travailleurs sociaux, les enseignants, les assistants
d'éducation, les personnels soignants, pour certains issus de la
petite-bourgeoisie déclassée, les autres appartenant à la grande masse
de celles et ceux qui n'ont que leur force de travail pour survivre
(auréolés quelquefois d'un diplôme d'État), beaucoup ne sont plus en
mesure d'assumer la violence des conditions de travail --- mais surtout :
comment apporter un soin à l'autre quand nous sommes nous-mêmes dans
l'incapacité de donner un sens à notre existence ?

Un immense appel à la désertion bruit aux quatre coins du pays. Même les
enfants de la bourgeoisie abandonnent leurs brillantes études pour
investir leur héritage dans un petit bout de terrain et vivre loin des
miroitements de la marchandise et de la dictature du bien-être.

Beaucoup en reviendront, se retrousseront les manches, acceptant
finalement, résignés et lucides, de participer au dernier grand défilé
du Progrès. Un baroud d'honneur. Fort à parier qu'il s'agira d'une
manifestation tout à la fois extrêmement militarisée et extrêmement
attrayante du point de vue de la séduction marchande.

Nous formons tous un désir de désertion, qui est aussi un désir de
destitution de ce corps mort qui gît en nous, celui du pouvoir, celui de
l'ordre, celui du discours, qui nous ronge la glotte, les bronches, la
trachée et nous brûle la langue comme un acide.

Nous, qui sommes nés sans histoire.

Nous, qui n'avons pas eu le beau crépuscule de nos aînés, cette
complaisance d'esthète, les ciels déchirés de la Mitteleuropa, derniers
*scugnizzi* de Naples essayant de nous voler un billet, l'art de vivre
tzigane se déployant dans des grottes de Grenade...

Nous, qui n'avons eu ni le Voyage ni la Grande Tradition Humaniste pour
nous étourdir ou simplement tenir à quelque chose.

Nous, qui sommes les héritiers chétifs du petit Hanno, dans les
*Buddenbrook* de Thomas Mann[^287], n'ayant plus la force d'affronter
les exigences d'une bourgeoisie qui nous somme de continuer ce en quoi
nous n'avons jamais cru.

Nous, qui demeurons sans histoire.

Ce pour quoi nous avons la passion des généalogies et des unions
intempestives.

Ce pour quoi nous sommes si souvent séduits par les crépuscules, les
ruines et la mélancolie vidée de toute substance de nos aînés.

Mais comment pourrions-nous bâtir une société plus juste et plus vivante
sur la brume lénifiante d'un parc d'attractions ayant pour thèmes les
décors de leurs propres crimes, dont ils ont pris soin de se dédouaner
--- voix chevrotante et larmes opportunistes des hommages que la
République se rend à elle-même ?

Nous, qui sommes nés à la fin des années 80, au début des années 90,
avec la chute du mur et de nos rêves de révolution : que ferons-nous de
cette nostalgie abstraite, caricaturale, soustraite à la matérialité des
pures formes de vie que la nouvelle préhistoire broie avant d'en
revendre les haillons sur des marchés impersonnels et tristes ?

Cher Pier Paolo, nous détenons quelque chose que vous n'aviez pas : nous
sommes nés après l'espérance. Notre tristesse, nos natures névrotiques, sinistres, angoissées ne tiennent qu'à la difficulté que nous avons à honorer et à perpétuer un certain héritage, celui d’une histoire qui s’est présentée à nous comme la seule histoire possible et qui n’était pourtant que l’histoire bourgeoise.

Mais dès lors que nous l'accepterons, que nous assumerons de n'être plus
solidaires des Nations et du Progrès, au nom desquels les crimes commis
sont innombrables, que nous ne nous soumettrons plus au bon sens
bourgeois et au règne de la Raison instrumentale, que nous refuserons
d'abandonner nos vies à un *principe de rendement*, que nous ne
baisserons plus les yeux devant la médiocre rhétorique des pourfendeurs
du communisme, de tous les communismes, du communisme entendu
schématiquement comme l'autre face et la seule alternative à la clôture
de l'histoire, alors nous serons forts d'une vigueur nouvelle. Et il ne
sera plus permis d'être déçus, trahis, abattus.

J'ose croire que cette force, *vitalité désespérée*, telle que tu lui as
donné forme et consistance dans ta poétique, et qui est consubstantielle
de notre (non-)rapport au monde, nous permettra, à l'échelle de notre
génération, mais surtout de celles qui viendront, d'éviter la répétition
des espérances creuses, des grandes trahisons, que nous réussirons ainsi
à assumer cette négativité, cet envers de la modernité, à nous rattacher
simplement à la nuit bleue des traditions résistantes, dans le feu de la
dernière origine, et non plus aux vieux relents, tables rases et
pseudo-avènements de l'Homme nouveau qui firent l'exaltation de nos
pères militants.

Cet « Homme nouveau », je n'ose en imaginer les traits : génériques,
bourgeois, adaptables. Osons dire que le pire stalinisme, celui qui
colora de rouge les valeurs occidentales bourgeoises, a échoué à abolir
le spécifique, visages, langues et paysages, là où le néocapitalisme a
triomphé.

Mais sache que nous avons toujours des mains de bâtisseurs, que nous
saurons les employer à réparer ce qui peut encore l'être.

Nous conservons la malice du jongleur et la grâce de l'équilibriste.

Notre poème se fonde sur le vide libéré par la destitution du Discours
et de l'idée de Progrès.

À nous d'organiser la posthumanité dont nous sommes les bâtards.

Contre la nouvelle Réaction, aussi terne que l'idée de Progrès, l'une et
l'autre imbriquées aujourd'hui par les nécessités tactiques du
moment, nous apprendrons le langage des choses qui ne furent pas les
nôtres : des savoir-faire, des langues dont nous n'avons pas hérités.

Nous forgerons de nouveaux outils et saurons utiliser ceux que le grand
Capital met déjà à notre disposition[^288].

Nous serons spontanément enclins à apporter notre soutien à toutes
celles et ceux qui souhaitent défendre un fragile héritage : une façon
de cultiver les patates, de cuire l'ail, de s'habiller, de croire.

Et nous aurons l'humilité de soutenir toutes les luttes sectorielles
sans sous-estimer pour autant les aspirations petites-bourgeoises qui
peuvent s'y loger.

Surtout si ces luttes sont en capacité de déborder le cadre sectoriel et
de construire, ici et maintenant, un *autre langage*, à la fois total et
inachevé.

L'expérience récente des gilets jaunes --- j'espère que tu me
pardonneras la liberté prise de te donner à leur sujet une opinion ---
nous a prouvé qu'un mouvement, dont tout portait à croire qu'il se
traduirait en dernière instance par un néopoujadisme petit-bourgeois et
raciste, pouvait échapper à tous les *ensignements* militants et
attaquer la gauche par sa gauche.

L'erreur fut, comme si souvent quand survient quelque chose à quoi on ne
croyait plus, de penser que ce mouvement pourrait se suffire à lui-même,
que le pouvoir néolibéral serait renversé, que la révolution était au
bout du chemin.

Au vide du politique répond un signifiant vide désormais : l'agrégation
de toutes les colères, de toutes les révoltes, construites ou
désarticulées. Un jour, pourtant, il nous faudra assumer de soutenir une
signification collective. Mais celle-ci verra le jour d'elle-même : il ne
faut surtout pas en hâter le cours.

Tout ce que nous pouvons dire aujourd'hui, c'est que le vide du
politique ne doit pas être rempli par un nouveau discours de gauche.
Tant que la gauche institutionnelle n'aura pas abandonné l'idée que
c'est à l'intérieur du développement capitaliste qu'il lui faut trouver
de meilleurs équilibres, une répartition des richesses plus juste, elle
se retrouvera inévitablement dans la position de devoir nous trahir.
Elle n'en est pas responsable, c'est la marche du monde qui est ainsi.
Sa responsabilité est de refuser d'admettre sa propre mort, ce qui
l'empêche de quitter la scène sous les larmes et les applaudissements.

Toi qui as diagnostiqué avant tout le monde la mort de la gauche en
Italie, lui as rendu le plus sincère des hommages dans *Uccellacci e
Uccellini*, à la fin du film, quand une foule d'ouvriers accompagne émue
la dépouille de Togliatti, tu sais bien que nous nous débattons toujours
avec la réalité, que nous refusons d'accepter que les années 50 sont
terminées, que nous refusons d'admettre que les *Trente
inglorieuses*[^289] ont permis une amélioration des conditions de vie
du prolétariat occidental sur le dos de la surexploitation coloniale,
qu'elles sont également responsables de l'éradication des cultures
populaires dont la *sauvegarde* justifie que nous nous battions.

En écrivant cela, je n'arrive plus à me souvenir d'un mot qu'on associe
inévitablement à la mort, à la dépouille, au cimetière : c'est celui de
cercueil. Offrons un beau cercueil à la gauche, et surtout aux
communistes officiels. Désormais il faut qu'ils se taisent. On ne peut
pas laisser les derniers faussaires d'un communisme institutionnel en
décrépitude brandir leurs « jours heureux », et souiller ainsi la
dignité d'une histoire qui fut l'une des plus belles qui soient.

Le poème n'est que balbutiement mais il contient toutes les langues,
disais-tu. Cette idée m'obsède et me revient comme un scalpel au-dessus
de toute espérance.

Comme toi, et sans doute bien plus tôt --- je veux dire en âge --- j'ai
cessé de croire en la littérature. Mais souviens-toi que si la main
tremble encore, à l'heure où nous avons tout perdu, que si le poème
balbutie et s'évanouit comme une fête lointaine ou un ballon rempli de
gaz, souviens-toi que c'est en riant que nous traversons cette si longue
saison de la posthistoire.

Ici, nous n'offrons aucune résolution, aucun au-delà --- tout ce qui
nous importe est d'épuiser notre sujet. Nous cherchons l'os de la
réalité, celui qui n'a rien d'antique ou de christique, « l'os / de la
vie de tous les jours[^290] » que tu évoquais dans « Les Pleurs de
l'excavatrice ».

Nous n'avons qu'un seul espoir : celui de réussir à continuer,
simplement continuer.

Jamais au-dessus de l'Histoire. Il s'agit de faire un pas de côté. Tout
est question de positionnement éthique : choisir la juste intensité, qui
ne renie rien de vivant, qui ne porte pas atteinte au visage de
l'*autre*, cet *autre* qui est le paysage de notre action.

Ainsi, peut-être, nous renouerons avec une certaine idée de la poésie en
tant que langage nécessitant pour se déployer un effort
d'expérimentation et une liberté qui s'affranchissent résolument du
Discours et de l'unité de combat, qui nécessitent également de tendre
vers la réalité, cette réalité contre laquelle la langue du pouvoir
s'est écrasée.

Il ne s'agira pas de recoller les morceaux d'un passé irrémédiablement
perdu, mais, par le travail du montage, de rendre possible la génération
de rapports nouveaux. Nulle table rase, mais l'obsession archéologique
jointe à la nécessité d'adapter nos formes à la nouvelle réalité
matérielle, technique et technologique du monde.

En d'autres termes, il s'agira d'éclairer
nos utopies en puisant à la source de cette quête de rédemption à
laquelle les vaincus nous somment de répondre, et de mobiliser pour ce
faire les moyens que nous avons à notre disposition. 

À ce titre, le poète n'a plus le droit de se soumettre à une
représentation littéraire éthérée de la beauté, il doit creuser la terre
du langage, abattre le régime de la représentation, éclater les
structures narratives établies, recommencer sans cesse à faire éclore
une parole neuve sur la ligne de crête qui lie sans les confondre le
signe et sa signification.

L'artiste aujourd'hui doit donc se remettre au travail. Non dans une
chambre à lui, mais sur un chemin rugueux.

L'un de tes derniers amours de poète fut pour Ezra Pound --- permets-moi
de le citer ici :

> La Beauté est difficile... la terre franche
> 
<div class="a-droite">

précède les couleurs[^291]

</div>

Fortini t'accusait de hurler dans un désert. C'est qu'il refusait
d'arpenter cette plaine aride, croyant que le discours et l'action
politique suffisaient à convoquer les couleurs. Si belles soient-elles,
ce n'est pas vrai.

Toi qui fus le premier des modernes, et le dernier des classiques, il
a fallu que tu t'adaptes. Et comme j'imagine à quel point cette
adaptation a dû te coûter.

(Ne se sont-ils jamais demandé, ceux qui te considéraient comme un
réactionnaire enlisé dans son passéisme, pourquoi tu t'étais adapté si
vite aux nouvelles formes de la technique cinématographique ? Et comment
cette technique cinématographique avait-elle pu déterminer en retour
l'écriture de tes derniers textes poétiques ?)

Après Elsa et son œuvre moderniste, sensible et pacifique --- mais
bombardée d'ogives ---, trois ans après, donc, *Le Monde sauvé par des
gamins*[^292], tu publies *Transhumaniser et organiser*. Ce texte, qui
est ton dernier grand texte poétique publié de ton vivant --- et qui n'a
jamais été traduit, à l'exception de quelques bribes dans l'anthologie
de Ceccatty[^293] --- est une prouesse d'adaptation formelle : tu
t'adaptais au morcellement de notre réalité, à la nécessité de rompre
avec l'idée de Grande Poésie Nationale, à une poétique de la brisure,
mêlant les registres, plus strictement poétiques ou documentaires.

Contrairement à Aragon qui, chez nous, fit le chemin à l'envers --- de
l'avant-garde surréaliste au renouveau de la tradition nationale --- et,
en ce sens, plus proche d'un Césaire, tu sacrifiais[^294] ta langue,
ton style, le prestige de la figure de l'auteur, et engageais ton
écriture sur des *voies nouvelles*[^295].

*Pétrole*, également, cette œuvre à la fois inachevée et fragmentaire
--- et qui ne pouvait que l'être[^296] ---, s'inscrit dans cet effort
d'adaptation formelle. Ainsi tu contribuais à réinventer la poésie,
quitte à piétiner Dante et Leopardi --- mais peut-être étaient-ce eux
secrètement qui te priaient de les piétiner ?

Tu reconnais alors le vide de notre époque et décides de fonder le poème
à partir de ce vide. Hésitations, ratures, variantes. Avec Pound en ton
sillage, articulant la tradition au modernisme le plus acide, tu
réinventes un langage. Ainsi tu apparais également comme le frère
lointain d'un Beckett ou d'un Celan.

Contrairement à nombre de préfaciers et de commentateurs, je ne crois
pas que les *Poèmes à Casarsa* soient de la poésie et que tes derniers
textes n'en soient plus[^297]. *Transhumaniser et organiser*,
*Pétrole*, *La Nouvelle jeunesse* sont à la fois en rupture avec ton
œuvre poétique antérieure et en constituent l'aboutissement le plus
cohérent.

Souviens-toi les bouffonneries de Ninetto, dans *Uccellacci e
Uccellini*, sa touffe bordélique et les grimaces de Totò, en marge de
l'enterrement de Togliatti... Tout était écrit, n'est-ce pas ?

Dans le fond, au nouveau type d'humanité qu'a engendré la révolution
consumériste, tu n'as jamais cessé d'opposer une posthumanité en son
programme : politique de l'amour, du rire et de la vie réellement vécue.

Pourquoi posthumanité ? Parce que l'humanité, c'est toujours l'humanité
bourgeoise : l'écart entre cette humanité générique et abstraite (le
citoyen du monde) et une réalité concrètement traversée d'antagonismes
(la lutte des classes) étant devenu de plus en plus insoutenable. Tout
ce qui vise à résorber cet écart, par des représentations et par des
mots, par des discours, qu'ils soient « sociaux » ou non, c'est de
l'idéologie, et toute idéologie est également bourgeoise.

Ici, la conception lukácsienne de l'idéologie en tant que mode de
livraison de la conflictualité sociale[^298] tient jusqu'à un certain
point. Un certain point seulement : l'art communiste n'a pas à sacrifier
sa liberté d'expérimentation au nom du culte de la totalité, cette
dernière étant nécessairement soumise à une grammaire de la
représentation qui, bien que *située*, se donne à voir comme
universelle. Si cette grammaire se déploie et se réinvente loin de
Garibaldi et Mazzini, pourquoi pas. Dès lors qu'elle se soumet au Roman
national, qu'elle n'entrevoit pas d'autres formes d'expression que
celles de sa langue instituée, qu'elle épouse instinctivement ses
représentations, ses valeurs, et, surtout, si ces valeurs cohabitent
avec les valeurs de la nouvelle bourgeoisie, les valeurs de
l'entrepreneuriat, du marketing et du bien-être, alors, cette grammaire,
qui nous tient lieu de structuration symbolique de la réalité, est la
matrice d'une langue morte.

Toi, tout ce qui t'a toujours intéressé, c'était de faire entrer la
réalité dans le poème. Non plus comme reflet ou fausse totalité, mais
comme source infinie de rapports possibles. Ici, quelque chose te lie
secrètement à Godard : citation, détournement, montage, etc.[^299],
visent toujours l'image (dialectique) et non la
représentation (statique) de la réalité.

Ayant trempé ta conscience et ta langue dans la grande tradition
nationale, humaniste et bourgeoise, il a fallu que tu adaptes
formellement ta poétique aux nouvelles possibilités réelles que
t'offrait la technique, et ce afin de préserver le maigre fil nous liant
à une réalité non encore dissoute dans l'irréalité bourgeoise.

Les valeurs de la nouvelle République italienne, celles des pères de la
démocratie libérale, sa langue, sa morale, tous ces éléments d'une
culture qui était celle d'un pouvoir avec lequel tu as toujours
entretenu des liens puérils[^300], ont été fondues dans les nouvelles
valeurs hédonistes-permissives de la société de consommation. Aux
premières, tu pouvais opposer de façon plus binaire un langage tissé de
toutes les langues, dialectes ou idiomes, et toutes les « pures formes
de vie » qu'elles permettaient encore de rendre visibles et de
manifester. Mais les secondes supposent un geste de destitution encore
plus grand, qui n'est plus seulement la promotion des traditions
spécifiques, vernaculaires contre la Langue et la Culture d'État, mais
une percée en deçà du langage articulé, un montage de sons, d'images,
mêlant le mythe et le documentaire, la tradition et la technique
moderniste, un *nouveau langage*.

Non seulement tu as toujours cherché à t'adapter aux nouvelles formes de
réalité que tu as vues naître (par exemple : la culture sous-prolétaire
de la banlieue romaine), mais tu réagissais également à cet écart
vertigineux entre le discours des intellectuels sur la réalité et la
réalité elle-même (par exemple : l'exclusion, chez les théoriciens du
communisme officiel, de la composante sous-prolétaire et/ou immigrée, et
ce au nom d'une conception homogène et vide du sujet révolutionnaire).

Aujourd'hui la langue des experts et des éditorialistes, intellectuels
organiques ayant réussi à supplanter les vieux intellectuels classiques,
n'est que la conséquence de cette trahison, sa forme renouvelée et
désormais hégémonique.

C'était précisément afin d'éviter de te soumettre à cette nouvelle
« réalité qui n'existait pas[^301] » que tu n'as jamais cessé de
t'adapter aux nouvelles formes de vie prolétaires et sous-prolétaires
qui seules te semblaient dignes d'être vécues --- ce qui t'a
naturellement conduit à adapter ta production poétique aux moyens de
t'emparer de leur objet[^302] --- réalité fragmentée, hybride, menacée
--- unique façon de continuer à produire du langage, c'est-à-dire à
exister en tant que poète.

Ils pensaient que tu te lamentais sur la tombe de Gramsci. Ils ne
comprenaient rien. Ils disaient que tu projetais ta propre dramaturgie
intérieure sur l'Histoire. Fortini le disait. Les communistes le
disaient. La jeunesse militante le disait. Tous ils se sont trompés.
Contrairement à eux, toi, tu n'as jamais cessé de rire. Tu étais
simplement un homme doté de passions réelles dans une société qui
devenait de plus en plus irréelle. Tu as vécu ta vie comme une tragédie,
et c'est pour cela que tu as vécu ta vie. Les dramaturgies bourgeoises,
les guerres de clans, le racisme des petites différences, tout cela
t'excédait car tu étais obsédé par tout ce qui vit et chante, par les
bouffonneries de Ninetto, par la grâce de Médée, par le hurlement
déchirant de Mamma Roma, ces visages et ces corps qui étaient et qui
continuent d'être tous les visages et tous les corps, tous les visages
et tous les corps qui échappent à la bourgeoisie et sauvent le monde de
l'irréalité à laquelle il est promis. Toi, tu n'as jamais considéré que
tu vivais dans le voisinage des *autres*. L'altérité la plus radicale
est un vertige, et c'est de cette école-là que tu te réclamais. Car tu
étais pur amour.

Quant à nous, qui t'aimons tant, qui aimons le monde également et non
sa parodie, grâce à toi nous savons désormais que le balbutiement
contient toutes les langues, et que le désespoir ne se confond
aucunement avec la résignation.

« Nous » qui étions pure lumière --- et ne le savions pas.

&nbsp;

<div class="italique a-droite">Pordenone, 10 juillet 2022.</div>

</div>

<div class="text" data-number="13">

# II. La chambre de Berlinguer (récit-fragments)


<div class="epigraphe">

Terra de artigianos e poetas,\
terra de ambulantes e pastores\
e de zente ospitale chi hat valores,\
c'hat tentu sempre sas gennas apertas.

--- Antonio Arru

</div>


*Santa Teresa Gallura*. Le gouffre creusé entre Réalité et Idéal m'a
convaincu de renoncer à suivre Pasolini *à la trace*.

<div class="asterisme">...</div>

*Pau*. Dans les hauteurs d'Ales, la ville natale de Gramsci, nous
trouvons pour quelques jours la moins onéreuse des locations. Lors de
notre second passage chez l'épicier du village, celui-ci nous installe
une table et deux chaises dans sa petite cour et nous offre de déguster
une grande *Ichnusa*[^303] avec des chips, ainsi qu'une salade
« pour plus tard ». Spontanément, nous parlons des élections d'octobre,
de la menace d'une conquête du pouvoir institutionnel par le parti de
Giorgia Meloni. À notre grand étonnement, il votera pour le parti
postfasciste : « Il n'y a qu'en France, nous dit-il un peu moqueur, que
*Fratelli d'Italia* est considéré comme un parti fasciste. Ici, à Pau,
il y a encore beaucoup de communistes, mais également beaucoup de
nostalgiques du fascisme. De toute façon, ajoute-t-il, tout ça ne veut
plus dire grand-chose, c'est de l'histoire ancienne. »

<div class="asterisme">...</div>

*Arborea*. Lors de notre seconde soirée à Pau, nous rencontrons
Antonino. Assis sur un banc, nous l'apercevons qui promène son chien.
C'est surtout avec A. qu'il discute ce soir-là. En raison notamment de
ses origines rurales tandis que je demeure un enfant de la ville : la
façon spécifique de cultiver des tomates, la distillation de
l'*acquavita*, l'élevage des chèvres, tout ceci me séduit comme un
mystère que je peine encore à percer. Durant deux heures, ils parlent
donc de choses honnêtes et précises que je ne comprends pas. Puis, quand
nous le questionnons ensemble sur Pau, l'évolution du village, sa vision
des changements qui ont transformé le pays, il nous raconte avec émotion
cette époque où les rues étaient pleines de vie, où les fêtes étaient
nombreuses et attiraient tout le pays. Lorsque je lui demande ce qu'il
pense du communisme, de Gramsci, de l'ancien temps où la région était à
la fois paysanne et plutôt à gauche, il me réplique que sa seule
nostalgie va au fascisme, le vrai, celui de Mussolini. Il nous parle
alors d'Arborea, de cette petite ville bâtie par le dirigeant fasciste,
des travaux entrepris afin de mieux organiser le système d'irrigation du
pays, de ces marécages transformés en terres fertiles qui continuent
aujourd'hui à être célèbres pour leurs fromages, leurs viandes et leurs
excellents légumes. Antonino est donc le second homme, en l'espace de
deux jours, qui manifeste à la fois sa grande générosité envers nous, un
intérêt sincère pour des étrangers de passage et sa sympathie pour le
parti de Meloni. Dans le cas d'Antonino, âgé de soixante-dix-huit ans,
et contrairement à l'épicier, qui doit avoir une petite quarantaine
d'années, il s'agit même d'une franche nostalgie mussolinienne.

<div class="asterisme">...</div>

*Défaut de transmission*. En réfléchissant, aux côtés de Pasolini, mais
également avec quelques amis italiens, militants, communistes ou
libertaires, j'en suis arrivé à la conclusion que l'Italie,
contrairement à la France et à l'Allemagne, n'avait jamais connu de
véritable discontinuité sur le plan de la morale collective entre la
période fasciste et les quarante années de gouvernance
démocrate-chrétienne. Et ce, bien sûr, malgré l'antifascisme
constitutionnel de la République italienne. Ce qui peut expliquer que la
sympathie envers Mussolini n'ait jamais cessé de s'exhiber, surtout loin
des grands centres urbains, plus ancrés dans l'antifascisme officiel du
pouvoir. Mais que peut signifier, en 2022, et notamment en Sardaigne,
cette percée de *Fratelli d'Italia ?* Cette dernière n'étant pas
seulement un simple accident démocratique, mais bien un véritable
mouvement d'adhésion populaire, une adhésion que nous avons pu constater
chez des Sardes d'une extrême gentillesse et dont les formes d'existence
sociale sont peut-être les plus préservées d'Italie. Est-ce en raison du
fait que l'histoire communiste, ouvrière et paysanne de la Sardaigne
n'est plus transmise ? Je ne saurais le dire. Il est en tout cas
remarquable qu'un homme comme Antonino puisse rapprocher, non par la
connaissance historique concrète[^304], mais par un sentiment vague de
l'histoire, Gramsci de Mussolini, comme deux figures regrettées d'un
même monde disparu.

<div class="asterisme">...</div>

*Les émigrés et les fainéants*. Avant de quitter Antonino, nous
acceptons de goûter son vin. Avec un pompier du coin, nous discutons
encore « politique », essayant de comprendre ce qui peut conduire à une
telle adhésion, explicite et revendiquée, à la tradition fasciste. Je
les questionne notamment sur leur perception de la situation italienne,
sur les causes des crises sociales et politiques que les travailleurs
d'Italie subissent depuis tant d'années, et en particulier les
travailleurs sardes. Pendant ce temps, A. est invitée par la femme
d'Antonino à visiter leur maison. Selon le pompier, ce sont les
fainéants rémunérés qui seraient la cause de toute cette déchéance. Chez
Antonino, c'est un sentiment plus vague encore d'avoir perdu un monde
qui prédomine. Il n'accuse personne en particulier, outre la classe
politique bien sûr, et ce de façon tout à fait abstraite : « *È
cosi*... », répète-t-il, las et mélancolique. Si le pompier évoque
également l'immigration et ses supposés ravages sur l'identité
italienne, jamais Antonino ne manifeste la moindre hostilité à l'égard
des réfugiés. Quand A. ressort de la maison, chargée d'un sac rempli de
petites tomates, je devine qu'elle est émue.

<div class="asterisme">...</div>

*Austis*. Sous une chaleur précoce, nous roulons vers le cœur de la
Barbagia. La région se nomme ainsi en raison du repli millénaire de ses
plus anciens habitants vers les montagnes afin de se protéger des vagues
successives d'envahisseurs. Un ami toscan me disait dernièrement que
nous ne pouvions pas pénétrer au cœur de la Sardaigne, que ce n'était
pas prohibé mais très fortement déconseillé. Antonino
lui-même nous avait décrit la veille les habitants d'Austis comme des
*ladroni*[^305], fourbes et arriérés --- une vieille histoire
d'affaires mal conclues, d'escroquerie, de vilenie évidente de la part
d'un homme d'Austis l'ayant définitivement convaincu de les haïr tous.
Nous redoutons donc un peu notre séjour là-bas. Et, en effet, notre
première rencontre avec une personne d'Austis, en l'occurrence la
logeuse, nous est assez désagréable. Nous l'entendons même railler nos
tenues vestimentaires : « Ce sont des Français », dit-elle à une voisine.

<div class="asterisme">...</div>

*Hégémonie linguistique*. Teti est un village à flanc de colline offrant
une perspective sur les massifs s'étalant vers Nuoro, mais également sur
des vallons, des lacs, des paysages secs parsemés de bois, de routes
terreuses et de clochers. Fascinée par les fruitiers sauvages bordant
les routes, éparpillés entre les ruines ou débordant de petits jardins
privatifs, A. s'aventure sur un sentier suspendu au-dessus du vide afin
d'y cueillir quelques mûres. Quant à moi, je décide de faire un tour du
*paese* afin de trouver un bar et de m'y installer pour lire. En
arrivant sur la terrasse du bar principal, en bordure d'une route qui
traverse le village, je suis immédiatement toisé par des dizaines de
paires d'yeux. Très vite, on me demande d'où je viens, ce que je fais
ici. En apprenant que je suis français, l'un d'eux, qui travaille
habituellement en Corse, s'exclame : « Viens avec nous ! » À table, nous
échangeons quelques banalités. Puis A. revient et s'installe également
avec nous. « Le Corse » tenant à épater la galerie en étalant sa
maîtrise du français, il nous faut alors batailler ferme pour parler
italien ; parler italien, de notre point de vue, c'est ouvrir les
échanges au groupe tout entier.

<div class="asterisme">...</div>

*Privilège*. Les grandes *Ichnusa* arrivent sans discontinuer, chaque
membre de l'assemblée payant sa tournée et remplissant méthodiquement
nos verres. Lorsque je demande à la serveuse s'il m'est également
possible d'offrir une tournée, elle me rétorque que non, que ce n'est
pas possible, que je n'ai pas le droit de payer car je ne suis pas
d'ici.

<div class="asterisme">...</div>

*Traditions et usages*. L'un des compères attablés est artisan
spécialisé dans la fabrication de masques traditionnels. Doté d'une
connaissance aiguë des traditions de son pays, il nous explique que le
*paese* est strictement cantonné au village et à ses environs
immédiats, mais que les traditions de chaque village sont imbriquées
dans une constellation de foyers culturels, chacun se manifestant comme
variation plus ou moins autonome d'une culture propre à la Barbagia. Ce
qui explique notamment pourquoi ces traditions sont si difficiles à
figer dans un savoir ; les spécificités culturelles du centre de la Sardaigne formant une matière composite, hétérogène, qu’unifie seulement l’usage vivant qui en est fait.

<div class="asterisme">...</div>

*Empire et mémoire*. Nous l'interrogeons longuement sur la résurgence
de la culture païenne archaïque : disparue avec l'impérialisme romain
aux environs du III^e^ siècle avant J.-C., la civilisation nuragique fut
redécouverte au XIX^e^ siècle ; il s'agit donc, prousuit l'érudit, d'une réappropriation récente de cultures depuis
longtemps enfouies dans les limbes de la mémoire collective.

<div class="asterisme">...</div>

*Chanteur*. L'un des membres de l'assemblée attire mon regard : c'est un
jeune homme d'à peine vingt-cinq ans, un peu fort, des yeux noirs,
pétillant d'intelligence. Il parle peu. Quand il prend la parole, c'est
pour raconter en sarde une histoire qui fait s'esclaffer toute
l'assemblée. Nous apprendrons plus tard qu'il est chanteur traditionnel.

<div class="asterisme">...</div>

*Espérance*. L'orage se met à gronder. Quelques gouttes de pluie nous
poussent à migrer vers une autre table couverte d'une grande tôle. Dans
la mesure où il n'a pas plu depuis quatre mois, la pluie est l'objet
d'une attente anxieuse pour les habitants du pays. Mais les nuages se
dissipent vite, l'espérance d'une pluie régénératrice également.

<div class="asterisme">...</div>

*Apparition*. Deux ânes magnifiques font alors leur apparition : deux
ânes marron et gris, en parfaite santé, suffisamment robustes pour
soutenir leur vigoureux cavalier, deux jeunes hommes d'ici, à peine plus
âgés que leur monture.

<div class="asterisme">...</div>

*L'« ami » de Gramsci*. Affublé d'une moustache proéminente et d'un air
très sympathique, un nouveau personnage fait son apparition. Ce dernier
a émigré en Belgique il y a longtemps mais revient chaque année dans son
village, avec sa femme et quelques amis, belges ou calabrais. Quand il
nous demande ce que nous faisons dans la région, je lui réponds que nous
souhaitons découvrir la terre de Gramsci --- façon un peu lyrique de
sonder ce que représente encore le théoricien communiste dans la région
qui l'a vu naître. Il me répond dans un français parfait que c'est une
belle raison de faire du tourisme. Puis se rapproche de moi : « Tu sais
qu'il y a un ami de Gramsci qui a séjourné ici ? ». Troublé, je lui
réponds que non. Il cherche alors à me faire deviner, et, devant mon
mutisme embarrassé, s'exclame : « C'est Enrico Berlinguer ! Il a
séjourné dans la rue là-bas, tu vois, avec la grande maison rouge à
l'entrée. »

<div class="asterisme">...</div>

*Fête traditionnelle*. L'érudit, faisant défiler l'écran de son
smartphone, montre à A. des photos de la dernière fête locale. Tout en
suivant sans doute d'une oreille distraite mes échanges avec le
moustachu, car il me dit soudain : « C'est elle, c'est Sebastiana ! La
dame qui vit dans la maison de Berlinguer... »

<div class="asterisme">...</div>

*Fusée*. Afin de nous rendre à la pizzeria du village, qui n'est qu'à
une petite centaine de mètres du bar principal, l'un des membres de
l'assemblée tient à ce que nous montions dans sa voiture. Nous filons
donc comme une fusée dans les ruelles. Et, quelques secondes plus tard,
nous sommes attablés en terrasse. C'est alors que nous voyons passer
l'érudit, accompagné de sa femme et d'un enfant en bas âge. Toute la
jolie famille nous salue chaleureusement.

<div class="asterisme">...</div>

*Rencontre avec Sebastiana*. A. s'exclame soudain : « C'est elle ! La
femme de la photo ! » Et se lève d'un bond pour la saluer, puis,
revenant à la table, m'annonce avec enthousiasme que nous sommes
attendus à la « Maison de Berlinguer » dès le lendemain matin.

<div class="asterisme">...</div>

*Vino della casa*. Le patron nous apprend que l'érudit a déjà payé notre
vin. Pour ce qui est des pizzas, il ne prend pas la carte mais nous
propose de revenir le payer quand nous le pourrons. En prenant soin
d'embarquer le litron dans une bouteille en plastique, car il est le
fruit de la production personnelle du patron et que c'est une offrande
de l'érudit, nous rentrons donc vers Austis, sur de petites routes qui
serpentent, en faisant attention. Et c'est ainsi que se conclut notre
première soirée au pays « des *ladroni*, fourbes et arriérés ». J'ajoute
que nous avons merveilleusement dormi.

<div class="asterisme">...</div>

*Maison de Berlinguer*. « Au fond de la rue, la maison derrière le grand
portail, vous entendrez des chiens aboyer. » Sebastiana, qui nous
accueille avec l'air affairé des bourgeoises de l'ancien temps, nous
prie de l'attendre sous le porche, le temps qu'elle finisse de s'occuper
de sa mère alitée au premier étage. Nous nous installons donc sur une
grande terrasse aménagée, avec multitude de pots de fleurs et de vieux
objets. Surgit alors Caterina, sa nièce, étudiante en langues à
Cagliari, qui est chargée de nous tenir compagnie.

<div class="asterisme">...</div>

*Visite*. Nous traversons une enfilade de petits salons ornés de
tableaux, de bibelots, de petites choses inutiles qui donnent le ton de
ces grandes maisons bourgeoises d'antan, provinciales, au style suranné
et kitsch. Ce genre d'intérieur semble avoir une sainte horreur du vide :
il faut entreposer, remplir, étaler jusqu'à l'écœurement, combattre
l'oubli et la ruine de sa caste en collectionnant compulsivement des
traces d'un temps révolu. Notre visite est interrompue par une
infirmière chargée d'apporter des soins à la mère. Sebastiana, afin de
l'accueillir comme il se doit, est obligée de nous abandonner une
seconde fois. Nous continuons donc la visite en présence de sa nièce.
Sur le toit, une grande terrasse permet d'embrasser le paysage, mais
également de mesurer l'ampleur du terrain qui jouxte la maison. Face à
nous, une autre maison semble laissée à l'abandon, comme si les travaux
débutés il y a plusieurs décennies n'avaient jamais été achevés.
Caterina nous dit que c'est là qu'elle dort, par commodité, mais
également par souci d'indépendance. Le soleil tapant vivement sur la
terrasse, je propose assez vite de redescendre. En attendant A. et
Caterina, j'en profite pour jeter un œil imprudent vers le couloir du
troisième étage. Je suis alors surpris par la voix stridente de
Sebastiana : « Où en êtes-vous ? Eh bien, c'est une fort longue
visite... »

<div class="asterisme">...</div>

*Berlinguer à Teti*. Un livre à la main, la maîtresse de maison nous
retrouve sous le porche. Il s'agit d'une édition récente d'un ouvrage
édité par la municipalité de Teti, sur la région et son histoire. Alors
que nous sommes invités à nous asseoir, je demande à Caterina si elle
peut lire en le traduisant le chapitre dédié à Enrico Berlinguer et à
son séjour ici. Ce qu'elle fait. Mais Sebastiana l'interrompt sans cesse :
pour nous parler de ses pruniers, de ses poiriers, des pommiers de son
jardin ; ou pour déplorer le fait que Biancha, l'une des filles du
dirigeant communiste, lorsqu'elle l'a croisée à Turin il y a quelques
années, ne s'était pas souvenue de son séjour à Teti ; et pour soupirer,
encore, d'exaspération et de mélancolie, à l'évocation de tel ou tel
souvenir de son enfance. La lecture du chapitre sur Berlinguer est ainsi
hachurée comme le sont à la radio les émissions d'histoire par des
réclames publicitaires. Je dois admettre, bien que jugeant amusantes les
interventions de Sebastiana, que mon impatience se cristallise alors sur
la présence de Berlinguer dans cette maison. Je veux tout savoir : quand
cela a-t-il eu lieu ? Pourquoi à Teti ? Et pourquoi dans cette maison en
particulier ? C'est Sebastiana, et non la lecture du chapitre en
question, qui nous renseigne : la famille Berlinguer et la sienne se
connaissaient, elles ont donc partagé cette maison durant un été, chose
tout à fait ordinaire entre membres de la petite noblesse sarde. Elle
ajoute également --- parmi une ribambelle de précisions loufoques, telle
l'odeur du caramel dégoulinant de leur tartine à l'heure du goûter
lorsqu'ils étaient enfants, précisions que je ne comprends pas toujours,
tant son italien plein de dialectismes fuse comme la voiture de notre
chauffeur de la veille, mais qu'A. me traduira plus tard --- qu'Enrico
Berlinguer, lorsqu'il séjournait ici, dans leur maison, alors qu'elle
n'était encore qu'une gamine, lui avait semblé très antipathique. Elle
le décrit comme un homme obsédé par les horaires, froid, distant,
contraignant beaucoup les enfants dans leur liberté de jeu et
d'exploration. Les apostrophes, les gestes et les outrances de
Sebastiana continuent ainsi à hachurer la lecture du chapitre que sa
nièce, tant bien que mal, essaie de poursuivre. Jusqu'au moment où la
maîtresse de maison se lève d'un bond, interrompt brutalement sa nièce
et nous ordonne de nous lever sur le champ afin de visiter son jardin.
Nous sommes contraints de nous exécuter. Et tant pis pour Berlinguer :
nous avançons sagement entre les poules et les excréments canins vers un
petit jardin en contrebas.

<div class="asterisme">...</div>

*Prunier*. Sebastiana nous désigne avec fierté un vigoureux citronnier,
un abricotier asséché ayant déjà fourni ses offrandes de l'année ainsi
qu'un plaqueminier dont je ne saurais dire l'état de maturation des
fruits. Puis elle demande à sa nièce d'apporter trois *bitter*, un
apéritif sans alcool dont le goût est l'une des composantes du désormais
hégémonique Spritz. Caterina s'exécute sans broncher. A. interroge alors
Sebastiana sur l'ampleur des travaux qu'elle réalise ici : l’entretien du potager, des fruitiers, du poulailler, de l'ancienne porcherie, de la maison surtout, qui s’étend sur quatre étages, des bâtiments annexes... c'est bien elle qui s'occupe de tout. Seule. Sacerdoce
des héritiers, ceux de la petite noblesse déclinante, qui consacrent
leur vie à la conservation d'un patrimoine en voie de décomposition.
Après avoir bu nos *bitter*, nous visitons le potager de Sebastiana.
Nous nous attardons sur un prunier particulier dont les fruits pourpres, bien que trop amers, ont, selon la maîtresse de maison, une « forme merveilleuse ».

<div class="asterisme">...</div>

*Attente*. Quand nous revenons sous le porche, afin de poursuivre la
lecture tant convoitée du récit de Berlinguer, Sebastiana nous prie
d'abandonner le chapitre en question et de passer immédiatement au
suivant. Il s'agit, nous dit-elle, d'un chapitre ayant l'immense mérite
de la mentionner. Il me faut alors beaucoup de pugnacité afin de réussir
à obtenir que nous puissions achever d'abord le chapitre consacré au
dirigeant communiste. Ce qui nous permet d'apprendre ce que je désire
tant savoir et me stupéfie.

<div class="asterisme">...</div>

*21 août 1964*. Ce fut donc sous ce porche, entouré de tous ces pots de
fleurs et de tous ces objets bizarres, déjà, peut-être, qu'un homme
entra de bonne heure pour annoncer la nouvelle à Berlinguer : Palmiro
Togliatti venait de mourir.

<div class="asterisme">...</div>

*Soulagement*. Le mouvement communiste d'Italie perdait alors l'un de
ses plus grands dirigeants. Bien que jeune encore, Berlinguer, qui avait
fait ses preuves d'organisateur au sein de différents mouvements sociaux
durant sa prime jeunesse, qui avait occupé ensuite différentes fonctions
d'importance au sein des structures institutionnelles de la gauche, était
alors pressenti pour lui succéder[^306]. Ce matin là, donc, un homme a
surgi et Berlinguer s'est levé d'un bond pour filer en voiture. On
imagine qu'il prit l'avion depuis Cagliari, ou Sassari, sa ville natale,
afin de se rendre à Rome. Son départ soudain fut en tout cas un grand
soulagement pour la petite Sebastiana, âgée de seulement dix ans, et qui
subissait l'autoritarisme d'Enrico Berlinguer durant ses vacances d'été.

<div class="asterisme">...</div>

*La chambre de Berlinguer*. Je demande à voir la chambre de Berlinguer.
Ayant conscience du caractère un peu fétichiste de ma demande, je
précise que c'est seulement afin de prendre une photo de Teti et de ses
environs depuis la chambre où le dirigeant communiste a séjourné.
Sebastiana accepte et Caterina m'accompagne donc jusqu'au fameux
lieu-dit. Il s'agit d'une toute petite chambre, très ordinaire, avec de
naïfs tableaux accrochés au mur, une représentation de la Vierge, une
croix, une table basse, une commode, un miroir et quelques bibelots.
Afin d'aérer, Caterina ouvre la fenêtre : je découvre alors les
montagnes de Barbagia se déployant à l'infini derrière le petit clocher
de Teti.

<div class="asterisme">...</div>

*Lundi soir*. Nous décidons de boire un café sur une longue terrasse où
une bonne dizaine d'hommes sont déjà installés. Chose toujours étonnante
pour des étrangers que de pénétrer dans un bistrot au cœur de la
Barbagia. Mais le malaise est vite rompu. Plus vite encore que la
veille.

<div class="asterisme">...</div>

*Transmission*. Lorsqu'un des hommes apprend que j'écris un livre, il se
précipite chez lui afin de me rapporter les deux derniers exemplaires
des livres de son père, un poète de la région, mort il y a une quinzaine
d'années. Un tel témoignage instinctif de reconnaissance induit une
responsabilité vertigineuse : alors que je connais « Mandarino » depuis
à peine quelques dizaines de minutes, je me trouve désormais en
possession des deux dernières traces écrites de son père défunt.

<div class="asterisme">...</div>

*Les larmes de la mère*. « Je ne sais pas lire le sarde, ici nous le
parlons seulement, mais je connais ces vers de mon père qui décrivent la
douleur de ma mère. C'était après la mort de mon frère, Alberto. Ils
disent à peu près ceci :

> Les larmes de la mère
>
> Coulent tout le jour
>
> Mais également la nuit. »

<div class="asterisme">...</div>

*Azienda di Mandarino*. La petite Fiat 500 bleue d'époque de Mandarino
épouse les oscillations d'une courbe mystérieuse. Au bout d'une dizaine
de minutes, nous arrivons sur un petit chemin de terre, et, 400 mètres
plus tard, à sa ferme. Son fils travaille encore. Salvatore et Fanni
m'expliquent que le fils est bien plus ambitieux que le père, que son
miel a déjà remporté de nombreux prix sur le continent ; la production
du père, quant à elle, est plus modeste : « L'un est un professionnel,
l'autre un amateur. »

<div class="asterisme">...</div>

*Pane e casu*. En guise d'apéritif, Salvatore extrait du coffre de la voiture de Fanni une
grosse meule de *fiore sardo* ainsi que du *pane*
*carasau*, un pain sec, une spécialité régionale.

<div class="asterisme">...</div>

*Alberto*. Dans le couloir de l'*azienda*, j'aperçois une photo d'un
groupe de jeunes. Salvatore, qui passe par là, me désigne Mandarino, âgé
d'une vingtaine d'années, couvert d'une grosse touffe brune et
bordélique. Une autre photo type *photomaton* est glissée dans le cadre :
« C'est Alberto, le frère disparu. »

<div class="asterisme">...</div>

*Massimo calma*. En cuisine, je suis chargé de tourner les pâtes dans
une grande marmite. Ce sont des *fregole*, des pâtes de la taille d'un
grain de riz baignant dans une petite flaque de sauce tomate avec de
gros morceaux d'olives. À la façon du risotto, je dois remuer la *pasta*
jusqu'à obtenir la texture souhaitée : « Quand tu sens que c'est bon,
c'est bon. » Quelques minutes plus tard, c'est bon, et j'apporte donc
triomphalement la marmite. Nous accompagnons les pâtes de parmesan et de
vin rouge. Et parlons de choses simples. Mandarino esquisse sa
philosophie de l'existence : « *Massimo calma*. » Ce à quoi je rétorque
que je ne peux m'empêcher de m'agiter afin de me donner l'illusion de
faire quelque chose. Il insiste, précise sa pensée : son fils, par
exemple, a une ambition de type industriel pour la ferme, tandis que
lui, Mandarino, demeure un simple artisan.

<div class="asterisme">...</div>

*Ricotta salata*. Je demande à Salvatore si la Sardaigne produit
également de la *ricotta salata* --- un fromage découvert en Sicile et
dont je raffole. Il s'empresse alors d'ouvrir de nouveau le coffre de la
voiture de Fanni et se saisit d'une meule énorme qu'il pose sur la
table. Après le repas, nous avons également droit à un bol de petites
prunes trempées dans de l'eau-de-vie. Puis nous goûtons le fameux miel
du fils, avec de petites tranches de *ricotta* posées sur du pain sec.

<div class="asterisme">...</div>

*Remerciements*. Vers trois heures du matin, nous rentrons en suivant la
voiture de Fanni. Au moment de nous garer, Mandarino passe devant nous à
bord de sa petite Fiat. Nous le remercions encore pour cette belle
soirée : « Votre présence suffit », nous répond-il.

<div class="asterisme">...</div>

*Sughero*. La route en direction d'Abini est jonchée de chênes dénudés.
C'est le boulot de Salvatore, l'été, de récolter du liège afin d'en
refourguer à de petits industriels qui en font le plus souvent des
bouchons pour des bouteilles de vin.

<div class="asterisme">...</div>

*Propriété collective*. Nous nous arrêtons en bord de route afin de
goûter de toutes petites poires sauvages. « Ces arbres fruitiers ont
beau n'appartenir à personne, ils sont régulièrement taillés » observe
A.

<div class="asterisme">...</div>

*Abini*. Lorsque nous arrivons à Abini, et ce en suivant quelques
indications écrites au feutre vert sur la roche, nous tombons nez à nez
sur une ferme et son extension dite « agrotouristique » --- autre nom
donné aux locations de vacances en milieu rural, souvent à des prix
faramineux. Nous garons la voiture et sortons en vue de trouver
quelqu'un qui pourrait nous renseigner sur la localisation exacte de ce
village nuragique. Des chiens aboient. Puis le propriétaire de la ferme
fait son apparition et nous indique un chemin. Nous traversons alors un
paysage de garrigue parsemé de petits buissons secs. Assez rapidement,
nous trouvons les premiers vestiges : amoncellements de pierres sous des
chênes, oliviers et myrtes. Nous poursuivons notre exploration de ce
village enfoui, en l'imaginant, lui et ses habitants, il y a plus de
2300 ans.

<div class="asterisme">...</div>

*Pancarte*. De petits édifices circulaires jouxtent de petits édifices
rectangulaires. Certains cercles de pierres sont plus larges. On peut
même observer en leur sein les restes de pièces séparées. Une pancarte,
aux caractères qui s'effacent déjà, indique qu'il a pu s'agir
d'édifices religieux. Nous apprenons également que les premières
fouilles sont très récentes, datant de la fin du XIX^e^ siècle, et que
seuls deux autres projets archéologiques ont été menés jusqu'ici, dont le
dernier il y a une vingtaine d'années. Au regard de sa richesse, les
maigres efforts pour percer les mystères de la civilisation nuragique
nous surprennent. Par ailleurs, le site est peu entretenu, la croissance
désordonnée de la flore menaçant sans cesse d'endommager les vestiges.

<div class="asterisme">...</div>

*Clairière*. En poursuivant notre visite du site, parmi les ronces, les
chardons, les buissons épineux et secs, nous entendons le faible débit
d'une rivière qui coule en contrebas. En suivant le bruit de l'eau, nous
apercevons de nouveaux édifices : de petites maisons sans doute, plus
éparses, plus isolées, qui s'étendent jusqu'à la rivière. Nous nous
plaisons à imaginer que le moindre vallon, le moindre terrassement a pu
abriter un foyer. Il devait s'agir d'un village immense, d'une ville
sans doute de plusieurs milliers d'habitants. Nous découvrons alors les
vestiges d'une petite maison cachée entre des buissons formant une sorte
de clairière ; la lumière y est très douce.

<div class="asterisme">...</div>

*Traduction*. En revenant à la ferme, le patron, qui se nomme également
Salvatore, nous propose une bière en compagnie de deux ouvriers qui
logent ici. Nous lui parlons de notre rencontre d'hier. Et sommes
stupéfaits d'apprendre qu'il sait déjà tout. Salvatore II connaît bien
Mandarino. Il a bien connu son frère également, Alberto. Il est ému en
en parlant. Je ne sais comment la discussion tourne alors autour de
Desulo, un village à quelques dizaines de kilomètres d'ici. Je me
souviens d'avoir lu un poème, dans le livre du père de Mandarino, dans
lequel ce village est évoqué. Je montre à Salvatore II le poème en
question sur mon téléphone. Et nous prenons part à un curieux atelier de
traduction collective, du sarde vers l'italien, en compagnie des deux
ouvriers, orchestré avec pédagogie et autorité par le patron de
l'*azienda* :

> Terre d'artisans et de poètes,
> 
> terre de marchands ambulants et de bergers
> 
> et de gens hospitaliers dotés de valeurs,
> 
> dont la porte toujours est ouverte.

<div class="asterisme">...</div>

*Agriturismo*. Aux deux tiers du poème, Salvatore II nous dit que c'en
est fini pour aujourd'hui. Si nous voulons la fin, il nous faut revenir.
Avec les deux ouvriers, ils partent visiter une maison dans la montagne
en vue sans doute d'y effectuer quelques travaux. C'est alors que nous
rencontrons la fille de Salvatore II. Nous discutons avec elle de la
possibilité de louer une chambre ici durant une semaine ou deux. Mais
ses prix sont beaucoup trop élevés pour nous. Et Susan est dure en
affaire.

<div class="asterisme">...</div>

*Offrande*. En rentrant à Austis, nous apercevons Salvatore I sur une
toute petite place. Celle-ci est structurée autour d'un monument aux
morts qui masque un bar dont nous n'avions jamais soupçonné l'existence.
Vieux, jeunes, tous installés sur un banc de fortune, une chaise ou les
petites marches de l'édifice funéraire. D'un geste, Salvatore nous
invite à boire une bière. Nous acceptons malgré les réticences de notre
foie. Et, toujours, cette situation étrange consistant à débarquer au
milieu d'un groupe déjà constitué d'hommes qui se connaissent tous. Mais
nous connaissons Salvatore, c'est lui qui nous invite à nous joindre à
eux, l'intégration est donc plus rapide encore que les fois précédentes.
La serveuse nous apporte immédiatement une grande *Ichnusa* à partager,
et nous glisse, comme si c'était une évidence, que c'est l'homme de
l'autre côté de la place, à moitié couvert par le monument aux morts,
qui nous l'offre. Nous le remercions de loin. Il nous renvoie un salut.
Nos échanges en restent là.

<div class="asterisme">...</div>

*Légalité*. Tandis que nous ne comptions boire qu'une seule bière, nos
verres se remplissent sans même que nous nous en apercevions. Quand le
bar s'apprête à fermer, nous gagnons un autre bar. Celui-ci n'ayant pas
de terrasse, nous demandons la permission de fumer à l'intérieur,
permission qui nous est accordée. À côté de nous, un *carabiniere* en
tenue civile est attablé. Juste au-dessus de lui, des hommes d'un
certain âge reniflent et s'échangent des sachets de marijuana. Tout se
déroule comme si les frontières de la légalité et de l'illégalité
étaient repoussées loin, bien loin de tout ce qui se trame sur le
continent. Nous comprenons que l'herbe ici se cultive sans problème dans
les jardins de chacun, qu'il n'est que très rarement question d'en
acheter ou d'en vendre, que les cultures des uns servent à la
consommation des autres. Un peuple d'artisans, et non d'industriels,
c'est bien ce que Mandarino tentait de nous faire comprendre la veille.

<div class="asterisme">...</div>

*Polyvalence*. Paolo, que nous venons tout juste de rencontrer, tient à
nous inviter chez lui. Il part le premier, en bicyclette, afin de faire
un brin de rangement. Mandarino, quant à lui, ne viendra pas dîner, mais
nous promet de passer boire un verre, un petit verre et c'est tout car
le lendemain il travaille tôt. Pas à la ferme, précise-t-il, en tant que
pompier volontaire, chargé avec son équipe d'observer les environs, de
guetter d'éventuels départs de feu, et ce à une période de l'année, et
tout particulièrement cette année-là, où les incendies sont capables de
terribles ravages. Tous ont donc plusieurs métiers : si Salvatore cumule
de nombreuses activités, dont la récolte du liège qu'il effectue à son
compte, d'autres, ayant le privilège de posséder une *azienda*, comme
Mandarino par exemple, travaillent le plus souvent pour l'administration
locale, la protection des forêts, les services communaux, etc.

<div class="asterisme">...</div>

*Tableau*. Dans le salon de Paolo, le portrait peint d'une femme
est accroché au mur. Des yeux noirs d'une gravité lointaine.

<div class="asterisme">...</div>

*Ultima cena*. J'insiste pour que le dîner soit simple. Les fromages
s'étalent donc sur la table : *scarmoza affumicata*, *ricotta salata*,
*crema di formaggio*, *fiore sardo*, etc. Mais nous sommes contraints de
refuser les nombreux jambons que Paolo s'apprête à extraire de son
garde-manger. Je redoute toujours un peu l'annonce de cette tare urbaine
--- nous sommes végétariens --- auprès de gens pour qui la chasse
constitue une tradition toujours vive. Pourtant jamais nous ne sommes
jugés : nous, étrangers, végétariens, avec nos dégaines d'artistes et
notre Kangoo aménagée.

<div class="asterisme">...</div>

*Fêlure*. Salvatore se livre un peu sur sa vie. Deux jumelles qu'il
élève seul depuis leur un an. Une fêlure dans le regard.

<div class="asterisme">...</div>

*Giardino Garibaldi*. Il faut que la vie continue, disent-ils. Il faut
être fort, disent-ils encore. Mais nous le sommes, puisque nous vivons.
Ce n'est pas tant que nous l'ayons choisi, la vie en nous tisse ses
ramifications, tels ces immenses Ficus centenaires, *Giardino Garibadi*
à Palerme, dont les racines cherchent encore la terre, verticalement,
depuis les hauteurs jusqu'au sol, leurs branches s'affaissant en
direction de leurs aïeules, mûrissant de nouvelles généalogies,
profanes, dans le dos de l'Histoire.

<div class="asterisme">...</div>

*Égarement*. Salvatore nous raccompagne jusqu'au centre d'Austis. Avant
de nous quitter, nous nous arrêtons sur un parking pour discuter. Je ne me souviens pas de nos échanges. Sans doute ai-je regardé le ciel avec égarement.
Comme à chaque fois que nous parlons de ce qui nous déchire. Lorsqu'A.
et Salvatore ont fini de discuter, nous lui disons bonne nuit et
rentrons.

<div class="asterisme">...</div>

*Liqueur de myrte*. Tard dans la nuit, je découvre qu'A. ne s'est
toujours pas couchée. Je la cherche dans les parties communes, puis la
découvre sur le petit balcon du salon en train de fixer le ciel.

<div class="asterisme">...</div>

*Brindisi sotto le stelle*. A. tient à assister à une soirée organisée
par une association de Teti. Comme notre logement est occupé ce soir,
nous trouvons une chambre dans la périphérie d'Austis, chez le mari de
Josepina, notre logeuse. Il faut que j'avance sur un chapitre, je décide
donc de m'installer à l'intérieur du bar où nous avons rendez-vous afin
de travailler un peu. Pendant ce temps, A. discute avec dix hommes
installés en terrasse. Je refuse la bière qu'on cherche à m'offrir, mais
accepte un café. Dès que je finis mon café, on m'en apporte un nouveau.
Une heure plus tard, je suis tendu comme une pile électrique. Je rejoins
alors A. sur la terrasse où l'on me fait comprendre qu'ici on
ne laisse pas une femme seule dans un bar.

<div class="asterisme">...</div>

*Les femmes de Barbagia*. Celles-ci ont bien évidemment une vie sociale,
mais rarement dans des lieux clos : elles se retrouvent dans la rue, sur
des places, devant chez elles, sur des marches, ne dépensent jamais
l'argent du foyer dans un bistrot. Les femmes de Barbagia ne boivent
pas, ou, si c'est le cas, ne le font pas à découvert.

<div class="asterisme">...</div>

*Cartographie a*. Plusieurs centaines d'amateurs sont réunis sous les
étoiles afin d'assister aux explications d'un spécialiste. Nous sommes la
nuit du 10 août, la fameuse dite des *stelle cadanti*. Mais la lune
brille trop fort ce soir-là pour que nous puissions en observer
autant que nous le souhaiterions. Il y a des gâteaux sur une table, à
l'amande et au citron, une spécialité d'ici : « *Da Teti, proprio*. »
A., tout comme la foule d’amateurs, est entièrement absorbée par l’exposé du spécialiste ; quant à moi, qui n'en comprends pas grand-chose, je m’en remets plutôt au mystère. Sa voix, à la
façon d'une petite brise de montagne, semble flotter autour de nous. En
me saisissant de nouveau d'un de ces délicieux gâteaux, je songe que
l'étude scientifique de la Voie lactée doit être une pulsion d'emprise
de l'Homme sur le cosmos.

<div class="asterisme">...</div>

*Solitudine*. Nous retrouvons Caterina, accompagnée de deux jeunes amis
à elle. L'un d'eux nous a confectionné un *panino* la veille. À Ovodda,
il s'en souvient. Sans nécessairement nous imposer le devoir d'une
conversation, nous restons auprès d'eux un certain temps. Au bout de
quelques heures, le col de Marghine est vide de présence humaine. Et
nous demeurons, Caterina, A. et moi, seuls sous les étoiles.

<div class="asterisme">...</div>

*Cartographie b*. Inéluctablement mon regard se tourne vers les massifs
environnants, les petits villages ou bourgades qui scintillent
devant nous. Je cherche alors la direction d'Oristano, de Nuoro, de
Cagliari, d'Olbia. Je voudrais connaître la direction des villes, le nom de tous ces villages, de toutes ces bourgades, apprendre à les situer depuis le col de Marghine.

<div class="asterisme">...</div>

*L'étranger et son aura*. Avant de rentrer à Austis, nous devons
raccompagner Caterina chez sa tante. Pour ce faire, je dois me glisser
dans le coffre de notre Kangoo, sur un matelas de fortune, l'aménagement
de notre véhicule impliquant de sacrifier la banquette arrière. Caterina
s'installe donc devant, aux côtés d'A., tandis que je me laisse porter
en position horizontale jusqu'à Teti. Je pense alors à l'exceptionnelle
hospitalité des habitants de Barbagia. Mais qu'en serait-il si nous
passions du statut d'étrangers de passage à celui d'étrangers installés
ici ? Serions-nous encore traités avec tant de générosité ? Ou seulement
tolérés ? Pasolini n'écrivait-il pas, lors de sa *Longue route de
sable*, faisant alors référence à je ne sais quelle bourgade des
Pouilles ou de Calabre, que l'aura qui accompagne la présence d'un
étranger se dissipe toujours en quelques jours ?

<div class="asterisme">...</div>

*Granit rose*. Sous une chaleur épouvantable, nous quittons la
Barbagia et roulons en direction de la mer. Verdict : les littoraux
sardes sont dépositaires d'une violence sociale que n'atténuent en rien
les séductions du paysage et la clarté cristalline de l'eau sur le
granit rose.

<div class="asterisme">...</div>

*Mélancolie des bords de mer*. Freud définissait la mélancolie comme la
perte de ce que nous sommes pour l'autre (cf. *Deuil et mélancolie*,
1917). La « mélancolie des bords de mer » semble une caractéristique
civilisationnelle de notre temps, le style et le motif de l'époque. S'y
atteste l'impossibilité de s'ancrer et de se reconnaître dans un monde
agonisant.

<div class="asterisme">...</div>

*La mort est partout*. À Mandarino, Salvatore ou Paolo, nous avons pu
confier nos déchirures. À eux, alors que nous ne parlons pas sarde,
alors que nous ne les connaissions que depuis un jour ou deux, alors que
nous ne le faisons pas souvent, pas même avec certains de nos amis les
plus proches. Dans les montagnes du centre de la Sardaigne, la mort
toujours s'invite à la table, au bar, dans la plus banale des paroles
échangées sur une place, chez le primeur, à la boulangerie. Jamais il
n'est interdit d'en rire. Jamais il n'est interdit d'en pleurer. Les
morts demeurent à la surface du monde. Sans avoir besoin d'être
mentionnés. Sans que leur évocation soit nécessairement explicite.

<div class="asterisme">...</div>

*Inspiration*. La perpétuation de mythologies hybrides et autonomes, la
survivance de traditions millénaires, bien loin d'enfermer la Barbagia
dans un folklore, contribuent à la maintenir hors du développement
capitaliste et des ravages anthropologiques qu'ils induisent. Non comme
un modèle à suivre, mais comme inspiration pour nos propres actes de
résistance.

<div class="asterisme">...</div>

*Digression*. En France, malgré tous les scepticismes, militants ou
instruits, les gilets jaunes ont fait irruption au cœur d'une
représentation figée de la réalité sociale. Véritable retour du refoulé :
ils ont rendu visible cette nouvelle précarité, matérielle et
symbolique, qui caractérise la condition des masses au sein des sociétés
capitalistes avancées --- perte des liens collectifs, atomisation,
disparition du sens de la réalité, etc. Sur un rond-point, nous sommes
condamnés à tourner en rond. Mais cette suspension du temps, cette
appropriation particulière de l'espace sont apparues comme conditions de
possibilité d'un *faire commun*. Toute rencontre, amoureuse ou
révolutionnaire, implique conjointement une mise à l'arrêt et
l'initiation d'un mouvement : les rythmes productifs s'estompent, tandis
que les groupes s'arrachent à leur torpeur mélancolique et s'organisent.

<div class="asterisme">...</div>

*Idéologie et politique*. En Barbagia, on peut regretter qu'un tel foyer
de survivance autochtone se livre au passéisme mussolinien. Cela nous
enseigne deux choses : tout d'abord, et à rebours de toute pureté
idéologique, qu'il faut assumer de nous salir les mains, en nous logeant
notamment dans les contradictions qui déchirent l'unité du monde social
et ses représentations ; et puis, dès lors que nous comprenons que les
habitants de Pau, d'Austis ou de Teti n'ont jamais connu le fascisme,
que leur passéisme, comme tout passéisme, est une loyauté au désastre et
non un amour lucide, que notre époque semble ainsi marquée par une
rupture épistémologique empêchant l'espèce humaine de se rapporter
librement à elle-même, par la médiation de l'expérience, certes, mais
également par la médiation d'une altérité radicale, proche ou lointaine,
humaine ou cosmique, alors nous comprenons qu'une suspension du jugement
est nécessaire en tant que préalable à la possibilité de nous unir de
nouveau collectivement.

<div class="asterisme">...</div>

*Hypothèse*. Le militant des sociétés capitalistes avancées, incapable
de reconnaître le potentiel insurrectionnaliste d'un mouvement naissant
depuis les marges de son propre espace national, doit cesser de scruter
l'arrière-monde de la civilisation capitaliste en légiférant sur la
validité révolutionnaire de tel ou tel groupe social et tel ou tel
espace de socialisation : il doit partir de l'existant et engager toute
sa volonté dans la constitution d'un « nous » qui serait le plus concret
possible. Autrement dit : il ne s'agit pas de combattre l'idéologique
par l'idéologique, mais de faire valoir le primat matériel et sensible
de ce qui nous lie sur toute idéologie.

<div class="asterisme">...</div>

*Hotel Supramonte*. Lors de notre première nuit hors de Barbagia, nous nous installons au coeur des massifs qui ourlent la côte est de l’île. À plus de mille mètres d’altitude, sur un parking. Une lune rouge s'est élevée dans un ciel parsemé d’éclats et de trous.

</div>

<div class="text" data-number="14">

# Appendice : Pasolini et la Sardaigne


Lors de l'été 59, Pasolini entreprend une traversée du littoral italien
qui lui offre d'exercer son acuité, indissociablement sociale et
poétique, en portant un regard sur le phénomène anthropologique de la
balnéarisation.

Dans le Nord de l'Italie, sur les côtes ligures et toscanes, ainsi que
sur quasiment toute la côte adriatique, la balnéarisation est alors
parvenue à son stade ultime, c'est-à-dire qu'elle exclut littéralement
le prolétariat autochtone de ses espaces marchands.

Tandis que dans le
Sud de l'Italie, à partir de Livourne et jusqu'en Sicile, à l'exception
de quelques territoires exclusivement réservés à la grande bourgeoisie
(telle que la côte Amalfitaine), Pasolini observe une vie populaire
d'une grande précarité cohabitant avec des minorités de touristes aisés,
industriels italiens, artistes étrangers, consommateurs de charmes
authentiques, etc. 

Ce processus d'appropriation capitaliste du paysage, et donc
d'éradication des cultures autochtones, a beau connaître des
développements différents dans le Nord et dans le Sud du pays
--- inégalités de développement rendant compte d'une fracture déjà bien
connue ---, il se traduit surtout par une tendance qui tend à s'affirmer
comme modèle de développement hégémonique sur l'ensemble de la péninsule
italienne.

Lors de sa *Longue route de sable*, Pasolini est donc allé jusqu'en
Sicile (de Messine à Syracuse), mais ne s'est pas rendu en Sardaigne.
Que signifie cet « oubli » ?

De fait, la Sardaigne se situe par-delà la
fracture évoquée, reléguée dans une sorte d'« ultra-sud » en raison de
sa situation à la fois sociale (l'une des régions considérées comme les
plus pauvres d'Europe) et géographique (littéralement isolée, surtout en
1959). 

Nous savons pourtant que Pasolini s'est rendu au moins deux
fois en Sardaigne. C'était lorsqu'il travaillait sur ses anthologies,
celle de la poésie dialectale, puis celle de la poésie populaire
italienne. Et, dans l'introduction à son anthologie dialectale du XX^e^ siècle,
nous pouvons lire ceci :

> Pour l'autre île, la Sardaigne, il faudra tenir compte au préalable
> d'une raison que nous avons également vue s'appliquer à la Sicile, et
> qui se constatera pour toute province dialectale « isolée ». Nous
> comprenons la mystique de la région comme « petite patrie », incubatrice
> séculaire de traditions et de folklores très anciens, dans certains cas
> même préhistoriques (comme en Sardaigne), pour laquelle chaque
> production littéraire est comme enveloppée d'une dimension
> hagiographique sous-jacente : de la chaleur d'une dédicace en tant que
> manifestation typique de ces poètes confinés des solitudes rurales, et
> qui exaspèrent leur affection naturelle pour la terre qui les a vus
> naître ; d'où le besoin expansif de se faire « chantres » de cette
> terre, nation seulement linguistiquement, et réduite, par une histoire
> qui, en réalité, ne se conçoit que par sa centralité et exclut ces
> territoires marginaux, afin de se consoler dans une épopée de la misère,
> de l'abandon, du travail. C'est que tout sentiment de l'histoire finit
> par s'anéantir, chez ces poètes, au point de devenir pur, sentimental
> acte d'amour, immobile tout comme leur histoire. Nul n'échappe ainsi au
> caractère « réactionnaire » des autonomies régionalistes : comme
> résultat, par exemple, de ce moment romantique qui voudrait ne pas voir
> exister l'écrivain, mais plutôt le démiurge anonyme d'une profonde âme
> populaire indigène, le locuteur acquérant ainsi une figure irrationnelle
> de perfection non seulement linguistique mais aussi plus largement
> humaine et ethnique. Cet excès d'amour, cette sublimation dans le
> sentiment d'une validité d'existence qui n'existe pas dans la réalité,
> car réalité signifie : faim, injustice, ignorance, finit par priver le
> poète de la capacité de voir lucidement autour de lui, de découvrir
> l'authentique beauté (qu'il idéalise *a priori*) de son pays[^307].

Ce que dit ici Pasolini de cette histoire tronquée, qui ferait valoir
l'homogénéité culturelle et l'unité linguistique d'une région afin de
s'objectiver dans un récit qui puisse résorber les contradictions et les
différences inhérentes à une « petite patrie » arriérée, pauvre,
déchirée par une multitude de conflits séculaires, nous l'appliquerions
plus aisément à la Corse qu'à la Sardaigne. 

Je me souviens d'avoir voulu savoir auprès d'un patron de bar, dans la
région des *Dui Sevi*, ce qui différenciait les différents dialectes
corses et expliquait leur répartition sur l'île. Je m'étais vu
rétorquer avec fermeté : « Il n'y a qu'une langue corse. » Il en est
très différemment de l'appréciation des Sardes sur leur prétendue unité
linguistique. Quant au régionalisme sarde, il ne nous a jamais paru
sacrifier sa pluralité à une supposée homogénéité culturelle.

On peut imaginer que cette faille dans l'acuité reconnue du poète n'est
pas insignifiante : si la Sardaigne n'occupe aucune place dans son
œuvre, ou si peu, c'est sans doute qu'elle s'exclut de fait des
tendances sociales et des mutations anthropologiques qui se sont
abattues sur l'Italie après la guerre. Au point, sans doute, que même
Pasolini ne fut en capacité d'en rendre compte.

Qu'importe finalement cette négligence du poète pour l'île de Gramsci :
envisager de refaire aujourd'hui la route entreprise en 1959 par
Pasolini nous a semblé vain. Tout d'abord parce qu'il s'agissait
d'inventer la nôtre, de route, et non de suivre un jeu de piste ; et
puis parce que le regard porté sur les phénomènes culturels ne peut plus
s'instituer depuis la construction nationale, mais concrètement depuis
les marges --- autrement dit : depuis ce qui excède toute cartographie.

</div>

<div class="text" data-number="15">

# III. Matériaux complémentaires (pièces détachées)

<div class="epigraphe">

Mon livre n'est pas un roman en brochette, mais à grouillement, il est donc compréhensible que le lecteur soit quelque peu désorienté.

--- Pier Paolo Pasolini

</div>

## Le chat du Colisée

<div class="epigraphe">

... quel sens y a-t-il à vivre sinon à être fidèle, désespérément et, peut-être de façon obtuse, à la première idée de liberté qui dans notre jeunesse nous pousse à agir ?

--- Pier Paolo Pasolini

</div>

<div class="noindent">

*1950*

nous errons

la nuit

sales chats

en quête d'amour

hors

de l'enfer bourgeois

auquel

nous étions destinés

gosses

héroïques

pissant

sur les godasses

cirées

de l'Histoire

en ces faubourgs

sous-prolétaires

nous nous aventurons

vers des foyers

vers des rires

vers des aubes

à mi-chemin

toujours

de la Ville et de la Campagne

&nbsp;

*1960*

vétusté du bâti

chômage --- image

pourtant radieuse

d'une communauté vivante

de voyous

de travailleurs sans lendemain

romanesco tissé

de calabrais

de napolitain

d'argots non identifiables

sur des terrains vagues

d'interminables parties de football

et des rires

encore

et des aubes

encore

et des foyers

(précaires)

encore

dans le marasme

des exils

des déracinements

dans la boue des *borgate*

sous des ciels

vierges encore

de pollution lumineuse

&nbsp;

*1964*

nous pleurons Palmiro Togliatti

et sommes réellement mutiques

devant cette histoire qui s'achève

&nbsp;

<div class="a-droite">--- NE PERDEZ PAS ESPOIR, CAMARADES !</div>

<div class="a-droite">--- FOUTAISE !</div>

&nbsp;

nos larmes ne seront jamais

la justification de leurs défaites

nous demeurons lucides

au cœur de la tempête

&nbsp;

*1968*

BIM BAM BOUM

*Vogliamo tutto*

éclaté/éclatant

Sujet Révolutionnaire

jeunesse se révolte

&

classe ouvrière

découvre enfin

les *chemins de l'autonomie*

cependant

Révolution

n'est pas

a

u

b

o

u

t

d

u

t

u

n

n

e

l

tout ne fait que commencer

&nbsp;

*1970-2022*

DESTRUCTION

MÉTHODIQUE

DES MONDES

OUVRIERS

PÉRIPHÉRIQUES

PAYSANS

...

&nbsp;

<div class="centre">

IMMENSE

ACCUMULATION

DE PLASTIQUE

</div>

&nbsp;

*25 septembre 2022*

forme

exemplaire

du « nouveau fascisme »

vieux-signifiant-terrible

fondu

\[dans\]

(néo)capitalisme latin

agrégeant :

&nbsp;

1) nouvelles technologies de gouvernance

2) soumission au règne de la marchandise

3) accroissement des violences contre toutes les minorités

4) éradication de nos forces de travail et d'errance

&nbsp;

ainsi

l'Italie

(3^e^

puissance

européenne)

épouse

la cause

de sa ruine :

tourisme

de

masse

&

folklore

nationaliste

&nbsp;

<div class="centre">

L'HISTOIRE

BOURGEOISE

EST

AINSI

RÉALISÉE

</div>

&nbsp;

<div class="a-droite">

--- mais que s'est-il passé ?

--- où est vraiment le corps du pouvoir ?

--- que reste-t-il de nos amours ?

--- et cette *image* de notre jeunesse ?

</div>

&nbsp;

*fin n^o^ 1 : errance & répétition*

nous errons

la nuit

sales chats

en quête d'amour

hors

de l'enfer bourgeois

auquel

nous étions destinés

...

&nbsp;

*fin n^o^ 2 : retour & bifurcation*

...

...

...

...

...

...

...

...

...

</div>

</div>

<div class="text" data-number="16">

## JLG/PPP

<div class="epigraphe">

Thématiser la politique c'est, de fait, la concevoir moins comme un rapport spécifique que comme un contenu (théorico-inerte), avec quelques dommages pour sa dynamique et sa capacité à bousculer les délimitations.

--- Olivier Neveux

</div>

godard ne cesse de renvoyer le cinéma de pasolini à quelque chose
d'écrit allant même jusqu'à traiter sa linguistique de table de montage
réactionnaire pasolini quant à lui place godard dans le rang des
poètes-cinéastes contribuant notamment à l'édition de ses textes dans un
ensemble qu'il a lui-même choisi d'intituler il cinema è il cinema
attestant ainsi de sa compréhension de l'aspect tautologique d'une œuvre
qui n'a d'autre horizon que le cinéma lui-même ce n'est donc pas la
défense obstinée de l'autonomie de son champ dont il comprend aisément
l'enjeu en tant que poète précisément ni même la sophistique de godard
qui agacent pasolini mais le caractère vulgaire et étroitement parisien
d'une langue séparée de la réalité prolétaire et sous-prolétaire pour
mieux comprendre cet agacement de pasolini (qui n'empêchera pas
l'amitié) intéressons-nous à deux figures antagoniques de leur œuvre
cinématographique des années 60 accattone d'abord un sous-prolétaire
romain n'ayant d'autre horizon que la périphérie de rome et ses petits
trafics pierrot le fou ensuite un bourgeois qui rompt avec sa classe en
s'embarquant dans une fuite kamikaze féerique et renouvelle ainsi son
horizon existentiel en des espaces jusqu'alors ignorés dans l'un et
l'autre film il est donc avant tout question d'espace et de clôture de
leur rapport de la possibilité ou de la non possibilité d'une brèche et
cela sous des modalités radicalement différentes c'est-à-dire ancrées
sur des terrains sociaux antagoniques la bourgeoisie parisienne ou le
sous-prolétariat romain l'errance ou la fuite deux dynamiques dotées de
leur poétique propre la première soustraite à la société bourgeoise et
se déployant en ses marges la seconde se faisant acte de rupture
volontaire depuis la bourgeoisie elle-même vers ses confins (mais en
sort-elle vraiment ?) le langage de l'image chez godard s'affranchit
immédiatement de la matérialité des rapports de classe et vise déjà un
au-delà du monde bourgeois (sa représentation) un monde que la
bourgeoisie encore et toujours ne cesse de façonner à son image tandis
que chez pasolini c'est dans la mobilisation de pures formes de vie
prolétaires et sous-prolétaires que ce langage se manifeste godard quant
à lui fabrique ses films à paris s'abandonnant obsessionnellement au
travail de l'image de l'image seule qui lui inspire cet art de la
découpe du contrepoint de la brèche qui caractérise déjà ses premières
créations et bien que ce ne soit pas le godard du mépris ou de pierrot
le fou qui nous intéresse ici non plus le godard des films politiques
mais bien celui de histoire(s) du cinéma ou du livre d'image un godard
qui se fait archiviste et monteur un godard qui a aboli la belle figure
de l'acteur éclaté tous les cadres de la représentation ainsi que la
structure narrative traditionnelle héritée du champ littéraire un godard
qui pratique une écologie de l'image qui fait de l'image avec de l'image
un godard qui est peut-être le plus intéressant et le plus radical de
tous un godard irrémédiablement poète irrémédiablement révolutionnaire
de ce godard-là nous n'entendrons jamais pasolini nous dire quoi que ce
soit il ne l'a pas connu c'est pourtant ce godard-là que nous
rapprochons de pasolini et il y a fort à parier également que ce
godard-là s'il a ouvert les derniers ouvrages de pasolini a pu y trouver
une conception du langage qui s'affranchit quelque peu de ce quelque
chose d'écrit tant reproché au poète italien de feint ou vrai amour de
ce quelque chose d'écrit pasolini s'en affranchit quelque peu il est
vrai mais jamais complètement d'une part parce que nous faisons
référence ici surtout à des ouvrages littéraires et non seulement à des
films que leur matériau demeure donc la langue même travaillée même
subvertie par un langage mais également parce que pasolini demeure
résolument attaché à ce dont godard semble vouloir s'affranchir je ne
nie pas écrit-il que certainement la chose la meilleure aurait été
d'inventer un alphabet si possible de caractère idéogrammatique ou
hiéroglyphique et d'imprimer le livre en entier comme ça mais ma
formation culturelle et mon caractère m'ont empêché de construire ma
forme à travers de semblables méthodes extrêmes oui mais aussi
extrêmement ennuyeuses voilà pourquoi j'ai choisi d'adopter pour ma
construction autosuffisante et inutile des matériaux apparemment
significatifs cet attachement au matériau-écrit pasolini le met donc à
mal (ici dans pétrole) en martyrisant la structure narrative
traditionnelle ainsi que la représentation réifiante de la réalité
sociale (loi des grands nombres disait gramsci) en faisant droit à une
poétique de l'image en tant que manifestation d'une grâce (comme
sursaut) révélant le caractère merveilleux du déjà existant c'est-à-dire
de la joie d'être au monde et de faire commun rappelons également que
sa conversion au marxisme s'est opérée durant son adolescence frioulane
auprès de journaliers en lutte contre un gros propriétaire terrien et
que ce n'est pas tant leur lutte en soi qui intéressa le jeune poète
qu'une certaine vitalité prolétaire se manifestant à travers elle une
langue particulière et une culture spécifique je suis habitué écrit-il
depuis ma plus tendre enfance à déceler le bonheur par le sourire par
les yeux par la manière dont on sourit par le regard c'est donc un
sourire c'est donc un regard qui défient l'histoire et cette vitalité
cette joie il les rencontre de nouveau au début des années 50 auprès de
sous-prolétaires romains de pigneto ou de rebibbia quand je faisais un
tour à pied dans rome tous les livreurs des épiceries des boucheries des
boulangeries sur leurs vélos avec leurs pantalons rapiécés au derrière
sillonnaient la ville et chantaient il n'y avait personne qui ne
chantait pas il n'y avait personne qui quand on lui adressait un regard
ne rendait pas un regard accompagné d'un sourire cela c'est une forme
de bonheur mais ce sourire a disparu le miracle économique fut un
désastre tant anthropologique qu'écologique une éradication systématique
des cultures périphériques leur colonisation par la centralité du
néocapitalisme latin l'avènement d'une ère sans étoiles désormais on
les voit au contraire pâles névrosés sérieux introvertis s'ils sont
plus sérieux ajoute-t-il c'est peut-être parce qu'ils se posent des
problèmes mais je ne le crois pas parce qu'ensuite en approfondissant un
peu en parlant avec eux j'ai bien vu qu'ils ne se posent pas de
problèmes ils vivent une forme d'infortune une forme d'impuissance
justement parce que leur condition économique ne leur permet pas pour
l'instant de réaliser le modèle petit-bourgeois qui leur est offert en
échange du modèle sous-prolétaire qui a été détruit si pasolini ne peut
plus fonder sa nécessité du communisme sur un sourire sur quoi la fonder ?
sur le terrain poétique il va armer son désespoir d'une jeunesse
nouvelle c'est-à-dire d'une langue neuve car il a beau tenir à ce
quelque chose d'écrit cela ne l'empêchera pas de produire une critique
immanente du discours en soi du discours en tant que structuration
autoritaire du symbolique la forme même de son œuvre la plus tardive en
raison précisément de cette critique se verra donc renouvelée pétrole
n'est déjà plus un roman mais un ensemble fragmentaire mêlant les
registres une profusion de paroles et de genres éviter à tout prix les
mensonges de la question du sens éparpiller la morale se loger dans
l'écart qui lie sans les confondre le signe et sa signification devenir
monteur et archéologue tout en demeurant poète pasolini attaquera ainsi
la grammaire du pouvoir en son cœur la belle langue académique la
structure narrative traditionnelle la hiérarchie des genres etc. bref
tous ces éléments structurants d'une certaine représentation bourgeoise
du monde sa poétique est donc éthique par essence et se fait
indissociablement l'expression d'une politique de l'art et non d'un art
politique et non d'un art moral ce qui implique une autre politisation
de l'art que celle entrevue par benjamin dans son célèbre essai une
réintégration de l'aura dissipée au cœur de la production sérielle cela
implique conjointement un communisme qui renouerait avec le langage et
s'émanciperait enfin radicalement du discours contre l'image sérielle
donc chargée d'un contenu c'est-à-dire d'un discours sans susciter
l'expérience une autre conception de l'image est donc possible en tant
qu'effraction de la raison sensible jlg/ppp c'est ici que la jonction se
fait dans l'image délivrée de sa fonction illustrative délivrée ainsi
d'un contenu qui la pille de son autonomie et la constitue comme
instrument communicationnel véhicule d'une vision du monde et ferment de
l'idéologie bourgeoise dans le champ littéraire au sein duquel l'image
se fait parole entendons ce que peut avoir de bouleversant une mise en
rapport accidentelle de mots de phrases ou de phonèmes de l'expérience
quasi inaugurale des surréalistes au symbolisme accidentel d'un t. s. eliot afin de rencontrer l'image ou la parole donc sur le terrain du
champ cinématographique ou sur celui des littératures que celles-ci nous
éclairent sur les chemins de la posthistoire il est donc nécessaire de
tenter un arrachement au déjà-écrit du discours (contenu
théorico-inerte) et de la représentation (négation de la vitalité des
forces sociales) moins un arrachement systématique à l'ordre des
significations peut-être qu'un maintien de cette obscurité native
(condition du poème selon paul celan) car la clarté aveuglante de la
raison triche ce qui n'implique guère que la vie sera privée de
significations seulement qu'elle sera enfin en mesure de produire les
siennes propres en dehors de la clôture néolibérale qu'induisent le
régime de la représentation et l'ordre du discours un pas de côté
vis-à-vis de la traditionnelle mise en récit du monde est nécessaire
quand le cours de l'expérience s'est effondré quand le récit contribue
à creuser cette distance qui nous sépare du monde plutôt que de fonder
ce qui nous y inscrit en tant que communauté réelle quand les nouvelles
normes de la narration commerciale substituent à l'expérience concrète
de la réalité une représentation d'un monde-déjà-mort dépouillé de son
aura arraché à sa forme vivante c'est en ce sens que benjamin dit qu'il
semble que nous ayons perdu une faculté que nous pouvions croire
inaliénable que nous considérions comme la moins menacée celle
d'échanger des expériences il nous faut donc trouver de nouveau un
moyen de faire commun à partir de la mutualisation de nos expériences et
cela implique sans doute de repenser radicalement la fonction et la
structure du récit de repenser sa fonction en tant que fondement d'une
communauté réelle et non plus fictive (à moins d'arracher la fiction à
sa forme vide) d'adapter sa structure c'est-à-dire également sa forme
au réel de nos expériences ou de ce qu'il en reste (ce que fit
notamment kafka) l'idée n'étant pas d'affiner notre représentation des
êtres et des choses ce que firent tous les réalismes critiques ou
acritiques mais de renoncer à représenter le monde et notamment ce monde
dont nous n'avons qu'une (mé)connaissance souvent odieuse au regard de
celles et ceux que nous enroulons dans cette représentation cela
implique de se soustraire au caractère instrumental de la langue une
forme soutenant un contenu qui en use comme pré-texte dialectique de
l'oubli et de la mémoire donc de l'effacement et de l'écriture car seule
la main qui efface peut écrire mais si on conçoit aisément que le strict
travail de l'image puisse s'affranchir d'une structure narrative (après
tout : demande-t-on nécessairement aux peintres de raconter une histoire ?)
et d'un contenu (quelle morale doit-on découvrir sous un chef-d'œuvre
de caravage ?) le travail littéraire se trouve lié plus viscéralement au
récit ce pour quoi le geste pasolinien revêt ici un caractère extrémiste
eh bien dit-il ces pages imprimées mais illisibles veulent proclamer
d'une façon extrême mais qui se pose comme symbolique également pour
tout le reste du livre ma décision qui n'est pas d'écrire une histoire
mais de construire une forme forme consistant simplement en quelque
chose d'écrit ce qui rejoint le travail de godard non seulement sur le
terrain cinématographique mais également sur le terrain littéraire
(pétrole transhumaniser et organiser etc.) en fabriquant du langage à
partir d'une action sur la langue elle-même en sortant de la langue par
la langue ainsi l'un et l'autre l'un avec l'autre l'un contre l'autre
ont guetté quelque chose de neuf sur le plan formel ce qui leur a valu
tant d'incompréhension et d'injures la société néolibérale se
caractérisant par une mise au pilori de tout ce qui échappe à sa
formidable capacité d'absorption et de neutralisation il faut maintenir
à tout prix cet état de douce sidération et d'impuissante compassion qui
caractérise les gentils électeurs de gauche l'enjeu de trouver une forme
qui puisse échapper à cette absorption à cette neutralisation se pose
nécessairement à l'artiste soucieux de produire du langage et non une
représentation statique de l'ordre existant la forme que nous devons
tenter ne sera donc pas la géolocalisation d'une brèche mais ce dans
quoi nous nous reconnaîtrons en tant que puissance commune c'est l'enjeu
d'une révolution esthétique inséparable d'une révolution sociale un
mouvement révolutionnaire qui se nierait dans une représentation
homogène de lui-même qui se complairait dans une vision du monde
appauvrissante et se soutiendrait d'un discours séparé de sa pratique
ne pourrait être le nôtre c'est la forme même de ce mouvement en tant
que produisant une nouvelle unité dynamique de la forme et du contenu
(de l'éthique et du politique) qui déterminera l'orientation de celui-ci
autrement dit c'est communistement qu'il faut parvenir au communisme ce
pour quoi nous ne saurions nous satisfaire d'un film comme le martin
eden de pietro marcello un beau film à la morale irréprochable un film
qui se fait sous une forme assez classique le véhicule de valeurs
morales (antifascistes antiracistes etc.) que nous approuvons certes
mais qui demeurent en tant que valeurs précisément un simple contenu
discursif que le régime néolibéral de la représentation peut aisément
diluer dans la société du spectacle et donc désarmer c'est autre chose
que nous réclamons à la production cinématographique qui se présente
comme indépendante il s'agit d'inventer une forme neuve quelque chose
qui ne soit pas une morale bien ficelée quand bien même elle le serait
de façon assez virtuose en choisissant naples comme décor de son martin
eden un naples restitué ancien et folklorique plutôt que le naples
d'aujourd'hui qui est à un tournant de son histoire sociale alors que
les luttes contre la gentrification les expériences autogestionnaires
les collectifs de solidarité concrète y prolifèrent en choisissant
également la fiction plutôt que la forme-documentaire pietro marcello
arrache l'antifascisme à sa signification historique pour en faire un
enjeu purement moral dépouillé des forces sociales qui en constituent la
matière vive nous nous étonnons ainsi de ce conformisme du champ
culturel et artistique à une époque qui est pourtant foisonnante de
nouvelles formes politiques on me rétorquera qu'il s'agissait de
combattre pragmatiquement la poussée du néofascisme italien dans un
contexte de forte poussée migratoire liée à différents conflits
meurtriers qu'il s'agissait donc de lutter sur le plan de la morale
collective contre le racisme et pour l'accueil des réfugiés ce
pragmatisme idéologique pose problème en soi les actions de solidarité
concrète sont nécessaires oui mais pour ce qui est de la lutte
culturelle que nous devons mener comment croire que c'est un discours
moral enrobé d'une forme conventionnelle qui sera en mesure d'agir sur
la conscience des masses agir sur la conscience des masses agir sur la
conscience des masses c'est aussi le problème le commun suppose un
langage une mise en rapport de matériaux hétérogènes et non la
transmission par tous les nouveaux moyens technologiques de la
propagande d'un socle de vérités établies sur le monde d'une
conception homogène de l'histoire et de ses destinées possibles ainsi
pasolini dit que c'est la lecture de rimbaud qui lui a fait choisir
instinctivement le camp de l'antifascisme la poésie symboliste en tant
que contestation de la fonction instrumentale de la langue est en mesure
d'invoquer une parole qui accuse la grammaire de faire naître la
disjonction en lieu et place de la coordination docile de rejeter la
mécanicité du discours ou d'en user pour le disqualifier de faire mûrir
un cri dans un désert de significations ce cri est tout ce que nous
avons personne ne nous l'enlèvera personne ne nous pétrira de nouveau de
terre et d'argile personne ne soufflera la parole sur notre
poussière personne la production culturelle dominante qui agit en tant
que morale à l'intérieur du régime de la représentation et de l'ordre du
discours n'a toujours prêché que des convaincus et en effet
l'antifascisme d'aujourd'hui ne prêche que des convaincus c'est pour
cela qu'il est si faible et si bruyant c'est un antifascisme idéologique
une valeur (inscrite d'ailleurs dans la constitution de la république
italienne) qui n'est que trop peu questionnée par la critique reprenant
le fil des interrogations de pasolini il est temps de distinguer
l'antifascisme en soi le fait de combattre toute oppression d'état sous
ses formes les plus coercitives les plus racistes et les plus sales qui
soient et l'antifascisme idéologique celui qui est hégémonique et sur
lequel l'état a toujours la mainmise celui auquel les plus fascistes de
nos gouvernants doivent également se soumettre s'ils veulent aujourd'hui
conquérir le pouvoir institutionnel cet antifascisme institutionnel donc
qui revêt des réalités fascistes à plein d'égards (israël en tant que
laboratoire de la répression ethnique et politique systématisée / la
russie de poutine qui en use également jusqu'à l'écœurement etc.) est
une construction opportuniste qui vise à désamorcer toute critique à
l'égard d'états réellement fascistes c'est également une arme
idéologique dirigée contre tout ceux qui attaqueraient la nature
impériale des états occidentaux ayant intégré l'antifascisme à leur
constitution ou à leur socle de valeurs morales le véritable
antifascisme ne peut plus s'abriter sous les vieilles bannières de
l'antifascisme traditionnel il doit attaquer la nature fasciste du
système en soi la perpétuation de systèmes de domination coloniale
(extérieure et intérieure) ou encore le totalitarisme de la société de
consommation qui implique non seulement la prolétarisation du producteur
mais également la prolétarisation du consommateur société de
travailleurs sans travail (disait arendt) et société de consommateurs
précarisés tandis que l'antifascisme de pietro marcello demeure enlisé
dans la morale compatissante et le discours lénifiant qu'il n'attaque
jamais le fascisme réel celui de l'état et du capital en leur
complémentarité organique et se contente d'une morale dont se repaissent
les braves qui n'a donc aucune efficacité politique (bien au contraire)
qui permet seulement au pouvoir de se justifier en ajustant ses normes
morales aux exigences de la situation (la fameuse opinion publique)
d'alimenter ainsi l'état de sidération et d'impuissance qui caractérise
notre époque en vérité écrit benjamin il s'agit beaucoup moins de
transformer l'artiste d'origine bourgeoise en maître de l'art
prolétarien que de le faire fonctionner fût-ce aux dépens de son
efficacité artistique en des endroits importants de cet espace d'image
ne pourrait-on aller jusqu'à dire que l'interruption de sa carrière
artistique représente une part essentielle de ce fonctionnement ? il fut
un temps où avec benjamin nous pouvions concevoir un certain
positionnement stratégique au sein de l'industrie culturelle nous
pensons que la proposition stratégique de benjamin mérite d'être
réactivée et ajustée aux exigences de l'époque non qu'il ne faille pas
prendre l'argent où il est mais il faut le prendre sans rendre de
comptes ne pas s'installer au cœur de l'institution faire un ou deux pas
de côté brouiller les pistes changer de nom etc. non pour offrir une
coloration militante à son art mais pour tenter une véritable révolution
de la forme elle-même une élaboration formelle qui échapperait de fait
aux tentatives d'absorption et de neutralisation par le spectacle de la
marchandise je sais affirme pasolini que la poésie n'est pas un produit
de consommation je vois bien ce qu'il y a de rhétorique dans le fait de
dire que même les livres de poésie sont des produits de consommation
parce que la poésie au contraire échappe à cette consommation les
sociologues se trompent sur ce point il leur faudra le reconnaître ils
pensent que le système avale et assimile tout c'est faux il y a des
choses que le système ne peut ni assimiler ni digérer une de ces choses
je le dis avec force est la poésie on peut lire des milliers de fois le
même livre de poésie on ne le consomme pas le livre peut devenir un
produit de consommation l'édition aussi la poésie non l'enjeu étant
désormais de construire les conditions de l'autonomie du champ
esthétique et de travailler conjointement sur le terrain social et
politique à la mise en place de dispositifs permettant aux formes non
assimilables de l'art d'habiter l'espace de la cité socialement et
culturellement nous sommes engagés dans une guerre de position longue et
éprouvante contre les structures du pouvoir nous devons donc accepter de
renoncer à greffer un contenu politique (toujours moral toujours
discursif) à nos productions culturelles et artistiques et faire
politiquement de l'art ce qui signifie construire une forme vivante une
action sur le réel un langage la gratuité de ce langage son absence de
contenu séparé sa forme toujours neuve cette mobilisation conjointe de
l'instinct créateur et de la raison critique en font sur le plan
esthétique le seul enjeu de notre époque au regard de notre civilisation
capitaliste et coloniale ce langage sera nécessairement désigné comme
barbare ce pour quoi un grand poème d'amour sera toujours
révolutionnaire dès lors qu'il invente un langage un langage pour ne
rien dire un langage pour manifester l'être et sa puissance d'aimer vous
dites l'amour mais rien n'est plus contraire à l'image de l'être aimé
que celle de l'état dont la raison s'oppose à la valeur souveraine de
l'amour l'état n'a nullement ou a perdu le pouvoir d'embrasser devant
nous la totalité du monde ce pour quoi la forme est tout ce qui doit
nous intéresser le contenu seul ne peut rien le contenu est ce que nous
prenons pour la réalité et dont nous sommes exclus l'histoire de
l'écriture qui est conjointement l'histoire du pouvoir se fonde sur
cette illusion ne concédant qu'une fonction instrumentale à la forme
celle de se faire le véhicule d'un contenu qui se dérobe toujours le
contenu n'existe pas ni le contenu ni la profondeur la profondeur n'est
qu'un pli de surface la crispation de l'être privé de forme l'illusion
d'un mystère qui se soustrait à notre regard une illusion que le pouvoir
entretient une illusion que le savoir entretient une illusion que la
grammaire entretient une illusion qui fonde l'illusion de la puissance
des rois une illusion qui fonde l'illusion de la séduction marchande une
illusion qui a rendu possible le fascisme l'idée qu'une vérité se
dissimule sous l'être une vérité-déjà-écrite une vérité qui justifie la
classe la race le genre une vérité qui tue une vérité dont a besoin le
capital pour se maintenir en tant que structure sociale naturelle une
vérité qui perpétue la destruction du monde au nom de l'idée de progrès
quand nous l'aurons compris si nous le comprenons un jour la face du
monde changera quand nous le comprendrons il n'y aura plus de roi il n'y
aura plus de forme-marchande il n'y aura plus de fascisme la forme aura
été libérée et tout se jouera ici-bas avec nos morts certains l'ont
pressenti nous mettrons longtemps encore à comprendre ce qu'ils ont
cherché à nous dire d'autres dès aujourd'hui en devinent l'importance
formulons modestement l'hypothèse que cela touche à notre condition
humaine et posthumaine qui est encore à découvrir cette découverte
dépendra d'une écologie à naître d'une écologie du sens d'une écologie
de l'image d'une écologie matérielle cette découverte aura une forme
purement négative elle sera donc une forme pure sans contenu sous-jacent
sans arrière-fond de signification une forme cosmique et donc
posthumaine c'est-à-dire une forme enfin humaine affranchie de
l'humanité bourgeoise de ses abstractions de ses frontières
épistémologiques ou nationales de sa fureur prométhéenne et de sa morale
d'esclave cette forme nous la nommons communisme d'autres préfèrent la
nommer poésie pour nous c'est la même chose c'est l'unité dialectique de
l'être et du langage une langue que nous devons tenter la possibilité
d'être réellement avec l'autre de faire commun une
possibilité déjà existante une possibilité sur laquelle nous fondons
notre utopie

<div class="ligneblanche">&nbsp;</div>

**Crédit**

<div class="ligneblanche">&nbsp;</div>

<div class="centre">

jean-luc godard

pier paolo pasolini

antonio gramsci

william butler yeats

simone weil

lucien sève

walter benjamin

theodor w. adorno

paul celan

hannah arendt

michel foucault

</div>

</div>

<div class="text" data-number="17">

## « Strappa da te la vanità »

<div class="epigraphe">

per frate vento et per aere\
et nubilo et sereno et onne tempo

--- François d'Assise

</div>

<div class="colright">

Une séquence datant de 1968[^308] montre Pasolini chez Pound, à
Venise, récitant un extrait des *Cantos* :

</div>
<div class="colleft">

<span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Rabaisse ta vanité, je dis rabaisse-la.

Apprends du monde verdoyant quelle peut être ta place

Dans l'échelle de la découverte ou de l'art vrai,

Rabaisse ta vanité,

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>Paquin rabaisse-le !

Le casque de verdure l'a emporté sur ton élégance[^309].

</div>
<div class="colright">


Ce passage, quelques autres, les ultimes fragments notamment, nous les
tenons pour les plus accessibles d'une œuvre parcourue de langues,
d'idéogrammes, de références mythologiques et d'anecdotes historiques.
« Pound, écrit Pasolini, bavarde dans le cosmos[^310]. » De telles
palabres ne se prêtent que difficilement à la clarté unifiante du
*logos*. Une logique, pourtant, est à l'œuvre : cryptique, si l'on veut,
mais moins éthérée qu'on pourrait le croire --- la folie n'étant pas la
suspension de la logique, mais l'une de ses formes, « autonomisée »,
qui tourne et flamboie au-dessus de la réalité. Ce caractère de
suspension de la parole poundienne, son chatoiement quasi cosmique,
les mille voix qui la traversent reposent cependant sur un *background*
bien réel : celui de l'Amérique dite profonde, cette immensité de
l'*espace ouvert*, son absence d'ancrage historique sur laquelle se
fonde pourtant un enthousiasme d'entrepreneur. À l'intérieur de ce
monde paysan, les peuples immigrés ont importé leur histoire
archaïque, unique héritage de ces exilés latins ou irlandais ayant
abandonné la vieille Europe pour fonder des empires agricoles ou
tenir une simple ferme. Selon Pasolini, c'est ce qui explique la
fascination exercée par la Chine sur le poète américain ; à travers
elle, il renoue avec un héritage perdu, rural, paysan, celui de ses ancêtres
exilés. De la poésie chinoise, Pound vénère le pragmatisme, le style
court et direct, il en vénère également les vertus, cette antique
sagesse paysanne :

</div>
<div class="colleft">

« Maîtrise-toi, alors les autres te supporteront »

<span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Rabaisse ta vanité

Tu es un chien battu sous la grêle,

Une pie gonflée dans un soleil changeant,

Moitié noire moitié blanche

Et tu ne reconnais pas l'aile de la queue

Rabaisse ta vanité

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>Que mesquines sont tes haines

Nourries dans l'erreur,

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>Rabaisse ta vanité,

Prompt à détruire, sordide dans la charité,

Rabaisse ta vanité,

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>Je dis rabaisse-la[^311].

</div>
<div class="colright">

Cette quête d'une origine perdue est peut-être ce qui explique son
arrivée en Europe, à Paris d'abord, puis à Venise. *Venise*. Pour un
américain comme Pound, c'est rompre avec l'anhistoricité de sa terre
natale et choisir de se loger dans le plus pur héritage, si pur qu'il
en devient vacillant, à vouloir contenir dans sa synthèse monumentale
toutes les mythologies, tous les arts, toutes les religions d'Occident
et d'Orient. Amour de l'origine, du berceau, de la pureté
civilisationnelle, donc, qui peut expliquer son affinité avec le
fascisme mussolinien, mais également, et c'est infiniment plus
intéressant, qui justifie une conception inédite de la tradition et de
l'histoire : un acte de recréation fondamental impliquant l'oubli
--- condition nécessaire de sa vitalité.

</div>
<div class="colleft">

Mais d'avoir fait au lieu de ne pas faire

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>ce n'est pas là de la vanité

D'avoir, par décence, frappé à la porte

Pour qu'un Blunt ouvre

<span style="margin-left: 2em; display: inline-block;">&nbsp;</span>D'avoir fait naître de l'air une tradition vivante

ou d'un vieil œil malin la flamme insoumise

Ce n'est pas là de la vanité.

<span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Ici-bas toute l'erreur est de n'avoir rien accompli,

toute l'erreur est, dans le doute, d'avoir tremblé[^312]...

</div>
<div class="colright">

Les *Cantos*, c'est le système de Hegel mais troué, son envers
bordélique ou baroque. La critique a cherché à lui attribuer une
« physionomie unitaire[^313] » qui n'existait pas. Sa composition est
fragmentaire, composite : interruptions, parenthèses, excursus
disproportionnés, etc. Elle rompt radicalement avec le mauvais poème
de printemps --- *Beauty is difficult* ---, ne se laisse guère saisir
comme fruit docile à l'approche de l'été. Pour y parvenir, il faut
effacer, recomposer, créer quelque chose de neuf sur les ruines de la
Littérature et de l'Histoire. C'est uniquement ainsi que « La
tradition \[ne sera\] pas un jeu de chaînes contraignantes qui nous
lie au passé mais la beauté qui dure en nous[^314]. » C'est ce qui
devait tant plaire à Pasolini : cette « marche à reculons au cœur du
monde paysan[^315] ». Mais le projet est impossible. Malgré quelques
éclats dont l'intelligibilité flatte l'érudition du lecteur, toute
unité se dérobe : « ... à la fin de la lecture des *Cantos*, dit
Pasolini, on se sent vidé et comme déçu. Leur savoir est trop
particulier et tragiquement privé pour pouvoir vraiment accroître
notre patrimoine de naissance[^316]. » À la fin, il ne reste rien,
si ce n'est le sentiment d'un grand fracas, avec le bruit du vent dans
les feuilles des arbres :

</div>
<div class="colleft">

J'ai voulu écrire le Paradis

Ne bouge pas,

<span style="margin-left: 2em; display: inline-block;">&nbsp;</span>Laisse parler le vent

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>Tel est le Paradis[^317].

</div>
<div class="colright">

Échec, donc, le *paradiso* n'adviendra pas. Goût de sulfite et de
pierre dans la bouche, « expérience pure du délire[^318] », vite
oubliée. Les gueules de bois poundiennes n'enseignent rien. Peut-on le
leur reprocher ? Mais elles bafouent l'Histoire, le vent également, et
se repentent quelques fois :

</div>
<div class="colleft">

Que les Dieux pardonnent ce

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>que j'ai fait

Puissent ceux que j'ai aimés tenter de pardonner

<span style="margin-left: 4em; display: inline-block;">&nbsp;</span>ce que j'ai fait[^319].

</div>
<div class="colright">

Le repentir est à la mesure de la sauvagerie. Elle a pourtant un
mérite inavouable : elle ne se prête pas aux jeux des récupérations
droitières ; son bavardage est suffisamment éclaté et cosmique pour
échapper à un discours sur l'Histoire. Autre symptôme de sa folie :
une quête obstinée et puérile d'historicité. Tel enfant capricieux qui
s'empresse de ramasser le jouet qu'il vient de mettre en morceaux.
Mais la locomotive de l'histoire est passée et ne repassera pas. La
folie du XX^e^ siècle nous adresse ses repentirs à travers les repentirs
de Pound. Et si la grande dialectique est impuissante à recoller les
morceaux, comment rebâtir sa boutique ? Autrement dit : comment faire
tenir le poème ? C'est un chant qui vient de loin, celui que la mère
de saint François murmurait à son fils, c'est le chant des
troubadours. Tradition ressassée et toujours neuve que Pasolini
inscrira également sur le fronton de ses premières poésies :

</div>
<div class="colleft">

Ab l'alen tir vas me l'aire

Qu'eu sen venir de Proensa :

Tot quant es de lai m'agensa[^320].

</div>
<div class="colright">

Un souffle perdu, ressaisi, offre sa structure rythmique au poème.
C'est le souffle d'une langue qui n'est pas nécessairement la langue
maternelle, mais l'autre, la source mère cachée de la tradition. Si
Pasolini écrira en frioulan, ce lointain cousin du provençal, Pound
choisira le mandarin, sans renier pour autant la tradition des
troubadours. Pour le poète exilé, c'est un double pied de nez. Double,
car on n'y comprend rien, au mandarin de Pound. Et parce que cette
incompréhension est un élément du poème lui-même. Contre toute
attente, la plus grande littéralité creuse la matière, aiguise le
*différend*. À moins d'un regard machinique où le mystère se dissipe.
Benjamin y fit référence à propos des traductions de Sophocle par
Hölderlin : « Dans ces traductions, écrit-il, l'harmonie des langues
est si profonde que le sens n'y est affleuré que comme harpe éolienne
par le vent de la langue[^321]. » L'« harmonie » des langues,
voilà le seul universel. Contre l'universel vide de la marchandise :
il se recompose en manipulant des morceaux d'histoire spécifique, des
bouts de givre et des idiomes. Ni passéisme ni forme vierge : le
rapport à l'histoire est fondamentalement heuristique ; l'histoire
*autrement*, morcelée, foisonnante, infinie --- mais ne serait-ce pas
là l'ultime possibilité de nous rapporter à elle, non plus par la
superbe unité perdue, mais depuis la brisure, non plus sous l'égide
des universaux conquérants, mais par un seul souffle, rescapé ?

</div>

</div>

<div class="text" data-number="18">

## Réécriture, recréation

<div class="epigraphe">

Comment nous sauver nous-mêmes de l'expérience de nos vingt ans ?

--- Walter Benjamin

</div>

En 1974, trente-deux ans après les *Poèmes à Casarsa*, paraît *La
Nouvelle jeunesse*, qui rassemble une sélection relativement exhaustive
des écrits de jeunesse de Pasolini :


> Fontaine d'eau de mon pays.
> 
> Il n'est d'eau plus fraîche qu'en mon pays.
> 
> Fontaine de rustique amour[^322].


La seconde partie de l'ouvrage est une réécriture de ces mêmes écrits
--- auxquels s'ajoutent quelques nouveaux poèmes en frioulan. Le geste est
singulier --- cette réécriture prenant le contrepied du lyrisme de sa
jeunesse en lui opposant la lucidité inquiétante du témoin :

> Fontaine d'amour d'un pays non mien
> 
> Il n'est d'eau plus vieille qu'en ce pays.
> 
> Fontaine d'amour pour personne[^323].

L'eau s'est tarie car elle n'était plus bue. La « fontaine de rustique
amour » est donc littéralement hors d'usage. Le poète d'âge mûr est
incapable de reconnaître son pays, cette terre prénatale au sein de
laquelle il traversa les épisodes les plus traumatiques, mais également
les plus lumineux de son adolescence : mort de son frère, détresse de
sa mère, rencontre avec le prolétariat paysan, « conversion[^324] » au
marxisme. Le jugement proféré en 1959 --- « je ne reconnais plus
rien[^325] » --- était encore circonscrit au littoral frioulan ;
désormais la perte est totale : les figures (paysan, Christ, papillon)
se désagrègent sous l'effet du développement de la civilisation
capitaliste. Et si adolescent, jeune adulte, Pasolini pouvait douter de
son propre corps, il doute maintenant de l'existence du monde humain, ou
de ce qu'il en reste quand « air, eau, lumière solaire : les soi-disant
“biens gratuits” \[sont désormais\] facturés jusque dans le budget de
l'État, à la rubrique “coûts externes”[^326] ». Si l'image d'un
Christ abandonné pouvait conduire le jeune Pasolini à la déploration, il
s'agissait encore d'une image d'un temps immobile.

> C'était un enfant qui faisait des rêves,
> 
> un enfant bleu comme son bleu de travail.
> 
> Et viendra le vrai Christ, ouvrier
> 
> &nbsp;
> 
> pour t'apprendre à faire de vrais rêves[^327].

Quant au nouveau Christ, le prolétariat, sa disparition a emporté avec
lui le corps même de la réalité --- le désespoir cryptochrétien de sa
jeunesse faisant ainsi place à un désespoir devant l'Histoire --- qui
contraste avec l'antique désolation de sa jeunesse et annonce déjà la
déploration des *Cendres* :

> Ô BEL ENFANT
> 
> ORGUEILLEUX,
> 
> UNE FRONDE À LA MAIN,
> 
> TU N'ES PLUS.
> 
> BEAU PAYS PARMI LES CANAUX,
> 
> AUX FÊTES JOYEUSES,
> 
> TU N'ES PLUS.
> 
> BELLE VERTE
> 
> CAMPAGNE
> 
> DONT JE CONNAIS CHAQUE RANGÉE D'ARBRES,
> 
> TU N'ES PLUS[^328].

Le prolétariat, « beau jeune homme » ayant troqué son sourire et sa
santé contre un bien-être de façade, a vu sa pauvreté réduite à un
aspect purement quantitatif --- sans terre, sans tradition, sans
espérance révolutionnaire :

> Sonnez, pauvres cloches, / sonnez l'Ave Maria,
> 
> car le garçon s'en revient / plein de mélancolie.
> 
> Sonnez, pauvres cloches, / sonnez les matines,
> 
> car il est désormais vieux, / le beau garçon[^329].

Flétrissure de l'antique jeunesse qui en convoque la mémoire émue :

> Ils ne connaissaient que cette façon
> 
> d'être jeune, de faire l'amour,
> 
> d'être dans les champs ou au coin du feu ;
> 
> cette terre est leur terre, parce qu'ils
> 
> sont seulement de cette terre[^330].

L'horizon des grands départs, l'exil, hier vers « Pordenone et le
monde », aujourd'hui vers Rome ou Milan, a débouché sur une promesse de
misère nouvelle :

> ils ont oublié leurs frondes
> 
> en descendant vers Pordenone et le monde[^331].

C'est tout le sens de cette « décadence ouvrière » qui s'est accomplie
à la vitesse des nouveaux trains emportant avec eux, non seulement ces
jeunes gens, mais un monde qui n'aura plus d'héritiers.

> Trains, venez emportez / au loin la jeunesse
> 
> chercher de par le monde / ce qui s'est ici perdu.
> 
> Trains, emportez à travers le monde / ces joyeux drilles
> 
> chassés du pays / pour que jamais plus ils ne rient[^332].

La seconde version des poésies de jeunesse débute par une réécriture des
*Poèmes à Casarsa*. Le rapport à l'histoire demeure ainsi dialectique :
il ne s'agit pas d'une matière neuve, mais d'une recréation à partir de
la matière ancienne de ses premières poésies et dans une langue qui
demeure le frioulan. C'est un élément fascinant de sa démarche, qui le
rapproche sans doute du modernisme traditionaliste d'un Pound pratiquant
la retraduction et l'usage de dialectes archaïques. Ni égotisme
transcendantal ni solitude larmoyante : l'histoire est ressaisie par sa
négative, ses craquèlements, sa totalité impossible. Le poème
introductif nous renseigne sur l'adresse : Pasolini offre son livre, ce poème « vécu et revécu, corps dans un corps », à une jeunesse
possible mais non encore advenue ; celle-ci sera le sujet d'une
dialectique nouvelle, « obéissance et désobéissance[^333] »,
assumant le négatif que refoule l'idéologie bourgeoise de
son temps tout en maintenant vive sa capacité d'insoumission, de rejet
global du capitalisme. Quant à la méthode de l'ouvrage, non seulement
elle se fera réécriture, mais elle déploiera un perspectivisme encore
plus grand en proposant à certaines réécritures des variantes, une
constellation de devenirs possibles. Ce qui tranche assez avec le
discours social des *Cendres*, sa loyauté à la narration classique, à la
versification de Dante (*terzina dantesca*).
Expérimentation, réécriture, fragmentation, etc. --- retour sur l'enfant
de Casarsa, son rêve d'un corps, « coquillage contre le mal[^334] », son
idéalisation du corps prolétaire. Reniement d'un langage qui fut
celui de sa jeunesse bourgeoise, d'un langage qui entrait en conflit
avec ce rêve de matière, d'immanence, d'incarnation. « Rien n'a changé » :
rejouant ainsi la psalmodie des nuits de Casarsa, les rejouant
autrement, trente-deux ans plus tard, Pasolini clame que ce n'est pas
lui qui a changé, que c'est le monde qui agonise :

> Mais le monde a touché à sa fin, et tu es
> 
> une lumière dans l'histoire du néant[^335].

Cette lumière était-elle le monde de la tradition, de l'immobilité
paysanne, ou celui de la première modernité capitaliste qui fut
conjointement celui de l'espérance prolétarienne ? Ce sont deux lumières
conflictuelles dans l'histoire personnelle et universelle du poète. Deux
lumières qui, l'une et l'autre, se sont désormais éteintes à l'heure
d'une fort longue et monstrueuse transition. Comment en préserver la mémoire ? L'histoire ne saurait tenir à une compilation de données abstraites, elle doit demeurer ce qui dure en nous ; car, dès lors qu'elle ne vit plus en nous, sous forme d'héritage concret, elle est maudite :

> je maudis l'histoire
> 
> qui n'est pas en moi et que je refuse[^336].

La réaction qui consisterait à s'envelopper dans une représentation du
passé ne tient pas ; il s'agit de s'adapter, sans se renier. Refus,
donc, d'une intégration formelle aux nouvelles normes de la société
capitaliste, usage stratégique des outils qu'elle met à notre
disposition pour la combattre :

> Je ne peux m'ensevelir
> 
> dans ce temps immobile
> 
> et vivre tel un traître[^337].

Songe-t-il à son amitié conflictuelle avec Fortini ? Ici, aucune
ambiguïté : refus de se joindre à la pestilence droitière, réactionnaire
et néofasciste. Il faut l'affirmer, le marteler si besoin : le fascisme
historique, pas plus que ses résidus contemporains, ne vise une
conservation de la tradition, mais bien sa désintégration. Et si le
regret d'un monde perdu peut sembler complaisant, entendons ce qu'il
peut contenir de *faible puissance utopique* :

> Je ne regrette pas une réalité, mais sa valeur.
> 
> Je ne regrette pas un monde, mais sa couleur[^338].

Les traditions permettaient de définir la valeur des choses, du travail,
des relations interindividuelles du point de vue de leur usage. Sous le
fascisme, agent massif d'acculturation, les populations autochtones ont
su perpétuer des traditions de façon clandestine --- et préserver ainsi
leur âme :

> ... Les fascistes n'entament
> 
> pas l'âme. Je sais que dans mon pays,
> 
> ils ont essayé vingt ans durant : mais les jeunes
> 
> &nbsp;
> 
> et les hommes sont restés ceux de tous les siècles.
> 
> Tuez-les, jetez-les en prison, comme
> 
> ils le font. Ils sont peu nombreux. Ils se flétrissent
> 
> et renaissent comme chiendent[^339].

Alors que le néocapitalisme a bel et bien accompli une destruction des
usages. Si Pasolini use de catégories théoriques (marxistes) pour
analyser le monde, il n'en demeure pas moins un sensualiste, pour qui le
monde, le monde d'avant, était surtout l'objet d'un plaisir esthétique.
Le temps de la superbe immobilité paysanne était ainsi marqué par le
*passage* et la *métamorphose* : hivers rugueux, étés de terrible
sécheresse, laissant toujours place au mouvement cyclique des saisons,
temps des renaissances printanières et de la mélancolie automnale ;
c'était donc le temps agricole, celui d'un monde en adéquation, pour le meilleur comme pour le pire, avec les forces de la nature. Tandis que les
rythmes de production, d'échange, de consommation, tels qu'ils se
déploient sous la civilisation néocapitaliste (une séquence qui débute,
selon Pasolini, après la Seconde Guerre), sous leur cadence effrénée,
leurs promesses de nouvelles couleurs et de fausse sensualité, couvent
désormais une néantisation, non seulement de l'histoire sociale, mais
également des mondes non humains :

> Du Dimanche au Lundi
> 
> toutes les herbes du monde
> 
> ont séché[^340].

Ce qui demeure vivant, ce ne sont plus les choses mêmes --- saisons,
artisanats, rites --- mais leurs formes (spectrales) dans la conscience
du poète :

> Aucune bénédiction.
> 
> (Seule sa forme, peut-être.)
> 
> Pourquoi le soleil
> 
> reste-t-il sur les prés[^341] ?

Si la beauté naturelle est toujours perçue depuis ce *quelque chose
d'humain* --- retraduction dans le langage de la culture d'un rapport
instinctif à l'espace et au temps ---, que faire désormais de toute cette
beauté sans usage ?

> Ô champs lointains ! Tamaris !
> 
> Chantant ou ne chantant
> 
> point, je ne sais ni quand ni comment
> 
> quelque chose d'humain est fini[^342] !

Malgré ce regret de voir disparaître des cultures humaines spécifiques,
Pasolini rompt avec l'humanisme de sa jeunesse ; ce qu'il déplore, c'est
que la terre se meurt, et avec elle, la *possibilité* d'une humanité
nouvelle (ou posthumanité). Ce pour quoi le terme de « nouvelle
préhistoire » peut porter à confusion, car il ne s'agit pas, ou plus, de
s'accrocher à une histoire strictement humaine :

> Pour toi quelque chose d'humain a changé,
> 
> Pour moi la terre seule a changé[^343].

La terre, mère du vivant, source et berceau de l'évolution animale, des
flores improbables et inouïes, demeure capable d'enfanter toujours
quelque chose de neuf. À condition que le fils humain ne l'ait pas
éventrée avec ses rythmes fous et ses cadences infernales.

> À l'origine du monde,
> 
> et au terme de la vie,
> 
> chacune de nos paroles
> 
> signifie « mère[^344] ».

Désir de voir la parole humaine se confronter de nouveau au vertige de
l'origine et de la finitude --- ce qu'elle ne peut désormais faire que de
façon négative :

> Un jeune homme au fond du miroir
> 
> écrit les jours de sa vie,
> 
> il efface plus qu'il n'écrit,
> 
> car il n'a pas d'histoire[^345].

L'extinction des cultures spécifiques a laissé place à un procès
civilisationnel dévastateur --- absence d'un rapport concret, réaliste,
vivant à l'histoire :

> La Parole a disparu
> 
> *mais le Livre est resté*[^346].

Orphelin de la Parole qui justifiait le Livre, la pétrification du
rapport à l'histoire fait écho à la pétrification de la Parole du Christ
dans l'institution qui s'en réclame. C'est ainsi qu'il faut accepter de
renoncer au Livre --- institution ecclésiale ou orthodoxie marxiste ---
afin de rendre possible la naissance de quelque chose de neuf :

> ... si le grain de froment ne meurt après qu'on l'a jeté dans la terre,
> il demeure : mais s'il meurt, il porte beaucoup de fruits.
> 
> (Jean, *Évangile* XII, 24, cité par Dostoïevski[^347])

Église, marxisme officiel, institution littéraire... la grande poésie
n'échappe pas à cette sentence de mort --- le poète non plus :

> --- il aurait voulu donner sa vie pour tout
> 
> <div class="a-droite">un monde inconnu,</div>
> 
> lui, modeste saint inconnu,
> 
> petite graine dans le champ perdue.
> 
> &nbsp;
> 
> Mais, au contraire, il a écrit
> 
> <span style="margin-left: 4em; display: inline-block;">&nbsp;</span>des poèmes pleins de sainteté
> 
> croyant qu'ainsi
> 
> <span style="margin-left: 4em; display: inline-block;">&nbsp;</span>son cœur deviendrait
> 
> <span style="margin-left: 4em; display: inline-block;">&nbsp;</span>plus grand. Les jours ont passé
> 
> à un travail qui a ruiné
> 
> <span style="margin-left: 4em; display: inline-block;">&nbsp;</span>la sainteté de son cœur :
> 
> la petite graine n'est pas morte
> 
> et seul il est resté[^348].

Ainsi s'achève la réécriture de l'ancienne jeunesse. Dans les derniers
poèmes, ceux qui concluent l'ouvrage, Pasolini assume désormais une
forme hybride, italo-frioulane :

> ... Je pleure un monde mort.
> 
> Pourtant, moi qui le pleure, je ne suis pas mort.
> 
> Si nous voulons aller de l'avant, il nous faut pleurer
> 
> le temps qui ne reviendra plus et dire non
> 
> &nbsp;
> 
> à cette réalité qui nous a enfermés
> 
> dans sa prison[^349]...

Cette ultime série de poèmes intitulée « Sombre enthousiasme » maintient
vive une tension entre un grand refus, celui du développement
capitaliste, et une utopie postindustrielle, un rêve de récession qui
marquerait le point de départ d'un nouveau monde au cœur de l'ancien :

> De quoi est-il question ? De l'acceptation d'une réalité fatale ? Étant
> donné que les choses sont ce qu'elles sont, le devoir historique doit-il
> consister à les améliorer à travers l'enthousiasme communiste ? Le
> « modèle de développement » est celui qu'a voulu la société capitaliste
> qui est en passe d'atteindre à sa plus grande maturité. Proposer
> d'*autres* modèles de développement revient à *accepter* ce modèle de
> développement là. Cela revient à vouloir l'améliorer, le modifier, le
> corriger. Non : il ne faut pas accepter ce « modèle de développement ».
> Il ne suffit pas non plus de refuser ce « modèle de développement ». *Il
> faut refuser le « développement[^350] »*.

Ce refus du développement en soi fait écho aux pages de *La
Convivialité* dans lesquelles Illich décrit une période de récession
nécessaire et toutes les difficultés qui seront celles, notamment, des
populations vivant au sein des nations capitalistes avancées : grande
frustration ressentie par des individus intoxiqués par la société de
consommation, souffrance inévitable de ces peuples devenus ignorants car
privés de leurs savoir-faire et qui auront donc à reconquérir leur
autonomie dans les domaines du soin, énergétique, alimentaire, éducatif,
des transports, etc. Ces souffrances ont beau être inévitables, nous dit
Illich, considérant la destruction du vivant, notamment humain, qui
s'annonce, si le développement capitaliste demeurait ce qu'il est, elles
devront être considérées comme des souffrances nécessaires. Ce
rapprochement d'Illich et de Pasolini n'est pas fortuit : on sait que
Pasolini a lu Illich --- il fait explicitement référence dans *Pétrole*
au concept d'austérité tel qu'esquissé dans *La Convivialité* ---, et
nous pouvons même supposer qu'en l'état de ses connaissances limitées
dans le champ économique, il s'en remet à l'utopie postindustrielle du
penseur autrichien. Mais Pasolini ne rédige pas un traité d'économie
critique, il s'en remet seulement à l'image poétique et contrastée d'une
récession qui permettrait aux cultures humaines spécifiques de
reconquérir une part d'autonomie :

> Nous verrons des pantalons rapiécés,
> 
> des couchers de soleil écarlates sur des villages
> 
> sans moteurs et pleins de jeunes en haillons
> 
> revenus de Turin ou d'Allemagne[^351].

Deux éléments nous paraissent émerger dans cette image, qui ne peuvent
que révulser la gauche progressiste inféodée corps et âme au
développement industriel et capitaliste :

1) l'image d'une austérité nécessaire, d'un dépouillement, qui
n'en est pas moins la condition nécessaire d'une joie
authentique ;

2) un retour à la ruralité abandonnée, un exil hors des
immenses villes, une reconquête de la lumière naturelle et
des nuits étoilées.

Pasolini et Illich ne peuvent-ils être considérés ici comme des
précurseurs des nouvelles radicalités écologiques prônant notamment le
terme ambigu de « décroissance » ? À vrai dire, nous invitons plutôt à
arracher ce terme à l'économie politique, à lui rendre sa dignité et sa
place au sein des processus biologiques et sociaux. Il ne s'agit pas de
« revenir à la nature », mais de mettre un coup fatal au développement
capitaliste, de renouer avec des développements hétérogènes autonomes
pour ce qui est des besoins les plus élémentaires, de libérer la
créativité humaine en recomposant partout des formes de vie spécifiques :

> Cinq années de « développement » ont fait des Italiens des névrotiques
> idiots, cinq années de misère peuvent leur rendre leur humanité, pour
> misérable qu'elle puisse être. Et alors ils pourront --- du moins les
> communistes --- mettre à profit cette expérience vécue : et puisqu'on
> devra recommencer derechef avec un nouveau développement, ce
> « développement » devra être totalement différent de ce qu'il a été.
> C'est autre chose que de proposer de nouveaux « modèles » au
> « développement » tel que nous le connaissons actuellement[^352] !

Alors pourquoi un tel mépris pour cette jeunesse aux cheveux longs ? Ne
partage-t-il pas bien plus avec cette jeunesse contestataire qu'il ne
veut bien le penser ? C'est ici que se joue, obstinément, le retour à
Marx : l’image de la récession, telle qu’esquissée à la fin de *La Nouvelle jeunesse*, a pour sujet une jeunesse prolétaire et non un groupe de militants petits-bourgeois. Ce n'est peut-être pas tant que les
petits-bourgeois ne devraient pas déserter, bifurquer, s'engager dans le
réinvestissement des cultures locales, rurales, paysannes, mais que
cette action doit être avant tout portée par ceux qui n'ont que leur
force de travail, qui agissent par nécessité et non par choix
idéologique. Ce qui invite donc, plutôt que de rejouer éternellement
l'opposition entre militants ouvriéristes et néoruraux, opposition ayant
une infinité de variations possibles que nous ne prendrons pas la peine
d'esquisser ici, à inscrire cet acte de refus de la civilisation
capitaliste dans la matérialité des rapports de classe. Par ailleurs, si le
développement capitaliste s'est considérablement accru depuis 1974, date
de publication de *La Nouvelle jeunesse*, dans le sillon de *La
Convivialité* en 1973, nous sommes désormais riches de nouvelles
traditions théoriques et pratiques. À nous de nous en saisir à l'aune
d'une période de récession qui a peut-être déjà débuté dans un contexte
de guerre en Europe et de crise énergétique inédite. La question qui
nous importe, et dont nous considérons qu'elle devra être au cœur de la
contre-offensive, est celle de l'accompagnement des plus fragiles au
sein de cet effondrement. Pasolini adopte ici une perspective
transhistorique qui néglige cette question. Mais son « sombre
enthousiasme » est désormais vieux de près de cinquante ans --- tout
comme les avertissements et les éléments programmatiques qui composent
*La Convivialité* d'Illich. Ici, on me reprochera sans doute de tirer
artificiellement Pasolini du côté de l'utopie. J’invite alors à lire ou à relire les derniers poèmes italo-frioulans de *La Nouvelle jeunesse* qui revêtent un caractère utopique indéniable. Et si, pour
beaucoup, il ne peut s'agir d'une utopie souhaitable, en raison de ce
qu'elle implique notamment d'austérité subie, nous proposons de nous
souvenir de ces quelques mots d'Ivan Illich :

> Pourtant le passage du présent état de choses à un mode de production
> convivial menacera beaucoup de gens jusque dans leur possibilité de
> survivre. Dans l'opinion de l'homme industrialisé, les premiers à
> souffrir et à mourir en raison des limites imposées à l'industrie
> seraient les pauvres. Mais déjà la domination de l'homme sur l'outil a
> pris un tour suicidaire. La survie du Bangladesh dépend du blé canadien
> et la santé des New-Yorkais demande la mise à sac des ressources
> planétaires. Le passage à une société conviviale s'accompagnera
> d'extrêmes souffrances : famine chez les uns, panique chez les autres.
> Cette transition, seuls ont le droit de la souhaiter ceux qui savent que
> l'organisation industrielle dominante est en train de produire des
> souffrances encore pires sous prétexte de les soulager[^353].

Si la perspective communiste de Pasolini repose sur l'hypothèse d'une
récession, et donc sur un appauvrissement des masses, elle se justifie
pourtant, comme chez Illich, par l'imminence d'une catastrophe
susceptible d'engendrer l'extinction du vivant. Quant au socialisme,
étatiste et productiviste, il ne peut constituer une alternative
crédible à l'effondrement de la civilisation capitaliste, à moins d'en
constituer l'ultime sursaut, et donc de voir se perpétuer la dynamique
impériale et mortifère de celle-ci. *La Nouvelle jeunesse*, avec
*Trasumanar e organizzar* et *Pétrole*, correspond, du fait précisément
de cette hypothèse stratégique, à la fois pessimiste et utopique, à un
renouvellement incontestable des perspectives politiques de Pasolini,
mais également à une phase absolument neuve de son travail poétique
--- René de Ceccatty ayant tout à fait raison de signaler que cela contredit
radicalement la thèse d'une mort volontaire.

</div>

<div class="text" data-number="19">

## Notes sur *Pétrole*

<div class="epigraphe">

Joignant les cimes l'une à l'autre,\
Ne pas dire un seul chemin de mots.

--- Empédocle

</div>

Repenser la totalité, en un sens lukácsien bouleversé, non plus comme
représentation homogène d'un monde parcouru de contradictions, mais
comme une certaine « organisation du chaos ».

<div class="asterisme">*</div>

Cette conception de la totalité implique un refus conjoint de l'ordre
et du désordre : les deux faces combinées du pouvoir.

<div class="asterisme">*</div>

*Pétrole* vise bien une totalité, mais par assemblage de matériaux
hétérogènes : « L'illusion est celle de connaître et donc d'énoncer le
monde entier[^354]. »

<div class="asterisme">*</div>

Ce travail de montage apparaît comme partie prenante de la création
--- ce qui suppose également : doutes, ratures, réécritures continuelles,
etc.

<div class="asterisme">*</div>

Il s'agit de viser une certaine unité (*l'œuvre*) tout en y intégrant
un caractère d'inachèvement intentionnel (*devenirs possibles de
l'œuvre*).

<div class="asterisme">*</div>

Réintégration de l'arbitraire et du jeu au sein de la composition
littéraire --- mais, comme chez Shakespeare (cf. *Hamlet*, 1603) cette
folie exige une méthode : 1) refus d'un commencement ; 2) fragmentisme
; 3) dialogue avec l'histoire italienne contemporaine ; 4) insertion
de variantes ; 5) trouble concernant la paternité de l'ouvrage ; 6) polymorphie : profusion de genres et de registres.

<div class="asterisme">*</div>

Il ne peut s'agir d'écrire un « roman historique », seulement de
« construire une forme » --- sachant que l'explicitation de la méthode
ne peut être qu'incorporée à la forme elle-même.

<div class="asterisme">*</div>

Ainsi faisant les coulisses de la création sont abolis --- »... je
*vis* la genèse de mon livre[^355]. »

<div class="asterisme">*</div>

Le grand roman historique (Lukács) misait sur la fonction générique de
figures archétypales ; ce qui pouvait induire la disparition de
l'individualité comme pôle de l'être social --- grande contradiction
de l'ère capitaliste : la conjointe promotion et disparition de
l'individualité.

<div class="asterisme">*</div>

Mutation et dédoublement de la figure classique du héros romanesque
(Bel-Ami, Rubempré, Julien Sorel, etc.) : monstruosité *et* pure
innocence (Carlo I et Carlo II).

<div class="asterisme">*</div>

La trame de *Pétrole* est la transformation du sentiment de la réalité
par le néocapitalisme latin --- ce qui implique de la part de
l'artisan-monteur une *adaptation* aux nouvelles formes (mutilées) de
la « vraie » réalité, mais non une *intégration* à la « fausse »
réalité du nouveau pouvoir.

<div class="asterisme">*</div>

Le néocapitalisme comme distorsion du sourire --- *Pétrole* est
l'histoire de cette distorsion : « ... il est atroce de vivre et de
connaître un monde où les yeux ne savent plus accorder un regard, je
ne dis pas d'amour, mais pas même de curiosité ou de
sympathie[^356]. »

<div class="asterisme">*</div>

Distorsion du sourire comme extension analogique, sur le corps des
individus, de la contamination du paysage par les nouveaux déchets
(issus de l'industrie pétrochimique) qui bordent les routes d'Italie.

<div class="asterisme">*</div>

Alexandre en tant que figure archétypale du grand rêve capitaliste :
« Dans les villages se déroulent diverses aventures (à partir de
traces de récits populaires locaux, persans, népalais, indiens) qui
sont *interrompues* par le passage d'Alexandre --- tout comme est
*interrompue* la noble vie des villages et des champs --- vidés et
contaminés par la nouvelle civilisation qui les recouvre d'immondices,
de déchets, d'objets qui ne sont pas naturels, etc[^357]. »

<div class="asterisme">*</div>

La possibilité de l'avenir ne saurait reposer sur un « bond en avant »
--- *avanti popolo !* --- mais sur une stase permettant aux cultures
spécifiques, anciennes et nouvelles, de tisser leurs ramifications et
de croître.

<div class="asterisme">*</div>

La possibilité de l'avenir ne saurait reposer sur une représentation
statique de la « communauté à venir » --- mais sur une imbrication de
la stase aux devenirs potentiellement émancipateurs des flux.

<div class="asterisme">*</div>

*Pétrole* contient une image dialectique de la réalité : celle-ci
comprend à la fois une image du pouvoir et une contre-image utopique
de sa désagrégation.

<div class="asterisme">*</div>

*Oscurità uguale luce*[^358] : ce pour quoi certaines scènes de
*Pétrole* sont intolérables ; ce pour quoi certaines scènes de
*Pétrole* sont aveuglantes de beauté.

<div class="asterisme">*</div>

La lumière suppose nécessairement l'obscurité --- ce qui signifie
également : double fond de la réalité cosmique et double fond du
désir.

<div class="asterisme">*</div>

Organiser le chaos, c'est convoquer conjointement les deux formes sous
lesquelles la réalité se laisse saisir : la *lutte des classes* et le
*langage*.

<div class="asterisme">*</div>

Pensons à la dernière scène de *Salò*. Après de terribles sévices
exercés sur des frères et sœurs de classe, les deux jeunes bourreaux
se mettent à danser et s'enlacent :

> --- Comment s'appelle ta copine ?
> 
> --- Margherita.

<div class="asterisme">*</div>

Si la lumière demeure fonction du récit pasolinien, c'est que la
réalité, négativement ou positivement envisagée, ne peut se révéler
que par la lumière, fard de la marchandise *ou* éclats obstinés de
l'utopie.

<div class="asterisme">*</div>

Cette « féerie dialectique » (cf. Walter Benjamin, *Le Livre des
passages*, 1982.) tisse désormais sa matérialité à travers les
multiples devenirs chimiques de l'or noir.

<div class="asterisme">*</div>

*Pétrole* (latin médiéval *petroleum*, du latin classique *petra*,
pierre, et *oleum*, huile) : huile minérale naturelle, de couleur très
foncée, d'une densité variant de 0,8 à 0,95, composée essentiellement
d'hydrocarbures paraffiniques, naphténiques et aromatiques. (On dit
aussi pétrole brut et, dans les opérations d'exploration et de
production, huile.)

<div class="asterisme">*</div>

L'industrie pétrolière a offert un modèle stratégique au nouveau
pouvoir : neutralisation de la lutte des classes par participation
active des masses à leur propre domination --- dépendance de ces masses
à l'égard du langage technologique.

<div class="asterisme">*</div>

La religion de l'échange a induit logiquement le culte du quantifiable :
« Il cherchait --- mais dans le monde, parmi les corps --- la
solitude la plus absolue[^359]. »

<div class="asterisme">*</div>

L'hyperaccélération des cadences productives et
l'hyperspectacularisation du théâtre politique qui l'accompagne et la
justifie ont aggravé l'état d'impuissance de l'intellectuel de
gauche.

<div class="asterisme">*</div>

Si ce dernier veut pouvoir continuer à servir le
pouvoir, il est dans la nécessité de trahir les valeurs qui fondent
les motifs de son « engagement ».

<div class="asterisme">*</div>

À moins de composer une fausse unité entre ses valeurs et son action, de désintégrer ainsi la possibilité d’une antithèse radicale --- *image d’un sucre d’orge qui tournerait sans discontinuer sur lui-même dans une fabrique de Berlin*.

<div class="asterisme">*</div>

L'une des erreurs majeures de l'intellectuel de gauche (« jeune homme,
pâle, impersonnel et un peu paroissial[^360] ») fut d'idéaliser la
classe ouvrière en tant qu'avant-garde du mouvement révolutionnaire et
figure générique (abstraite donc) du prolétariat : « il *ne voulait
pas savoir*, évidemment, comme tous ses semblables, qu'il existait des
sous-prolétaires[^361] ».

<div class="asterisme">*</div>

Cette ignorance est le fond de toute idéologie nationale --- dont la
*République* est l'avatar le plus invariant : « Même sur le plan
idéologique, du reste, notre Tristram a accommodé son progressisme
antiautoritaire et anticolonialiste dans le cadre plus vaste d'un
eurocentrisme qui constitue son réel, et bien plus profond et
inextirpable, préjugé racial[^362]. »

<div class="asterisme">*</div>

L'intellectuel au service du pouvoir (pléonasme) n'est pas hermétique
ou illisible, bien au contraire, il resplendit de clarté, celle du
pragmatisme économique qui est au fondement de tout fascisme ; son
style est donc purement ornemental, chargé d'injecter des nuances
d'opacité à l'hyperlisibilité de son contenu.

<div class="asterisme">*</div>

À rebours : Pasolini revendique l'illisibilité de *Pétrole* --- c.-à-d. :
son absence de contenu séparé.

<div class="asterisme">*</div>

Recréer une forme qui soit en capacité de ressaisir la parole sans la
trahir --- ce qui implique d'assumer une part d'illisibilité.

<div class="asterisme">*</div>

Cette obsession pour la forme n'est pas un formalisme (ni
réactionnaire ni avant-gardiste), elle est contemporaine d'un sursaut
intime *et* révolutionnaire : c'est le motif d'une réécriture et d'une
recréation de l'histoire, l'amorce d'une *nouvelle jeunesse*.

<div class="asterisme">*</div>

Un déplacement s'opère depuis l'Histoire (intégrant la contre-Histoire
communiste) vers une cosmicisation du communisme : « Il y a un léger
vent chaud qui soulève des tourbillons de poussière lourde et sale
(\[pleine\] toutefois de ces lois, anciennes et oubliées, qui ont
toujours animé, depuis que le monde est monde, l'histoire et le
cosmos[^363]). »

<div class="asterisme">*</div>

C'est le plus souvent à l'intérieur de la séparation de l'Homme et de
la Nature (et donc subjectivement à l'intérieur de l'idéologie
bourgeoise) que se déploie un rapport abrutissant au cosmos : « La
sincérité du rapport au cosmos s'épuise vite : elle tend à se
transformer aussitôt en un acte d'hommage, en un recueillement obligé,
dont l'homme essaie de se libérer, réprimant hypocritement son
impatience, comme un enfant lors de ses cours de
catéchisme[^364]... »

<div class="asterisme">*</div>

Autre rapport au cosmos (tissé sans doute à partir d’une image inaugurale de notre jeunesse) : « Au fond, effleurés par les pieds de
la foule qui marchait le long du grand mur, avec les portes ouvertes
des entrepôts, les marchands étaient debout : trois jeunes, entre
quinze et trente ans, que l'excitation et la nuit passée à la belle
étoile (à dormir par terre, là où ils se trouvaient) avaient réduits
au silence : un silence toutefois extraordinairement
expressif[^365]. »

<div class="asterisme">*</div>

Tout travail rigoureux accompli depuis le point de vue d'une
discipline particulière (sociologie, psychologie, historiographie,
etc.) doit renoncer à la totalisation achevée du monde. Ce quelque
chose qui excède toute totalisation, la littérature s'en préoccupe.
Non comme nouvelle discipline totalisante, mais comme traitement d'un
excédent qui échappe à l'analyse théorique : « De façon analogue, je
pourrais dire que --- là aussi, fût-ce à travers la culture marxiste ---
je connais assez bien la “socialité” d'un individu. Mais, même
dans ce cas, la connaissance de l'esprit humain est quelque chose de
plus que cette connaissance sociale. Qu'est-ce que l'esprit humain ?
C'est une présence ; une réalité ; voilà tout. Il plane à travers
l'individu auquel il appartient, et sur lui, comme un double
monumental et en même temps insaisissable. Cette “figure planante”
(qui d'une façon ou d'une autre, si mystérieuse fût-elle, est aussi
physique) *ne se trouve que là où elle peut se trouver*. Elle a la
propriété des corps[^366]. »

<div class="asterisme">*</div>

Ce quelque chose qui échappe à la psychologie, à l'historiographie, à
la sociologie, etc., n'est donc pas un arrière-fond de signification,
c'est une forme corporelle, vivante, irréductible à tout système
d'interprétation unitaire.

<div class="asterisme">*</div>

À rebours de la méthode sociologique, science positive reposant sur
des déterminations plus ou moins abstraites, *Pétrole* fait valoir une
autre méthode de connaissance du monde social : une *connaissance
négative*.

<div class="asterisme">*</div>

Cette connaissance est négative car elle constitue le corps
inaccaparable de l'être comme son objet --- et cet objet lui échappe
toujours.

<div class="asterisme">*</div>

Toute politique de l'espace vise à circonscrire les corps, à les
empêcher de manifester un excédent de présence et de vitalité, à les
« fonctionnaliser » en vue de servir les rapports de production,
d'échange et de consommation.

<div class="asterisme">*</div>

*Ère du Pétrole* --- une politique de l'espace se caractérisant par
« une forme privée de redondances : \[dont le\] sens est entièrement
défini et consiste essentiellement dans le manque de marges et
d'espaces pour “quelque chose d'autre[^367]” ».

<div class="asterisme">*</div>

Qu'est-ce que ce « quelque chose d'autre » ? « Ce quelque chose
d'autre pourrait être la lutte des classes, ou une agitation
autodestructrice de l'entropie de classe, ou encore le mysticisme
religieux, ou encore la littérature ou l'amour de la culture[^368]. »

<div class="asterisme">*</div>

Rupture avec le néoréalisme --- *les marges sont surnaturelles* --- au
sens où elles échappent à la naturalisation des tendances sociales du
néocapitalisme : « Restez dans le mirobolant sous la lumière des Nuées
Pourpres[^369]. »

<div class="asterisme">*</div>

Des marges qui n'en demeurent pas moins une matière à laquelle se soumet
obstinément l'auteur : « ... la fonction de mon récit est autre, et
exige de rester dans la matière[^370]. »

<div class="asterisme">*</div>

Au traitement des marges fait écho, sur un mode antagonique, le
traitement du pouvoir : « Ceux qui --- comme dans mon cas --- haïssent
le pouvoir, à un moment ou l'autre de leur vie, à un moment augural,
l'ont aimé, parce que c'est naturel et parce que c'est ce qui provoque
ensuite une haine justifiée et pas seulement pratiquement
“religieuse[^371]”. »

<div class="asterisme">*</div>

Cette ambivalence assumée prémunit Pasolini d'un rejet unilatéral et factice
du pouvoir : « Et même, il est difficile de penser comment il a pu
venir à l'esprit de quelqu'un de faire le choix contraire :
c'est-à-dire de renoncer à suivre le cours de la vie qui, s'il le met
au service du pouvoir, avec sa juvénile \[hardiesse\] lui assure pour
sa vieillesse et son grand âge puissance et prestige ; et de choisir,
en revanche, une vie de victime, exclue du grand festin paternel du
pouvoir : de la répétition glorieuse de la vie comme Passé \[qui se
perpétue[^372]\]. »

<div class="asterisme">*</div>

Toutes les hagiographies font état d'une jeunesse souillée par le vice
--- l'innocence originaire est donc, pour ainsi dire, *toujours déjà
perdue* --- ce qui n'abolit pas la possibilité d'une *seconde
innocence*.

<div class="asterisme">*</div>

Cette *nouvelle innocence*, telle que la conçoit Pasolini, vise une
libération du vécu sensible --- c'est un « profond appel que je n'ose
pas même nommer[^373] ».

<div class="asterisme">*</div>

Elle implique une suspension du jugement : « L'épochè, peut-être,
l'épochè s'accomplissait[^374]. »

<div class="asterisme">*</div>

Mais le « Modèle » bourgeois disqualifie toujours une telle entreprise :
« Un bourgeois ne sait pas réellement apprécier une innocence qui ne
soit pas celle qu'enseignent les livres de classe et dans le code non
écrit de la société. Il éprouve même, pour l'innocence, une certaine
répugnance de caractère raciste. Les devoirs, il les a inventés pour
pouvoir condamner l'innocence qu'il ne connaît pas. D'autant plus que
souvent l'innocence est liée à la délinquance : et si, donc, elle
échappe à la culture, elle échappe aussi à la loi. Le bourgeois
éprouve toujours devant l'innocence un sentiment d'épouvante et, dans
le meilleur des cas, la juge comme un produit inférieur de sa manière
de vivre, ne sachant ou ne voulant pas imaginer les termes de l'autre
façon de vivre, à laquelle il appartient. Ainsi, l'annexant à sa
propre histoire, il peut s'en libérer, s'en laver les mains, lui
échapper, \[rompre les ponts\] avec des lieux où elle est
reléguée[^375]. »

<div class="asterisme">*</div>

Au grand damne du poète, ce Modèle bourgeois n'est plus l'apanage des seuls bourgeois : il s'agit désormais d'un Modèle hégémonique.

<div class="asterisme">*</div>

Ce qui n'a aucune raison de nous surprendre : « Les pensées de la classe
dominante sont aussi, à toutes les époques, les pensées dominantes,
autrement dit la classe qui est la puissance *matérielle* dominante de
la société est aussi la puissance dominante *spirituelle*[^376]. »

<div class="asterisme">*</div>

Pour autant, le règne du nouveau pouvoir, et c'est ce qui le distingue de l'ancien, celui de la
vieille bourgeoisie traditionnelle, se caractérise par une capacité
exceptionnelle d'intégration, corps et âme, et donc d'anéantissement
de toute poétique prolétarienne : « Une certaine égalité acquise (dont
ils ne savent pas qu'elle est parfaitement illusoire) et la dignité
humaine abjurée en faveur d'une normalité qui parle parfaitement la
“prose” des bourgeois, ils sont devenus totalement, inéluctablement
dépourvus de poésie[^377]. »

<div class="asterisme">*</div>

Marx lui-même ne pouvait aucunement le prédire : rien ne laissait
supposer, en son temps, une adhésion aussi massive des individus et
des groupes sociaux à leur propre extinction.

<div class="asterisme">*</div>

Hypothèse tragique : rien n'aurait été possible sans l'existence de la
gauche qui a prêché, avec son misérabilisme et sa morale, au nom de
l'Éducation, du Transport et du Soin, une intégration totalitaire au
développement capitaliste.

<div class="asterisme">*</div>

En captant les éléments du dissensus populaire, et en les intégrant à
des représentations consensuelles, électorales, mensongères, elle
transformait la radicalité en une posture inoffensive.

<div class="asterisme">*</div>

Toutes les cultures spécifiques, multiséculaires, quelquefois
millénaires, sont ainsi devenues une seule et même anomalie : la
*pauvreté*.

<div class="asterisme">*</div>

Aussi *pauvres* ils le furent deux fois : d'un point de vue culturel
*et* d'un point de vue économique.

<div class="asterisme">*</div>

Lorsque la gauche vénézuélienne décida de lutter contre la vétusté du
bâti, elle interdit la possibilité pour les *pauvres* de bâtir leur
maison eux-mêmes ; on troqua donc, contre une standardisation de
l'habitat et un petit gain de bien-être, un savoir millénaire.

<div class="asterisme">*</div>

Les *pauvres* ont alors perdu ce qui constitue la seule et véritable
liberté : celle d'être *chez soi dans le monde* (cf. Hegel, *La Raison
dans l'histoire*, 1837).

<div class="asterisme">*</div>

Ce monopole radical de l'habitat se conjugue aux divers monopoles du
Capital et de l'État sur nos vies : on a désappris à soigner une
entorse et on ne sait plus réparer un vélo.

<div class="asterisme">*</div>

Cet aspect du développement capitaliste, loin des paillettes de la
mode et de l'industrie des biens non nécessaires, est le pire symptôme
du capitalisme intégral : c'est à la gauche universaliste que revient
la responsabilité de cette perte absolue d'autonomie devant la vie
--- comme devant la mort.

<div class="asterisme">*</div>

Cette hétéronomie massive est responsable de désordres instinctuels
d'une ampleur inédite : charge au *capitalisme thérapeutique* de les
résoudre.

<div class="asterisme">*</div>

Mais aucune thérapie et aucun remède marchand ne pourra contrecarrer
la disparition du sens de la réalité.

<div class="asterisme">*</div>

Lire *Pétrole* au XXI^e^ siècle, c'est ressaisir notre destin sous les
prismes de la *disparition du paysage* et de la *distorsion du
sourire*.

<div class="asterisme">*</div>

Il y a la mer d'un bleu sévère et profond qui pèse sur la gravitation
de ces légers nuages blancs et gris.

<div class="asterisme">*</div>

Il y a le sourire édenté de Bartolo, le sourd éclat de ses
exclamations dialectales, son jeune chien noir et fougueux, sa petite
femme qui les rappelle tous deux fermement à l'ordre.

<div class="asterisme">*</div>

Ces impressions visuelles ne peuvent imprimer la conscience du poète
si celui-ci fait abstraction du sol depuis lequel elles sont produites :
l'*épochè* suspend le jugement idéologique, mais non la
détermination historique de l'être social.

<div class="asterisme">*</div>

Et pourtant, le rapport entre le vécu et l'histoire s'est désagrégé :
« Non, non, je le répète, l'historique ne peut jamais coïncider avec
le vécu, à moins que *nous ne voulions nous mentir à
nous-mêmes*[^378]. »

<div class="asterisme">*</div>

Ou encore : « Si l'historique ne coïncidait pas avec le vécu, sinon
par hypocrisie, voici que le vécu, autoritairement, voulait
s'instituer comme historique[^378bis]. »

<div class="asterisme">*</div>

Le *mysticisme du réel* est l'autre nom de cette dialectique qui
excède le mouvement transcendantal de la logique et se refuse à
neutraliser la réalité dans une synthèse facile : « Je ne voulais pas
de la contradiction commodément dépassée par une synthèse, et la
poursuite évidente de l'histoire, fût-ce “en brochette” à côté de
son déroulement linéaire[^379]. »

<div class="asterisme">*</div>

Ne pas choisir Nietzsche contre Hegel --- c'est dans leur tension que
se déploie un rapport nouveau entre forme et contenu : « La
contradiction n'est qu'intermittence de coexistence. Hegel
naturellement s'est, fût-ce divinement, trompé. Le seul infini
véritable est celui qu'il appelle “mauvais infini” (il le savait
donc !). Par conséquent, les deux termes de la contradiction ne se
dépassent pas du tout, mais procèdent dans l'infini en échangeant leur
droit d'exister, à une rapidité qui, pour être surnaturelle, n'empêche
pas que ces deux termes coexistants ne soient pris en considération
alternativement et donc soient isolés, analysés rien qu'en eux-mêmes.
Justement : notre histoire isole et analyse *en soi* le moment de
l'autonomie de la forme[^380]. »

<div class="asterisme">*</div>

Sans Hegel, cette forme serait anhistorique et se déploierait dans
l'universel vide de la théorie kantienne ; sans Nietzsche, cette forme
serait asservie à l'Histoire et négligerait donc le vécu.

<div class="asterisme">*</div>

Penser le renouveau de l'histoire à partir du vécu de l'expérience
--- ne plus partir de la fin (eschatologie), ne plus demeurer dans
l'éternel présent de la joute (éternel retour), mais éprouver depuis
la disparition l'imminence de notre révolte : « Bientôt, la terre
disparaît, et ne reste plus que le vide profond, sans vie. Je l'ai
rêvé, ça aussi, je l'ai parfaitement vécu en rêve[^381]. »

<div class="asterisme">*</div>

Cette « nouvelle origine » doit nous conduire à une réélaboration des conditions de notre vécu et de la conscience que nous en avons : « Dans mon rêve
révélateur, j'ai compris, une fois pour toutes, que ce n'est pas la
mer qui est notre véritable origine, à savoir le ventre maternel
\[originaire\] (auquel, de toutes nos forces nous tendons à revenir) :
notre véritable origine, c'est l'espace. C'est là que nous sommes
vraiment nés : dans la sphère du cosmos. Dans la mer, nous sommes
peut-être nés une deuxième fois. Et donc l'attraction de la mer est
profonde, mais celle de l'espace céleste l'est infiniment
davantage[^382]. »

<div class="asterisme">*</div>

Jusqu'au bout --- c.-à-d. : jusqu'à la disparition de la disparition ---
demeure la trace de ce qui fut/sera : « ... c'était quelque
chose qui ne pouvait pas ne pas donner un \[coup\] au cœur et faire
venir d'irrépressibles larmes aux yeux : il s'agissait d'un petit
champ de maïs, avec au milieu des rangées de vignes, où clairement on
sentait la... d'une main humaine[^383]. »

<div class="asterisme">*</div>

Ou encore : « Terre anonyme, avec de la mauvaise herbe, des pauvres
arbres pelés incapables d'être tragiques dans leur misère[^384]... »

<div class="asterisme">*</div>

Le *communisme déjà existant* doit trouver sa propre voie du cosmos.

<div class="asterisme">*</div>

Celle-ci ne saurait se déployer par-delà l'histoire humaine, mais doit
la ressaisir en son sein, en lui reconnaissant une place, certes
acentrique, mais nécessaire car bien réelle.

<div class="asterisme">*</div>

Et c'est sans doute ainsi que notre communisme sera en
mesure d'éclairer des voies nouvelles. Des voies qui existent déjà. Ce
qui caractérisera leur nouveauté ne sera que l'usage qui en sera fait :
un chemin en mesure d'articuler la radicalité révolutionnaire et la
vie réellement vécue --- la connaissance des causes qui nous
déterminent et la pure joie d'exister.

<div class="asterisme">*</div>

Contre la dérision nihiliste qui demeure ancrée dans les valeurs
bourgeoises, proposer le rire comme paradigme du rapport de l'Homme au
cosmos : « D'ailleurs, chacun sait que le “rire” a une fonction de
résolution par rapport aux crises cosmiques[^385]... »

<div class="asterisme">*</div>

Un rire qui sonne le glas du symbolisme (« J'AI ÉRIGÉ CETTE STATUE
POUR RIRE[^386] ») --- éternelle mort devrait-on dire --- c.-à-d. :
sans cesse recommencée --- le rire cosmique de Rimbaud contre la
dérision platement intellectuelle de Cioran.

<div class="asterisme">*</div>

Un rire qui déploie une métalinguistique : il s'agit de devancer
l'ordre des significations en faisant valoir la polysémie farouche de
la forme.

<div class="asterisme">*</div>

« Rappelez-vous ce que vous dit un poète du fond de sa tombe (près
d'un puits de pétrole) : vous n'êtes pas “en retard”, vous n'êtes
pas “postérieurs” : vous êtes les “derniers[^387]” » --- ce qui
peut être lu comme une sentence, l'imminence de notre extinction, ou
comme un motif d'espérance nouvelle.

<div class="asterisme">*</div>

C'est un appel au vide : non l'envers de la philosophie du sujet
(néantisation du sujet par tout ce qui lui échappe), mais l'espace où
se forme et se déforme le monde (sujet compris).

<div class="asterisme">*</div>

Autrement dit : ce vide n'est plus celui du sens (de l'histoire ou de
l'existence), mais celui que présuppose la gravité des corps en
mouvement dans l'espace --- il s'agit donc d'un vide éminemment
créateur.

<div class="asterisme">*</div>

Pasolini s'est jeté dans le volcan de l'Histoire. Mais la cavité
magmatique n'a pas recraché ses sandales, seulement la nécessité d'une
forme neuve, arrachée aux philosophies de l'histoire et réinstituant
le vécu comme seule réalité matérielle et symbolique sur laquelle il
serait envisageable de fonder notre communisme.

<div class="asterisme">*</div>

Le *communisme déjà existant*, tel que nous entendons le penser et lui
donner corps, se distingue du communisme des *lendemains qui chantent*
dans la mesure où il fonde sa vérité sur la réalité du vécu, et non
sur l'espérance d'une fin abstraite.

<div class="asterisme">*</div>

L'opposition ci-dessus doit cependant être nuancée : si nous arrivons
toujours trop tard quelque part, c'est bien que le motif qui nous meut
n'a pas de rapport véritable avec la destination ; celle-ci n'est donc
que le motif fictionnel qui nous engage sur une voie déterminée. En
d'autres termes, il n'existe pas d'identité de fait entre cette
destination imaginaire et la destination réelle, une correspondance
peut-être, plus ou moins vague, plus ou moins fondée, mais qui
justifie d'avoir vécu et agi (et ce malgré la faiblesse des motifs qui
ont pu conduire la vie et l'œuvre).

<div class="asterisme">*</div>

Dans le *Porno-Théo-Kolossal*, nous suivons l'épopée burlesque d'un
roi mage et de son serviteur qui reçoivent l'annonce de la naissance
du Messie et partent afin de le célébrer. Cependant ils arrivent trop
tard : non seulement celui-ci est né et a vécu, ayant accompli sa
destinée de Messie, mais il est mort. Faut-il pour autant
regretter d'avoir marché jusqu'à lui ? Autrement dit : si l'idéologie
nous a engagés à découvrir un lieu qui n'existe pas, faut-il pour
autant regretter le chemin bien réel qui nous y a menés ?

<div class="asterisme">*</div>

La Vérité perd sa justification devant les exigences de la vie. Et,
pour autant, de façon tout à fait dialectique, au-delà de Nietzsche
donc, rien ne manifeste plus la force de vie que de choisir de mourir
pour *ne pas disparaître* en tant que poète.

<div class="asterisme">*</div>

Cette imbrication de la réalité et de l'idéologie a bien des destins
multiples, ancrés dans des situations déterminées, et ne peut conduire
au primat définitif de l'une sur l'autre, comme chez Socrate, comme
chez Nietzsche[^388], mais à sa mise en tension perpétuelle dans un
corps sommé de donner à sa vie une signification historique.

<div class="asterisme">*</div>

*Historique* --- moins dans le sens de Hegel que dans le sens de
Nietzsche qui reprend et critique Hegel : « Ce qui nous sépare aussi
bien de Kant que de Platon : nous croyons uniquement au devenir, y
compris pour ce qui relève de l'esprit, nous sommes *historiques* de
part en part[^389]. »

<div class="asterisme">*</div>

Ce qui distingue Pasolini de la tradition existentialiste athée (celle
de Sartre par exemple) : la contingence doit fonder sa propre
nécessité, même factice, au nom d'une foi invincible dans la lumière
du monde, passée, présente et future, qui engage *entièrement* notre
vie.

<div class="asterisme">*</div>

Ce qui peut impliquer de suivre *une comète qui ne va nulle part*.

<div class="asterisme">*</div>

En assumant de suivre une telle comète, l'humain peut ainsi s'arracher
à l'égotisme et au cynisme petit-bourgeois qui caractérisent la fin des
idéologies. Prendre le risque d'agir, de se tromper, de vivre. Voici
la dernière leçon de Pasolini.

<div class="asterisme">*</div>

Et c'est ainsi que reviennent des chants révolutionnaires, des chants
qui s'épaississent jusqu'à étouffer tous les murmures de la ville
--- est-ce la renaissance d'une comète capable de faire trembler
l'histoire ? Nouvelle illusion, donc ? Qu'importe : « ... c'était donc
une illusion qui a guidé \[Epifanio\] à travers le monde --- mais c'est
cette illusion qui lui a fait connaître la réalité du
monde[^390]... »

<div class="asterisme">*</div>

Cette conception de l'histoire tient sans doute plus à Nietzsche qu'à
Hegel, mais à un Nietzsche réformé par Marx, un Nietzsche qui
assumerait la validité de l'hypothèse révolutionnaire.

<div class="asterisme">*</div>

Ainsi l'éternel retour n'est plus la farce redoutée de l'hégélien
(trahi), mais un rapport éthique à l'histoire, l'exigence de nous en
tenir à l'existant (en tant que contre-ordre), le refus de le
sacrifier au nom d'une promesse de monde meilleur.

<div class="asterisme">*</div>

Et bien que nous soyons incapables de renoncer à un désir de
révolution.

<div class="asterisme">*</div>

Mais il s'agit alors de lui trouver une forme qui soit la plus
concrète possible --- c.-à-d. : qui se fonde sur le vécu de
l'expérience collective et non sur la représentation homogène d'une
communauté pacifiée (la République, la Gauche, la Classe ouvrière,
etc.).

<div class="asterisme">*</div>

Contre toute attente, un poète-philosophe comme Pasolini contribua
également, dans le sillon d'Adorno et d'autres, à donner à la
négativité destituante toutes ses lettres de noblesse. Mais il ne
s'agit pas d'une négativité nihiliste : cette négativité couve
toujours une positivité préfigurative (*ethos* sous-prolétaire,
cultures vernaculaires, etc.).

<div class="asterisme">*</div>

*Ce par quoi le scandale arriva*. La prise en charge centraliste et
autoritaire de « la » positivité révolutionnaire fut défaite dans les
années 68 (France, Italie, etc.) --- réactualisation, dans la sphère
productive et au-delà, du mythe sorélien de la Grève générale : « Vous
savez, aussi bien que moi, que ce qu'il y a de meilleur dans la
conscience moderne est le tourment de l'infini[^391]. »

<div class="asterisme">*</div>

Selon Sorel, la Grève générale est le moyen par lequel le prolétariat
se ressaisit lui-même hors du temps hyperspatialisé du Capital (Marx +
Bergson) ; elle trace donc un chemin vers une positivité créatrice de
possibles.

<div class="asterisme">*</div>

Mais dans quelle mesure ce mythe de la Grève générale est-il encore
scandaleux[^392] ?

<div class="asterisme">*</div>

À rebours d'un autre mythe, celui du Progrès, et de tout ce qu'il
charrie d'adhésions implicites ou explicites au développement
capitaliste, il a en tout cas le mérite d'ouvrir le passé, le présent
et l'avenir sur une infinité de possibles --- sans pour autant
s'acharner tel un vautour sur le corps « inaccaparable » de la
réalité.

<div class="asterisme">*</div>

En soustrayant ainsi notre désir aux ombres théologiques du Progrès,
que restera-t-il du Voyage, de la Littérature, de l'Amour ? « Tout ce
décor --- où il n'y avait pas de nuances, sinon peut-être aux bords du
lambeau phosphorescent de ciel éclairé par la lune --- était rempli par
une odeur unique, profonde, celle du fenouil sauvage. Tout le cosmos
était là, dans ce terrain vague, dans ce ciel, dans ces horizons
urbains à peine visibles et dans cette enivrante odeur d'herbe
estivale[^393]. »

</div>


[^1]: Marcel Proust, *À l'ombre des jeunes filles en fleurs*, Gallimard,
    Folio, 2019, p. 582.

[^2]: À rebours de Proust, donc : « Les belles choses que nous écrirons
    si nous avons du talent sont en nous, indistinctes, comme le
    souvenir d'un air, qui nous charme sans que nous puissions en
    retrouver le contour, le fredonner, ni même en donner un dessin
    quantitatif, dire s'il y a des pauses, des suites de notes rapides.
    Ceux qui sont hantés de ce souvenir confus des vérités qu'ils n'ont
    jamais connues sont les hommes qui sont doués. » (Marcel Proust,
    *Contre Sainte-Beuve*, Gallimard, Folio, 1987, p. 307.)

[^3]: Cf. Georg Lukács, *Histoire et conscience de classe*, traduction
    de Kostas Axelos et Jacqueline Bois, Les Éditions de Minuit, 1960.

[^4]: Pierre Bourdieu, « Vous avez dit “populaire” ? » (1983), in
    *Qu'est-ce qu'un peuple ?*, La Fabrique, 2013, p. 46.

[^5]: Cf. Vladimir Maïakovski, « Comment faire les vers » (1926), in
    *Vers et proses*, traduction d'Elsa Triolet, Le Temps des Cerises,
    2014.

[^6]: Pier Paolo Pasolini, *L'Inédit de New York*, traduction d'Anne
    Bourguignon, Arléa, 2015, p. 49.

[^7]: Jean-Paul Sartre, *Critique de la raison dialectique*, I,
    Gallimard, Philosophie, 1985, p. 14.

[^8]: Karl Marx, *La Guerre civile en France*, traduction de Paul Meier,
    Éditions Sociales, 1953, p. 53.

[^9]: Pier Paolo Pasolini, *L'Inédit de New York*, *op. cit.*, p. 53.

[^10]: Pier Paolo Pasolini, « La Religion de notre temps » (1961), in
    *Une vitalité désespérée. Anthologie personnelle. 1953-1964*,
    traduction de José Guidi, Gallimard, Poésie, 2020, p. 99.

[^11]: *Ibid.*

[^12]: Nous savons aujourd'hui que la mère de Pasolini a perdu un enfant
    avant la naissance de Pier Paolo.

[^13]: Pier Paolo Pasolini, *L'Inédit de New York*, *op. cit.*, p. 54.

[^14]: Lénine, cité par Nina Gourfinkel, in *Lénine*, Agone, 2024, p.
    18.

[^15]: Selon Lénine, la volonté révolutionnaire doit demeurer la volonté
    du peuple incarnée *par* une avant-garde *dans* un parti qui
    organise et centralise l'action révolutionnaire *sans se couper*
    pour autant *des masses* ; celle-ci s'oppose donc à la fausse
    « volonté du peuple » d'un groupuscule de gauchistes (comme celui
    auquel participa son frère). En 1904, Rosa Luxembourg conteste
    pourtant l'ultracentralisme de la direction révolutionnaire russe et
    sa justification par Lénine : « Mais voici que le *moi* du
    révolutionnaire russe se hâte de pirouetter sur sa tête et, une fois
    de plus, se proclame dirigeant tout-puissant de l'histoire, cette
    fois-ci en la personne de son altesse le Comité central du mouvement
    ouvrier social-démocrate. L'habile acrobate ne s'aperçoit même pas
    que le seul “sujet” auquel incombe aujourd'hui le rôle du
    dirigeant, est le seul “moi” collectif de la classe ouvrière, qui
    se réclame résolument le droit de faire elle-même des fautes et
    d'apprendre elle-même la dialectique de l'histoire. Et enfin,
    disons-le sans détours : les erreurs commises par un mouvement
    ouvrier vraiment révolutionnaire sont infiniment plus fécondes et
    plus précieuses que l'infaillibilité du meilleur “Comité central” ».
    (Rosa Luxembourg, « Centralisme et démocratie » [1904], in *Marxisme
    et dictature*, Éditions du Sandre, 2012, p. 33.)

[^16]: Lénine, *La Maladie infantile du communisme. Le « gauchisme »*
    (1920), en libre accès sur *Marxists.org*.

[^17]: Notamment dans ses articles publiés entre 1968 et 1975 et
    compilés dans *Le Chaos*, les *Écrits corsaires* et les *Lettres
    luthériennes*.

[^18]: Cette formulation par Lénine de l'inefficience et même de la
    contreproductivité du gauchisme tend à édifier une opposition entre
    deux figures de révolutionnaire : le professionnel et l'amateur, le
    compétent et l'incompétent, le sérieux et le frivole. « Il est
    beaucoup plus difficile, écrit Lénine, --- et beaucoup plus précieux ---
    de se montrer révolutionnaire quand la situation ne permet *pas
    encore* la lutte directe, déclarée, véritablement massive,
    véritablement révolutionnaire, de savoir défendre les intérêts de la
    révolution (par la propagande, par l'agitation, par
    l'organisation) dans des institutions non révolutionnaires, voire
    nettement réactionnaires, dans une ambiance non révolutionnaire,
    parmi des masses incapables de comprendre tout de suite la nécessité
    d'une méthode d'action révolutionnaire. Savoir trouver,
    pressentir, déterminer exactement la voie concrète ou le tour
    spécial des événements, qui *conduira* les masses vers la grande
    lutte révolutionnaire véritable, décisive et finale : tel est le
    principal objet du communisme actuel en Europe occidentale et en
    Amérique. » (Lénine, *La Maladie infantile du communisme. Le
    « gauchisme »* [1920], *op. cit.*)

[^19]: « ... tout sera bien clair lorsque j'aurai précisé que par le mot
    bourgeoisie, j'entends moins désigner une classe sociale qu'une
    véritable maladie. » (Pier Paolo Pasolini, *Le Chaos*, traduction de
    Philippe Di Meo, R&N Éditions, 2018, p. 18.)

[^20]: Pier Paolo Pasolini, « La Religion de notre temps » (1961), in
    *Une vitalité désespérée...*, *op. cit.*, p. 101.

[^21]: *Ibid.*

[^22]: *Ibid.*

[^23]: « ... quelque chose qui a modifié ma conscience et ma
    personne... » (Pier Paolo Pasolini, *L'Inédit de New York*, *op.
    cit.*, p. 54.) ; « une conversion » (*Ibid.*) ; « ce qui pour un
    jeune d'aujourd'hui serait un engagement à différents mouvements de
    protestation et de contestation, une révolte contre sa propre
    famille, impliquant parfois le rejet de cette famille... » (*Ibid.*)

[^24]: *Ibid.*, p. 55.

[^25]: « Nous sommes quelques-uns à cette époque à avoir voulu attenter
    aux choses, créer en nous des espaces à la vie, des espaces qui
    n'étaient pas et ne semblaient pas devoir trouver place dans
    l'espace. » (Antonin Artaud, *Le Pèse-Nerfs*, Abrüpt, 2020, p. 9.)

[^26]: « J'appelle partage du sensible ce système d'évidences
    sensibles qui donne à voir en même temps l'existence d'un commun
    et les découpages qui y définissent les places et les parts
    respectives. Un partage du sensible fixe donc en même temps un
    commun partagé et des parts exclusives. Cette répartition des parts
    et des places se fonde sur un partage des espaces, des temps et des
    formes d'activité qui détermine la manière même dont un commun se
    prête à participation et dont les uns et les autres ont part à ce
    partage. » (Jacques Rancière, *Le Partage du sensible*, La Fabrique,
    2000, p. 12.)

[^27]: Michel Leiris, « Communication au congrès culturel de La Havane »
    (1968), in *Cinq études d'ethnologie*, Gallimard, Tel, 1988, p.
    148-149.

[^28]: Karl Marx, *Le Capital*, traduction de Joseph Roy, Flammarion,
    Champs, 2008, p. 204.

[^29]: Michel Leiris, « Communication au congrès culturel de La Havane »
    (1968), in *Cinq études d'ethnologie*, *op. cit.*, p. 149-150.

[^30]: *Ibid.*, p. 151.

[^31]: Karl Marx et Friedrich Engels, *L'Idéologie allemande*,
    traduction de Henri Auger, Gilbert Badia, Jean Baudrillard et Renée
    Cartelle, Éditions Sociales, 2012, p. 49.

[^32]: En d'autres termes, elles reposent sur des conditions objectives
    et des faits matériels : propriété des grands moyens de production,
    exploitation capitaliste d'une classe par une autre, extractivisme
    effréné, etc.

[^33]: Karl Marx, *Les Manuscrits de 1844. Économie politique et
    philosophie, *traduction d'Émile Bottigelli, Éditions Sociales,
    1962, p. 87.

[^34]: Le « sentiment de la nature » en tant qu'expression contemporaine
    de l'ère industrielle et de ses ravages : « La nature est une
    invention des temps modernes. Pour l'Indien de la forêt amazonienne
    ou, plus près de nous, pour le paysan français de la III^e^ République,
    ce mot n'a pas de sens. Parce que l'un et l'autre
    restent engagés dans le cosmos. À l'origine l'homme ne se distingue
    pas de la nature ; il est partie d'un univers sans fissure où
    l'ordre des choses continue celui de son esprit : le même souffle
    animait les individus, les sociétés, les rocs et les fontaines.
    Quand la brise effleurait la cime des chênes de Dodone, la forêt
    retentissait d'innombrables paroles. Pour le païen primitif il n'y avait pas de nature, il n'y avait que des dieux, bénéfiques ou
    terribles, dont les forces, aussi bien que les mystères, dépassaient
    la faiblesse humaine d'infiniment haut. » (Bernard Charbonneau, *Le
    Jardin de Babylone*, Éditions de l'Encyclopédie des Nuisances,
    2002, p. 17.). De façon analogue, le culte de la tradition, en un
sens folklorique et donc pétrifié, doit être dépouillé de ses
    fantasmes ; il ne peut être question de faire prospérer
    l'économie-monde néocapitaliste sur le dos de la Nature ou de la
    Tradition.

[^35]: Friedrich Hölderlin, « La Mort d'Empédocle », traduction de Robert Rovini, in *Œuvres*, sous
la direction de Philippe Jaccottet, Gallimard, Pléiade, 1967, p.
    522-523.

[^36]: Le fait que cette unité soit caractérisée ici de « dialectique »
    nous permet d'éviter toute pétrification identitaire de l'être, d'en
    appeler à ses métamorphoses, à la multiplicité de ses devenirs.

[^37]: Friedrich Nietzsche, *Ainsi parlait Zarathoustra* (1883-1885),
    traduction de Henri Albert revue par Jean Lacoste
    et Jacques Le Rider, in *Œuvres*, II, sous la direction de Jean
    Lacoste et Jacques Le Rider, Robert Laffont, 1993, p. 303.

[^38]: Benjamin Fouché, « Petit chaperon loup », *Revue Pøst*, en libre
    accès sur le site de la revue.

[^39]: « ... j'entends, oui, par Mercure, consacrer la portion
    d'errance qui m'est encore impartie à recueillir dans un *ABC de la
    barbarie* (ou *Bréviaire des bruits*) quelques assourdissantes
    rumeurs de notre époque. » Jacques-Henri Michot, *Un ABC de la
    barbarie*, Al Dante, 2022, p. 11.)

[^40]: Nathalie Quintane, *Chaussure*, P.O.L, 1997, quatrième de
    couverture.

[^41]: Jean-Marie Gleize, *Littéralité*, Questions théoriques, 2015, p.
    50-51.

[^42]: Jean-Marie Gleize, *Le Livre des cabanes*, Seuil, Fiction & Cie,
    2015, p. 71.

[^43]: Pier Paolo Pasolini, *L'Inédit de New York*, *op. cit.*, p. 50.

[^44]: « Là où elle est arrivée au pouvoir, la bourgeoisie a détruit
    tous les rapports féodaux, patriarcaux, idylliques. Elle a
    impitoyablement déchiré la variété bariolée des liens féodaux qui
    unissaient l'homme à ses supérieurs naturels et n'a laissé
    subsister d'autre lien entre l'homme et l'homme que l'intérêt
    tout nu, le dur “paiement comptant”. Elle a noyé dans les eaux
    glacées du calcul égoïste les frissons sacrés de l'exaltation
    religieuse, de l'enthousiasme chevaleresque, de la mélancolie
    sentimentale des petits-bourgeois. Elle a dissous la dignité
    personnelle dans la valeur d'échange et substitué aux innombrables
    libertés reconnues par lettres patentes et chèrement acquises la
    *seule* liberté sans scrupule du commerce. En un mot, elle a
    substitué à l'exploitation que voilaient les illusions religieuses
    et politiques l'exploitation ouverte, cynique, directe et toute
    crue. » (Karl Marx et Friedrich Engels, *Manifeste du parti
    communiste*, traduction d'Émile Bottigelli, Flammarion, GF, 1998, p.
    76.)

[^45]: « Et c'est de là que naissent la plupart des controverses : les
    hommes n'expliquent pas rigoureusement ce qu'ils ont dans l'esprit,
    ou ils interprètent mal la pensée des autres. En réalité, dans le
temps même où ils se combattent le plus, ou ils pensent en fait la
    même chose ou ils pensent à des choses différentes, de sorte que ce
    que l'on croit être erreur ou absurdité chez l'autre n'est en
réalité ni faux ni absurde. » (Baruch Spinoza, *Éthique*, traduction
    de Robert Misrahi, Le Livre de poche, 2011, p. 185.)

[^46]: Rainer Maria Rilke, « Lettre à Witold von Hulewicz » (1925), in
    *Élégies de Duino*, traduction de Rainer Biemel, Allia, 2015, p.
    69-70.

[^47]: Ce à quoi nous pourrions ajouter un troisième terme,
    *Veräusserung*, qui appartient au vocabulaire juridique : « aliéner
    un bien ».

[^48]: « *Voilà ce qu'il en est de l'esthétisation du politique à
    laquelle travaille le fascisme. Le communisme lui répond par la
    politisation de l'art.* » (Walter Benjamin, « L'Œuvre d'art à
    l'époque de sa reproductibilité technique » [1939], traduction de
    Frédéric Joly, in *Œuvres*, Éditions Payot & Rivages, Rivages poche,
    2022, p. 172.)

[^49]: Cf. Theodor W. Adorno, *Notes sur Beckett*, traduction de
    Christophe David, Éditions NOUS, 2008 ; Theodor W. Adorno/Paul
    Celan, *Correspondance*, traduction de Christophe David, Éditions
    NOUS, 2008.

[^50]: « Nulle part ni jamais la forme n'est résultat acquis,
    parachèvement, conclusion. Il faut l'envisager comme genèse, comme
    mouvement. Son être est le devenir et la forme comme apparence n'est
    qu'une maligne *apparition*, un dangereux fantôme. » (Paul Klee,
    *Théorie de l'art moderne*, traduction de Pierre-Henri Gonthier,
    Gallimard, Folio, 1998, p. 60.)

[^51]: Rainer Maria Rilke, « Lettre à Witold von Hulewicz » (1925), in
    *Élégies de Duino*, *op. cit.*, p. 66.

[^52]: Pier Paolo Pasolini, *La Nouvelle jeunesse*, traduction de
    Philippe Di Meo, Gallimard, 2003, p. 15.

[^53]: Pier Paolo Pasolini, « Trasumanar e organizzar » (1971), in *Le
    Grandi poesie*, Garzanti, 2021, p. 470 ; nous traduisons.

[^54]: Pier Paolo Pasolini, *La Nouvelle jeunesse*, *op. cit.*, p. 189.

[^55]: Pier Paolo Pasolini, « Poésie en forme de rose » (1964), in *Une
    vitalité désespérée...*, *op. cit.*, p. 229.

[^56]: « Qui nous a fourni --- à nous, vieux et jeunes --- le langage
    officiel de la protestation ? Le marxisme, dont l'unique veine
    poétique est le souvenir de la Résistance, qui se renouvelle à la
    pensée du Vietnam et de la Bolivie. » (Pier Paolo Pasolini, « Lettre
    à Allen Ginsberg » [1967], in *Correspondance générale*, traduction
    de René de Ceccatty, Gallimard, 1991, p. 290.)

[^57]: Pier Paolo Pasolini, *Poésie en forme de rose*, traduction de
    René de Ceccatty. Éditions Payot & Rivages, Rivages poche, 2015, p.
    181.

[^58]: Giacomo Leopardi, *La Poésie de Giacomo Leopardi en vers
    français*, traduction d'Auguste Lacaussade, Alphonse Lemerre
    Éditeur, 1889, p. 190.

[^59]: « Le vrai problème est de savoir si l'histoire est une
    totalisation ou non, et, si elle est une totalisation, de savoir
    quelles sont les structures d'un ensemble réel qui se totalise.
    C'est à ce niveau-la que je parle de dialectique. » (Jean-Paul
    Sartre, *Qu'est-ce que la subjectivité ?*, Les Prairies ordinaires,
    2013, p. 94.)

[^60]: Pier Paolo Pasolini, « Lettre à Franco Farolfi » (1941), in
    *Correspondance générale*, *op. cit.*, p. 37-38.

[^61]: « Je te le dis parce que je suis un marxiste non officiel, et mon
    espoir n'est pas rhétorique. » (Pier Paolo Pasolini, « Lettre à
    Massimo Ferretti » [1959], *op. cit.*, p. 252.)

[^62]: Walter Benjamin, « Paris, la capitale du XIX^e^ siècle » (1935),
    traduction d'Olivier Mannoni, in *Œuvres*, *op. cit.*, p. 192.

[^63]: Walter Benjamin, « Le Paris du Second Empire chez Baudelaire »
    (1938), in *Charles Baudelaire*, traduction de Jean Lacoste,
    Éditions Payot & Rivages, Rivages poche, 2021, p. 84-85.

[^64]: *Ibid.*, p. 64-65.

[^65]: Collectif, *Nous sommes partout*, Abrüpt, 2021, p. 165.

[^66]: La parenthèse se justifie étant entendu que le « sentiment de la
    nature » est toujours un produit du développement industriel et
    capitaliste, un égarement lyrique de la conscience bourgeoise, le
    symptôme d'un déchirement inaugural.

[^67]: Pier Paolo Pasolini, *Pétrole*, traduction de René de Ceccatty,
    Gallimard, 1995, p. 84.

[^68]: Et, en effet, où Baudelaire les aurait-il concrètement
    rencontrées ? Nous sommes en droit de parier que ce fût lors de
    l'exil océanique de sa jeunesse plutôt qu'au cœur de Paris où les
    premières installations de lumière artificielle plombèrent le ciel
    dès les années 1840.

[^69]: Charles Baudelaire, *Les Fleurs du mal*, Gallimard, Folio, 2020,
    p. 34.

[^70]: *Ibid.*, p. 38.

[^71]: *Ibid.*, p. 35.

[^72]: *Ibid.*, p. 36-37.

[^73]: *Ibid.*, p. 38.

[^74]: Pier Paolo Pasolini, *Écrits corsaires*, traduction de Philippe
    Guilhon, Flammarion, Champs, 2009, p. 49.

[^75]: Pier Paolo Pasolini, *Le Chaos*, *op. cit.*, p. 174.

[^76]: Pier Paolo Pasolini, « Les Cendres de Gramsci » (1957), in *Une
    vitalité désespérée...*, *op. cit.*, p. 67.

[^77]: *Ibid.*, p. 55.

[^78]: Pier Paolo Pasolini, « Trasumanar e organizzar » (1971), in *Le
    Grandi poesie*, *op. cit.*, p. 467 ; nous traduisons.

[^79]: Pier Paolo Pasolini, « Les Cendres de Gramsci » (1957), in *Une
    vitalité désespérée...*, *op. cit.*, p. 53.

[^80]: Hegel a forgé une philosophie de l'Histoire reposant sur une foi
    inaliénable dans le Progrès ; l'Esprit (ou la Raison) se rapportant
    à lui-même (ou à elle-même) via les différentes objectivations des
    sociétés humaines (État, Religion, Art). L'État moderne apparaît
    notamment comme moyen de restaurer la belle totalité grecque qui
    liait autrefois les humains au monde par la médiation d'un système
    éthico-politico-théologique. Consécutivement à l'avènement du
    Christianisme en tant que religion d'Empire, celui-ci se morcela
    sous l'effet de la privatisation des consciences et du règne de la
    morale pétrifiée. L'Esprit devenu conscient de lui-même est alors le
    moment conclusif de cette épopée téléologique : la conscience du
    philosophe, qui s'affirme dans le « nous » surplombant du langage
    conceptuel, ayant pour destin de contenir l'Histoire en la dominant.
    Autrement dit : le Concept *réalise* le Réel en l'absolutisant sous
la forme d'un Savoir, et l'Histoire ainsi s'achève.

[^81]: Pier Paolo Pasolini, « Les Cendres de Gramsci » (1957), in *Une
    vitalité désespérée...*, *op. cit.*, p. 43.

[^82]: Ernst Bloch, *L'Athéisme dans le christianisme*, traduction
    d'Éliane Kaufholz-Messmer et Gérard Raulet, Gallimard, Philosophie,
    1978, p. 331.

[^83]: « En d'autres termes, notre faute, en tant que pères,
    consisterait à croire que l'histoire n'est et ne saurait être que
    l'histoire bourgeoise. » (Pier Paolo Pasolini, *Lettres
    luthériennes*, traduction d'Anna Rocchi-Pullberg, Point, 2002, p.
    17.)

[^84]: Jean Hyppolite, *Introduction à la philosophie de l'histoire de
    Hegel*, Point, 1983, p. 46.

[^85]: Ernst Bloch, *L'Athéisme dans le christianisme*, *op. cit.*, p.
    331.

[^86]: Ernst Bloch, *Sujet-Objet. Éclaircissements sur Hegel*,
    traduction de Maurice de Gandillac, Gallimard, Philosophie, 1977, p.
    491.

[^87]: Barbara Glowczewski, « Résister au désastre : des Aborigènes
    d'Australie à Notre-Dame-des-Landes » (2017), en libre accès sur
    Vimeo.

[^88]: Friedrich Hölderlin, *Poèmes. 1806-1843*, traduction de Bernard
    Pautrat, Éditions Payot & Rivages, Rivages poche, 2001, p. 29.

[^89]: Empédocle, in Jean Bollack, *Empédocle*, II, Gallimard, Tel,
    1992, p. 24.

[^90]: David Hebert Lawrence, « Birds, Beasts and Flowers » (1923), in
    *D. H. Lawrence. Complete Works* \[édition numérique\], Delphi
Classics, 2024 ; nous traduisons.

[^91]: Robert Sayre et Michael Löwy, *Romantisme anticapitaliste et
    nature*, Éditions Payot & Rivages, Rivages poche, 2022, p. 130-131.

[^92]: Walter Benjamin, « Sens unique » (1928), traduction de Frédéric
    Joly, in *Œuvres*, *op. cit.*, p. 258-259.

[^93]: Simone Weil, *L'Inspiration occitane*, L'Éclat, 2014, p. 71.

[^94]: Ernst Bloch, *Sujet-Objet...*, *op. cit.*, p. 489-490.

[^95]: *Ibid.*, p. 490.

[^96]: *Ibid.*, p. 491.

[^97]: *Ibid.*, p. 492.

[^98]: « ... inadapté à l'histoire, inadapté à moi, / je m'adapterai à la
    terre future, / quand la Culture redeviendra Nature. » (Pier Paolo
    Pasolini, *Poésie en forme de rose*, *op. cit.*, p. 235.)

[^99]: Jacques Lacan, *Le Séminaire. Livre XX. Encore*, Seuil, 1975, p.
    32-33.

[^100]: Theodor W. Adorno, *Minima moralia. Réflexion sur la vie
    mutilée*, traduction d'Éliane Kaufholz-Messmer et Jean-René Ladmira,
    Éditions Payot & Rivages, Rivages poche, 2003, p. 230.

[^101]: *Ibid.*

[^102]: *Ibid.*

[^103]: Distinction opérée par René Girard dans *Mensonge romantique et
    vérité romanesque *(Grasset, 1961).

[^104]: Cf. Stendhal, *Le Rouge et le Noir*, Gallimard, Folio, 2020.

[^105]: « Une maladie très contagieuse : c'est si vrai qu'elle a
    contaminé presque tous ceux qui la combattent : des ouvriers du
    Nord, aux ouvriers immigrés du Sud, aux bourgeois de l'opposition,
    aux “seuls” (comme je le suis). Le bourgeois --- disons-le de façon
    spirituelle --- est un vampire, qui ne trouve pas la paix aussi
    longtemps qu'il n'a pas mordu le cou de sa victime pour le pur et
simple plaisir naturel de la voir devenir pâle, triste, laide,
    dévitalisée, tordue, corrompue, inquiète, pleine d'un sentiment de
    culpabilité, calculatrice, agressive, terroriste, *tout comme
    lui*. » (Pier Paolo Pasolini, *Le Chaos*, *op. cit.*, p. 18.)

[^106]: Nous nous appuyons ici sur le livre conjointement au film.

[^107]: Pier Paolo Pasolini, *Théorème*, traduction de José Guidi,
    Gallimard, Folio, 1988, p. 11.

[^108]: *Ibid.*, p. 14.

[^109]: *Ibid.*, p. 161.

[^110]: Giovanni Pascoli, « Le Vertige » (1909), in *L'Impensé la
    poésie. Choix de poèmes* (1890-1911), traduction de Jean-Charles
    Vegliante, Éditions Mimésis, 2018, p. 94.

[^111]: *Ibid.*, p. 95.

[^112]: Michel Foucault, « Préface de Michel Foucault à la traduction
    américaine du livre de Gilles Deleuze et Félix Guattari :
    *L'Anti-Œdipe : capitalisme et schizophrénie* » (1977), in *Dits et
    Écrits*, II (1976-1988), Gallimard, Quarto, 2001, p. 135-136.

[^113]: Cf. Georg Lukács, *Ontologie de l'être social*, III, 
    traduction de Jean-Pierre Morbois et Didier Renault, Delga, 2012.

[^114]: Pier Paolo Pasolini, *Poésie en forme de rose*, *op. cit.*, p. 67.

[^115]: Gilles Deleuze et Claire Parnet, « D comme Désir », in
    *L'Abécédaire de Gilles Deleuze*, 1995, en libre accès sur YouTube.

[^116]: Note sur la « femme-paysage » (Deleuze/Proust) : l'amour en tant
que *commun paysage* ; à rebours, donc, des cristallisations
    proustiennes qui contribuent à chosifier autrui et à neutraliser
    ainsi la possibilité d'une rencontre.

[^117]: André Breton, *Nadja*, Gallimard, Folio Plus, 1998, p. 161.

[^118]: Rien ne sera jamais plus comme avant... dans le cadre des
    possibilités bourgeoises. Cette chute apparaît comme paradigmatique
    du rapport que le bourgeois entretient à lui-même et au monde, entre
    maîtrise et « lâcher-prise », entre surplomb et soumission, entre
    orgueil et honte. La dépossession impliquant la possession en tant
que motif préalable de domination (économique et symbolique) et
    d'encombrement (matériel et psychique). *A contrario*, l'âme
    prolétarienne est libre car elle ne constitue pas le monde comme
    objet de possession ou de convoitise ; dans l'acte d'abandon, elle
    ne « chute » pas, donc, mais se dilue dans une totalité qui la
    comprend (à plus d'un sens) et l'excède (à plus d'un sens
    également).

[^119]: « pure, de n'être que trop / proche, absolue, de n'être que /
    trop misérablement humaine » (Pier Paolo Pasolini, « Les Cendres
    de Gramsci » [1957], in *Une vitalité désespérée...*, *op. cit.*, p.
    67.)

[^120]: « Il est vrai que l'arme de la critique ne peut pas remplacer
    la critique des armes, que le pouvoir matériel ne peut être renversé
    que par un pouvoir matériel, mais la théorie devient elle aussi un
    pouvoir matériel dès lors qu'elle s'empare des masses. La théorie
    est capable de s'emparer des masses dès lors qu'elle démontre *ad
hominem*, et elle démontre *ad hominem* dès qu'elle devient
    radicale. Être radical, c'est saisir la chose à la racine. » (Karl
    Marx, *Contribution à la critique de la philosophie du droit de
    Hegel*, traduction de Victor Béguin, Alix Bouffard, Paul Guerpillon
    et Florian Nicodème, Éditions Sociales, 2018, p. 292.)

[^121]: Louis Althusser, *Réponse à John Lewis*, François Maspero, 1973,
    p. 11.

[^122]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 161.

[^123]: Pier Paolo Pasolini, « Lettre à Sandro Penna » (1970), traduction
    d'Étienne Dobenesque et Isabella Checcaglini, in Sandro
    Penna, *Croix et délice*, traduction de Bernard Simeone, Ypsilon,
    2023, p. 211.

[^124]: Franco Fortini, « Pasolini Politico » (1979), *Attraverso
Pasolini*, Einaudi, 1993, p. 199 ; nous traduisons.

[^125]: Pier Paolo Pasolini, *Lettre luthérienne*, *op. cit.*, p. 81-87.

[^126]: Alessandro La Monica, « Une lecture de *Italia 1942* », in
    *Franco Fortini. Les Contradictions de la forme*, sous la direction
    d'Andrea Agliozzo, Paolo Desogus et Laura Maver Borges, Éditions
    Mimésis, 2020, p. 102-103.

[^127]: Walter Benjamin, « Paris, capitale du XIX^e^ siècle » (1935), in
    *Le Livre des passages*, traduction de Jean Lacoste, Éditions du
    Cerf, 2021, p. 46.

[^128]: Franco Fortini, *Feuille de route*, traduction de Giulia Camin
    et Benoît Casas, Éditions NOUS, 2022, p. 16.

[^129]: Pier Paolo Pasolini, *Poésie en forme de rose*, *op. cit.*, p.
    75-77.

[^130]: « Dans cette solitude qui est la sienne, le lecteur du roman
    s'empare de sa matière plus jalousement que tout autre lecteur. Il
    est prêt à se l'approprier sans laisser le moindre reste, à
    l'engloutir pour ainsi dire. » (Walter Benjamin, « Le Conteur.
    Considérations sur l'œuvre de Nicolas Leskov » [1936], traduction de
    Cédric Cohen-Skalli, in *Œuvres*, *op. cit.*, p. 43.)

[^131]: « Dans le tout présent manifeste, Brecht ne sera jamais nommé.
    Il a été le dernier homme de théâtre à pouvoir faire une révolution
    théâtrale à l'intérieur du théâtre même : et c'est parce qu'à son
    époque l'hypothèse était qu'un théâtre traditionnel existât \[et en
    effet, il existait\]. Or, comme nous le verrons à travers les
    articles du présent manifeste, l'hypothèse est que le théâtre
    traditionnel n'existe plus \[ou qu'il est en train de cesser
    d'exister\]. À l'époque de Brecht, on pouvait donc opérer des
    réformes, profondes même, sans remettre en question le théâtre :
    plutôt, la finalité de telles réformes était de faire du théâtre un
    théâtre authentique. Aujourd'hui, au contraire, c'est le théâtre
    lui-même qui est remis en question ; la finalité de ce manifeste est
    donc, paradoxalement, la suivante : le théâtre devrait être ce que
    le théâtre n'est pas. De toute façon, une chose est sûre : l'époque de Brecht est révolue
    à jamais. » (Pier Paolo Pasolini, *Manifeste pour un nouveau théâtre*,
    traduction de Marie Fabre, Ypsilon Éditeur, 2019, p. 6.)

[^132]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 285.

[^133]: *Ibid.*

[^134]: « Pas Marx. Simplement ce qui désormais est parole, / sa parole
    muette, pas la clarté » (Pier Paolo Pasolini, *La Religion de notre
    temps*, *op. cit.*, p. 311.)

[^135]: Franco Fortini, *Il Silenzio d'Italia* (1944), cité par
    Alessandro La Monica, « Une lecture de *Italia 1942* », in *Franco
    Fortini...*, *op. cit.*, p. 105.

[^136]: Pier Paolo Pasolini, *La Nouvelle jeunesse*, *op. cit.*, p. 269.

[^137]: Franco Fortini, « Perché un film aiuta a capire cosa è successo
    negli ultimi venti anni e quel che *dovrà* essere » (1979), cité par
    Andrea Agliozzo, « Une extraordinaire leçon de métrique. Fortini et
    le cinéma de Straub-Huillet », in *Franco Fortini...*, *op. cit.*, p.
    130.

[^138]: Andrea Cavazzini, « Entre *Forma* et *Figura*. Pour une autre
    querelle de l'humanisme », *Ibid.*, p. 46.

[^139]: « ... jusqu'à la figure de style la plus fréquente, cette
    sous-catégorie d'oxymore que la rhétorique antique nommait
    *sineciosi*, et avec laquelle s'affirment, en même temps qu'un
    sujet, ses contraires. » (Franco Fortini, *Attraverso Pasolini*
    [1993], cité par Silvia Cucchi, « Tu m'obsèdes toujours quand tu
    n'es pas là. Sur la correspondance entre Franco Fortini et Walter
    Siti », *Ibid.*, p. 252.)

[^140]: Pier Paolo Pasolini, « La Religion de notre temps » (1961), in
    *Une vitalité désespérée...*, *op. cit.*, p. 163.

[^141]: Michel Foucault, « Qu'est-ce que les lumières ? » (1984), in
    *Dits et Écrits*, II (1976-1988), *op. cit.*, p. 1397.

[^142]: Pier Paolo Pasolini, *Saint Paul*, traduction de Giovanni
    Joppolo, Flammarion, 1980, p. 47.

[^143]: Pier Paolo Pasolini, *Il Cinema in forma di poesia*, Edizioni
Cinemazero, 1979, p. 97 ; nous traduisons.

[^144]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 33.

[^145]: « ... Ils auront vécu là les pires / années de leur vie : POUR
    AVOIR ACCEPTÉ / UNE RÉALITÉ QUI N'EXISTAIT PAS... » (Pier Paolo
    Pasolini, « Poésie en forme de rose » [1964], in *Une vitalité
    désespérée...*, *op. cit.*, p. 269.)

[^146]: Cette expression renvoie à une période de forte croissance
    économique en Italie entre 1958 et 1963.

[^147]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 286.

[^148]: *Ibid.*, p. 284.

[^149]: Ancien patron de l'ENI, avant de devenir celui de la Montedison,
    Eugenio Cefis est un personnage emblématique de ce nouvel ordre
    capitaliste qui imposa sa vision du monde à l'Italie contemporaine.
    Cherchant à produire une « image » de ce nouveau pouvoir, Pasolini
    avait notamment l'intention d'intégrer à *Pétrole* plusieurs de ses
    interventions. Cette intention n'ayant pas été respectée, Carla
Benedetti et Giovanni Giovannetti se sont chargés de réparer cette
    négligence éditoriale dans *Pédé, et c'est tout. Pétrole ou les
    dessous cachés du meurtre de Pasolini* (traduction de Laurent
    Lombard et Davide Luglio, Éditions Mimésis, 2017).

[^150]: Nous choisissons intentionnellement ici d'évoquer la « société
    néocapitaliste » plutôt que la « société néolibérale » afin
    d'insister sur les surdéterminations que les nouveaux modes de
    production et d'échange font peser sur la société. En choisissant
    d'invoquer le néolibéralisme, nous aurions privilégié la
    détermination idéologique et les nouveaux modes de gouvernance des
    populations. Aussi les deux termes nous semblent complémentaires.
    Nous renvoyons notamment à *« Il faut s'adapter » : Sur un nouvel
    impératif politique* de Barbara Stiegler (Gallimard, Essais, 2019)
    pour une explicitation de la rupture néolibérale à l'égard du vieux
    libéralisme.

[^151]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 285.

[^152]: *Ibid.*, p. 284-285.

[^153]: « N'y a-t-il pas une véritable grandeur dans cette manière
    d'envisager la vie, avec ses puissances diverses insufflées
    primitivement dans un petit nombre de formes, ou même à une seule ?
    Or, tandis que notre planète, obéissant à la loi fixe de la
    gravitation, continue à tourner dans son orbite, une quantité
    infinie de belles et admirables formes, sorties d'un commencement si
    simple, n'ont pas cessé de se développer et se développent
    encore ! » (Charles Darwin, *L'Origine des espèces*, traduction
    d'Edmond Barbier, Flammarion, GF, 2022, p. 563.)

[^154]: Cf. Jacques Ellul, *Le Bluff technologique*, Hachette, 2012.

[^155]: Ivan Illich, *La Convivialité*, Point, 2021, p. 141.

[^156]: Pier Paolo Pasolini, *Le Chaos*, *op. cit.*, p. 164.

[^157]: Gilbert Simondon, *Du mode d'existence des objets techniques*,
    Aubier, 2012, p. 176.

[^158]: Frédéric Neyrat, *Le Cosmos de Walter Benjamin. Un communisme du
    lointain*, KIMÉ, 2022, p. 30.

[^159]: « Un communisme du vivant libéré du centralisme humain, un
    communisme de l'acosmos germe dans la pénombre que l'ivraie ébauche
    sur une réalité de la production. Semer la zizanie permet de
    recouvrer l'acosmos du cosmos, de faire dialectique dans ce qui se
    donne à la raison comme une évidence de l'ordre, et de la hiérarchie
    implicite que celui-ci fabrique, pour que se devine une
    structuration radicalement autre, une harmonie du désordre qui prend
    la mesure de la contingence de son devenir. L'ivraie n'ébrèche pas
    seulement l'espace productiviste du champ, elle laisse paraître la
    lumière brute d'un cosmos négatif, inhumain, délié des bornes que
    l'humain impose à sa réalité. » (Kosmokritik, « Lolium temulentum :
    l'ivresse de la zizanie », *Lundimatin*, #378, 2023.)

[^160]: Cf. Daniel Bensaïd, *Une lente impatience*, Stock, 2004.

[^161]: « La seule philosophie, dont on puisse encore assumer la
    responsabilité face à la désespérance, serait la tentative de
    considérer toutes les choses telles qu'elles se présenteraient du
    point de vue de la rédemption. La connaissance n'a d'autre lumière
    que celle de la rédemption portant sur le monde : tout le reste
    s'épuise dans la reconstruction et reste simple technique. »
    (Theodor W. Adorno, *Minima moralia...*, *op. cit.*, p. 333.)

[^163]: Pier Paolo Pasolini, *La Rage*, traduction de Patrizia Atzei et
    Benoît Casas, Éditions NOUS, 2020, p. 20.

[^164]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 467.

[^165]: Pier Paolo Pasolini, « Poésie en forme de rose » (1964), in *Une
    vitalité désespérée...*, *op. cit.*, p. 229.

[^166]: Pier Paolo Pasolini, *Divina Mimesis*, Mondadori, 2019, p. 10 ;
    nous traduisons.

[^167]: Barbara Stiegler, *Nietzsche et la vie. Une nouvelle histoire de
    la philosophie*, Gallimard, Folio, 2021, p. 48.

[^168]: Ivan Illich, *La Convivialité*, *op. cit.*, p. 150.

[^169]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 27-28.

[^170]: Pier Paolo Pasolini, *La Religion de notre temps*, *op. cit.*, p.
    293.

[^171]: Pier Paolo Pasolini, *La Rage*, *op. cit.*, p. 70.

[^172]: « Je ne propose *pas une utopie normative*, mais les
    conditions formelles d'une procédure qui permette à chaque
    collectivité de choisir continuellement son utopie réalisable. La
    convivialité est multiforme. » (Ivan Illich, *La Convivialité*, *op.
    cit.*, p. 33.)

[^173]: Cf. Theodor W. Adorno, *La « Critique de la raison pure » de
    Kant*, traduction de Michèle Cohen-Halimi, Klincksieck, Critique de
    la politique, 2024.

[^174]: Pier Paolo Pasolini, *Dialogues en public. 1960-1965*,
    traduction de François Dupuigrenet Desroussilles, Les Éditions du
    sorbier, 1980, p. 163.

[^175]: Critique en un sens kantien, nous nous situons ici dans une
    démarche analogue à celle du Sartre de la *Critique de la raison
    dialectique* (*op. cit.*).

[^176]: Cf. Georg Lukács, *La Destruction de la Raison*, I, II & III,
    traduction d'Aymeric Monville et Didier Renault, Delga, 2006, 2010
    & 2017.

[^177]: Cf. Georg Lukács, *Ontologie de l'être social*, II & III,
    traduction de Jean-Pierre Morbois et Didier Renault, Delga, 2011 &
    2012.

[^178]: Pier Paolo Pasolini, *La Religion de notre temps*, *op. cit.*, p.
    311.

[^179]: *Ibid.*, p. 295.

[^180]: Pier Paolo Pasolini, *Poésie en forme de rose*, *op. cit.*, p. 59.

[^181]: Pier Paolo Pasolini, *La Religion de notre temps*, *op. cit.*, p.
    227-229.

[^182]: *Ibid.*, p. 229.

[^183]: *Ibid.*, p. 233-235.

[^184]: Pier Paolo Pasolini, *Poésie en forme de rose*, *op. cit.*, p.
    439.

[^185]: *Ibid.*, p. 235.

[^186]: Frédéric Neyrat, *L'Ange Noir de l'Histoire. Cosmos et technique
    de l'Afrofuturisme*, MF, 2022, p. 15-16.

[^187]: Pier Paolo Pasolini, *Poésie en forme de rose*, *op. cit.*, p.
    381.

[^188]: Antonio Gramsci, *Guerre de mouvement et guerre de position*,
    textes choisis et présentés par Ramzig Keucheyan, La Fabrique, 2012,
    p. 156.

[^189]: *Ibid.*

[^190]: Denis de Rougemont, *Penser avec les mains*, Gallimard, Idées,
    1972, p. 146.

[^191]: Pier Paolo Pasolini, « Poésie en forme de rose » (1964), in *Une
    vitalité désespérée...*, *op. cit.*, p. 279-281.

[^193]: Frédéric Neyrat, « La Planète Terre, les humains et la condition
    d'errance : un antéhumanisme », in *Abolir la condition humaine :
    entre chimères et calamités*, Écologie & Politique n^o^ 55, Éditions Le
    Bord de l'eau, 2017, en libre accès sur Cairn.

[^194]: Frédéric Neyrat, « Localiser la démesure » (2010), en libre
    accès sur Vimeo.

[^194bis]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 220.

[^195]: Herbert Marcuse, *Éros et Civilisation. Contribution à Freud*,
    traduction de Jean-Guy Nény et Boris Fraenkel, Les Éditions de
    Minuit, 1963, p. 42.

[^196]: Paul Mattick, *Les Limites de l'intégration*, Grévis, 2021, p.
    151.

[^197]: Theodor W. Adorno, « Lettre à Samuel Beckett » (1969), in *Notes
    sur Beckett*, *op. cit.*, p. 21 ; voir également la réponse de Samuel
    Beckett (« Lettre à Theodor W. Adorno » [1969], *Ibid.*, p. 22).

[^198]: Cf. Herbert Marcuse, *L'Homme unidimensionnel*, traduction de
    Monique Wittig revue par l'auteur, Les Éditions de Minuit, 1968.

[^199]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 184.

[^200]: *Ibid.*, p. 185.

[^201]: *Ibid.*, p. 187.

[^202]: *Ibid.*, p. 183.

[^203]: *Ibid.*, p. 210.

[^204]: Hannah Arendt, *La Condition de l'homme moderne*, traduction
    de Georges Fradier, Pocket, 1992, p. 38.

[^205]: « Le lumpenisme : maladie infantile de l'intellectuel. »
    (Roberto Bolaño, *Les Détectives sauvages*, traduction de Robert
    Amutio, Point, 2022, p. 246.)

[^206]: Cf. Pierre Bourdieu, « Vous avez dit “populaire” ? » (1983),
    in *Qu'est-ce qu'un peuple ?*, *op. cit.*

[^207]: Cf. James C. Scott, *La Domination et les arts de la
    résistance. Fragments du discours subalterne*, traduction
    d'Olivier Rucher, Éditions Amsterdam, 2009.

[^208]: Ivan Illich, *La Convivialité*, *op. cit.*, p. 150.

[^209]: *Ibid.*, p. 153-154.

[^210]: « Le rapprochement de la banlieue vers le centre, de la province
    vers les capitales, avait, entre-temps, détruit également les
    cultures populaires variées et particulières. La banlieue de Rome ou
    les terres pauvres du Midi, les petites villes traditionnelles et
    les régions paysannes du Nord, ne produisaient plus de modèles
    humains qui leur fussent propres, nés précisément de leurs vieilles
    cultures ; des modèles humains à opposer à ceux qu'offrait le
    centre, comme forme de résistance et de liberté... » (Pier Paolo
    Pasolini, *Pétrole*, *op. cit.*, p. 285.)

[^211]: Pier Paolo Pasolini, « Trasumanar e organizzar » (1971), in *Le
    Grandi poesie*, *op. cit.*, p. 467 ; nous traduisons.

[^212]: Cf. Lénine, *L'impérialisme, stade suprême du capitalisme*
    (1916), en libre accès sur *Marxists.org* ; si cette analyse est
    contestée, c'est qu'elle tend à penser l'impérialisme depuis le
    développement du capitalisme plutôt qu'à identifier les structures
    raciales et les origines coloniales du capitalisme, ce à quoi
    s'évertue notamment la tradition radicale noire (cf. Cedric J.
    Robinson, *Marxisme noir*, traduction de Selim Nadi et Sophie
    Coudray, Entremonde, 2023).

[^213]: Pier Paolo Pasolini, *La Religion de notre temps*, *op. cit.*, p.
    317.

[^214]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p.
    228-229.

[^215]: Walter Benjamin, « Le surréalisme. Le dernier instantané de
    l'intelligentsia européenne » (1929), traduction d'Olivier Mannoni,
    in *Œuvres*, *op. cit.*, p. 222.

[^216]: « Reprendre l'initiative de sa propre histoire est un long
    processus, qui implique de se réapproprier tous les moyens par
    lesquels un peuple se définit. Le choix d'une langue, l'usage que
    les hommes décident d'en faire, la place qu'ils lui accordent,
    tout cela est déterminant et conditionne le regard qu'ils portent
    sur eux-mêmes et sur leur environnement naturel et social, voire sur
    l'univers entier. » (Ngũgĩ wa Thiong'o, *Décoloniser l'esprit*,
    traduction de Sylvain Prudhomme, La Fabrique, 2011, p. 19.)

[^217]: Pier Paolo Pasolini, *La Langue vulgaire*, *op. cit.*, p. 24.

[^218]: *Ibid.*, p. 85.

[^219]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 181.

[^220]: Pier Paolo Pasolini, *La Langue vulgaire*, *op. cit.*, p. 39.

[^221]: *Ibid.*

[^222]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 202.

[^223]: Félix Guattari, *Lignes de fuite*, Éditions de l'Aube, 2011, p.
    94.

[^224]: Cf. Henri Lefebvre, *Le Droit à la ville*, Économica, 2009.

[^225]: Cf. Kristin Ross, *Rimbaud, la Commune de Paris et l'invention
    de l'histoire spatiale*, traduction de Christine Vivier, Les
    Prairies ordinaires, 2013.

[^226]: Cf. Tahar Kessi, « Hétérographie du rouge et phénoménologie de
    la barricade », *Lundimatin*, #350, 2022.

[^227]: Cf. Rainer Maria Rilke, « Lettre à Witold von Hulewicz » (1925),
    in *Élégies de Duino*, *op. cit.*

[^228]: Olivier Neveux, *Armand Gatti. Théâtre-utopie*, Libertalia,
    2024, p. 95.

[^229]: Nina Gourkinkel, *Lénine*, *op. cit.*, p. 161.

[^230]: « ... il faut s'efforcer le plus possible d'élever le niveau
    de la conscience des ouvriers en général, il faut qu'ils ne se
    confinent pas dans le cadre artificiellement rétréci de la
    “littérature pour ouvriers” et apprennent à comprendre de mieux en
mieux la littérature pour tous. Il serait même plus juste de dire,
    au lieu de “se confinent”, ne soient pas confinés, parce que les
    ouvriers eux-mêmes lisent et voudraient lire tout ce qu'on écrit
    aussi pour les intellectuels, et seuls quelques (pitoyables)
    intellectuels pensent qu'il suffit de parler “aux ouvriers” de la
    vie de l'usine et de rabâcher ce qu'ils savent depuis longtemps. »
    (Lénine, *Que faire ?* (1902), en libre accès sur *Marxists.org*.)

[^231]: Olivier Neveux, *Armand Gatti...*, *op. cit.*, p. 53.

[^232]: *Ibid.*, p. 52-53.

[^233]: Néologisme forgé par Félix Guattari (cf. *Lignes de fuite*, *op.
    cit.*).

[^234]: À ce titre, Gramsci insiste à plusieurs reprises sur la
    nécessité d'une école (ici pour les ouvriers) qui soit
    « désintéressée » --- autrement dit : qui ne soit réduite ni à la
    seule « information » ni à la seule « pratique manuelle ». (Antonio
    Gramsci [1916], cité par Romain Descendre et Jean-Claude Zancarini,
    in *L'Œuvre-vie d'Antonio Gramsci*, La Découverte, 2023, p. 316.)

[^235]: Pier Paolo Pasolini, *Le Chaos*, *op. cit.*, p. 139.

[^236]: Cf. Ivan Illich, *Une société sans école*, traduction de Gérard
    Durand, Point, 2015.

[^237]: Nous reproduisons ici l'extrait en question de la préface de
    Laurence de Cock : « En France, de Mai 68 à nos jours, les griefs de
    la gauche radicale contre l'école publique ne relèvent pas vraiment
    de la lutte des classes. L'école est plutôt fustigée comme le lieu
    de prédilection de la disciplinarisation des corps, de
    l'autoritarisme, l'expression d'une République de l'ordre --- et
    surtout de l'ennui. À l'égal d'autres structures étatiques (l'usine,
    la prison, l'asile), l'école est vue comme une des institutions les
    plus aliénantes. Le pas est vite franchi vers la réclamation d'une
    “société sans école” puis, plus récemment, la promotion d'écoles
    privées, hors contrat, où enfants, enseignants et, surtout, parents
    sont libérés du poids de l'État. » (Laurence de Cock, « Préface. Une
    école du peuple », in Collectif, *Lettre à une enseignante*, traduction de
    Susanna Spero, Agone, 2022, p. XIX.)

[^238]: « ... il n'y a rien de plus \[solidaire\] que l'ordre et le
    désordre. » (Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 41.)

[^239]: Pier Paolo Pasolini, « Avant-propos. La culture rurale de
    l'école de Barbiana » (1968), in Collectif, *Lettre à une enseignante*, *op.
    cit.*, p. XXX.

[^240]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 75.

[^241]: Cf. Ernst Bloch, *Thomas Münzer. Théologien de la révolution*,
    traduction de Maurice de Gandillac, Éditions Amsterdam, 2022.

[^242]: Nous nous appuyons sur son *Saint Paul* (*op. cit.*) ainsi que sur
    plusieurs articles extraits du *Chaos* (*op. cit.*) portant
    explicitement sur les notions d'espérance, de foi et de charité.

[^243]: « Une marée de rondes qui suivent / la courbe de la muraille...
    Les voici, les enfants / de la faim, les enfants de la révolte, /
    les enfants du sang, les voici, les enfants / des pionniers qui ont
    lutté, / des héros sans nom, les enfants / du lointain lendemain qui
    déchante ! / Les voici au monde, maintenant : et maîtres / du monde.
    Et le monde, non, il n'est pas bienheureux / pour eux, bien que,
    humblement joyeux » (Pier Paolo Pasolini, *La Religion de notre
    temps*, *op. cit.*, p. 185-187.)

[^244]: « Ce qui impressionne le plus, quand on marche dans une ville
    d'Union soviétique, c'est l'uniformité de la foule : on ne remarque
    jamais de différence importante entre les passants dans la façon de
    s'habiller, de marcher, d'être sérieux, de sourire, de faire des
    gestes, dans, en somme, la façon de se comporter. » (Pier Paolo
    Pasolini, *Écrits corsaires*, *op. cit.*, p. 94.)

[^245]: « Dans une ville russe, le “système de signes” du langage
    physico-mimique n'a pas de variantes : il est parfaitement identique
    en tous. Mais quelle est donc la proposition première de ce langage
    physico-mimique ? La voici : “qu'il n'y ait pas de différences de
    classes” ; et c'est quelque chose de merveilleux. Malgré toutes les
    erreurs et toutes les répressions, malgré les délits politiques et
    les génocides (dont est complice tout l'univers paysan russe)
    perpétrés par Staline, le fait que le peuple ait remporté, en 1917,
    une fois pour toutes, la lutte des classes et qu'il ait atteint
    l'égalité des citoyens donne un profond et exaltant sentiment de
    gaieté et de confiance en l'homme. Le peuple a, en effet, conquis la
    liberté suprême --- personne ne la lui a offerte. Il l'a conquise. »
    (*Ibid.*, p. 94-95.)

[^246]: « La fièvre de consommation est une fièvre d'obéissance à un
    ordre non énoncé. Chacun, en Italie, ressent l'anxiété, dégradante,
    d'être comme les autres dans l'acte de consommer, d'être heureux,
    d'être libre, parce que tel est l'ordre que chacun a inconsciemment
    reçu et auquel il “doit” obéir s'il se sent différent. Jamais la
    différence n'a été une faute aussi effrayante qu'en cette période de
    tolérance. L'égalité n'a, en effet, pas été conquise, mais est, au
contraire, une “fausse” égalité reçue en cadeau. » (*Ibid.*, p.
    95.)

[^247]: « La manière dont se présente l'Autre, dépassant l'idée de
    l'Autre en moi, nous l'appelons visage. » (Emmanuel Lévinas,
    *Totalité et infini. Essai sur l'intériorité*, Martinus Nijhoff,
    1965, p. 21.)

[^248]: Saint Paul, « Première Lettre aux Corinthiens », en libre accès
    sur le site de l'AELF.

[^249]: Pier Paolo Pasolini, *Écrits corsaires*, *op. cit.*, p. 130.

[^250]: Pier Paolo Pasolini, *Lettres luthériennes*, *op. cit.*, p. 29-30.

[^251]: Ivan Illich, *La Convivialité*, *op. cit.*, p. 13-14.

[^252]: Silvia Federici, *Par-delà les frontières du corps*, Divergence,
    2020, p. 139-140.

[^253]: Barbara Stiegler, *Nietzsche et la vie...*, *op. cit.*, p. 51.

[^254]: « Or, contrairement à ce que croyait Kant, ni l'espace ni le
    temps ne sont *a priori*. Et ils ont été démolis. » (Barbara
    Stiegler, *Du cap aux grèves. Récit d'une mobilisation : 17 novembre
    2018-17 mars 2020*, Verdier, 2020, p. 120.)

[^255]: Il n'échappera pas aux amateurs de marxologie que nous faisons
    jouer ici Gramsci contre Bordiga.

[^256]: Karl Marx, *Manuscrits de 1844...*, *op. cit.*, p. 107-108.

[^257]: Gramsci juge que les formes d'organisation ouvrière, politiques
    et syndicales, sont généralement déterminées par le développement du
    capitalisme occidental : « En Europe centrale et occidentale le
    développement du capitalisme a déterminé non seulement la formation
    de larges couches prolétariennes, mais il a aussi et pour cette
    raison créé une couche supérieure, l'aristocratie ouvrière avec ses
    annexes de bureaucratie syndicale et de groupes
    sociaux-démocrates. » (Antonio Gramsci [1924], cité par Romain
    Descendre et Jean-Claude Zancarini, in *L'Œuvre-vie d'Antonio
    Gramsci*, *op. cit.*, p. 196.) Ce pour quoi, et à contre-courant du
    mouvement ouvrier officiel, Gramsci appelle quelques années
    auparavant à la constitution de « conseils d'usine » non inféodés
    aux organisations syndicales hégémoniques ; des conseils qui firent
    notamment la démonstration de leur efficacité lors des grèves
    turinoises de 1919-1920.

[^258]: Simone Weil, « Notes sur la suppression générale des partis
    politiques » (1950), in *Œuvres*, Éditions Payot & Rivages, Rivages
    poche, 2023, p. 218.

[^259]: « La première fin, et, en dernière analyse, l'unique fin de
    tout parti politique, est sa propre croissance, et cela sans aucune
    limite. » (*Ibid.*, p. 219.)

[^260]: Walter Benjamin, *Gesammelte Schriften*, V, 1, Suhrkamp Verlag,
    1983, p. 574, cité et traduit par Robert Sayre et Michael Löwy, in
    *Romantisme anticapitaliste et nature*, *op. cit.*, p. 136.

[^261]: «  Le travail est de prime abord un acte qui se passe entre
    l'homme et la nature. L'homme y joue lui-même vis-à-vis de la
    nature le rôle d'une puissance naturelle. Les forces dont son corps
    est doué, bras et jambes, tête et mains, il les met en mouvement
    afin de s'assimiler des matières en leur donnant une forme utile à
    sa vie. En même temps qu'il agit par ce mouvement, sur la nature
    extérieure et la modifie, il modifie sa propre nature, et développe
    les facultés qui y sommeillent. » (Karl Marx, *Le Capital*, *op.
    cit.*, p. 203-204.)

[^262]: « Ce communisme en tant que naturalisme achevé = humanisme, en
tant qu'humanisme achevé = naturalisme... » (Karl Marx, *Les
    Manuscrits de 1844...*, *op. cit.*, p. 87.)

[^263]: Cf. John Bellamy Foster, *Marx écologiste*, traduction
    d'Aurélien Blanchard, Charlotte Nordmann et Joséphine Gross,
    Éditions Amsterdam, 2011.

[^264]: Bernard Charbonneau insiste sur l'angle mort productiviste de la
    tradition marxiste : « Le marxisme est le premier à souligner la
    relation dialectique qui unit le capitalisme et la dictature du
    prolétariat. La lutte sans merci qui les oppose ne doit pas nous
    faire oublier les caractères communs à ces deux termes de la société
    industrielle, ce qui explique les emprunts mutuels qu'ils se sont
    faits depuis la guerre. Ces deux camps ont en effet la même religion
    de l'industrie, et le même terrain de jeu : la ville. Pour l'un et
    l'autre, la campagne est un corps étranger qu'on supporte tant bien
que mal, en attendant de l'éliminer brutalement par la révolution,
    et méthodiquement par la technique. » (*Le Jardin de Babylone*, *op.
    cit.*, p. 35.) Contrairement à Charbonneau, nous adressons une telle
    critique, non « au marxisme en général », mais au marxisme orthodoxe
    en particulier.

[^265]: Walter Benjamin, *Gesammelte Schriften*, I, 3, Suhrkamp Verlag,
    1983, p. 1232, cité et traduit par Michael Löwy, in *Walter Benjamin
    : avertissement d'incendie. Une lecture des thèses « Sur le concept
    d'histoire »*, L'Éclat, 2018, p. 123.

[^266]: Henri Lefebvre, « Vers un romantisme révolutionnaire », in *La
    Nouvelle Revue française*, n^o^ 58, 1957, p. 45.

[^267]: Cf. Jack London, *Le Talon de fer*, traduction de Philippe
Mortimer, Libertalia, 2016.

[^268]: Henri Lefebvre, « Vers un romantisme révolutionnaire », in *La
    Nouvelle Revue française*, *op. cit.*, p. 50.

[^269]: *Ibid.*, p. 45.

[^270]: Ivan Illich, *La Convivialité*, *op. cit.*, p. 30.

[^271]: Max Horkheimer et Theodor W. Adorno, *La Dialectique de la
    raison*, *op. cit.*, p. 362-363.

[^272]: Henri Lefebvre, « Vers un romantisme révolutionnaire », in *La
    Nouvelle Revue française*, *op. cit.*, p. 52.

[^273]: « Mais les choses se présentent de telle sorte que, surtout
    depuis l'époque la plus moderne --- et même ailleurs dans la
    littérature philosophique ---, ce problème fondamental de l'homme qui
    se perçoit dans un isolement absolu et ne peut se défendre de douter
    que quelque chose ou quelqu'un existe à l'extérieur de lui ne se
    limite pas à quelques personnes particulières mais subsiste au fil
    des siècles avec une extraordinaire ténacité. Une cascade d'ouvrages
    de la deuxième moitié du XX^e^ siècle présentent au public de lecteurs
    des versions toujours nouvelles d'un personnage qui est
    fondamentalement le même : l'individu isolé sous la forme de l'*homo
    clausus* ou du je sans nous, au cœur d'une solitude délibérément
    choisie ou non. Et le puissant écho qu'éveillent ces écrits, la
    durée de leur succès prouvent bien que l'image de l'individu isolé
    et l'expérience fondamentale qui lui confère sa force ne sont pas
    des phénomènes sporadiques. » (Norbert Elias, *La Société des
    individus*, traduction de Jeanne Étoré, Pocket, 2021, p. 258-259.)

[^274]: Henri Lefebvre, « Vers un romantisme révolutionnaire », in *La
    Nouvelle Revue française*, *op. cit.*, p. 39.

[^275]: *Ibid.*, p. 60.

[^276]: Walter Benjamin, « Sens unique » (1928), in *Œuvres*, *op. cit.*,
    p. 304.

[^277]: Cf. Norbert Elias, *La Civilisation des mœurs*, traduction de
    Pierre Kamnitzer, Pocket, 2002 ; Norbert Elias, *La Dynamique de
    l'Occident*, traduction de Pierre Kamnitzer, Pocket, 2003.

[^278]: Cf. Simone Weil, *L'Enracinement*, Gallimard, Folio, 1990.

[^279]: Henri Lefebvre, « Vers un romantisme révolutionnaire », in *La
    Nouvelle Revue française*, *op. cit.*, p. 60.

[^280]: Barbara Stiegler, *Du cap aux grèves...*, *op. cit.*, p. 120.

[^281]: Ce que Simone Weil relativise dans ses écrits anticoloniaux
    ultérieurs (cf. *Écrits contre le colonialisme*, Éditions Payot &
    Rivages, Rivages poche, 2015).

[^282]: Simone Weil, « La Vie et la grève des ouvrières métallos »
    (1936), in *Œuvres*, *op. cit.*, p. 254-255.

[^283]: Ernst Bloch, *Le Principe espérance*, I, traduction de
    Françoise Wuilmart, Gallimard, Philosophie, 1976, p. 11.

[^284]: « We were the last romantics---chose for theme / Traditional
    sanctity and loveliness; / Whatever's written in what poets name /
    The book of the people; whatever most can bless / The mind of man or
    elevate a rhyme; / But all is changed, that high horse riderless, /
    Though mounted in that saddle Homer rode / Where the swan drifts
    upon a darkening flood. » (William Butler Yeats, « The Winding Stair
    and Other Poems », in *W. B. Yeats. Complete Works* \[édition
    numérique\], Delphi Classics, 2013.)

[^285]: Ne cherchez pas la source de cette citation, bien que de
    première main, elle n'apparaît nulle part. Si vous tenez vraiment à
    en attester l'authenticité, il faudra le demander aux ruines, ou, à
    près de cent kilomètres de la capitale allemande, dans la forêt de
    la Spree, aux vieux aulnes verdis.

[^286]: Dans les *Écrits corsaires* (*op. cit.*) notamment.

[^287]: Cf. Thomas Mann, *Les Buddenbrook*, traduction de Geneviève
    Bianquis, Le Livre de poche, 1993.

[^288]: Cf. AAA, « Nihil », *Error*, 2022.

[^289]: Cf. Jacques Rancière, *Les Trente inglorieuses*, La Fabrique,
    2022.

[^290]: Pier Paolo Pasolini, « Les Cendres de Gramsci » (1957), in
    *Une vitalité désespérée...*, *op. cit.*, p. 67.

[^291]: Ezra Pound, « Cantos pisans » (1948), traduction de Denis Roche,
    in *Les Cantos*, sous la direction d'Yves di Manno, Flammarion,
    Mille et une pages, 2013, p. 489.

[^292]: Cf. Elsa Morante, *Le Monde sauvé par des gamins*, traduction de
    Jean-Noël Schifano, Gallimard, 1991.

[^293]: Cf. Pier Paolo Pasolini, *Poésies. 1943-1970*, traduction de
    Nathalie Castagné, René de Ceccatty, José Guidi et Jean-Charles
    Vegliante, Gallimard, 1990.

[^294]: Était-ce un sacrifice volontaire ou une destitution involontaire
    déterminée par les nouveaux rapports de production, d'échange et de
    consommation, par la nouvelle société qu'ils induisent ? Il me
    semble que les deux sont liés, que ton mérite fut de continuer à
    faire un pas vers le monde, bien que ce monde ne te reconnaisse
    plus, quitte à assumer une posture régressive, scandaleuse ou
    sacrificielle.

[^295]: Si nous considérons évidemment Cendrars et Apollinaire comme des
    rampes de lancement de ces *voies nouvelles* --- et les surréalistes
    en leur sillage ---, ce n'est pas uniquement parce qu'ils ont promu
    le vers libre. Dans l'absolu, nous n'opposons pas la forme fixe au
    vers libre --- cela ne résume pas la distinction langue/langage que
    nous tentons de produire ici. Ce n'est pas non plus, selon nous, le
    cœur du différend entre Césaire et Aragon. Par ailleurs --- et
    notamment depuis *Le Condamné à mort* de Jean Genet ---, nous savons
    que la métrique classique peut fournir une contrainte formelle tout
à fait féconde, qu'elle ne se confond pas avec la *langue*, qu'elle
    peut devenir un *nouveau langage*. Je ne résoudrai pas ici les
    problèmes que cette distinction conceptuelle continue à nous poser,
    contentons-nous de dire que cette distinction ne peut pas faire
    l'économie d'une articulation vivante (dialectique) de l'un et de
    l'autre terme, en réintégrant par exemple la langue au langage au
sein d'un travail de montage (Godard) et/ou de retraduction (Pound).

[^296]: « Du vivant de l'auteur, toute décision est provisoire ; avec sa
    disparition, tout choix provisoire devient définitif ; le principe
    vaut d'autant plus quand, comme dans le projet de *Pétrole*, le
    caractère non définitif était considéré par l'auteur comme
    constitutif de la “forme” intentionnelle. » (Aurelio Roncaglia,
    « Note philologique », in *Pétrole*, *op. cit.*, p. 612.)

[^297]: Ainsi nous affirmons notre désaccord avec Massimo Cacciari quand
    il écrit que le Pasolini de la seconde forme « alourdit » de
    « dénonciations » et d'« invectives » la pureté élégiaque de la
    première forme (« Pasolini “provençal” ? », in Biagio Marin/Pier
    Paolo Pasolini, *Une amitié poétique*, édition préparée par Michel
    Valensi et Laurent Feneyrou, L'Éclat, 2022, p. 262.). *A contrario*,
    nous pensons même que cette « seconde forme » s'aiguise en se
    confrontant à la compréhension du monde du point de vue de la lutte
    des classes. Ajoutons également --- sur un plan plus général --- que
    deux grandes tendances s'esquissent dans l'usage actuel de Pasolini
    : une première tendance qui se manifeste assez dans les mots de
    Massimo Cacciari, faisant ainsi jouer le jeune Pasolini contre le
    plus vieux (à la manière dont on fait souvent jouer le jeune Marx
    des *Manuscrits* contre celui du *Capital*) ; et une seconde
    tendance qui, si elle reconnaît l'importance de l'œuvre ultérieure,
    opère une entreprise de démarxisation de cette dernière, comme s'il
    s'agissait d'en abstraire le logiciel périmé --- en l'occurrence :
    une interprétation marxiste de la réalité sociale et de l'histoire ---,
    la « contextualisation » opérant alors comme
    « relativisation », et ceci afin de forger une représentation
    inoffensive et *cool*, anhistorique, du « poète contestataire ».

[^298]: Cf. Georg Lukács, *Ontologie de l'être social*, II, traduction
    de Jean-Pierre Morbois et Didier Renault, Delga, 2011.

[^299]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 13-14.

[^300]: Citation d'Ossip Mandelstam en exergue de *Pétrole* : « Avec le
    monde du pouvoir je n'ai eu que des liens puérils ». (*Ibid.*, p. 17.)

[^301]: Pier Paolo Pasolini, « Poésie en forme de rose » (1964), in
    *Une vitalité désespérée...*, *op. cit.*, p. 269.

[^302]: « M'emparer, peut-être, sur le plan doux et intellectuel de la
    connaissance ou de l'expression ; mais malgré tout, essentiellement,
    brutalement et violemment, comme cela se passe pour chaque
    possession, pour chaque conquête. » (Pier Paolo Pasolini, *Pétrole*,
    *op. cit.*, p. 444.)

[^303]: Bière sarde.

[^304]: « Vous savez aussi que Gramsci est mort en prison au bout des
    longues années de détention que lui imposa le régime fasciste dans
    le but clairement proclamé de l'empêcher de penser... Pourquoi la
    prison plutôt que la mort ou la drogue ? Sans doute pour faire durer
    le spectacle de l'empêchement et se donner le plaisir de voir
    l'adversaire le plus intelligent, donc le plus redoutable,
    s'affaiblir peu à peu... Une œuvre admirable est née de tout cela,
    alors qu'il ne reste des bourreaux qu'une exemplaire bêtise... »
    (Bernard Noël, *Le Syndrome de Gramsci*, P.O.L., 1994, p. 9-10.)

[^305]: Voleurs.

[^306]: Berlinguer n'occupera finalement la fonction de secrétaire du
    Parti communiste italien que quelques années plus tard. Le règne de
    Berlinguer à la tête du PCI signera un tournant dans l'histoire du
    communisme officiel italien : le fameux *compromis historique*
    marquant à la fois une distance à l'égard de Moscou mais également
    une soumission au récit des vainqueurs.

[^307]: Pier Paolo Pasolini, « Introduzione », in *Poesia dialettale del
    Novecento*, a cura di Mario dell'Arco e Pier Paolo Pasolini,
    Einaudi, 1995, p. XLVII-XLVIII ; nous traduisons.

[^308]: Une vidéo intitulée « La conspiration des troubadours » (2023),
    en libre accès sur YouTube, accompagne ce texte.

[^309]: Ezra Pound, « Cantos pisans » (1948), in *Les Cantos*, *op.
    cit.*, p. 564.

[^310]: Pier Paolo Pasolini, *Descriptions de descriptions*, traduction
    de René de Ceccatty, Éditions Payot & Rivages, Rivages poche, 1984,
    p. 200.

[^311]: Ezra Pound, « Cantos pisans » (1948), in *Les Cantos*, *op.
    cit.*, 564-565.

[^312]: *Ibid.*, p. 565.

[^313]: Pier Paolo Pasolini, *Descriptions de descriptions*, *op. cit.*,
    p. 161.

[^314]: Ezra Pound, « The tradition », in *Poetry*, Vol. III, N^o^ IV,
    1914, p. 137 ; nous traduisons.

[^315]: Pier Paolo Pasolini, *Descriptions de descriptions*, *op. cit.*,
    p. 161.

[^316]: *Ibid.*, p. 162.

[^317]: Ezra Pound, « Esquisses et fragments » (posthume), traduction de
    Yves di Manno, in *Les Cantos*, *op. cit.*, p. 821.

[^318]: Pier Paolo Pasolini, *Descriptions de descriptions*, *op. cit.*,
    p. 162.

[^319]: Ezra Pound, « Esquisses et fragments » (posthume), in *Les
    Cantos*, *op. cit.*, p. 821.

[^320]:  Citation de Peire Vidal en exergue de « La Meilleure
    jeunesse », in Pier Paolo Pasolini, *La Nouvelle jeunesse*, *op. cit.*, p. 11.

[^321]: Walter Benjamin, « La Tâche du traducteur » (1923), traduction
    de Cédric Cohen-Skalli, in *Œuvres*, *op. cit.*, p. 76.

[^322]: Pier Paolo Pasolini, *La Nouvelle jeunesse*, *op. cit.*, p. 15.

[^323]: *Ibid.*, p. 189.

[^324]: Pier Paolo Pasolini, *L'Inédit de New York*, *op. cit.*, p. 54.

[^325]: Pier Paolo Pasolini, *La Longue route de sable*, *op. cit.*, p. 90.

[^326]: Pier Paolo Pasolini, *La Nouvelle jeunesse*, *op. cit.*, p. 254.

[^327]: *Ibid.*, p. 134.

[^328]: *Ibid.*, p. 148.

[^329]: *Ibid.*, p. 153.

[^330]: *Ibid.*, p. 155.

[^331]: *Ibid.*, p. 156.

[^332]: *Ibid.*, p. 175.

[^333]: *Ibid.*, p. 184.

[^334]: *Ibid.*, p. 192.

[^335]: *Ibid.*, p. 200.

[^336]: *Ibid.*, p. 203.

[^337]: *Ibid.*, p. 206.

[^338]: *Ibid.*, p. 210.

[^339]: *Ibid.*, p. 263.

[^340]: *Ibid.*, p. 213.

[^341]: *Ibid.*, p. 214.

[^342]: *Ibid.*, p. 217.

[^343]: *Ibid.*, p. 218.

[^344]: *Ibid.*, p. 219.

[^345]: *Ibid.*, p. 244.

[^346]: *Ibid.*, p. 250.

[^347]: *Ibid.*, p. 258.

[^348]: *Ibid.*, p. 259.

[^349]: *Ibid.*, p. 269.

[^350]: *Ibid.*, p. 274.

[^351]: *Ibid.*, p. 276.

[^352]: *Ibid.*, p. 274-275.

[^353]: Ivan Illich, *La Convivialité*, *op. cit.*, p. 32.

[^354]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 403.

[^355]: *Ibid.*, p. 62.

[^356]: *Ibid.*, p. 402.

[^357]: *Ibid.*, p. 218.

[^358]: Pier Paolo Pasolini, *Divina Mimesis*, *op. cit.*, p. 7.

[^359]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 55.

[^360]: *Ibid.*, p. 61.

[^361]: *Ibid.*

[^362]: *Ibid.*, p. 177.

[^363]: *Ibid.*, p. 347.

[^364]: *Ibid.*, p. 91.

[^365]: *Ibid.*, p. 101.

[^366]: *Ibid.*, p. 135.

[^367]: *Ibid.*, p. 264.

[^368]: *Ibid.*, p. 264-265.

[^369]: *Ibid.*, p. 164.

[^370]: *Ibid.*, p. 309.

[^371]: *Ibid.*, p. 265.

[^372]: *Ibid.*, p. 282.

[^373]: *Ibid.*, p. 258.

[^374]: *Ibid.*, p. 328.

[^375]: *Ibid.*, p. 330.

[^376]: Karl Marx et Friedrich Engels, *L'Idéologie allemande*, *op. cit.*, p. 44.

[^377]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 395.

[^378]: *Ibid.*, p. 442.

[^378bis]: *Ibid.*, p. 444.

[^379]: *Ibid.*

[^380]: *Ibid.*, p. 435.

[^381]: *Ibid.*, p. 463.

[^382]: *Ibid.*, p. 467.

[^383]: *Ibid.*, p. 510.

[^384]: *Ibid.*, p. 519.

[^385]: *Ibid.*, p. 410-411.

[^386]: *Ibid.*, p. 410.

[^387]: *Ibid.*, p. 164.

[^388]: Cf. Friedrich Nietzsche, *La Naissance de la tragédie*,
    traduction de Patrick Wotling, Le Livre de poche, 2013.

[^389]: Friedrich Nietzsche (1885), cité par Barbara Stiegler, in
    *Nietzsche et la vie...*, *op. cit.*, p. 112.

[^390]: Pier Paolo Pasolini, *Porno-Théo-Kolossal*, traduction de Davide
Luglio, Éditions Mimésis, 2016, p. 114.

[^391]: Georges Sorel, *Réflexions sur la violence*, FV éditions, 2016,
    p. 31.

[^392]: Cf. Maria Kakogianni, « Queeriser la lutte des classes » (2017),
    en libre accès sur *La Vie manifeste*.

[^393]: Pier Paolo Pasolini, *Pétrole*, *op. cit.*, p. 220.
