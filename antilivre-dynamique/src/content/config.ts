// Import utilities from `astro:content`
import { z, defineCollection } from "astro:content";
// Define a `type` and `schema` for each collection
const metaCollection = defineCollection({
  type: "content",
  schema: z.object({}),
});
// Export a single `collections` object to register your collection(s)
export const collections = {
  meta: metaCollection,
};
