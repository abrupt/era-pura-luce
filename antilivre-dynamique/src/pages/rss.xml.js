import rss, { pagesGlobToRssItems } from "@astrojs/rss";
import { SITE } from "../site-config";

export async function GET(context) {
  return rss({
    title: SITE.title,
    description: SITE.description,
    site: context.site,
    items: await pagesGlobToRssItems(
      import.meta.glob("./content/**/*.{md,mdx}"),
    ),
  });
}
