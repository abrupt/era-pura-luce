export const SITE = {
  author: "AAA",
  title: "Era pura luce | Antilivre | Abrüpt",
  displayTitle: "Era pura luce",
  description:
    "Cette cartographie vise à inscrire l’œuvre-vie de Pasolini dans les enjeux esthétiques, éthiques, sociaux et stratégiques de notre temps — et ce en la confrontant aux héritages marxistes hétérodoxes ou postmarxistes ayant infusé les nouvelles formes de radicalité dont nous sommes les contemporains. *Avec* Pasolini, donc, dans la confrontation heuristique de sa lucidité, de ses obsessions, de ses excès quelquefois, avec nos propres exigences de lutte pour la défense du vivant et de ses territoires. À rebours de l’eschatologie néolibérale et des impasses de la raison pétrifiée, en subvertissant l’esprit et la lettre du dogmatisme révolutionnaire, nous tenterons ici d’opposer aux nouvelles configurations sociales et culturelles du capitalisme mondialisé, à ses fausses libertés, à ses frontières bien réelles, matérielles et sémantiques, une nouvelle forme d’espérance.",
  url: "https://www.antilivre.org/era-pura-luce",
  titlelink: "https://abrupt.cc/pierre-aurelien-delabre/era-pura-luce",
  lang: "fr",
  locale: "fr_FR",
  warning: "",
  credits: "Antitexte&nbsp;: Pierre-Aurélien Delabre<br>Antimonde&nbsp;: AAA",
};

// // Theme configuration
// export const PAGE_SIZE = 10;
export const THREE_SITE = true;
