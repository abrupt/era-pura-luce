module.exports = {
  plugins: {
    'postcss-import': { path: './src/styles' },
    'postcss-nested': {},
    'autoprefixer': { cascade: false },
    'cssnano': { reduceIdents: false, normalizeUrl: { stripWWW: false } },
  }
}
